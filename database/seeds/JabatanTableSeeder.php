<?php

use Illuminate\Database\Seeder;
use App\Jabatan;

class JabatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $manajer = new Jabatan();
        $manajer->nama_jabatan='Manajer';
        $manajer->save();

        $kasir = new Jabatan();
        $kasir->nama_jabatan='Kasir';
        $kasir->save();

        $kurir = new Jabatan();
        $kurir->nama_jabatan='Kurir';
        $kurir->save();
    }
}
