<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_kasir = Role::where('nama','kasir')->first();
        $role_manajer = Role::where('nama','manajer')->first();
        $role_pelanggan = Role::where('nama','pelanggan')->first();
        $role_kurir = Role::where('nama','kurir')->first();

        $kasir = new User();
        $kasir->nama = 'Kasir Utama';
        $kasir->username = 'inikasir';
        $kasir->password = bcrypt('inikasir');
        $kasir->save();
        $kasir->roles()->attach($role_kasir);

        $manajer = new User();
        $manajer->nama = 'Manajer Utama';
        $manajer->username = 'inimanajer';
        $manajer->password = bcrypt('inimanajer');
        $manajer->save();
        $manajer->roles()->attach($role_manajer);

        $pelanggan = new User();
        $pelanggan->nama = 'Pelanggan Utama';
        $pelanggan->username = 'inipelanggan';
        $pelanggan->password = bcrypt('inipelanggan');
        $pelanggan->save();
        $pelanggan->roles()->attach($role_pelanggan);

        $kurir = new User();
        $kurir->nama = 'Kurir Utama';
        $kurir->username = 'inikurir';
        $kurir->password = bcrypt('inikurir');
        $kurir->save();
        $kurir->roles()->attach($role_kurir);
    }
}
