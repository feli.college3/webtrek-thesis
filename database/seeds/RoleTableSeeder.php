<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_manajer = new Role();
        $role_manajer->nama='manajer';
        $role_manajer->deskripsi='Mengakses semua fitur';
        $role_manajer->save();

        $role_kasir = new Role();
        $role_kasir->nama='kasir';
        $role_kasir->deskripsi='Menghandle transaksi keluar';
        $role_kasir->save();

        $role_pelanggan = new Role();
        $role_pelanggan->nama='pelanggan';
        $role_pelanggan->deskripsi='Mengakses menu customer';
        $role_pelanggan->save();

        $role_kurir = new Role();
        $role_kurir->nama='kurir';
        $role_kurir->deskripsi='Mengakses menu kurir';
        $role_kurir->save();
    }
}
