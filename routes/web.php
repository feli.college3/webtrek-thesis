<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::resource('login','LoginController');
// Route::resource('register','RegisterController');
Route::resource('jabatan','JabatanController');
Route::resource('pegawai','PegawaiController');
Route::resource('kategori','KategoriController');
Route::get('daftar-produk','ProdukController@index_cust');
Route::resource('produk','ProdukController');
Route::resource('supplier','SupplierController');
Route::get('profile/{id}','PelangganController@index_profile')->name('profile');
Route::post('simpan-profile','PelangganController@simpan_data')->name('simpan-profile');
Route::resource('pelanggan','PelangganController');
Route::resource('produk-konsinyasi','ProdukKonsinyasiController');

Route::get('nota-beli/create/getdetailproduk/{id}','NotaBeliController@get_detail_produk');
Route::get('nota-beli/create/getkembar/{data}','NotaBeliController@cek_dobel_nota');
Route::get('nota-beli-post','NotaBeliController@index_postorder')->name('nota-beli.post');
Route::resource('nota-beli','NotaBeliController');

//$nota,$toko,$konsi,$pelanggan,$grand_total,$ongkir
Route::get('nota-jual/cetak/{nota}/{toko}/{konsi}/{pelanggan}/{grand_total}/{ongkir}', 'NotaJualController@cetak')->name('cetak-nota-jual');
Route::get('nota-jual/detail-pengiriman/{id}','NotaJualController@detail_pengiriman')->name('nota-jual.detail-pengiriman');
Route::resource('nota-jual','NotaJualController');

Route::get('titip-konsinyasi-post','BeliKonsinyasiController@index_postorder')->name('titip-konsinyasi.post');
Route::get('beli-konsinyasi/create/getdetailproduk/{id}','BeliKonsinyasiController@get_detail_produk');
Route::resource('beli-konsinyasi','BeliKonsinyasiController');
Route::resource('kembali-konsinyasi','KembaliKonsinyasiController');
Route::get('kembali-konsinyasi/create/getnotabelikonsi/{id}','KembaliKonsinyasiController@get_nota_beli');
Route::get('kembali-konsinyasi/create/getprodukkonsinyasi/{id}','KembaliKonsinyasiController@get_produk_konsinyasi');

Route::get('histori-pembayaran-pembelian/{id}/create','HistoriPembayaranPembelianController@create')->name('histori-pembayaran-pembelian.createid');
Route::resource('histori-pembayaran-pembelian','HistoriPembayaranPembelianController');
Route::get('histori-pembayaran-penjualan/{id}/create','HistoriPembayaranPenjualanController@create')->name('histori-pembayaran-penjualan.createid');
Route::resource('histori-pembayaran-penjualan','HistoriPembayaranPenjualanController');

Route::get('penyesuaian-stok/{id}/create','PenyesuaianStokController@create')->name('penyesuaian-stok.createid');
Route::get('penyesuaian-stok/menunggu-validasi','PenyesuaianStokController@menunggu_validasi')->name('penyesuaian-stok-validasi');
Route::post('penyesuaian-stok/validasi','PenyesuaianStokController@validasi')->name('penyesuaian-stok.validasi');
Route::resource('penyesuaian-stok','PenyesuaianStokController');

Auth::routes();

Route::get('retur-pembelian/create/gettanggalkedaluwarsa/{id}','ReturBeliController@get_tanggal_kedaluwarsa');
Route::get('retur-pembelian/create/getnotabeli/{id}/{tgl}','ReturBeliController@get_nota_beli');
Route::get('retur-pembelian/create/getprodukbeli/{nota}/{id}/{tgl}','ReturBeliController@get_produk_retur');
Route::resource('retur-pembelian','ReturBeliController');

Route::resource('retur-penjualan','ReturJualController');
Route::get('retur-penjualan/create/getnotajual/{id}','ReturJualController@get_nota_jual');
Route::get('retur-penjualan/create/getprodukjual/{id}','ReturJualController@get_produk_retur');

// Route::post('retur-pembelian/fetch', 'ReturBeliController@fetch')->name('retur-pembelian.fetch');

Route::get('perlu-restock','ProdukController@perlu_restock')->name('perlu-restock');
Route::get('perlu-dibatalkan','NotaJualController@perlu_dibatalkan')->name('perlu-dibatalkan');
Route::get('batalkan-pesanan/{nota}','NotaJualController@batalkan_pesanan')->name('batalkan-pesanan');
Route::get('dashboard','NotaJualController@dashboard_admin')->name('dashboard');
Route::post('edit-konfigurasi/{id}','IndexController@update')->name('edit-konfigurasi');
Route::resource('pengaturan-awal','IndexController');

//////PESANAN ADMIN///////

Route::get('transaksi/{kategori}','NotaJualController@nota_admin')->name('transaksi');
Route::get('detail-transaksi/{id}','NotaJualController@detail_transaksi')->name('detail-transaksi');
Route::get('validitas-bukti/{id}/{validitas}','NotaJualController@validitas_bukti')->name('validitas-bukti');
Route::get('terima-pesanan/{id}','NotaJualController@terima_pesanan')->name('terima-pesanan');
Route::get('tolak-pesanan/{id}','NotaJualController@tolak_pesanan')->name('tolak-pesanan');
Route::post('nota-jual/final-tolak-pesanan','NotaJualController@final_tolak_pesanan')->name('nota-jual.tolak');
Route::get('kirim-pesanan/{id}','NotaJualController@kirim_pesanan')->name('kirim-pesanan');
Route::get('selesai-diproses/{id}','NotaJualController@selesai_diproses')->name('selesai-diproses');
Route::get('sudah-diambil/{id}','NotaJualController@sudah_diambil')->name('sudah-diambil');

/////PENGIRIMAN////

Route::get('pengiriman-hari-ini','PengirimanController@pengiriman_hari_ini')->name('pengiriman-hari-ini');
Route::get('selesai-dikirim','PengirimanController@selesai_dikirim_kurir')->name('selesai-dikirim');
Route::get('pesanan-selesai/{id}','PengirimanController@pesanan_selesai')->name('pesanan-selesai');
Route::resource('atur-pengiriman','PengirimanController');

// Route::resource('home','HomeController');
// Route::get('/home', function () {
//     return view('index.welcome');
// });

///////////LAPORAN///////////////
Route::get('laporan-penjualan/cetak/{tahun}/{bulan}/{jenis}', 'LaporanPenjualanController@cetak')->name('cetak-penjualan');
Route::resource('laporan-penjualan','LaporanPenjualanController');
Route::get('laporan-pembelian/cetak/{tahun}/{bulan}/{jenis}', 'LaporanPembelianController@cetak')->name('cetak-pembelian');
Route::resource('laporan-pembelian','LaporanPembelianController');
Route::resource('laporan-labarugi-tahunan','LaporanLabaRugiTahunanController');
Route::get('laporan-labarugi/cetak/{tahun}/{bulan}', 'LaporanLabaRugiController@cetak')->name('cetak-labarugi');
Route::resource('laporan-labarugi','LaporanLabaRugiController');
Route::get('laporan-produk/cetak/{tahun}/{bulan}', 'LaporanProdukController@cetak')->name('cetak-produk');
Route::resource('laporan-produk','LaporanProdukController');
Route::get('laporan-konsinyasi/cetak/{tahun}/{bulan}', 'LaporanKonsinyasiController@cetak')->name('cetak-konsinyasi');
Route::resource('laporan-konsinyasi','LaporanKonsinyasiController');
// Route::get('laporan-penjualan','LaporanController@penjualan')->name('laporan-penjualan');
// Route::get('laporan-penjualan-periode','LaporanController@penjualan_show')->name('laporan-penjualan-periode');


Route::get('/index', 'IndexController@pegawai');
Route::get('/home', 'IndexController@pelanggan')->name('home');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::resource('belanja','BelanjaController');
Route::get('belanja/kategori/{kategori}','BelanjaController@perkategori')->name('belanja-perkategori');
Route::resource('keranjang','KeranjangController');
Route::get('keranjang/{aksi}/{id}','KeranjangController@ubah_jumlah')->name('ubah-keranjang');
Route::get('keranjang/tambah/satuan/{id}','KeranjangController@simpan_satuan')->name('keranjang-satuan');
Route::resource('checkout','CheckOutController');
Route::resource('pembayaran','PembayaranController');

/////////HALAMAN PESANAN///////////
Route::get('pesanan/{kategori}','NotaJualController@nota_pelanggan')->name('pesanan');
Route::get('detail-pesanan/{nonota}','NotaJualController@detail_pesanan')->name('detail-pesanan');
// Route::get('checkout','NotaJualController@checkout')->name('checkout');


/////////////////KURIR//////////////////
Route::resource('kurir','PengirimanController');
Route::get('kirim-berlangsung','PengirimanController@berlangsung')->name('kirim-berlangsung');
Route::get('kirim-selesai','PengirimanController@selesai')->name('kirim-selesai');
Route::get('terima-pengiriman','PengirimanController@terima_kirim')->name('terima-pengiriman');
Route::get('rute-pengiriman','PengirimanController@rute_pengiriman')->name('rute-pengiriman');
Route::get('rute-pengiriman/{nonota}','PengirimanController@kirim_sekarang')->name('kirim-sekarang');
Route::get('sukses-dikirim/{nonota}/{penerima}','PengirimanController@sukses_dikirim')->name('sukses-dikirim');