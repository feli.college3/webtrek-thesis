<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';
    protected $primaryKey = 'idkategori';
    protected $fillable = ['nama_kategori','kode_kategori'];
    public $timestamps = false;

    public function produk()
    {
        return $this->hasMany('App\Produk');
    }
}
