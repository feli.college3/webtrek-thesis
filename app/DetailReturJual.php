<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailReturJual extends Model
{
    protected $table = 'detail_retur_penjualan';
    protected $fillable = ['idretur_penjualan','idproduk','tanggal_kadaluwarsa_lama','jumlah','tanggal_kadaluwarsa_baru'];
    public $incrementing = false;
    public $timestamps = false;

    public function returjual()
    {
        return $this->belongsTo('App\ReturJual');
    }

    public function produk()
    {
        return $this->belongsTo('App\Produk');
    }
}
