<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table = 'jabatan';
    protected $primaryKey = 'idjabatan';
    protected $fillable = ['nama_jabatan'];
    public $timestamps = false;

    public function pegawai()
    {
        return $this->hasMany('App\Pegawai');
    }
}
