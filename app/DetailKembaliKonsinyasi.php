<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailKembaliKonsinyasi extends Model
{
    protected $table = 'detail_nota_kembali_konsinyasi';
    protected $fillable = ['no_nota_kembali','idproduk_konsinyasi','jumlah_terjual','jumlah_kembali'];
    public $incrementing = false;
    public $timestamps = false;

    public function nota_kembali_konsinyasi()
    {
        return $this->belongsTo('App\NotaKembaliKonsinyasi');
    }

    public function produk_konsinyasi()
    {
        return $this->belongsTo('App\ProdukKonsinyasi');
    }
}
