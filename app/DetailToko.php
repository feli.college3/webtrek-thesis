<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailToko extends Model
{
    protected $table = 'detail_toko';
    protected $primaryKey = 'nama_toko';
    protected $fillable = ['nama_toko','alamat_toko','telepon_toko','deskripsi_toko','nama_bank','nomor_rekening','pemilik_rekening','pembayaran_tunai','pembayaran_cod','pembayaran_transfer','pembayaran_debit','pembayaran_kredit','pembayaran_hutang','pembayaran_piutang','fitur_retur','fitur_konsinyasi','fitur_harga_grosir','jumlah_grosir','fitur_stok_minimal_perproduk','stok_minimal','fitur_stok_fifo','fitur_kirim','maksimal_jarak','interval_jarak','ongkir_interval','fitur_batas_hutang','fitur_pembatalan_pesanan','maksimal_pembatalan'];
    public $incrementing = false;
    public $timestamps = false;
}
