<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use App\Pelanggan;
use App\KeranjangBiasa;
use App\KeranjangKonsinyasi;
use App\DetailToko;
use App\NotaJual;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // $username = 'username';
        // if(Auth::user()){
        //     $username = Auth::user()->username;
        // }
        view()->composer(['layouts.pelanggan', 'customer.keranjang'], function($view)
        {
            if(Auth::user()){
                $username = Auth::user()->username;
                $role = Auth::user()->roles->first()->nama;
                $total_produk=0;
                $belum_bayar=0;
                $diproses=0;
                $dikirim=0;
                $siap_diambil=0;
                $selesai=0;
                $dibatalkan=0;
                if($role=='pelanggan'){
                    $pelanggan = Pelanggan::select('idpelanggan')
                    ->where('username', '=', $username)
                    ->first();
                // 
                    $jumprodukbiasa = KeranjangBiasa::select(DB::raw('count(keranjang_biasa.idproduk) as jum_prod_biasa'))
                        ->join('produk', 'produk.idproduk', '=', 'keranjang_biasa.idproduk')
                        ->where('keranjang_biasa.idpelanggan', '=', $pelanggan->idpelanggan)
                        ->get();
                    $jumprodukkonsi = KeranjangKonsinyasi::select(DB::raw('count(keranjang_konsinyasi.idproduk_konsinyasi) as jum_prod_konsi'))
                    ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'keranjang_konsinyasi.idproduk_konsinyasi')
                    ->where('keranjang_konsinyasi.idpelanggan', '=', $pelanggan->idpelanggan)
                    ->get();
                    $total_produk = $jumprodukbiasa[0]->jum_prod_biasa + $jumprodukkonsi[0]->jum_prod_konsi;
                    
                    
                    // $list_nota_jual = NotaJual::select(DB::raw("nota_jual.*, (SELECT p.* FROM detail_nota_jual d inner join produk p on d.idproduk =p.idproduk where d.no_nota_jual=nota_jual.no_nota_jual LIMIT 1)"))
                    $belum_bayar = NotaJual::select(DB::raw('count(nota_jual.no_nota_jual) as jumlah'))
                        ->where('nota_jual.idpelanggan','=',$pelanggan->idpelanggan)
                        ->where('nota_jual.status_transaksi', 'Belum Dibayar')
                        ->first();
                    $belum_bayar=$belum_bayar->jumlah;

                    $diproses = NotaJual::select(DB::raw('count(nota_jual.no_nota_jual) as jumlah'))
                        ->where('nota_jual.idpelanggan','=',$pelanggan->idpelanggan)
                        ->where(function($con){
                              $con->where('nota_jual.status_transaksi', 'Belum Diproses')
                                ->orWhere('nota_jual.status_transaksi', 'Belum Divalidasi')
                                ->orWhere('nota_jual.status_transaksi', 'Diproses')
                                ->orWhere('nota_jual.status_transaksi', 'Siap Dikirim');
                          })
                        ->first();
                    $diproses=$diproses->jumlah;

                    $dikirim = NotaJual::select(DB::raw('count(nota_jual.no_nota_jual) as jumlah'))
                        ->where('nota_jual.idpelanggan','=',$pelanggan->idpelanggan)
                        ->where('nota_jual.status_transaksi', 'Dikirim')
                        ->first();
                    $dikirim=$dikirim->jumlah;

                    $siap_diambil = NotaJual::select(DB::raw('count(nota_jual.no_nota_jual) as jumlah'))
                        ->where('nota_jual.idpelanggan','=',$pelanggan->idpelanggan)
                        ->where('nota_jual.status_transaksi', 'Siap Diambil')
                        ->first();
                    $siap_diambil=$siap_diambil->jumlah;

                    $selesai = NotaJual::select(DB::raw('count(nota_jual.no_nota_jual) as jumlah'))
                        ->where('nota_jual.idpelanggan','=',$pelanggan->idpelanggan)
                        ->where('nota_jual.status_transaksi', 'Selesai')
                        ->first();
                    $selesai=$selesai->jumlah;

                    $dibatalkan = NotaJual::select(DB::raw('count(nota_jual.no_nota_jual) as jumlah'))
                        ->where('nota_jual.idpelanggan','=',$pelanggan->idpelanggan)
                        ->where('nota_jual.status_transaksi', 'Dibatalkan')
                        ->first();
                    $dibatalkan=$dibatalkan->jumlah;
                }
                $view->with('username', Auth::user()->username);
                $view->with('role', $role);
                $view->with('total_produk', $total_produk);
                $view->with('belum_bayar', $belum_bayar);
                $view->with('diproses', $diproses);
                $view->with('dikirim', $dikirim);
                $view->with('siap_diambil', $siap_diambil);
                $view->with('selesai', $selesai);
                $view->with('dibatalkan', $dibatalkan);
            }
        });

        view()->composer(['layouts.index-admin','layouts.kurir','auth.*','customer.profile-pelanggan'], function($view)
        {
            if(Auth::user()){
                $username = Auth::user()->username;
                $pelanggan = Pelanggan::select('idpelanggan')
                    ->where('username', '=', $username)
                    ->get();
                $view->with('username', Auth::user()->username);
            }
            $detail_toko = DetailToko::select('*')
                ->first();
            $view->with('detail_toko', $detail_toko);
        });

        view()->composer(['layouts.index-admin','layouts.kurir','auth.*','layouts.pelanggan','index.dashboard','customer.keranjang','nota-jual.*','nota-beli.*','supplier.*','pelanggan.*','produk.*','customer.checkout','customer.pembayaran','customer.pesanan','customer.detail-produk','customer.detail-pesanan','transaksi-online.*','kurir.*','laporan.*','penyesuaian-stok.*','retur-beli.*','retur-jual.*'], function($view)
        {
            $detail_toko = DetailToko::select('*')
                ->first();
            $view->with('detail_toko', $detail_toko);
        });

        view()->composer(['index.dashboard','layouts.index-admin','pelanggan.index-pelanggan','supplier.index-supplier','kategori.index-kategori','produk.index-produk','produk-konsinyasi.index-produk'], function($view)
        {
            $role = Auth::user()->roles->first()->nama;
            $view->with('role', $role);
        });
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
