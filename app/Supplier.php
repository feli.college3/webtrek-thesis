<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'supplier';
    protected $primaryKey = 'idsupplier';
    protected $fillable = ['nama_supplier','alamat_supplier','telepon_supplier','konsinyator','nomor_rekening','total_piutang','status'];
    public $timestamps = false;
}
