<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    protected $table = 'pelanggan';
    protected $primaryKey = 'idpelanggan';
    protected $fillable = ['idpelanggan','nama_pelanggan','alamat_pelanggan','telepon_pelanggan','status','batas_hutang','total_hutang','username'];
    public $timestamps = false;
}