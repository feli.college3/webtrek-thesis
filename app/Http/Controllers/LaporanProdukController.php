<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetailToko;
use App\NotaBeli;
use App\NotaJual;
use App\DetailNotaBeli;
use App\DetailNotaJual;
use App\StokProduk;
use App\PenyesuaianStok;
use App\Produk;
use DB;
use PDF;

class LaporanProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $tahun = NotaBeli::selectRaw('substr(tanggal_dipesan,1,4) as tahun')->pluck('tahun')->unique();
        return view('laporan.index-produk', ['bulan'=>$bulan,'tahun'=>$tahun]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function tanggal_awal($bulan,$tahun){
        if($bulan<10){
            $bulan='0'.$bulan;
        }
        $from = date($tahun.$bulan.'01');
        return $from;
    }

    public function tanggal_akhir($bulan,$tahun){
        $bulanplus=$bulan+1;
        $tahunplus=$tahun;
        if($bulan==12){
            $bulanplus=1;
            $tahunplus++;
        }
        if($bulanplus<10){
            $bulanplus='0'.$bulanplus;
        }
        $to = date($tahunplus.$bulanplus.'01');
        return $to;
    }

    public function get_nota_jual($bulan,$tahun){
        $from=$this->tanggal_awal($bulan,$tahun);
        $to=$this->tanggal_akhir($bulan,$tahun);

        // $list_nota_jual = NotaJual::select('produk.idproduk','produk.nama_produk','detail_nota_jual.harga_jual',DB::raw('sum(detail_nota_jual.jumlah) as jumlah'))
        //     ->join('detail_nota_jual', 'detail_nota_jual.no_nota_jual', '=', 'nota_jual.no_nota_jual')
        //     ->join('produk', 'detail_nota_jual.idproduk', '=', 'produk.idproduk')
        //     ->whereBetween('tanggal', [$from, $to])
        //     ->groupBy('produk.idproduk')
        //     ->groupBy('detail_nota_jual.harga_jual')
        //     ->get();
        $subquery = NotaJual::select('produk.idproduk','produk.nama_produk','detail_nota_jual.harga_jual',DB::raw('sum(detail_nota_jual.jumlah) as jumlah,(detail_nota_jual.harga_jual*sum(detail_nota_jual.jumlah)) as subtotal'))
            ->join('detail_nota_jual', 'detail_nota_jual.no_nota_jual', '=', 'nota_jual.no_nota_jual')
            ->join('produk', 'detail_nota_jual.idproduk', '=', 'produk.idproduk')
            ->where(function($con) {
                      $con->where('nota_jual.status_transaksi', 'Selesai');
                  })
            ->whereBetween('tanggal', [$from, $to])
            ->groupBy('produk.idproduk')
            ->groupBy('detail_nota_jual.harga_jual');

        $list_jual=DB::table(DB::raw("({$subquery->toSql()}) as sub"))->select('sub.idproduk','sub.nama_produk',DB::raw('sum(jumlah) as jumlah_jual,sum(subtotal) as subtotal_jual'))
            ->mergeBindings($subquery->getQuery())
            ->groupBy('sub.idproduk')
            ->get();

        $list_nota_jual=array();
        $semua_produk=Produk::select('idproduk','nama_produk')->get();
        foreach($semua_produk as $produk){
            if(count($list_jual)>0){
                for($i=0;$i<count($list_jual);$i++){
                    if($produk->idproduk==$list_jual[$i]->idproduk){
                        $tampung=array();
                        $tampung['idproduk']=$list_jual[$i]->idproduk;
                        $tampung['nama_produk']=$list_jual[$i]->nama_produk;
                        $tampung['jumlah']=$list_jual[$i]->jumlah_jual;
                        $tampung['subtotal']=$list_jual[$i]->subtotal_jual;
                        array_push($list_nota_jual, $tampung);
                        break;
                    }
                    else{
                        if($i==count($list_jual)-1){
                            $tampung=array();
                            $tampung['idproduk']=$produk->idproduk;
                            $tampung['nama_produk']=$produk->nama_produk;
                            $tampung['jumlah']=0;
                            $tampung['subtotal']=0;
                            array_push($list_nota_jual, $tampung);
                        }
                    }
                }
            }
            else{
                $tampung=array();
                $tampung['idproduk']=$produk->idproduk;
                $tampung['nama_produk']=$produk->nama_produk;
                $tampung['jumlah']=0;
                $tampung['subtotal']=0;
                array_push($list_nota_jual, $tampung);
            }
            
        }
        return $list_nota_jual;
    }

    public function get_nota_beli($bulan,$tahun,$laporan){
        $from=$this->tanggal_awal($bulan,$tahun);
        $to=$this->tanggal_akhir($bulan,$tahun);

        //iki get yg hargae beda" dgn 1 produk yg sama
        // $list_nota_beli = NotaBeli::select('produk.idproduk','produk.nama_produk','detail_nota_beli.harga_beli',DB::raw('sum(detail_nota_beli.jumlah) as jumlah'))
            // ->join('detail_nota_beli', 'detail_nota_beli.no_nota_beli', '=', 'nota_beli.no_nota_beli')
            // ->join('produk', 'detail_nota_beli.idproduk', '=', 'produk.idproduk')
            // ->whereBetween('tanggal_diterima', [$from, $to])
            // ->groupBy('produk.idproduk')
            // ->groupBy('detail_nota_beli.harga_beli')
            // ->get();
        if($laporan=='Produk')
            $subquery = NotaBeli::select('produk.idproduk','produk.nama_produk','detail_nota_beli.harga_beli',
                DB::raw('sum(detail_nota_beli.jumlah) as jumlah, (detail_nota_beli.harga_beli*sum(detail_nota_beli.jumlah)) as subtotal'))
                ->join('detail_nota_beli', 'detail_nota_beli.no_nota_beli', '=', 'nota_beli.no_nota_beli')
                ->join('produk', 'detail_nota_beli.idproduk', '=', 'produk.idproduk')
                ->where('nota_beli.status_transaksi','=','Sudah Datang')
                ->whereBetween('nota_beli.tanggal_diterima', [$from, $to])
                ->groupBy('produk.idproduk')
                ->groupBy('detail_nota_beli.harga_beli');
        else{
            $subquery = NotaBeli::select('produk.idproduk','produk.nama_produk','detail_nota_beli.harga_beli',
                DB::raw('sum(detail_nota_beli.jumlah) as jumlah, (detail_nota_beli.harga_beli*sum(detail_nota_beli.jumlah)) as subtotal'))
                ->join('detail_nota_beli', 'detail_nota_beli.no_nota_beli', '=', 'nota_beli.no_nota_beli')
                ->join('produk', 'detail_nota_beli.idproduk', '=', 'produk.idproduk')
                ->where('nota_beli.status_pembayaran','=','Lunas')
                ->whereBetween('nota_beli.tanggal_diterima', [$from, $to])
                ->groupBy('produk.idproduk')
                ->groupBy('detail_nota_beli.harga_beli');
        }
        $list_beli=DB::table(DB::raw("({$subquery->toSql()}) as sub"))->select('sub.idproduk','sub.nama_produk',DB::raw('sum(jumlah) as jumlah_beli,sum(subtotal) as subtotal_beli'))
            ->mergeBindings($subquery->getQuery())
            ->groupBy('sub.idproduk')
            ->get();

        $list_nota_beli=array();
        $semua_produk=Produk::select('idproduk','nama_produk')->get();
        foreach($semua_produk as $produk){
            if(count($list_beli)>0){
                for($i=0;$i<count($list_beli);$i++){
                    if($produk->idproduk==$list_beli[$i]->idproduk){
                        $tampung=array();
                        $tampung['idproduk']=$list_beli[$i]->idproduk;
                        $tampung['nama_produk']=$list_beli[$i]->nama_produk;
                        $tampung['jumlah']=$list_beli[$i]->jumlah_beli;
                        $tampung['subtotal']=$list_beli[$i]->subtotal_beli;
                        array_push($list_nota_beli, $tampung);
                        break;
                    }
                    else{
                        if($i==count($list_beli)-1){
                            $tampung=array();
                            $tampung['idproduk']=$produk->idproduk;
                            $tampung['nama_produk']=$produk->nama_produk;
                            $tampung['jumlah']=0;
                            $tampung['subtotal']=0;
                            array_push($list_nota_beli, $tampung);
                        }
                    }
                }
            }
            else{
                $tampung=array();
                $tampung['idproduk']=$produk->idproduk;
                $tampung['nama_produk']=$produk->nama_produk;
                $tampung['jumlah']=0;
                $tampung['subtotal']=0;
                array_push($list_nota_beli, $tampung);
            }
        }
        return $list_nota_beli;
    }

    public function get_penyesuaian($bulan,$tahun){
        $from=$this->tanggal_awal($bulan,$tahun);
        $to=$this->tanggal_akhir($bulan,$tahun);
        $penyesuaian = Produk::select('penyesuaian_stok.idproduk','penyesuaian_stok.jenis',DB::raw('sum(jumlah) as jumlah'))
            ->leftJoin('penyesuaian_stok', 'penyesuaian_stok.idproduk', '=', 'produk.idproduk')
            ->whereBetween('tanggal', [$from, $to])
            ->groupBy('idproduk')
            ->groupBy('jenis')
            ->get();
        $sesuai=array();
        $prod_exist=array();
        foreach($penyesuaian as $pen){
            array_push($prod_exist, $pen->idproduk);
            $stok=PenyesuaianStok::select('idproduk','jenis',DB::raw('sum(jumlah) as jumlah'))
                ->where('idproduk','=',$pen->idproduk)
                ->whereBetween('tanggal', [$from, $to])
                ->groupBy('idproduk')
                ->groupBy('jenis')
                ->get();
            $jumlah_data=count($stok);
            if($jumlah_data==1){
                if($pen->jenis=='Masuk'){
                    $tampung['idproduk']=$pen->idproduk;
                    $tampung['jenis']=$pen->jenis;
                    $tampung['jumlah']=$pen->jumlah;
                    array_push($sesuai, $tampung);
                    $tampung['idproduk']=$pen->idproduk;
                    $tampung['jenis']='Keluar';
                    $tampung['jumlah']=0;
                    array_push($sesuai, $tampung);
                }
                else{
                    $tampung['idproduk']=$pen->idproduk;
                    $tampung['jenis']='Masuk';
                    $tampung['jumlah']=0;
                    array_push($sesuai, $tampung);
                    $tampung['idproduk']=$pen->idproduk;
                    $tampung['jenis']=$pen->jenis;
                    $tampung['jumlah']=$pen->jumlah;
                    array_push($sesuai, $tampung);
                }
            }
            else if($jumlah_data==2){
                $tampung['idproduk']=$pen->idproduk;
                $tampung['jenis']=$pen->jenis;
                $tampung['jumlah']=$pen->jumlah;
                array_push($sesuai, $tampung);
            }
        }
        $semua_produk=Produk::all();
        foreach($semua_produk as $prod){
            $bool = in_array( $prod->idproduk, $prod_exist);
            if($bool==false){
                $tampung['idproduk']=$prod->idproduk;
                $tampung['jenis']='Masuk';
                $tampung['jumlah']=0;
                array_push($sesuai, $tampung);
                $tampung['idproduk']=$prod->idproduk;
                $tampung['jenis']='Keluar';
                $tampung['jumlah']=0;
                array_push($sesuai, $tampung);
            }
        }
        $list_sesuai=array();
        // dd(count($sesuai));
        for($i=0;$i<count($sesuai);$i+=2){
            $tampung=array();
            $tampung['idproduk']=$sesuai[$i]['idproduk'];
            $harga_jual=DetailNotaJual::select('detail_nota_jual.harga_jual')
                ->join('nota_jual', 'detail_nota_jual.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                ->where('detail_nota_jual.idproduk','=',$tampung['idproduk'])
                ->whereBetween('nota_jual.tanggal', [$from, $to])
                ->first();
            $harga_jual_lain=DetailNotaJual::select('detail_nota_jual.harga_jual')
                ->join('nota_jual', 'detail_nota_jual.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                ->where('detail_nota_jual.idproduk','=',$tampung['idproduk'])
                ->where('nota_jual.tanggal', 'like', $tahun.'%')
                ->first();
            if($harga_jual==null){
                $harga_jual=$harga_jual_lain;
                if($harga_jual==null){
                    $harga_jual=Produk::select('harga_jual_eceran as harga_jual')
                        ->where('idproduk','=',$tampung['idproduk'])
                        ->first();
                }
            }
            $tampung['harga_jual']=$harga_jual->harga_jual;
            $index=$i+1;
            if($sesuai[$i]['jenis']=='Masuk'){
                $tampung['jumlah_masuk']=$sesuai[$i]['jumlah'];
                $tampung['jumlah_keluar']=$sesuai[$index]['jumlah'];
            }
            else if($sesuai[$i]['jenis']=='Keluar'){
                $tampung['jumlah_masuk']=$sesuai[$index]['jumlah'];
                $tampung['jumlah_keluar']=$sesuai[$i]['jumlah'];
            }
            $tampung['subtotal_masuk']=$tampung['harga_jual']*$tampung['jumlah_masuk'];
            $tampung['subtotal_keluar']=-1*$tampung['harga_jual']*$tampung['jumlah_keluar'];
            array_push($list_sesuai, $tampung);
        }
        return $list_sesuai;
    }

    public function get_tanpa_piutang($list_nota_jual){
        $tanpa_piutang=0;
        foreach ($list_nota_jual as $nota) {
            $tanpa_piutang+=$nota->grand_total;
            $tanpa_piutang-=$nota->sisa_piutang;
        }
        return $tanpa_piutang;
    }

    public function store(Request $request)
    {
        $detail_toko=DetailToko::select('*')->first();
        $bulan=$request->get('bulan');
        $tahun=$request->get('tahun');
        $list_jual=$this->get_nota_jual($bulan,$tahun);
        $list_beli=$this->get_nota_beli($bulan,$tahun,'Produk');
        $penyesuaian=$this->get_penyesuaian($bulan,$tahun);
        $list_bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        return view('laporan.laporan-produk', ['list_jual'=>$list_jual,'list_beli'=>$list_beli,'penyesuaian'=>$penyesuaian,'list_bulan'=>$list_bulan,'tahun'=>$tahun,'bulan'=>$bulan]);
    }

    public function cetak($tahun,$bulan)
    {
        $detail_toko=DetailToko::select('*')->first();
        $list_jual=$this->get_nota_jual($bulan,$tahun);
        $list_beli=$this->get_nota_beli($bulan,$tahun,'Produk');
        $penyesuaian=$this->get_penyesuaian($bulan,$tahun);
        $list_bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        
        $pdf = PDF::loadview('laporan.cetak-produk',['list_jual'=>$list_jual,'list_beli'=>$list_beli,'penyesuaian'=>$penyesuaian,'list_bulan'=>$list_bulan,'tahun'=>$tahun,'bulan'=>$bulan])->setPaper('a4', 'potrait');;

        return $pdf->stream();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
