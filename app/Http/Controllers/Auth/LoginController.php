<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';
    // public $user_login = Auth::user()->nama;
    
    public function redirectTo(){
        $role = Auth::user()->roles->first()->nama;
        $username = Auth::user()->username;
        
        switch ($role) {
            case 'manajer':
                return '/dashboard';
                break;
            case 'kasir':
                return '/dashboard';
                break; 
            case 'kurir':
                return '/kurir';
                break; 
            case 'pelanggan':
                return '/home';
                break; 
            default:
                return '/home'; 
                break;
        }
    }
    // protected function authenticated() {
    //     if (Auth::check()) {
    //         return redirect()->route('home');
    //     }
    // }

    // protected function authenticated(Request $request, $user) {
    //  if ($user->role_id == 1) {
    //     return redirect('/produk');
    //  } else if ($user->role_id == 2) {
    //     return redirect('/penjualan');
    //  } else {
    //     return redirect('/home');
    //  }
    // }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    protected function validateLogin(Request $request)
    {
        $this->validate(
        $request,
        [
            'username' => 'required|string',
            'password' => 'required|string',
        ],
        [
            'username.required' => 'Username wajib diisi',
            'password.required' => 'Password wajib diisi',
        ]
        );
    }
    /**
    * @param Request $request
    * @throws ValidationException
    */
    // protected function sendFailedLoginResponse(Request $request)
    // {
    //     $request->session()->put('login_error', trans('auth.failed'));
    //     throw ValidationException::withMessages(
    //     [
    //         'error' => [trans('auth.failed')],
    //     ]
    //     );
    // }

    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];

        // Load user from database
        $user = \App\User::where($this->username(), $request->{$this->username()})->first();

        // Check if user was successfully loaded, that the password matches
        // and active is not 1. If so, override the default error message.
        if ($user && \Hash::check($request->password, $user->password) && $user->active != 1) {
            $errors = [$this->username() => 'Your account is not active.'];
        }

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }
    
    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }
}
