<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\NotaBeli;
use App\Charts\ChartLabaRugi;

class LaporanLabaRugiTahunanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }
    
    public function index(){
        $role=$this->get_role();
        if($role=='manajer'){
            $tahun = NotaBeli::selectRaw('substr(tanggal_dipesan,1,4) as tahun')->pluck('tahun')->unique();
            return view('laporan.index-labarugi-tahunan', ['tahun'=>$tahun]);
        }
        else{
            return redirect()->back();
        }
    }

    public function store(Request $request){
        $tahun=$request->get('tahun');
        $list_pembelian=array();
        $list_penjualan=array();
        $list_keuntungan=array();
        for($i=1;$i<=12;$i++){
            $all_list=app('App\Http\Controllers\LaporanLabaRugiController')->get_semua_nota($i,$tahun);
            $total_pembelian=app('App\Http\Controllers\LaporanLabaRugiController')->get_total_pembelian($all_list);
            $total_penjualan=app('App\Http\Controllers\LaporanLabaRugiController')->get_total_penjualan($all_list);
            $keuntungan=app('App\Http\Controllers\LaporanLabaRugiController')->get_keuntungan($all_list);
            array_push($list_pembelian,$total_pembelian);
            array_push($list_penjualan,$total_penjualan);
            array_push($list_keuntungan,$keuntungan);
        }
        // dd($list_pembelian);
        $list_bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

        $chart = new ChartLabaRugi;
        $chart->labels($list_bulan);
        $chart->dataset('Pembelian','line',$list_pembelian)
        		->color('red')->fill(false);
        $chart->dataset('Penjualan','line',$list_penjualan)
        		->color('blue')->fill(false);
        $chart->dataset('Keuntungan','line',$list_keuntungan)
        		->color('green')->fill(false);
        return view('laporan.laporan-labarugi-tahunan', ['tahun'=>$tahun,'chart'=>$chart]);
    }
}
