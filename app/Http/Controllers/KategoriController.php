<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Kategori;
use Validator;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }
    
    public function index()
    {
        $list_kategori = Kategori::all();
        return view('kategori.index-kategori', ['list_kategori'=>$list_kategori]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            return view('kategori.tambah-kategori');
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required | string',
            'kode_kategori' => 'required|unique:kategori|size:3|string',
        ]);
        
        if($validator->fails()){
            return redirect()
                ->action('KategoriController@create')
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $kat = new Kategori([
            'nama_kategori' => $request->get('nama'),
            'kode_kategori' => $request->get('kode_kategori'),
            ]);

            $kat->save();
            return redirect('/kategori');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $kat = Kategori::find($id);    
            return view('kategori.edit-kategori', ['kat'=>$kat, 'id'=>$id]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required | string',
        ]);
        
        if($validator->fails()){
            return redirect()
                ->action('KategoriController@edit', $id)
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $kat = Kategori::find($id);
            $kat->nama_kategori = $request->get('nama');

            $kat->save();
            return redirect('/kategori');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            Kategori::where('idkategori',$id)->first()->delete();
            return redirect('/kategori')->with('status', 'Kategori terhapus');
        }
        else{
            return redirect()->back();
        }
    }
}
