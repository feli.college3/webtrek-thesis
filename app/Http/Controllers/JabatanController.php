<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jabatan;
use Validator;

class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $list_jabatan = Jabatan::all();
        return view('jabatan.index-jabatan', ['list_jabatan'=>$list_jabatan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $list_jabatan = Jabatan::all();
        return view('jabatan.tambah-jabatan', ['list_jabatan'=>$list_jabatan]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required | string',
        ]);
        
        if($validator->fails()){
            return redirect()
                ->action('JabatanController@create')
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $jab = new Jabatan([
            'nama_jabatan' => $request->get('nama'),
            ]);

            $jab->save();
            return redirect('/jabatan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jab = Jabatan::find($id);    
        return view('jabatan.edit-jabatan', ['jab'=>$jab, 'id'=>$id]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required | string',
        ]);
        
        if($validator->fails()){
            return redirect()
                ->action('JabatanController@edit', $id)
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $jab = Jabatan::find($id);
            $jab->nama_jabatan = $request->get('nama');

            $jab->save();
            return redirect('/jabatan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Jabatan::where('idjabatan',$id)->first()->delete();
        return redirect('/jabatan')->with('status', 'Jabatan terhapus');
    }
}
