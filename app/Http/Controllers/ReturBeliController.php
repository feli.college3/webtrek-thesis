<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ReturBeli;
use App\DetailReturBeli;
use App\Supplier;
use App\Pegawai;
use App\NotaBeli;
use App\Produk;
use App\StokProduk;
use DB;

class ReturBeliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }
    
    public function index()
    {
        // $list_retur = ReturBeli::select('retur_pembelian.*', 'supplier.nama_supplier', 'pegawai.nama_pegawai')
        //             ->join('nota_beli', 'nota_beli.no_nota_beli', '=', 'retur_pembelian.no_nota_beli')
        //             ->join('supplier', 'supplier.idsupplier', '=', 'nota_beli.idsupplier')
        //             ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_beli.idpegawai')
        //             ->orderBy('retur_pembelian.tanggal', 'desc')
        //             ->get();
        //(select produk.nama_produk from produk where produk.idproduk=detail_retur_pembelian.idproduk limit 1) as nama_produk
        // $subquery = Produk::select('produk.nama_produk as nama_produk')
        //     ->where('produk.idproduk','=','detail_retur_pembelian.idproduk')
        //     ->first();

        //SELECT retur_pembelian.*, pegawai.nama_pegawai, supplier.nama_supplier, (select produk.nama_produk from produk where produk.idproduk=detail_retur_pembelian.idproduk limit 1) as nama_produk FROM retur_pembelian INNER join `detail_retur_pembelian` on retur_pembelian.idretur=detail_retur_pembelian.idretur_pembelian inner join pegawai on retur_pembelian.idpegawai=pegawai.idpegawai inner join nota_beli on nota_beli.no_nota_beli=retur_pembelian.no_nota_beli inner join supplier on nota_beli.idsupplier=supplier.idsupplier group by retur_pembelian.idretur
        
        $role=$this->get_role();
        if($role=='manajer'){
            $list_retur=ReturBeli::select('retur_pembelian.*','pegawai.nama_pegawai','supplier.nama_supplier', DB::raw('(select produk.nama_produk from produk where produk.idproduk=detail_retur_pembelian.idproduk limit 1) as nama_produk'))
                ->join('detail_retur_pembelian', 'detail_retur_pembelian.idretur_pembelian', '=', 'retur_pembelian.idretur')
                ->join('pegawai', 'pegawai.idpegawai', '=', 'retur_pembelian.idpegawai')
                ->join('nota_beli', 'nota_beli.no_nota_beli', '=', 'retur_pembelian.no_nota_beli')
                ->join('supplier', 'supplier.idsupplier', '=', 'nota_beli.idsupplier')
                ->groupBy('retur_pembelian.idretur')
                ->groupBy('nama_produk')
                ->orderBy('retur_pembelian.tanggal', 'desc')
                ->get();
            return view('retur-beli.index-retur-beli', ['list_retur'=>$list_retur]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function get_tanggal_kedaluwarsa($id)
    {
        $stok_produk=StokProduk::where('idproduk','=',$id)
            ->where('stok','>',0)
            ->get();
        $array_tanggal=array();
        foreach($stok_produk as $produk){
            array_push($array_tanggal, date('d-m-Y', strtotime($produk->tanggal_kadaluwarsa)));
        }
        return json_encode($array_tanggal);
    }

    public function get_nota_beli($id,$tgl)
    {
        $thn=substr($tgl,6);
        $bln=substr($tgl,3,2);
        $hari=substr($tgl,0,2);
        $tgl=$thn.'-'.$bln.'-'.$hari;
        $nota_beli=NotaBeli::select('nota_beli.no_nota_beli','supplier.nama_supplier')
            ->join('detail_nota_beli', 'detail_nota_beli.no_nota_beli', '=', 'nota_beli.no_nota_beli')
            ->join('supplier', 'supplier.idsupplier', '=', 'nota_beli.idsupplier')
            ->where('detail_nota_beli.idproduk','=',$id)
            ->where('detail_nota_beli.tanggal_kadaluwarsa','like', $tgl.'%')
            ->get();
        $array_nota=array();
        foreach($nota_beli as $nota){
            $nomor_nota=$nota->no_nota_beli;
            $supplier=$nota->nama_supplier;
            array_push($array_nota, $nomor_nota.' - '.$supplier);
        }
        return json_encode($array_nota);
    }

    public function get_produk_retur($nota,$id,$tgl)
    {
        $thn=substr($tgl,6);
        $bln=substr($tgl,3,2);
        $hari=substr($tgl,0,2);
        $tgl=$thn.'-'.$bln.'-'.$hari;
        $jumlah_beli=NotaBeli::select('detail_nota_beli.harga_beli','detail_nota_beli.jumlah')
            ->join('detail_nota_beli', 'detail_nota_beli.no_nota_beli', '=', 'nota_beli.no_nota_beli')
            ->where('nota_beli.no_nota_beli','=',$nota)
            ->where('detail_nota_beli.idproduk','=',$id)
            ->where('detail_nota_beli.tanggal_kadaluwarsa','like', $tgl.'%')
            ->first();
        // $array_nota=array();
        // foreach($nota_beli as $nota){
        //     $nomor_nota=$nota->no_nota_beli;
        //     $supplier=$nota->nama_supplier;
        //     array_push($array_nota, $nomor_nota.' - '.$supplier);
        // }
        $beli=$jumlah_beli->harga_beli.','.$jumlah_beli->jumlah;
        return json_encode($beli);
    }

    public function create()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $list_supplier = Supplier::all();
            $list_nota_beli = NotaBeli::all();
            $list_produk = Produk::all();
            return view('retur-beli.tambah-retur', ['list_supplier'=>$list_supplier,'list_nota_beli'=>$list_nota_beli,'list_produk'=>$list_produk]);
        }
        else{
            return redirect()->back();
        }
    }

    // public function fetch(Request $request)
    // {
    //     $select = $request->get('select');
    //     $value = $request->get('value');
    //     $dependent = $request->get('dependent');
    //     $data = NotaBeli::select('*')
    //             ->where($select, $value)
    //             ->groupBy($dependent)
    //             ->get();
    //         $output = '<option value="">Pilih '.ucfirst($dependent).'</option>';
    //     foreach($data as $row){
    //         $output .= '<option value="'.$row->dependent.'">'.$row->$dependent.'</option>';
    //     }
    //     echo $output;
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $username = Auth::user()->username;
        $pegawai = Pegawai::select('idpegawai')
            ->where('username', '=', $username)
            ->first();
        $no_nota = $request->get('nota_beli');
        $jenis_retur=$request->get('jenis_retur');

        date_default_timezone_set('Asia/Jakarta');
        $tanggal= date('dmy');
        $tanggaljam = date('Y-m-d H:i:s');

        $retur = new ReturBeli([
            'no_nota_beli' => $no_nota,
            'idpegawai' => $pegawai->idpegawai,
            'tanggal' => $tanggaljam,
            'deskripsi' => $request->get('deskripsi'),
            'grand_total' => $request->get('grand_total'),
            'jenis_retur' => $jenis_retur,
        ]);
        $retur->save();

        //detail retur beli
        $retur_terakhir = ReturBeli::select('idretur')
                ->orderBy('idretur', 'desc')
                ->first();

        $idproduk=$request->get('produk');
        $lokasi=$request->get('lokasi');
        $tanggal_kedaluwarsa=$request->get('tanggal_kedaluwarsa');
        $thn=substr($tanggal_kedaluwarsa,6);
        $bln=substr($tanggal_kedaluwarsa,3,2);
        $hari=substr($tanggal_kedaluwarsa,0,2);
        $tanggal_kedaluwarsa=$thn.'-'.$bln.'-'.$hari;
        $tanggal_kedaluwarsa=date('Y-m-d H:i:s', strtotime($tanggal_kedaluwarsa));
        $jumlah_retur=$request->get('jumlah_retur');
        if($jenis_retur=='Produk'){
            $detail_retur = new DetailReturBeli([
                'idretur_pembelian' => $retur_terakhir->idretur,
                'idproduk' => $idproduk,
                'tanggal_kadaluwarsa_lama' => $tanggal_kedaluwarsa,
                'tanggal_kadaluwarsa_baru' => $request->get('tanggal_baru'),
                'jumlah' => $jumlah_retur,
            ]);
        }
        else{
            $detail_retur = new DetailReturBeli([
                'idretur_pembelian' => $retur_terakhir->idretur,
                'idproduk' => $idproduk,
                'tanggal_kadaluwarsa_lama' => $tanggal_kedaluwarsa,
                'jumlah' => $jumlah_retur,
            ]);
        }
        $detail_retur->save();

        //kurangi stok produk lama
        $stok_lama = StokProduk::select('*')
                    ->where('idproduk','=',$idproduk)
                    ->where('tanggal_kadaluwarsa','=',$tanggal_kedaluwarsa)
                    ->first();
        $stok_lama->stok-=$jumlah_retur;
        $stok_lama->save();

        //tambah stok produk baru
        $semua_stok_lama = StokProduk::select('*')
                    ->where('idproduk','=',$idproduk)
                    ->get();
        if($jenis_retur=='Produk'){
            for($j=0;$j<count($semua_stok_lama);$j++){
                $tanggal_baru=$request->get('tanggal_baru');
                $tanggal_baru = date('Y-m-d H:i:s', strtotime($tanggal_baru));
                if($semua_stok_lama[$j]->idproduk==$idproduk&&$semua_stok_lama[$j]->tanggal_kadaluwarsa==$tanggal_baru){
                    $semua_stok_lama[$j]->stok+=$jumlah_retur;
                    $semua_stok_lama[$j]->save();
                    break;
                }
                else{
                    if($j==count($semua_stok_lama)-1){
                        $stok_produk = new StokProduk([
                        'idproduk' => $idproduk,
                        'tanggal_kadaluwarsa' => $tanggal_baru,
                        'stok' => $jumlah_retur,
                        'lokasi' => $lokasi,
                        ]);
                        $stok_produk->save();
                    }
                }
            }
        }
        return redirect('/retur-pembelian/'.$retur_terakhir->idretur);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $retur=ReturBeli::select('jenis_retur')
                ->where('idretur','=',$id)
                ->first();
            $produk = DetailReturBeli::select('detail_retur_pembelian.*', 'produk.nama_produk')
                ->join('produk', 'produk.idproduk', '=', 'detail_retur_pembelian.idproduk')
                ->where('detail_retur_pembelian.idretur_pembelian', '=', $id)
                ->get();
            return view('retur-beli.detail-retur-beli', ['retur'=>$retur, 'produk'=>$produk, 'id'=>$id]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
