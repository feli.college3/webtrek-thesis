<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\NotaBeliKonsinyasi;
use App\DetailBeliKonsinyasi;
use App\DetailJualKonsinyasi;
use App\NotaKembaliKonsinyasi;
use App\DetailKembaliKonsinyasi;
use App\Supplier;
use App\Pegawai;
use App\ProdukKonsinyasi;
use Validator;

class KembaliKonsinyasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }

    public function index()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $list_nota = NotaKembaliKonsinyasi::select('nota_kembali_konsinyasi.*', 'supplier.idsupplier', 'supplier.nama_supplier', 'pegawai.idpegawai', 'pegawai.nama_pegawai')
                        ->join('supplier', 'supplier.idsupplier', '=', 'nota_kembali_konsinyasi.idkonsinyator')
                        ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_kembali_konsinyasi.idpegawai')
                        ->orderBy('nota_kembali_konsinyasi.tanggal', 'desc')
                        ->get();
            return view('kembali-konsinyasi.index-nota-kembali', ['list_nota'=>$list_nota]);
        }
        else{
            return redirect()->back();
        }
    }

    public function get_nota_beli($id)
    {
        $nota_beli = NotaBeliKonsinyasi::select('no_nota_beli_konsinyasi')
                ->where('idkonsinyator',$id)
                ->where('status_transaksi','!=','Selesai')
                ->get();
        $nota_array=array();
        foreach($nota_beli as $nota){
            array_push($nota_array, $nota->no_nota_beli_konsinyasi);
        }
        return json_encode($nota_array);
    }

    public function get_produk_konsinyasi($id)
    {
        $produk = NotaBeliKonsinyasi::select('produk_konsinyasi.idproduk','produk_konsinyasi.nama_produk','produk_konsinyasi.harga_jual','detail_beli_konsinyasi.harga_beli','detail_beli_konsinyasi.jumlah')
                ->join('detail_beli_konsinyasi', 'detail_beli_konsinyasi.no_nota_beli_konsinyasi', '=', 'nota_beli_konsinyasi.no_nota_beli_konsinyasi')
                ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'detail_beli_konsinyasi.idproduk_konsinyasi')
                ->where('nota_beli_konsinyasi.no_nota_beli_konsinyasi',$id)
                ->orderBy('produk_konsinyasi.idproduk','ASC')
                ->get();
        $tanggal_beli = NotaBeliKonsinyasi::select('tanggal_diterima')
            ->where('no_nota_beli_konsinyasi', '=', $id)
            ->first();

        $tanggal_beli=$tanggal_beli->tanggal_diterima;
        date_default_timezone_set('Asia/Jakarta');
        $tanggal_sekarang= date('Y-m-d H:i:s');

        $produk_array=array();
        for($i=0;$i<count($produk);$i++){
            $tampung=array();
            $tampung['id']=$produk[$i]->idproduk;
            $tampung['nama']=$produk[$i]->nama_produk;
            $tampung['harga_beli']=$produk[$i]->harga_beli;
            $tampung['harga_jual']=$produk[$i]->harga_jual;
            $tampung['jumlah_dibeli']=$produk[$i]->jumlah;
            $jumlah_terjual = DetailJualKonsinyasi::select(DB::raw('sum(detail_jual_konsinyasi.jumlah) as jumlah_terjual'))
            ->join('nota_jual','detail_jual_konsinyasi.no_nota_jual', '=', 'nota_jual.no_nota_jual')
            ->where('nota_jual.tanggal','>=',$tanggal_beli)
            ->where('nota_jual.tanggal','<=',$tanggal_sekarang)
            ->where('detail_jual_konsinyasi.idproduk_konsinyasi','=',$tampung['id'])
            ->groupBy('detail_jual_konsinyasi.idproduk_konsinyasi')
            ->orderBy('detail_jual_konsinyasi.idproduk_konsinyasi','ASC')
            ->first();
            if($jumlah_terjual==null){
                $jumlah_terjual=0;
            }
            else{
                $jumlah_terjual=$jumlah_terjual->jumlah_terjual;
            }
            $tampung['jumlah_terjual']=$jumlah_terjual;
            $jumlah_kembali=$tampung['jumlah_dibeli']-$tampung['jumlah_terjual'];
            $tampung['jumlah_kembali']=$jumlah_kembali;
            array_push($produk_array, $tampung);
        }
        return json_encode($produk_array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $list_supplier = Supplier::select('*')
                ->where('konsinyator','=',1)
                ->get();
            // $list_nota_beli = NotaBeliKonsinyasi::all();
            // $list_produk = ProdukKonsinyasi::all();
            return view('kembali-konsinyasi.tambah-nota-kembali', ['list_supplier'=>$list_supplier]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $username = Auth::user()->username;
        $pegawai = Pegawai::select('idpegawai')
            ->where('username', '=', $username)
            ->first();
        date_default_timezone_set('Asia/Jakarta');
        $tanggal= date('dmy');
        $tanggaljam = date('Y-m-d H:i:s');
        $total_trans = NotaKembaliKonsinyasi::where('no_nota_kembali','like', $tanggal.'%')
                     ->count();
        $no_nota=$total_trans+1;
        if($no_nota < 10){
            $no_nota = $tanggal.'KK00'.$no_nota;
        }
        else if($no_nota >= 10){
            $no_nota = $tanggal.'KK0'.$no_nota;
        }
        else if($no_nota >= 100){
            $no_nota = $tanggal.'KK'.$no_nota;
        }
        $no_nota_beli = $request->get('no_nota');
        $konsinyator=$request->get('konsinyator');
        $keterangan=$request->get('keterangan');
        $total_dibayar=$request->get('total_dibayar');
        $total_untung=$request->get('total_untung');
        $total_produk=$request->get('total_produk');

        $nota_kembali = new NotaKembaliKonsinyasi([
            'no_nota_kembali' => $no_nota,
            'no_nota_beli_konsinyasi' => $no_nota_beli,
            'idkonsinyator' => $konsinyator,
            'idpegawai' => $pegawai->idpegawai,
            'tanggal' => $tanggaljam,
            'keterangan' => $keterangan,
            'total_dibayar' => $total_dibayar,
            'total_keuntungan' => $total_untung,
        ]);
        $nota_kembali->save();


        //detail kembali
        for($i=0;$i<$total_produk;$i++){
            $produk=$request->get('produk'.$i);
            $jumlah_terjual=$request->get('jumlah_terjual'.$i);
            $jumlah_kembali=$request->get('jumlah_kembali'.$i);
            $detail_kembali = new DetailKembaliKonsinyasi([
                'no_nota_kembali' => $no_nota,
                'idproduk_konsinyasi' => $produk,
                'jumlah_terjual' => $jumlah_terjual,
                'jumlah_kembali' => $jumlah_kembali,
            ]);
            $detail_kembali->save();
            //update stok produk konsi
            $produk=ProdukKonsinyasi::find($produk);
            $produk->stok-=$jumlah_kembali;
            $produk->save();
        }

        //status nota beli konsi menjadi 'Selesai'
        $nota_beli=NotaBeliKonsinyasi::find($no_nota_beli);
        $nota_beli->status_transaksi='Selesai';
        $nota_beli->save();

        return redirect('/kembali-konsinyasi/'.$no_nota);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $produk = DetailKembaliKonsinyasi::select('detail_nota_kembali_konsinyasi.*', 'produk_konsinyasi.nama_produk')
                ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'detail_nota_kembali_konsinyasi.idproduk_konsinyasi')
                ->where('detail_nota_kembali_konsinyasi.no_nota_kembali', '=', $id)
                ->get();
            return view('kembali-konsinyasi.detail-nota-kembali', ['produk'=>$produk, 'id'=>$id]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
