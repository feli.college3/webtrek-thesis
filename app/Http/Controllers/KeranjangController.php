<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pelanggan;
use App\KeranjangBiasa;
use App\KeranjangKonsinyasi;
use App\DetailToko;
use App\Produk;
use App\ProdukKonsinyasi;
use DB;

class KeranjangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //tambahan untuk update with 2 primary keys
    protected $primaryKey = ['idpelanggan', 'idproduk'];
    public $incrementing = false;
    //tutup tambahan

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $detail_toko=DetailToko::select('*')->first();
        $username = Auth::user()->username;
        $pelanggan = Pelanggan::select('idpelanggan')
            ->where('username', '=', $username)
            ->first();

        $produk_toko = KeranjangBiasa::select('keranjang_biasa.*','produk.*')
        ->join('produk', 'produk.idproduk', '=', 'keranjang_biasa.idproduk')
        ->where('keranjang_biasa.idpelanggan', '=', $pelanggan->idpelanggan)
        // ->orderBy('keranjang_biasa.updated_at')
        ->get();

        if($detail_toko->fitur_harga_grosir==1){
            $subtotal_toko=array();
            foreach($produk_toko as $produk){
                if($produk->jumlah>=$detail_toko->jumlah_grosir){
                    $tampung=KeranjangBiasa::select(DB::raw("sum(keranjang_biasa.jumlah*produk.harga_jual_grosir) as subtotal"))
                        ->join('produk', 'produk.idproduk', '=', 'keranjang_biasa.idproduk')
                        ->where('keranjang_biasa.idpelanggan', '=', $pelanggan->idpelanggan)
                        ->where('keranjang_biasa.idproduk', '=', $produk->idproduk)
                        ->first();
                }
                else{
                    $tampung=KeranjangBiasa::select(DB::raw("sum(keranjang_biasa.jumlah*produk.harga_jual_eceran) as subtotal"))
                        ->join('produk', 'produk.idproduk', '=', 'keranjang_biasa.idproduk')
                        ->where('keranjang_biasa.idpelanggan', '=', $pelanggan->idpelanggan)
                        ->where('keranjang_biasa.idproduk', '=', $produk->idproduk)
                        ->first();
                }
                array_push($subtotal_toko, $tampung);
            }
        }
        else{
            $subtotal_toko = KeranjangBiasa::select(DB::raw("sum(keranjang_biasa.jumlah*produk.harga_jual_eceran) as subtotal"))
            ->join('produk', 'produk.idproduk', '=', 'keranjang_biasa.idproduk')
            ->where('keranjang_biasa.idpelanggan', '=', $pelanggan->idpelanggan)
            // ->orderBy('keranjang_biasa.updated_at')
            ->groupBy('keranjang_biasa.idproduk')
            ->get();
        }
        $produk_konsinyasi = KeranjangKonsinyasi::select('keranjang_konsinyasi.*', 'produk_konsinyasi.*')
            ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'keranjang_konsinyasi.idproduk_konsinyasi')
            ->where('keranjang_konsinyasi.idpelanggan', '=', $pelanggan->idpelanggan)
            // ->orderBy('keranjang_konsinyasi.updated_at')
            ->get();
        $subtotal_konsinyasi = KeranjangKonsinyasi::select(DB::raw("sum(keranjang_konsinyasi.jumlah*produk_konsinyasi.harga_jual) as subtotal"))
        ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'keranjang_konsinyasi.idproduk_konsinyasi')
        ->where('keranjang_konsinyasi.idpelanggan', '=', $pelanggan->idpelanggan)
        // ->orderBy('keranjang_konsinyasi.updated_at')
        ->groupBy('keranjang_konsinyasi.idproduk_konsinyasi')
        ->get();

        $jumlah_produk=count($produk_toko)+count($produk_konsinyasi);
        $total_awal=0;
        foreach($subtotal_toko as $produk){
            $total_awal+=$produk->subtotal;
        }
        foreach($subtotal_konsinyasi as $produk){
            $total_awal+=$produk->subtotal;
        }

        return view('customer.keranjang', ['produk_toko'=>$produk_toko,'produk_konsinyasi'=>$produk_konsinyasi,'subtotal_toko'=>$subtotal_toko,'subtotal_konsinyasi'=>$subtotal_konsinyasi,'total_keranjang'=>$total_awal,'jumlah_produk'=>$jumlah_produk]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function simpan_satuan($idproduk)
    {
        $username = Auth::user()->username;
        $role = Auth::user()->roles->first()->nama;
        if($role!='kasir'&&$role!='kurir'&&$role!='manajer'){
            $pelanggan = Pelanggan::select('idpelanggan')
                ->where('username', '=', $username)
                ->first();
            $panjang_id = strlen($idproduk);
            if($panjang_id==6){
                $keranjang=KeranjangBiasa::where('idpelanggan','=',$pelanggan->idpelanggan)
                    ->where('idproduk','=',$idproduk)
                    ->first();
                if($keranjang==null){
                    $produk = new KeranjangBiasa([
                    'idpelanggan' => $pelanggan->idpelanggan,
                    'idproduk' => $idproduk,
                    'jumlah' => 1,
                    ]);
                    $produk->save();
                }
            }
            else{
                $keranjang=KeranjangKonsinyasi::where('idpelanggan','=',$pelanggan->idpelanggan)
                    ->where('idproduk_konsinyasi','=',$idproduk)
                    ->first();
                if($keranjang==null){
                    $produk = new KeranjangKonsinyasi([
                    'idpelanggan' => $pelanggan->idpelanggan,
                    'idproduk_konsinyasi' => $idproduk,
                    'jumlah' => 1,
                    ]);
                    $produk->save();
                }
            }
        }
        return redirect()->back();
    }

    public function store(Request $request)
    {
        $username = Auth::user()->username;
        $id = $request->get('idproduk');
        $role = Auth::user()->roles->first()->nama;
        if($role!='kasir'&&$role!='kurir'&&$role!='manajer'){
            $pelanggan = Pelanggan::select('idpelanggan')
                ->where('username', '=', $username)
                ->first();
            $panjang_id = strlen($id);
            if($panjang_id==6){
                $keranjang=KeranjangBiasa::where('idpelanggan','=',$pelanggan->idpelanggan)
                    ->where('idproduk','=',$id)
                    ->first();
                if($keranjang==null){
                    $produk = new KeranjangBiasa([
                    'idpelanggan' => $pelanggan->idpelanggan,
                    'idproduk' => $id,
                    'jumlah' => $request->get('jumlah'),
                    ]);
                    $produk->save();
                }
            }
            else{
                $keranjang=KeranjangKonsinyasi::where('idpelanggan','=',$pelanggan->idpelanggan)
                    ->where('idproduk_konsinyasi','=',$id)
                    ->first();
                if($keranjang==null){
                    $produk = new KeranjangKonsinyasi([
                    'idpelanggan' => $pelanggan->idpelanggan,
                    'idproduk_konsinyasi' => $id,
                    'jumlah' => $request->get('jumlah'),
                    ]);
                    $produk->save();
                }
            }
        }
        return redirect()->back();
    }

    public function ubah_jumlah($aksi, $id)
    {
        $panjang_id = strlen((string)$id);
        $username = Auth::user()->username;
        $role = Auth::user()->roles->first()->nama;
        if($role!='kasir'&&$role!='kurir'&&$role!='manajer'){
            $pelanggan = Pelanggan::select('idpelanggan')
                ->where('username', '=', $username)
                ->first();
            if($panjang_id==6){
                $keranjang=KeranjangBiasa::where('idpelanggan','=',$pelanggan->idpelanggan)
                    ->where('idproduk','=',$id)
                    ->first();
                $produk=Produk::select('produk.idproduk',DB::raw('SUM(stok_produk.stok) as stok'))
                ->leftjoin('stok_produk','stok_produk.idproduk','=','produk.idproduk')
                ->groupBy('produk.idproduk')
                ->where('produk.idproduk','=',$id)
                ->first();
                // echo $keranjang.$produk;
            }
            else{

                //tambahan untuk update with 2 primary keys
                $primaryKey = ['idpelanggan', 'idproduk_konsinyasi'];
                $keranjang=KeranjangKonsinyasi::where('idpelanggan','=',$pelanggan->idpelanggan)
                    ->where('idproduk_konsinyasi','=',$id)
                    ->first();
                $produk=ProdukKonsinyasi::find($id);
            }
            if($aksi=='tambah'){
                if($keranjang->jumlah<$produk->stok){
                    $keranjang->jumlah+=1;
                    $keranjang->save();
                }
            }
            else if($aksi=='kurang'){
                if($keranjang->jumlah!=1){
                    $keranjang->jumlah-=1;
                    $keranjang->save();
                }
            }
        }
        // $produk->save();
        // echo 'mantap';
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $idproduk = $id;
        $panjang_id = strlen($idproduk);
        $role = Auth::user()->roles->first()->nama;
        if($role!='kasir'&&$role!='kurir'&&$role!='manajer'){
            $username = Auth::user()->username;
            $pelanggan = Pelanggan::select('idpelanggan')
                ->where('username', '=', $username)
                ->first();
            $idpelanggan = $pelanggan->idpelanggan;
            if($panjang_id==6){
                KeranjangBiasa::where(['idproduk' => $idproduk, 'idpelanggan'=>$idpelanggan])->delete();
            }
            else{
                KeranjangKonsinyasi::where(['idproduk_konsinyasi' => $idproduk, 'idpelanggan'=>$idpelanggan])->delete();
            }
            return redirect('/keranjang')->with('status', 'Produk terhapus dari keranjang');
        }
        else{
            return redirect()->back();
        }
        // $users = User::where([
        //        'column1' => value1,
        //        'column2' => value2,
        //        'column3' => value3
        // ])->get();
        // return redirect(action('PelangganController@index'));
        // $plg = Pelanggan::find($id)->delete();
    }
}
