<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\NotaBeliKonsinyasi;
use App\DetailBeliKonsinyasi;
use App\Supplier;
use App\Pegawai;
use App\ProdukKonsinyasi;
use Validator;

class BeliKonsinyasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //tambahan untuk update with 2 primary keys
    protected $primaryKey = ['no_nota_beli_konsinyasi','idproduk_konsinyasi'];
    public $incrementing = false;
    //tutup tambahan
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }
    
    public function index()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $list_nota = NotaBeliKonsinyasi::select('nota_beli_konsinyasi.*', 'supplier.idsupplier', 'supplier.nama_supplier', 'pegawai.idpegawai', 'pegawai.nama_pegawai')
                        ->join('supplier', 'supplier.idsupplier', '=', 'nota_beli_konsinyasi.idkonsinyator')
                        ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_beli_konsinyasi.idpegawai')
                        ->orderBy('nota_beli_konsinyasi.tanggal_dipesan', 'desc')
                        ->get();
            // dd($list_nota_beli);
            // print_r($list_nota_beli);
            // die();
            return view('beli-konsinyasi.index-nota-beli', ['list_nota'=>$list_nota]);
        }
        else{
            return redirect()->back();
        }
    }

    public function get_detail_produk($produk)
    {
        $id=ProdukKonsinyasi::where('nama_produk','=',$produk)->first();
        $id=$id->idproduk;
        $harga_beli=DetailBeliKonsinyasi::select('harga_beli')
            ->join('nota_beli_konsinyasi', 'detail_beli_konsinyasi.no_nota_beli_konsinyasi', '=', 'nota_beli_konsinyasi.no_nota_beli_konsinyasi')
            ->where('detail_beli_konsinyasi.idproduk_konsinyasi','=',$id)
            ->orderBy('nota_beli_konsinyasi.tanggal_diterima','desc')
            ->first();
        if($harga_beli==null){
            $harga_beli=0;
        }
        else{
            $harga_beli=$harga_beli->harga_beli;
        }
        return json_encode($harga_beli);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $list_supplier = Supplier::select('*')
                ->where('konsinyator','=',1)
                ->get();
            $list_produk = ProdukKonsinyasi::orderBy('produk_konsinyasi.nama_produk')->get();
            date_default_timezone_set('Asia/Jakarta');
            $tanggal= date('dmy');

            $total_trans = NotaBeliKonsinyasi::where('nota_beli_konsinyasi.no_nota_beli_konsinyasi','like', $tanggal.'%')
                         ->count();
            $id_baru=$total_trans+1;
            if($total_trans < 10){
                $id_baru = $tanggal."BK00".$id_baru;
            }
            else if($total_trans >= 10){
                $id_baru = $tanggal."BK0".$id_baru;
            }
            else if($total_trans >= 100){
                $id_baru = $tanggal."BK".$id_baru;
            }
            return view('beli-konsinyasi.tambah-nota-beli', ['list_supplier'=>$list_supplier, 'list_produk'=>$list_produk, 'id_baru'=>$id_baru]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $total_data = $request->get('total_data');
        $grand_total=$request->get('grand_total');

        date_default_timezone_set('Asia/Jakarta');
        $tanggal= date('dmY');
        $tanggaljam = date('Y-m-d H:i:s');

        $nomor_nota = $request->get('no_nota');

        $username = Auth::user()->username;
        $pegawai = Pegawai::select('*')
            ->where('username', '=', $username)
            ->first();

        $status=$request->get('status_transaksi');
        $nota_beli = new NotaBeliKonsinyasi([
            'no_nota_beli_konsinyasi' => $nomor_nota,
            'idkonsinyator' => $request->get('konsinyator'),
            'idpegawai' => $pegawai->idpegawai,
            'tanggal_dipesan' => $tanggaljam,
            'tanggal_diterima' => $tanggaljam,
            'lama_penitipan' => 0,
            'keterangan' => $request->get('keterangan'),
            'status_transaksi' => 'Dipesan',
        ]);

        $nota_beli->save();

        ////////////////////DETAIL BELI KONSI////////////////////
        $produk = ProdukKonsinyasi::all();
        $idproduk_terpilih = "";
        $stokLama = 0;
        for($i=1;$i<=$total_data;$i++)
        {
            $produk_dibeli=$request->get('produk'.$i);
            foreach($produk as $prod)
            {
                if($prod->nama_produk == $produk_dibeli){
                    $idproduk_terpilih = $prod->idproduk;
                    $harga = $request->get('harga'.$i);
                    $jumlah = $request->get('jumlah'.$i);
                    $jumlah=(float)$jumlah;

                    //tambah stok
                    // $prod->stok+=$jumlah;
                    // $prod->save();

                    //detail beli
                    $detail_eksis = DetailBeliKonsinyasi::select('*')
                        ->where('no_nota_beli_konsinyasi','=',$nomor_nota)
                        ->where('idproduk_konsinyasi','=',$idproduk_terpilih)
                        ->get();
                    if(count($detail_eksis)>0){
                        $detail_eksis[0]->jumlah+=$jumlah;
                        $detail_eksis[0]->save();
                    }
                    else{
                        $detail_nota_beli = new DetailBeliKonsinyasi([
                            'no_nota_beli_konsinyasi' => $nomor_nota,
                            'idproduk_konsinyasi' => $idproduk_terpilih,
                            'harga_beli' => (float)$harga,
                            'jumlah' => $jumlah,
                        ]);
                        $detail_nota_beli->save();
                    }
                }
            }
        }
        return redirect('/beli-konsinyasi/'.$nomor_nota);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $produk = DetailBeliKonsinyasi::select('detail_beli_konsinyasi.*', 'produk_konsinyasi.nama_produk')
                ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'detail_beli_konsinyasi.idproduk_konsinyasi')
                ->where('detail_beli_konsinyasi.no_nota_beli_konsinyasi', '=', $id)
                ->get();
            return view('beli-konsinyasi.detail-nota-beli', ['produk'=>$produk, 'id'=>$id]);
        }
        else{
            return redirect()->back();
        }
    }

    public function index_postorder()
    {
        $list_nota = NotaBeliKonsinyasi::select('nota_beli_konsinyasi.*', 'supplier.idsupplier', 'supplier.nama_supplier', 'pegawai.idpegawai', 'pegawai.nama_pegawai')
                    ->join('supplier', 'supplier.idsupplier', '=', 'nota_beli_konsinyasi.idkonsinyator')
                    ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_beli_konsinyasi.idpegawai')
                    ->orderBy('nota_beli_konsinyasi.tanggal_dipesan', 'asc')
                    ->where('nota_beli_konsinyasi.status_transaksi', '=', 'Dipesan')
                    ->get();
        return view('beli-konsinyasi.nota-beli-dipesan', ['list_nota'=>$list_nota]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $nota_titip=NotaBeliKonsinyasi::join('supplier', 'supplier.idsupplier', '=', 'nota_beli_konsinyasi.idkonsinyator')
                ->where('nota_beli_konsinyasi.no_nota_beli_konsinyasi', '=', $id)
                ->first();
            $detail_nota=DetailBeliKonsinyasi::join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'detail_beli_konsinyasi.idproduk_konsinyasi')
                ->where('detail_beli_konsinyasi.no_nota_beli_konsinyasi','=',$id)
                ->get();
            $db=ProdukKonsinyasi::all();
            $produk_db=array();
            $produk_nota=array();
            foreach($db as $p){
                array_push($produk_db, $p->idproduk);
            }
            foreach($detail_nota as $detail){
                array_push($produk_nota, $detail->idproduk);
            }
            $produk_lain = array_diff($produk_db,$produk_nota);
            $semua_produk=array();
            foreach($produk_lain as $lain){
                $prod=ProdukKonsinyasi::find($lain);
                array_push($semua_produk, $prod);
            }
            $total_data=count($detail_nota);
            return view('beli-konsinyasi.edit-nota-beli', ['nota_titip'=>$nota_titip,'detail_nota'=>$detail_nota,'total_data'=>$total_data,'semua_produk'=>$semua_produk,'id'=>$id]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $total_data = $request->get('total_data');

        date_default_timezone_set('Asia/Jakarta');
        $tanggal= date('dmY');
        $tanggaljam = date('Y-m-d H:i:s');


        $username = Auth::user()->username;
        $pegawai = Pegawai::select('*')
            ->where('username', '=', $username)
            ->first();

        $nota_beli=NotaBeliKonsinyasi::find($id);
        $nota_beli->idpegawai=$pegawai->idpegawai;
        $nota_beli->tanggal_diterima=$tanggaljam;
        $nota_beli->lama_penitipan=$request->get('lama_penitipan');
        $nota_beli->status_transaksi='Sudah Dipesan';
        $nota_beli->keterangan=$request->get('keterangan');
        $nota_beli->save();

        //hapus detail lama
        $detail_lama = DetailBeliKonsinyasi::where('no_nota_beli_konsinyasi','=',$id)->get();
        foreach($detail_lama as $detil){
            $detil->delete();
        }

        ////////////////////DETAIL BELI KONSI////////////////////
        $produk = ProdukKonsinyasi::all();
        $idproduk_terpilih = "";
        $stokLama = 0;
        for($i=1;$i<=$total_data;$i++)
        {
            $produk_dibeli=$request->get('produk'.$i);
            foreach($produk as $prod)
            {
                if($prod->idproduk == $produk_dibeli){
                    $idproduk_terpilih = $prod->idproduk;
                    $harga = $request->get('harga'.$i);
                    $jumlah = $request->get('jumlah'.$i);
                    $jumlah=(float)$jumlah;

                    //tambah stok
                    $prod->stok+=$jumlah;
                    $prod->save();

                    //detail beli
                    $detail_eksis = DetailBeliKonsinyasi::select('*')
                        ->where('no_nota_beli_konsinyasi','=',$id)
                        ->where('idproduk_konsinyasi','=',$idproduk_terpilih)
                        ->get();
                    if(count($detail_eksis)>0){
                        $detail_eksis[0]->jumlah+=$jumlah;
                        $detail_eksis[0]->save();
                    }
                    else{
                        $detail_nota_beli = new DetailBeliKonsinyasi([
                            'no_nota_beli_konsinyasi' => $id,
                            'idproduk_konsinyasi' => $idproduk_terpilih,
                            'harga_beli' => (float)$harga,
                            'jumlah' => $jumlah,
                        ]);
                        $detail_nota_beli->save();
                    }
                }
            }
        }
        return redirect('/beli-konsinyasi/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
