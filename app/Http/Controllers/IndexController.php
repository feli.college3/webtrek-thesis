<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Kategori;
use App\Produk;
use App\ProdukKonsinyasi;
use App\Pelanggan;
use App\DetailToko;
use DB;
use View;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }
    
    public function pelanggan()
    {
        // $warna=array("red","brown","green","yellow","purple","blue","orange","grey","pink","tan");
        // $list_kategori = Kategori::all();
        $array_toko_dibeli=array();
        $array_konsi_dibeli=array();
        $array_toko_terlaris=array();
        $array_konsi_terlaris=array();
        if (Auth::user()){
            $detail_toko=DetailToko::select('*')->first();
            $role = Auth::user()->roles->first()->nama;
            $username = Auth::user()->username;
            if($role=='pelanggan'){
                $pelanggan = Pelanggan::select('idpelanggan')
                    ->where('username', '=', $username)
                    ->first();
                $id_dibeli = Produk::select(DB::raw('distinct(produk.idproduk)'))
                    ->join('detail_nota_jual', 'produk.idproduk', '=', 'detail_nota_jual.idproduk')
                    ->join('nota_jual', 'detail_nota_jual.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                    ->where('nota_jual.idpelanggan', $pelanggan->idpelanggan)
                    ->get();
                $toko_dibeli=array();
                foreach($id_dibeli as $id){
                    $prod=Produk::find($id);
                    array_push($toko_dibeli, $prod);
                }
                $jumlah_toko_dibeli=count($toko_dibeli);
                $konsi_dibeli=array();

                $array_toko_dibeli=$toko_dibeli;
                $array_konsi_dibeli=array();

                if($detail_toko->fitur_konsinyasi==1){
                    $id_dibeli = ProdukKonsinyasi::select(DB::raw('distinct(produk_konsinyasi.idproduk)'))
                        ->join('detail_jual_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'detail_jual_konsinyasi.idproduk_konsinyasi')
                        ->join('nota_jual', 'detail_jual_konsinyasi.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                        ->where('nota_jual.idpelanggan', $pelanggan->idpelanggan)
                        ->where('produk_konsinyasi.stok','>', 0)
                        ->get();
                    foreach($id_dibeli as $id){
                        $prod=ProdukKonsinyasi::find($id);
                        array_push($konsi_dibeli, $prod);
                    }
                    $jumlah_konsi_dibeli=count($konsi_dibeli);
                    $array_konsi_dibeli=$konsi_dibeli;
                    if($jumlah_konsi_dibeli>0){
                        if($jumlah_toko_dibeli>3){
                            $array_toko_dibeli=array();
                            for($i=0;$i<3;$i++){
                                array_push($array_toko_dibeli, $toko_dibeli[$i]);
                            }
                        }
                        if($jumlah_konsi_dibeli>3){
                            $array_konsi_dibeli=array();
                            for($i=0;$i<3;$i++){
                                array_push($array_konsi_dibeli, $konsi_dibeli[$i]);
                            }
                        }
                    }
                    else{
                        if($jumlah_toko_dibeli>5){
                            $array_toko_dibeli=array();
                            for($i=0;$i<5;$i++){
                                array_push($array_toko_dibeli, $toko_dibeli[$i]);
                            }
                        }
                    }
                }
                else{
                    if($jumlah_toko_dibeli>5){
                        $array_toko_dibeli=array();
                        for($i=0;$i<5;$i++){
                            array_push($array_toko_dibeli, $toko_dibeli[$i]);
                        }
                    }
                }
                // dd($array_toko_dibeli[0][0]->idproduk);

                // return view('customer.index', ['array_toko_dibeli'=>$array_toko_dibeli, 'array_konsi_dibeli'=>$array_konsi_dibeli]);
            }
        }
        $toko_terlaris=Produk::select(DB::raw('sum(detail_nota_jual.jumlah) as jumlah_terjual'),'produk.idproduk')
            ->leftJoin('detail_nota_jual', 'detail_nota_jual.idproduk', '=', 'produk.idproduk')
            ->groupBy('produk.idproduk')
            ->orderBy('jumlah_terjual','DESC')
            ->get();
        $konsi_terlaris=ProdukKonsinyasi::select(DB::raw('sum(detail_jual_konsinyasi.jumlah) as jumlah_terjual'),'produk_konsinyasi.idproduk')
            ->leftJoin('detail_jual_konsinyasi', 'detail_jual_konsinyasi.idproduk_konsinyasi', '=', 'produk_konsinyasi.idproduk')
            ->groupBy('produk_konsinyasi.idproduk')
            ->orderBy('jumlah_terjual','DESC')
            ->get();
        $jumlah_toko_terlaris=count($toko_terlaris);
        $jumlah_konsi_terlaris=count($konsi_terlaris);
        $total_toko=$jumlah_toko_terlaris;
        $total_konsi=$jumlah_konsi_terlaris;
        if($jumlah_toko_terlaris>4){
            $total_toko=4;
        }
        for($i=0;$i<$total_toko;$i++){
            $produk=Produk::find($toko_terlaris[$i]->idproduk);
            array_push($array_toko_terlaris, $produk);
        }
        if($jumlah_konsi_terlaris>4){
            $total_konsi=4;
        }
        for($i=0;$i<$total_konsi;$i++){
            $produk=ProdukKonsinyasi::find($konsi_terlaris[$i]->idproduk);
            array_push($array_konsi_terlaris, $produk);
        }
        // dd($array_toko_dibeli,$array_konsi_dibeli);
                // dd($array_toko_dibeli);
        return view('customer.index', ['array_toko_dibeli'=>$array_toko_dibeli, 'array_konsi_dibeli'=>$array_konsi_dibeli, 'array_toko_terlaris'=>$array_toko_terlaris, 'array_konsi_terlaris'=>$array_konsi_terlaris]);
    }

    public function pegawai()
    {
        return view('index.pegawai');
    }

    public function pengaturan_awal()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nama_toko=$request->get('nama');
        $alamat=$request->get('alamat');
        $telepon=$request->get('telepon');
        $deskripsi=$request->get('deskripsi');
        $tunai=0;
        $cod=0;
        $transfer=0;
        $debit=0;
        $kredit=0;
        $hutang=0;
        $piutang=0;
        $retur=0;
        $konsinyasi=0;
        $grosir=0;
        $jumlah_grosir=0;
        $pengiriman=0;
        $kadaluwarsa=0;
        $batas_hutang=0;
        $maksimal_toggle=0;
        $stok_global_toggle=0;
        $nama_bank='-';
        $norek='-';
        $pemilik_rek='-';
        $waktu_maksimal=0;
        $stok_minimal=0;
        $maksimal_jarak=0;
        $interval_jarak=0;
        $ongkir=0;
        if (isset($request->tunai)){
            $tunai=1;
        }
        if (isset($request->cod)){
            $cod=1;
        }
        if (isset($request->transfer)){
            $transfer=1;
            $nama_bank=$request->get('nama_bank');
            $norek=$request->get('norek');
            $pemilik_rek=$request->get('pemilik_rek');
        }
        if (isset($request->debit)){
            $debit=1;
        }
        if (isset($request->kredit)){
            $kredit=1;
        }
        if (isset($request->hutang)){
            $hutang=1;
            if (isset($request->batas_hutang)){
                $batas_hutang=1;
            }
        }
        if (isset($request->piutang)){
            $piutang=1;
        }
        if (isset($request->retur)){
            $retur=1;
        }
        if (isset($request->konsinyasi)){
            $konsinyasi=1;
        }
        if (isset($request->grosir)){
            $grosir=1;
            $jumlah_grosir=$request->get('jumlah_grosir');
        }
        if (isset($request->pengiriman)){
            $pengiriman=1;
            $maksimal_jarak=$request->get('maksimal_jarak');
            $interval_jarak=$request->get('interval_jarak');
            $ongkir=$request->get('ongkir');
        }
        if (isset($request->kadaluwarsa)){
            $kadaluwarsa=1;
        }
        if (isset($request->maksimal_toggle)){
            $maksimal_toggle=1;
            $waktu_maksimal=$request->get('waktu_maksimal');
        }
        if (isset($request->stok_global_toggle)){
            $stok_global_toggle=1;
        }
        if($stok_global_toggle==0){
            $stok_minimal=$request->get('stok_minimal');
        }
        $detail = new DetailToko([
            'nama_toko' => $nama_toko,
            'alamat_toko' => $alamat,
            'telepon_toko' => $telepon,
            'deskripsi_toko' => $deskripsi,
            'nama_bank' => $nama_bank,
            'nomor_rekening' => $norek,
            'pemilik_rekening' => $pemilik_rek,
            'pembayaran_tunai' => $tunai,
            'pembayaran_cod' => $cod,
            'pembayaran_transfer' => $transfer,
            'pembayaran_debit' => $debit,
            'pembayaran_kredit' => $kredit,
            'pembayaran_hutang' => $hutang,
            'pembayaran_piutang' => $piutang,
            'fitur_retur' => $retur,
            'fitur_konsinyasi' => $konsinyasi,
            'fitur_harga_grosir' => $grosir,
            'jumlah_grosir' => $jumlah_grosir,
            'fitur_stok_minimal_perproduk' => $stok_global_toggle,
            'stok_minimal' => $stok_minimal,
            'fitur_stok_fifo' => $kadaluwarsa,
            'fitur_kirim' => $pengiriman,
            'maksimal_jarak' => $maksimal_jarak,
            'interval_jarak' => $interval_jarak,
            'ongkir_interval' => $ongkir,
            'fitur_batas_hutang' => $batas_hutang,
            'fitur_pembatalan_pesanan' => $maksimal_toggle,
            'maksimal_pembatalan' => $waktu_maksimal,
            ]);
        $detail->save();
        return redirect('dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $detail_toko=DetailToko::find($id);
            return view('index.edit-konfigurasi', ['detail_toko'=>$detail_toko]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $detail_toko=DetailToko::find($id);
        $tunai=0;
        $cod=0;
        $transfer=0;
        $debit=0;
        $kredit=0;
        $hutang=0;
        $piutang=0;
        $retur=0;
        $konsinyasi=0;
        $grosir=0;
        $jumlah_grosir=0;
        $pengiriman=0;
        $kadaluwarsa=0;
        $batas_hutang=0;
        $maksimal_toggle=0;
        $stok_global_toggle=0;
        $nama_bank='-';
        $norek='-';
        $pemilik_rek='-';
        $waktu_maksimal=0;
        $stok_minimal=0;
        $maksimal_jarak=0;
        $interval_jarak=0;
        $ongkir=0;
        if (isset($request->tunai)){
            $tunai=1;
        }
        if (isset($request->cod)){
            $cod=1;
        }
        if (isset($request->transfer)){
            $transfer=1;
            $nama_bank=$request->get('nama_bank');
            $norek=$request->get('norek');
            $pemilik_rek=$request->get('pemilik_rek');
        }
        if (isset($request->debit)){
            $debit=1;
        }
        if (isset($request->kredit)){
            $kredit=1;
        }
        if (isset($request->hutang)){
            $hutang=1;
            if (isset($request->batas_hutang)){
                $batas_hutang=1;
            }
        }
        if (isset($request->piutang)){
            $piutang=1;
        }
        if (isset($request->retur)){
            $retur=1;
        }
        if (isset($request->konsinyasi)){
            $konsinyasi=1;
        }
        if (isset($request->grosir)){
            $grosir=1;
            $jumlah_grosir=$request->get('jumlah_grosir');
        }
        if (isset($request->pengiriman)){
            $pengiriman=1;
            $maksimal_jarak=$request->get('maksimal_jarak');
            $interval_jarak=$request->get('interval_jarak');
            $ongkir=$request->get('ongkir');
        }
        if (isset($request->kadaluwarsa)){
            $kadaluwarsa=1;
        }
        if (isset($request->maksimal_toggle)){
            $maksimal_toggle=1;
            $waktu_maksimal=$request->get('waktu_maksimal');
        }
        if (isset($request->stok_global_toggle)){
            $stok_global_toggle=1;
        }
        if($stok_global_toggle==0){
            $stok_minimal=$request->get('stok_minimal');
        }
        $detail_toko->nama_toko=$request->get('nama');
        $detail_toko->alamat_toko=$request->get('alamat');
        $detail_toko->telepon_toko=$request->get('telepon');
        $detail_toko->deskripsi_toko=$request->get('deskripsi');

        $detail_toko->nama_bank=$nama_bank;
        $detail_toko->nomor_rekening=$norek;
        $detail_toko->pemilik_rekening=$pemilik_rek;
        $detail_toko->pembayaran_tunai=$tunai;
        $detail_toko->pembayaran_cod=$cod;
        $detail_toko->pembayaran_transfer=$transfer;
        $detail_toko->pembayaran_debit=$debit;
        $detail_toko->pembayaran_kredit=$kredit;
        $detail_toko->pembayaran_hutang=$hutang;
        $detail_toko->pembayaran_piutang=$piutang;
        $detail_toko->fitur_retur=$retur;
        $detail_toko->fitur_konsinyasi=$konsinyasi;
        $detail_toko->fitur_harga_grosir=$grosir;
        $detail_toko->jumlah_grosir=$jumlah_grosir;
        $detail_toko->fitur_stok_minimal_perproduk=$stok_global_toggle;
        $detail_toko->stok_minimal=$stok_minimal;
        // $detail_toko->fitur_stok_fifo=$kadaluwarsa;
        $detail_toko->fitur_kirim=$pengiriman;
        $detail_toko->maksimal_jarak=$maksimal_jarak;
        $detail_toko->interval_jarak=$interval_jarak;
        $detail_toko->ongkir_interval=$ongkir;
        // $detail_toko->fitur_batas_hutang=$batas_hutang;
        $detail_toko->fitur_pembatalan_pesanan=$maksimal_toggle;
        $detail_toko->maksimal_pembatalan=$waktu_maksimal;
        $detail_toko->save();
        return redirect('dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
