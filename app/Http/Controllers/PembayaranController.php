<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\NotaJual;
use App\Pelanggan;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PembayaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $username = Auth::user()->username;
        $pelanggan = Pelanggan::select('idpelanggan')
            ->where('username', '=', $username)
            ->get();
        $idpelanggan=$pelanggan[0]->idpelanggan;
        $nota_jual = NotaJual::select('nota_jual.*','pengiriman.ongkos_kirim')
                ->join('pengiriman', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                ->where('nota_jual.idpelanggan','=',$idpelanggan)
                ->where('nota_jual.keterangan','=', 'Belum Dibayar')
                ->orderBy('nota_jual.tanggal', 'desc')
                ->first();
        return view('customer.pembayaran',['nota_jual'=>$nota_jual]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $no_nota = $request->get('no_nota');
        if($request->hasFile('foto')){
            $this->validate($request, [
              'foto'  => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
             ]);
            $filefoto = $request->file('foto');
            $nama_foto = $no_nota;
            $ekstensi = $filefoto->getClientOriginalExtension();
            Storage::disk('public')->put($nama_foto.'.'.$ekstensi, File::get($filefoto));
        }
        $nota_jual = NotaJual::select('*')
            ->where('no_nota_jual','=',$no_nota)
            ->first();
        $nota_jual->bukti_transfer = $nama_foto.'.'.$ekstensi;
        $nota_jual->status_transaksi = 'Belum Divalidasi';
        $nota_jual->save();
        return redirect('/pesanan/diproses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nota_jual = NotaJual::select('nota_jual.*','pengiriman.ongkos_kirim')
                ->leftJoin('pengiriman', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                ->where('nota_jual.no_nota_jual','=',$id)
                ->first();
        return view('customer.pembayaran',['nota_jual'=>$nota_jual]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nota_jual = NotaJual::select('*')
                ->where('nota_jual.no_nota_jual','=',$id)
                ->first(); 
        return view('customer.pembayaran',['nota_jual'=>$nota_jual]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasFile('foto')){
            $this->validate($request, [
              'foto'  => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
             ]);
            $filefoto = $request->file('foto');
            $nama_foto = $no_nota;
            $ekstensi = $filefoto->getClientOriginalExtension();
            Storage::disk('public')->put($nama_foto.'.'.$ekstensi, File::get($filefoto));
        }
        $nota_jual = NotaJual::select('*')
            ->where('no_nota_jual','=',$no_nota)
            ->first();
        $nota_jual->bukti_transfer = $nama_foto.'.'.$ekstensi;
        $nota_jual->keterangan = 'Belum Divalidasi';
        $nota_jual->save();
        return redirect('/pembayaran/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
