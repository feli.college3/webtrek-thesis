<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Supplier;
use App\DetailToko;
use Validator;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }
    
    public function index()
    {
        $list_supplier = Supplier::all();
        return view('supplier.index-supplier', ['list_supplier'=>$list_supplier]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            return view('supplier.tambah-supplier');
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required | string',
            'alamat' => 'required | string',
            'telepon' => 'required | numeric',
        ]);
        
        if($validator->fails()){
            return redirect()
                ->action('SupplierController@create')
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $konsinyator=0;
            $detail_toko=DetailToko::select('*')->first();
            if($detail_toko->fitur_konsinyasi==1){
                $konsinyator=$request->get('konsinyator');
            }
            $sup = new Supplier([
            'nama_supplier' => $request->get('nama'),
            'alamat_supplier' => $request->get('alamat'),
            'telepon_supplier' => $request->get('telepon'),
            'konsinyator' => $konsinyator,
            'nomor_rekening' => $request->get('rekening'),
            'total_piutang' => 0,
            'status' => 'Aktif',
            ]);

            $sup->save();
            return redirect('/supplier');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $sup = Supplier::find($id);    
            return view('supplier.edit-supplier', ['sup'=>$sup, 'id'=>$id]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required | string',
            'alamat' => 'required | string',
            'telepon' => 'required | numeric',
            'status' => 'required | string',
        ]);
        
        if($validator->fails()){
            return redirect()
                ->action('SupplierController@edit', $id)
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $sup = Supplier::find($id);
            $konsinyator=0;
            $detail_toko=DetailToko::select('*')->first();
            if($detail_toko->fitur_konsinyasi==1){
                $konsinyator=$request->get('konsinyator');
            }
            $sup->nama_supplier = $request->get('nama');
            $sup->alamat_supplier = $request->get('alamat');
            $sup->telepon_supplier = $request->get('telepon');
            $sup->konsinyator = $konsinyator;
            $sup->nomor_rekening = $request->get('rekening');
            $sup->status = $request->get('status');

            $sup->save();
            return redirect('/supplier');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            Supplier::where('idsupplier',$id)->first()->delete();
            return redirect('/supplier')->with('status', 'Supplier terhapus');
        }
        else{
            return redirect()->back();
        }
    }
}
