<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Produk;
use App\StokProduk;
use App\ProdukKonsinyasi;
use App\PenyesuaianStok;
use App\DetailToko;
use DB;

class PenyesuaianStokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $produk = StokProduk::select('*')
            ->where('idproduk','=',$id)
            ->where('tanggal_kadaluwarsa','not like','2000-01-01%')
            ->get();
        $nama_produk=Produk::select('nama_produk')
            ->where('idproduk','=',$id)
            ->first();
        return view('penyesuaian-stok.tambah-penyesuaian',['id'=>$id,'produk'=>$produk,'nama_produk'=>$nama_produk]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $detail_toko=DetailToko::select('*')->first();
        date_default_timezone_set('Asia/Jakarta');
        $tanggal = date('Y-m-d H:i:s');

        $idproduk =$request->get('idproduk');

        if($detail_toko->fitur_stok_fifo==1){
            $tanggal_kadaluwarsa =$request->get('tanggal_kadaluwarsa');
        }
        else{
            $stok_produk = StokProduk::select('*')
                ->where('idproduk','=',$idproduk)
                ->get();
            $stok_produk=$stok_produk[0];
            $tanggal_kadaluwarsa=$stok_produk->tanggal_kadaluwarsa;
        }
        $jenis = $request->get('jenis');
        $jumlah = $request->get('jumlah');
        $keterangan = $request->get('keterangan');


        $penyesuaian = new PenyesuaianStok([
        'idproduk' => $idproduk,
        'tanggal_kadaluwarsa' => $tanggal_kadaluwarsa,
        'tanggal' => $tanggal,
        'jenis' => $jenis,
        'jumlah' => $jumlah,
        'keterangan' => $keterangan,
        'status' => 'Menunggu Validasi',
        ]);

        $penyesuaian->save();
        return redirect('/penyesuaian-stok/'.$idproduk);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $list_penyesuaian = PenyesuaianStok::select('*')
                   ->where('idproduk','=',$id)
                   ->where('status','=','Selesai')
                   ->orWhere('status','=','Menunggu Validasi')
                   ->get();
        $produk=Produk::find($id);
        return view('penyesuaian-stok.index-penyesuaian', ['list_penyesuaian'=>$list_penyesuaian, 'id'=>$id, 'produk'=>$produk]);
    }

    public function menunggu_validasi()
    {
        $list_penyesuaian = PenyesuaianStok::select('*')
            ->join('produk','produk.idproduk','=','penyesuaian_stok.idproduk')
            ->where('penyesuaian_stok.status','=','Menunggu Validasi')
            ->get();
        return view('penyesuaian-stok.validasi', ['list_penyesuaian'=>$list_penyesuaian]);
    }

    public function validasi(Request $request){
        // dd($request->all());
        $action = $request->input('btn-action');
        $detail_toko=DetailToko::select('*')->first();
        $list_penyesuaian = PenyesuaianStok::where('penyesuaian_stok.status','=','Menunggu Validasi')
            ->count();
        for($i=0;$i<$list_penyesuaian;$i++){
            if ($request->has('produk'.$i)){
                $idpenyesuaian=$request->input('produk'.$i);
                $penyesuaian = PenyesuaianStok::find($idpenyesuaian);
                if($action=='setuju'){
                    $penyesuaian->status='Selesai';
                    if($detail_toko->fitur_stok_fifo==1){
                        $stok_produk = StokProduk::select('*')
                            ->where('idproduk','=',$penyesuaian->idproduk)
                            ->where('tanggal_kadaluwarsa','=',$penyesuaian->tanggal_kadaluwarsa)
                            ->first();
                    }
                    else{
                        $stok_produk = StokProduk::select('*')
                            ->where('idproduk','=',$penyesuaian->idproduk)
                            ->first();
                    }
                    if($penyesuaian->jenis=='Masuk'){
                        $stok_produk->stok+=$penyesuaian->jumlah;
                    }
                    else if($penyesuaian->jenis=='Keluar'){
                        $stok_produk->stok-=$penyesuaian->jumlah;
                    }
                    $stok_produk->save();
                }
                else{
                    $penyesuaian->status='Ditolak';
                }
                $penyesuaian->save();
            }
        }
        return redirect()->back();
    }

    // public function tolak_validasi(Request $request){
    //     $detail_toko=DetailToko::select('*')->first();
    //     $list_penyesuaian = PenyesuaianStok::where('penyesuaian_stok.status','=','Menunggu Validasi')
    //         ->count();
    //     for($i=0;$i<$list_penyesuaian;$i++){
    //         if ($request->has('produk'.$i)){
    //             $idpenyesuaian=$request->input('produk'.$i);
    //             $penyesuaian = PenyesuaianStok::find($idpenyesuaian);
    //             $penyesuaian->status='Ditolak';
    //             $penyesuaian->save();
    //         }
    //     }
    //     return redirect()->back();
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
