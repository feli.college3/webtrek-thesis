<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pegawai;
use App\Jabatan;
use App\User;
use App\Role;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;
use Validator;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }
    
    public function index()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $list_pegawai = Pegawai::select('*')
               ->join('jabatan', 'jabatan.idjabatan', '=', 'pegawai.idjabatan')
               ->get();

            return view('pegawai.index-pegawai', ['list_pegawai'=>$list_pegawai]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $list_jabatan = Jabatan::all();
            return view('pegawai.tambah-pegawai', ['list_jabatan'=>$list_jabatan]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|string',
            'username' => 'required|unique:pegawai|string',
            'password' => 'required|min:6',
            'ulang_pass' => 'required|same:password',
        ]);
        
        if($validator->fails()){
            return redirect()
                ->action('PegawaiController@create')
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $nama = $request->get('nama');
            $uname = $request->get('username');
            $pass = $request->get('password');
            $idjab = $request->get('id_jabatan');
            $peg = new Pegawai([
            'nama_pegawai' => $nama,
            'username' => $uname,
            'password' => $pass,
            'idjabatan' => $idjab,
            'status' => 'Aktif',
            ]);

            $peg->save();

            
            $jabatan = Jabatan::select('nama_jabatan')
                ->where('idjabatan', '=', $idjab)
                ->first();
            $jabatan = $jabatan->nama_jabatan;
            
            if($jabatan=='Manajer'||$jabatan=='Kasir'||$jabatan=='Kurir'){
                $user = User::create([
                    'nama' => $nama,
                    'username' => $uname,
                    'password' => bcrypt($pass),
                ]);
                if($jabatan=='Manajer'){
                    $user->roles()
                         ->attach(Role::where('nama','manajer')->first());
                }
                else if($jabatan=='Kasir'){
                    $user->roles()
                         ->attach(Role::where('nama','kasir')->first());
                }
                else if($jabatan=='Kurir'){
                    $user->roles()
                         ->attach(Role::where('nama','kurir')->first());
                }
            }

            return redirect('/pegawai');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $peg = Pegawai::find($id);

            $idJab = DB::table('pegawai')
                ->where('idpegawai', '=', $id)
                ->value('idjabatan');
            
            $selectedJab = Jabatan::find($idJab);
            $list_jabatan = Jabatan::select()->get();
            return view('pegawai.edit-pegawai', compact('peg','selectedJab','list_jabatan','id'));
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required | string',
        ]);
        
        if($validator->fails()){
            return redirect()
                ->action('PegawaiController@edit', $id)
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $peg = Pegawai::find($id);
            $peg->nama_pegawai = $request->get('nama');
            $peg->username = $request->get('username');
            $peg->password = $request->get('password');
            $peg->idjabatan = $request->get('id_jabatan');
            $peg->status = $request->get('status');

            $peg->save();
            return redirect('/pegawai');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            Pegawai::where('idpegawai',$id)->first()->delete();
            return redirect('/pegawai')->with('status', 'Pegawai terhapus');
        }
        else{
            return redirect()->back();
        }
    }
}
