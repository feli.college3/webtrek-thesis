<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\HistoriPembayaranPembelian;
use App\Pegawai;
use App\NotaBeli;
use Validator;

class HistoriPembayaranPembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    public function create($id)
    {
        $nota = NotaBeli::find($id);
        return view('histori-pembayaran-pembelian.tambah-histori',['id'=>$id,'nota'=>$nota]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'jumlah' => 'required | numeric',
        ]);

        // if($validator->fails()){
        //     return redirect()
        //         ->action('HistoriPembayaranPembelianController@create')
        //         ->withErrors($validator)
        //         ->withInput();
        // }
        // else{
            $username = Auth::user()->username;
            $pegawai = Pegawai::select('idpegawai')
                ->where('username', '=', $username)
                ->first();
            date_default_timezone_set('Asia/Jakarta');
            $tanggal= date('dmY');
            $tanggal_jam = date('Y-m-d H:i:s');
            $no_nota =$request->get('no_nota');
            $jumlah = $request->get('jumlah');

            $histori = new HistoriPembayaranPembelian([
            'no_nota_beli' => $no_nota,
            'idpegawai' => $pegawai->idpegawai,
            'jumlah_dibayar' => $jumlah,
            'metode_pembayaran' => $request->get('metode'),
            'tanggal' => $tanggal_jam,
            ]);

            $histori->save();

            $nota = NotaBeli::find($no_nota);
            $nota->sisa_hutang-=$jumlah;

            if($nota->sisa_hutang<=0){
                $nota->status_pembayaran='Lunas';
            }
            $nota->save();
            return redirect('/histori-pembayaran-pembelian/'.$no_nota);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $list_histori = HistoriPembayaranPembelian::select('*')
                   ->join('pegawai', 'histori_pembayaran_pembelian.idpegawai', '=', 'pegawai.idpegawai')
                   ->where('no_nota_beli','=',$id)
                   ->get();
        $nota = NotaBeli::find($id);
        return view('histori-pembayaran-pembelian.index-histori', ['list_histori'=>$list_histori, 'id'=>$id,'nota'=>$nota]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
