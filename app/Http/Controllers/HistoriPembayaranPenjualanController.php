<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\HistoriPembayaranPenjualan;
use App\Pegawai;
use App\Pelanggan;
use App\NotaJual;
use Validator;

class HistoriPembayaranPenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    public function create($id)
    {
        // $list_pegawai = Pegawai::select('*')
        //                 ->join('jabatan', 'pegawai.idjabatan', '=', 'jabatan.idjabatan')
        //                ->where('jabatan.nama_jabatan','=','Manajer')
        //                ->orWhere('jabatan.nama_jabatan','=','Kasir')
        //                ->get();
        $nota = NotaJual::find($id);
        return view('histori-pembayaran-penjualan.tambah-histori',['id'=>$id,'nota'=>$nota]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'jumlah' => 'required | numeric',
        ]);

        // if($validator->fails()){
        //     return redirect()
        //         ->action('HistoriPembayaranPenjualanController@create')
        //         ->withErrors($validator)
        //         ->withInput();
        // }
        // else{
            $jumlah = $request->get('jumlah');
            $username = Auth::user()->username;
            $pegawai = Pegawai::select('idpegawai')
                ->where('username', '=', $username)
                ->first();
            date_default_timezone_set('Asia/Jakarta');
            $tanggal= date('dmY');
            $tanggal_jam = date('Y-m-d H:i:s');
            $no_nota =$request->get('no_nota');
            $histori = new HistoriPembayaranPenjualan([
            'no_nota_jual' => $no_nota,
            'idpegawai' => $pegawai->idpegawai,
            'jumlah_dibayar' => $jumlah,
            'metode_pembayaran' => $request->get('metode'),
            'tanggal' => $tanggal_jam,
            ]);
            $histori->save();

            $nota = NotaJual::find($no_nota);
            $nota->sisa_piutang-=$jumlah;

            if($nota->sisa_piutang<=0){
                $nota->status_pembayaran='Lunas';
                $nota->status_transaksi='Selesai';
            }
            $nota->status_transaksi='Pembayaran';
            $nota->save();

            //tambah piutang
            $nota_jual=NotaJual::find($no_nota);
            $plg=Pelanggan::find($nota_jual->idpelanggan);
            $plg->total_hutang-=$jumlah;
            $plg->save();
            return redirect('/histori-pembayaran-penjualan/'.$no_nota);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $list_histori = HistoriPembayaranPenjualan::select('*')
                   ->join('pegawai', 'histori_pembayaran_penjualan.idpegawai', '=', 'pegawai.idpegawai')
                   ->where('no_nota_jual','=',$id)
                   ->get();
        $nota = NotaJual::find($id);
        return view('histori-pembayaran-penjualan.index-histori', ['list_histori'=>$list_histori, 'id'=>$id,'nota'=>$nota]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
