<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\DetailToko;
use App\NotaJual;
use App\DetailJualKonsinyasi;
use App\NotaBeliKonsinyasi;
use App\DetailBeliKonsinyasi;
use App\NotaKembaliKonsinyasi;
use App\ProdukKonsinyasi;
use PDF;

class LaporanKonsinyasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }
    
    public function index()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
            $tahun = NotaBeliKonsinyasi::selectRaw('substr(tanggal_diterima,1,4) as tahun')->pluck('tahun')->unique();
            return view('laporan.index-konsinyasi', ['bulan'=>$bulan,'tahun'=>$tahun]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_nota($bulan,$tahun){
        $bulan_index=$bulan;
        $bulanmin=$bulan;
        $bulanplus=$bulan+1;
        $tahunmin=$tahun;
        $tahunplus=$tahun;
        if($bulan==12){
            $bulanplus=1;
            $tahunplus++;
        }
        if($bulan<10){
            $bulan='0'.$bulan;
        }
        if($bulanmin<10){
            $bulanmin='0'.$bulanmin;
        }
        if($bulanplus<10){
            $bulanplus='0'.$bulanplus;
        }
        $from = date($tahunmin.$bulanmin.'01');
        $to = date($tahunplus.$bulanplus.'01');

        $nota_kembali = NotaKembaliKonsinyasi::select('produk_konsinyasi.idproduk','produk_konsinyasi.nama_produk','nota_beli_konsinyasi.tanggal_diterima as tanggal_diterima','nota_kembali_konsinyasi.tanggal as tanggal_kembali','detail_nota_kembali_konsinyasi.jumlah_terjual','detail_nota_kembali_konsinyasi.jumlah_kembali')
            ->join('detail_nota_kembali_konsinyasi', 'nota_kembali_konsinyasi.no_nota_kembali', '=', 'detail_nota_kembali_konsinyasi.no_nota_kembali')
            ->join('produk_konsinyasi', 'detail_nota_kembali_konsinyasi.idproduk_konsinyasi', '=', 'produk_konsinyasi.idproduk')
            ->join('nota_beli_konsinyasi', 'nota_kembali_konsinyasi.no_nota_beli_konsinyasi', '=', 'nota_beli_konsinyasi.no_nota_beli_konsinyasi')
            ->whereBetween('nota_kembali_konsinyasi.tanggal', [$from, $to])
            ->orderBy('produk_konsinyasi.idproduk')
            ->get();

        // $nota_beli = NotaBeliKonsinyasi::select('produk_konsinyasi.idproduk','produk_konsinyasi.nama_produk','detail_jual_konsinyasi.harga_jual',DB::raw('sum(detail_jual_konsinyasi.jumlah) as jumlah_jual'))
        //     ->join('detail_beli_konsinyasi', 'nota_beli_konsinyasi.no_nota_beli_konsinyasi', '=', 'detail_beli_konsinyasi.no_nota_beli_konsinyasi')
        //     ->join('produk_konsinyasi', 'detail_beli_konsinyasi.idproduk_konsinyasi', '=', 'produk_konsinyasi.idproduk')
        //     ->whereBetween('nota_jual.tanggal', [$from, $to])
        //     ->groupBy('produk_konsinyasi.idproduk')
        //     ->groupBy('detail_jual_konsinyasi.harga_jual')
        //     ->get();
        $kembali=array();
        foreach($nota_kembali as $nota){
            $harga_beli=DetailBeliKonsinyasi::select('detail_beli_konsinyasi.harga_beli')
                ->join('nota_beli_konsinyasi', 'detail_beli_konsinyasi.no_nota_beli_konsinyasi', '=', 'nota_beli_konsinyasi.no_nota_beli_konsinyasi')
                ->where('nota_beli_konsinyasi.tanggal_diterima','=',$nota->tanggal_diterima)
                ->where('detail_beli_konsinyasi.idproduk_konsinyasi','=',$nota->idproduk)
                ->first();
            $from=$nota->tanggal_diterima;
            $to=$nota->tanggal_kembali;
            $harga_jual=DetailJualKonsinyasi::select('detail_jual_konsinyasi.harga_jual')
                ->join('nota_jual', 'detail_jual_konsinyasi.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                ->whereBetween('nota_jual.tanggal', [$from, $to])
                ->where('detail_jual_konsinyasi.idproduk_konsinyasi','=',$nota->idproduk)
                ->first();
            $harga_jual_lain=DetailJualKonsinyasi::select('detail_jual_konsinyasi.harga_jual')
                ->join('nota_jual', 'detail_jual_konsinyasi.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                ->where('detail_jual_konsinyasi.idproduk_konsinyasi','=',$nota->idproduk)
                ->first();
            if($harga_jual==null){
                $harga_jual=$harga_jual_lain;
            }
            $tampung=array();
            $tampung['idproduk']=$nota->idproduk;
            $tampung['nama_produk']=$nota->nama_produk;
            $tampung['tanggal_diterima']=$nota->tanggal_diterima;
            $tampung['tanggal_kembali']=$nota->tanggal_kembali;
            $tampung['jumlah_beli']=$nota->jumlah_terjual+$nota->jumlah_kembali;
            $tampung['jumlah_terjual']=$nota->jumlah_terjual;
            $tampung['harga_beli']=$harga_beli->harga_beli;
            $tampung['harga_jual']=$harga_jual->harga_jual;
            $tampung['total_beli']=$tampung['jumlah_beli']*$tampung['harga_beli'];
            $tampung['total_jual']=$tampung['jumlah_terjual']*$tampung['harga_jual'];
            $tampung['keuntungan']=$tampung['total_jual']-$tampung['total_beli'];
            array_push($kembali, $tampung);
        }
        return $kembali;
    }

    public function get_keuntungan($list){
        $keuntungan=0;
        foreach($list as $row){
            $keuntungan+=$row['keuntungan'];
        }
        return $keuntungan;
    }

    public function store(Request $request)
    {
        $detail_toko=DetailToko::select('*')->first();
        $bulan=$request->get('bulan');
        $tahun=$request->get('tahun');
        $list_nota=$this->get_nota($bulan,$tahun);
        $keuntungan=$this->get_keuntungan($list_nota);
        
        $list_bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        return view('laporan.laporan-konsinyasi', ['list_nota'=>$list_nota,'list_bulan'=>$list_bulan,'tahun'=>$tahun,'bulan'=>$bulan,'keuntungan'=>$keuntungan]);
    }

    public function cetak($tahun,$bulan)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $detail_toko=DetailToko::select('*')->first();
            $list_nota=$this->get_nota($bulan,$tahun);
            $keuntungan=$this->get_keuntungan($list_nota);
            $list_bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
            $pdf = PDF::loadview('laporan.cetak-konsinyasi',['list_nota'=>$list_nota,'list_bulan'=>$list_bulan,'tahun'=>$tahun,'bulan'=>$bulan,'keuntungan'=>$keuntungan])->setPaper('a4', 'landscape');;

            return $pdf->stream();
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
