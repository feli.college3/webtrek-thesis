<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Alert;
use App\DetailToko;
use App\NotaBeli;
use App\DetailNotaBeli;
use App\StokProduk;
use App\Supplier;
use App\Pegawai;
use App\Produk;
use App\ProdukKonsinyasi;
use Validator;

class NotaBeliController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }

    public function index()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $list_nota_beli = NotaBeli::select('nota_beli.*', 'supplier.idsupplier', 'supplier.nama_supplier', 'pegawai.idpegawai', 'pegawai.nama_pegawai')
                        ->join('supplier', 'supplier.idsupplier', '=', 'nota_beli.idsupplier')
                        ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_beli.idpegawai')
                        ->orderBy('nota_beli.tanggal_dipesan', 'desc')
                        ->get();
            return view('nota-beli.index-nota-beli', ['list_nota_beli'=>$list_nota_beli]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $list_supplier = Supplier::select('*')
                ->where('konsinyator','=',0)
                ->get();
            $list_pegawai = Pegawai::all();
            $list_produk = Produk::orderBy('produk.nama_produk')->get();
            date_default_timezone_set('Asia/Jakarta');
            $tanggal= date('dmy');

            $total_trans = NotaBeli::where('nota_beli.no_nota_beli','like', $tanggal.'%')
                         ->count();
            $id_baru=$total_trans+1;
            if($total_trans < 10){
                $id_baru = $tanggal."B00".$id_baru;
            }
            else if($total_trans >= 10){
                $id_baru = $tanggal."B0".$id_baru;
            }
            else if($total_trans >= 100){
                $id_baru = $tanggal."B".$id_baru;
            }
            return view('nota-beli.tambah-nota-beli', ['list_supplier'=>$list_supplier, 'list_pegawai'=>$list_pegawai, 'list_produk'=>$list_produk, 'id_baru'=>$id_baru]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function get_detail_produk($produk)
    {
        $id=Produk::where('nama_produk','=',$produk)->first();
        $id=$id->idproduk;
        $harga_beli=DetailNotaBeli::select('harga_beli')
            ->join('nota_beli', 'detail_nota_beli.no_nota_beli', '=', 'nota_beli.no_nota_beli')
            ->where('detail_nota_beli.idproduk','=',$id)
            ->orderBy('nota_beli.tanggal_diterima','desc')
            ->first();
        if($harga_beli==null){
            $harga_beli=0;
        }
        else{
            $harga_beli=$harga_beli->harga_beli;
        }
        $total_dipesan=DetailNotaBeli::select(DB::raw('sum(detail_nota_beli.jumlah) as total_beli'))
            ->join('nota_beli', 'detail_nota_beli.no_nota_beli', '=', 'nota_beli.no_nota_beli')
            ->where('nota_beli.status_transaksi','Dipesan')
            ->where('detail_nota_beli.idproduk','=',$id)
            ->first();
        if($total_dipesan->total_beli==null){
            $total_dipesan=0;
        }
        else{
            $total_dipesan=$total_dipesan->total_beli;
        }
        $detail_produk=$total_dipesan.','.$harga_beli;
        return json_encode($detail_produk);
    }

    public function cek_dobel_nota($total_data)
    {
        $nota_dipesan=NotaBeli::select('produk.nama_produk')
                    ->join('detail_nota_beli', 'detail_nota_beli.no_nota_beli', '=', 'nota_beli.no_nota_beli')
                    ->join('produk', 'produk.idproduk', '=', 'detail_nota_beli.idproduk')
                    ->where('nota_beli.status_transaksi','=','Dipesan')
                    ->get();
        $sama=array();
        foreach($nota_dipesan as $nota_lama){
            for($i=1;$i<=$total_data;$i++){
                $produk=$request->get('produk'.$i);
                if($nota_lama->nama_produk==$produk){
                    array_push($sama, $produk);
                }
            }
        }
        if(count($sama)>0){
            dd($sama);
            return $sama;
        }
        else{
            return 0;
        }
    }

    public function store(Request $request)
    {
        // return redirect()->back()->with('alert', 'Deleted!');
        $detail_toko=DetailToko::select('*')->first();
        $aksi=$request->input('action');
        // if($aksi=='cek'){
        //     return 'cek';
        // }
        // else if($aksi=='simpan'){
        //     return 'simpan';
        // }
        $total_data = $request->get('total_data');
        if($aksi=='cek_dobel'){
            $nota_dipesan=NotaBeli::select('produk.nama_produk')
                    ->join('detail_nota_beli', 'detail_nota_beli.no_nota_beli', '=', 'nota_beli.no_nota_beli')
                    ->join('produk', 'produk.idproduk', '=', 'detail_nota_beli.idproduk')
                    ->where('nota_beli.status_transaksi','=','Dipesan')
                    ->get();
            $sama=array();
            foreach($nota_dipesan as $nota_lama){
                for($i=1;$i<=$total_data;$i++){
                    $produk=$request->get('produk'.$i);
                    if($nota_lama->nama_produk==$produk){
                        array_push($sama, $produk);
                        break;
                    }
                }
            }
            $sama=array_unique($sama);
            $produk_kembar="";
            foreach($sama as $s){
                if(count($sama)==1){
                    $produk_kembar=$produk_kembar.$s;
                }
                else{
                    $produk_kembar=$produk_kembar.$s.", ";
                }
            }
            if(count($sama)>0){
                $text=$produk_kembar." telah dipesan.";
                return redirect()->back()->with('alert', $text)->withInput();
            }
            else{
                return redirect()->back()->with('alert', 'Semua produk belum dipesan sebelumnya')->withInput();
            }
        }
        else if($aksi=='simpan'){
            $grand_total=$request->get('grand_total');

            date_default_timezone_set('Asia/Jakarta');
            $tanggal= date('dmY');
            $tanggaljam = date('Y-m-d H:i:s');

            $nomor_nota = $request->get('no_nota');
            $keterangan="-";
            if($request->get('keterangan')!=""){
                $keterangan=$request->get('keterangan');
            }

            $username = Auth::user()->username;
            $pegawai = Pegawai::select('*')
                ->where('username', '=', $username)
                ->first();

            $status='Belum Lunas';
            // if($detail_toko->pembayaran_hutang==1){
            //     $status=$request->get('status');
            // }
            $sisa_hutang=$grand_total;
            $tanggal_jatuh_tempo=$tanggaljam;
            // if($status=='Belum Lunas'){
            //     $sisa_hutang=$grand_total;
            //     $tanggal_jatuh_tempo = $request->get('jatuh_tempo');
            // }
            $nota_beli = new NotaBeli([
                'no_nota_beli' => $nomor_nota,
                'idsupplier' => $request->get('supplier'),
                'idpegawai' => $pegawai->idpegawai,
                'tanggal_dipesan' => $tanggaljam,
                'tanggal_diterima' => $tanggaljam,
                'grand_total' => $grand_total,
                'sisa_hutang' => $sisa_hutang,
                'metode_pembayaran' => $request->get('metode_pembayaran'),
                'status_pembayaran' => $status,
                'tanggal_jatuh_tempo' => $tanggal_jatuh_tempo,
                'keterangan' => $keterangan,
                'status_transaksi' => 'Dipesan',
            ]);

            $nota_beli->save();

            ////////////////////DETAIL NOTA BELI////////////////////
            $produk = Produk::all();
            $idproduk_terpilih = "";
            for($i=1;$i<=$total_data;$i++)
            {
                foreach($produk as $prod)
                {
                    if($prod->nama_produk == $request->get('produk'.$i)){
                        $idproduk_terpilih = $prod->idproduk;
                        $kadaluwarsa='2000-01-01 00:00:00';
                        $harga = $request->get('harga'.$i);
                        $jumlah = $request->get('jumlah'.$i);
                        $detail_nota_beli = new DetailNotaBeli([
                            'no_nota_beli' => $nomor_nota,
                            'idproduk' => $idproduk_terpilih,
                            'tanggal_kadaluwarsa' => $kadaluwarsa,
                            'harga_beli' => (float)$harga,
                            'jumlah' => (float)$jumlah,
                        ]);
                        $detail_nota_beli->save();
                    }
                }
            }
            return redirect('/nota-beli/'.$nomor_nota);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $table='detail_nota_beli';
            $kategori='Dipesan';
            $nota_beli=NotaBeli::find($id);
            if($nota_beli->status_transaksi!='Dipesan'){
                $kategori=$nota_beli->status_transaksi;
            }
            $produk_toko = DetailNotaBeli::select('detail_nota_beli.*', 'produk.nama_produk','stok_produk.lokasi')
                ->join('produk', 'produk.idproduk', '=', 'detail_nota_beli.idproduk')
                ->join('stok_produk', function($join) use ($table)
                {
                    $join->on($table.'.idproduk', '=',  'stok_produk.idproduk');
                    $join->on($table.'.tanggal_kadaluwarsa','=', 'stok_produk.tanggal_kadaluwarsa');
                })
                ->where('detail_nota_beli.no_nota_beli', '=', $id)
                ->get();
            // $produk_konsinyasi = DetailNotaBeli::select('detail_nota_beli.*', 'produk_konsinyasi.nama_produk')
            //     ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'detail_nota_beli.idproduk')
            //     ->where('detail_nota_beli.no_nota_beli', '=', $id)
            //     ->get();
            return view('nota-beli.detail-nota-beli', ['produk_toko'=>$produk_toko, 'id'=>$id, 'kategori'=>$kategori]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function index_postorder()
    {
        $list_nota_beli = NotaBeli::select('nota_beli.*', 'supplier.idsupplier', 'supplier.nama_supplier', 'pegawai.idpegawai', 'pegawai.nama_pegawai')
                    ->join('supplier', 'supplier.idsupplier', '=', 'nota_beli.idsupplier')
                    ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_beli.idpegawai')
                    ->orderBy('nota_beli.tanggal_dipesan', 'asc')
                    ->where('nota_beli.status_transaksi','=','Dipesan')
                    ->get();
        return view('nota-beli.nota-beli-dipesan', ['list_nota_beli'=>$list_nota_beli]);
    }

    public function edit($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $nota_beli=NotaBeli::join('supplier', 'supplier.idsupplier', '=', 'nota_beli.idsupplier')
                ->where('nota_beli.no_nota_beli','=',$id)
                ->first();
            $list_produk=DetailNotaBeli::select('produk.nama_produk','detail_nota_beli.idproduk','detail_nota_beli.harga_beli','detail_nota_beli.jumlah')
                ->join('produk', 'produk.idproduk', '=', 'detail_nota_beli.idproduk')
                ->where('detail_nota_beli.no_nota_beli','=',$id)
                ->get();
            $total_data=count($list_produk);
            $produk_toko=Produk::where('status','=','Dijual')->get();
            return view('nota-beli.edit-nota-beli', ['nota_beli'=>$nota_beli,'list_produk'=>$list_produk,'total_data'=>$total_data,'produk_toko'=>$produk_toko,'id'=>$id]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    protected $primaryKey = ['no_nota_beli','idproduk','tanggal_kadaluwarsa'];
    public $incrementing = false;
    public function update(Request $request, $id)
    {
        $total_data = $request->get('total_data');
        $detail_toko=DetailToko::select('*')->first();
        $grand_total=$request->get('grand_total');
        date_default_timezone_set('Asia/Jakarta');
        $tanggal= date('dmY');
        $tanggaljam = date('Y-m-d H:i:s');
        $username = Auth::user()->username;
        $pegawai = Pegawai::select('*')
            ->where('username', '=', $username)
            ->first();
        $status='Lunas';
        if($detail_toko->pembayaran_hutang==1){
            $status=$request->get('status');
        }
        $nota_beli=NotaBeli::find($id);
        $nota_beli->grand_total=$grand_total;
        $nota_beli->tanggal_diterima=$tanggaljam;
        $nota_beli->idpegawai=$pegawai->idpegawai;
        $nota_beli->metode_pembayaran=$request->get('metode_pembayaran');
        $nota_beli->status_pembayaran=$status;
        $sisa_hutang=0;
        $tanggal_jatuh_tempo=$tanggaljam;
        if($status=='Belum Lunas'){
            $sisa_hutang=$grand_total;
            $tanggal_jatuh_tempo = $request->get('jatuh_tempo');
            $nota_beli->tanggal_jatuh_tempo=$tanggal_jatuh_tempo;
        }
        $nota_beli->sisa_hutang=$sisa_hutang;
        $nota_beli->keterangan=$request->get('keterangan');
        $nota_beli->status_transaksi='Sudah Datang';
        $nota_beli->save();
        
        //hapus detail beli lama
        $detail_lama=DetailNotaBeli::select('*')->where('no_nota_beli','=',$id)->get();
        foreach($detail_lama as $detil){
            $detil->delete();
        }
        // ////////////////////DETAIL NOTA BELi////////////////////
        $produk = Produk::where('status','=','Dijual')->get();
        for($i=1;$i<=$total_data;$i++)
        {
            foreach($produk as $prod)
            {
                if($prod->idproduk == $request->get('produk'.$i)){
                    $tanggal_kadaluwarsa_baru='2000-01-01 00:00:00';
                    if($detail_toko->fitur_stok_fifo==1){
                        $tanggal_kadaluwarsa_baru = $request->get('tanggal_kadaluwarsa'.$i);
                        $tanggal_kadaluwarsa_baru = date('Y-m-d H:i:s', strtotime($tanggal_kadaluwarsa_baru));
                    }
                    // dd($tanggal_kadaluwarsa_baru);
                    $harga = $request->get('harga'.$i);
                    $jumlah = $request->get('jumlah'.$i);

                    $lokasi='-';
                    if($detail_toko->fitur_stok_fifo==1){
                        $lokasi = $request->get('lokasi'.$i);
                    }

                    //tambah stok
                    $semua_stok_lama = StokProduk::select('*')
                        ->where('idproduk','=',$prod->idproduk)
                        ->get();
                    if(count($semua_stok_lama)>0){
                        for($j=0;$j<count($semua_stok_lama);$j++){
                            if($semua_stok_lama[$j]->idproduk==$prod->idproduk&&$semua_stok_lama[$j]->tanggal_kadaluwarsa==$tanggal_kadaluwarsa_baru){
                                $semua_stok_lama[$j]->stok+=$jumlah;
                                $semua_stok_lama[$j]->save();
                                break;
                            }
                            else{
                                if($j==count($semua_stok_lama)-1){
                                    $stok_produk = new StokProduk([
                                    'idproduk' => $prod->idproduk,
                                    'tanggal_kadaluwarsa' => $tanggal_kadaluwarsa_baru,
                                    'stok' => $jumlah,
                                    'lokasi' => $lokasi,
                                    ]);
                                    $stok_produk->save();
                                }
                            }
                        }
                    }
                    else{
                        $stok_produk = new StokProduk([
                            'idproduk' => $prod->idproduk,
                            'tanggal_kadaluwarsa' => $tanggal_kadaluwarsa_baru,
                            'stok' => $jumlah,
                            'lokasi' => $lokasi,
                        ]);
                        $stok_produk->save();
                    }

                    //detail beli
                    $detail_nota_beli = new DetailNotaBeli([
                        'no_nota_beli' => $id,
                        'idproduk' => $prod->idproduk,
                        'tanggal_kadaluwarsa' => $tanggal_kadaluwarsa_baru,
                        'harga_beli' => (float)$harga,
                        'jumlah' => (float)$jumlah,
                    ]);
                    $detail_nota_beli->save();


                    //update HPP dan harga jual
                    $harga_beli=DetailNotaBeli::select(DB::raw('avg(harga_beli) as harga_beli'))
                        ->where('idproduk','=',$prod->idproduk)
                        ->first();
                    $harga_beli=ceil($harga_beli->harga_beli);
                    $harga_jual_eceran=1.1*$harga_beli;
                    $harga_jual_grosir=1.08*$harga_beli;
                    $produk_terpilih=Produk::find($prod->idproduk);
                    $produk_terpilih->hpp=$harga_beli;
                    $produk_terpilih->harga_jual_eceran=$harga_jual_eceran;
                    $produk_terpilih->harga_jual_grosir=$harga_jual_grosir;
                    $produk_terpilih->save();
                }
            }
        }
        return redirect('/nota-beli/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
