<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use PDF;
use App\DetailToko;
use App\NotaBeli;
use App\NotaJual;
use App\DetailJualKonsinyasi;
use App\NotaBeliKonsinyasi;
use App\DetailBeliKonsinyasi;
use App\NotaKembaliKonsinyasi;
use App\ProdukKonsinyasi;
use App\DetailNotaBeli;
use App\DetailNotaJual;
use App\StokProduk;
use App\PenyesuaianStok;
use App\Produk;
use App\Http\Controllers\LaporanProdukController;

class LaporanLabaRugiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }

    public function index()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
            $tahun = NotaBeli::selectRaw('substr(tanggal_dipesan,1,4) as tahun')->pluck('tahun')->unique();
            return view('laporan.index-labarugi', ['bulan'=>$bulan,'tahun'=>$tahun]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function get_konsinyasi($list){
        $list_konsi=array();
        if(count($list)>0){
            $tampung=array();
            $tampung['idproduk']=$list[0]['idproduk'];
            $tampung['nama_produk']=$list[0]['nama_produk'];
            $tampung['jumlah_beli']=$list[0]['jumlah_beli'];
            $tampung['jumlah_jual']=$list[0]['jumlah_terjual'];
            $tampung['total_beli']=$list[0]['total_beli'];
            $tampung['total_jual']=$list[0]['total_jual'];
            $tampung['keuntungan']=$list[0]['keuntungan'];
            array_push($list_konsi, $tampung);
            for($i=1;$i<count($list);$i++){
                for($j=0;$j<count($list_konsi);$j++){
                    $jumlahbeli=0;
                    if($list[$i]['idproduk']==$list_konsi[$j]['idproduk']){
                        $jumlahbeli=$list[$i]['jumlah_beli'];
                        $list_konsi[$j]['jumlah_beli']=$list_konsi[$j]['jumlah_beli']+$jumlahbeli;
                        $list_konsi[$j]['jumlah_jual']+=$list[$i]['jumlah_terjual'];
                        $list_konsi[$j]['total_beli']+=$list[$i]['total_beli'];
                        $list_konsi[$j]['total_jual']+=$list[$i]['total_jual'];
                        $list_konsi[$j]['keuntungan']+=$list[$i]['keuntungan'];
                        break;
                    }
                    else{
                        if($j==count($list_konsi)-1){
                            $tampung=array();
                            $tampung['idproduk']=$list[$i]['idproduk'];
                            $tampung['nama_produk']=$list[$i]['nama_produk'];
                            $tampung['jumlah_beli']=$list[$i]['jumlah_beli'];
                            $tampung['jumlah_jual']=$list[$i]['jumlah_terjual'];
                            $tampung['total_beli']=$list[$i]['total_beli'];
                            $tampung['total_jual']=$list[$i]['total_jual'];
                            $tampung['keuntungan']=$list[$i]['keuntungan'];
                            array_push($list_konsi, $tampung);
                            break;
                        }
                    }
                }
            }
        }
        return $list_konsi;
    }

    public function get_total_pembelian($list){
        $total=0;
        foreach($list as $produk){
            $total+=$produk['subtotal_beli'];
        }
        return $total;
    }

    public function get_total_penjualan($list){
        $total=0;
        foreach($list as $produk){
            $total+=$produk['subtotal_jual'];
        }
        return $total;
    }

    public function get_keuntungan($list){
        $total=0;
        foreach($list as $produk){
            $total+=$produk['keuntungan'];
        }
        return $total;
    }

    public function get_semua_nota($bulan,$tahun){
        $list_jual=app('App\Http\Controllers\LaporanProdukController')->get_nota_jual($bulan,$tahun);
        $list_beli=app('App\Http\Controllers\LaporanProdukController')->get_nota_beli($bulan,$tahun,'LabaRugi');
        $penyesuaian=app('App\Http\Controllers\LaporanProdukController')->get_penyesuaian($bulan,$tahun);
        $konsinyasi=app('App\Http\Controllers\LaporanKonsinyasiController')->get_nota($bulan,$tahun);
        $list_konsi=$this->get_konsinyasi($konsinyasi);
        $all_list=array();
        for($i=0;$i<count($list_beli);$i++){
            $tampung=array();
            $tampung['idproduk']=$list_beli[$i]['idproduk'];
            $tampung['nama_produk']=$list_beli[$i]['nama_produk'];
            $tampung['jumlah_beli']=$list_beli[$i]['jumlah'];
            $tampung['subtotal_beli']=$list_beli[$i]['subtotal'];
            $tampung['jumlah_jual']=$list_jual[$i]['jumlah'];
            $tampung['subtotal_jual']=$list_jual[$i]['subtotal'];
            $tampung['jumlah_masuk']=$penyesuaian[$i]['jumlah_masuk'];
            $tampung['subtotal_masuk']=$penyesuaian[$i]['subtotal_masuk'];
            $tampung['jumlah_keluar']=$penyesuaian[$i]['jumlah_keluar'];
            $tampung['subtotal_keluar']=$penyesuaian[$i]['subtotal_keluar'];
            $tampung['keuntungan']=$tampung['subtotal_jual']+$tampung['subtotal_masuk']-$tampung['subtotal_beli']+$tampung['subtotal_keluar'];
            array_push($all_list, $tampung);
        }
        foreach($list_konsi as $konsi){
            $tampung=array();
            $tampung['idproduk']=$konsi['idproduk'];
            $tampung['nama_produk']=$konsi['nama_produk'];
            $tampung['jumlah_beli']=$konsi['jumlah_beli'];
            $tampung['subtotal_beli']=$konsi['total_beli'];
            $tampung['jumlah_jual']=$konsi['jumlah_jual'];
            $tampung['subtotal_jual']=$konsi['total_jual'];
            $tampung['jumlah_masuk']=0;
            $tampung['subtotal_masuk']=0;
            $tampung['jumlah_keluar']=0;
            $tampung['subtotal_keluar']=0;
            $tampung['keuntungan']=$konsi['keuntungan'];
            array_push($all_list, $tampung);
        }
        return $all_list;
    }

    public function store(Request $request)
    {
        $detail_toko=DetailToko::select('*')->first();
        $bulan=$request->get('bulan');
        $tahun=$request->get('tahun');
        $all_list=$this->get_semua_nota($bulan,$tahun);
        $total_pembelian=$this->get_total_pembelian($all_list);
        $total_penjualan=$this->get_total_penjualan($all_list);
        $keuntungan=$this->get_keuntungan($all_list);
        $list_bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        return view('laporan.laporan-labarugi', ['all_list'=>$all_list,'total_pembelian'=>$total_pembelian,'total_penjualan'=>$total_penjualan,'keuntungan'=>$keuntungan,'list_bulan'=>$list_bulan,'tahun'=>$tahun,'bulan'=>$bulan]);
    }

    public function cetak($tahun,$bulan)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $detail_toko=DetailToko::select('*')->first();
            $all_list=$this->get_semua_nota($bulan,$tahun);
            $total_pembelian=$this->get_total_pembelian($all_list);
            $total_penjualan=$this->get_total_penjualan($all_list);
            $keuntungan=$this->get_keuntungan($all_list);
            $list_bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
            $pdf = PDF::loadview('laporan.cetak-labarugi',['all_list'=>$all_list,'total_pembelian'=>$total_pembelian,'total_penjualan'=>$total_penjualan,'keuntungan'=>$keuntungan,'list_bulan'=>$list_bulan,'tahun'=>$tahun,'bulan'=>$bulan])->setPaper('a4', 'landscape');
            return $pdf->stream();
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
