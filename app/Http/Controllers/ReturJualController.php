<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ReturJual;
use App\DetailReturJual;
use App\Pelanggan;
use App\Pegawai;
use App\NotaJual;
use App\Produk;
use App\StokProduk;
use DB;

class ReturJualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }
    
    public function index()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $list_retur = ReturJual::select('retur_penjualan.*', 'pelanggan.nama_pelanggan', 'pegawai.nama_pegawai')
                        ->join('nota_jual', 'nota_jual.no_nota_jual', '=', 'retur_penjualan.no_nota_jual')
                        ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
                        ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_jual.idpegawai_kasir')
                        ->orderBy('retur_penjualan.tanggal', 'desc')
                        ->get();
            return view('retur-jual.index-retur-jual', ['list_retur'=>$list_retur]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function get_nota_jual($id)
    {
        $nota_jual = NotaJual::select('no_nota_jual')
                ->where('idpelanggan',$id)
                ->get();
        $nota_array=array();
        foreach($nota_jual as $nota){
            array_push($nota_array, $nota->no_nota_jual);
        }
        return json_encode($nota_array);
    }

    public function get_produk_retur($id)
    {
        $produk = NotaJual::select('produk.idproduk','produk.nama_produk','detail_nota_jual.harga_jual','detail_nota_jual.jumlah')
                ->join('detail_nota_jual', 'detail_nota_jual.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                ->join('produk', 'produk.idproduk', '=', 'detail_nota_jual.idproduk')
                ->where('nota_jual.no_nota_jual',$id)
                ->get();
        $produk_array=array();
        foreach($produk as $prod){
            // $row=array();
            // $row['id']=$prod->idproduk;
            // $row['nama']=$prod->nama_produk;
            // $produk_array[]=$row;
            $tampung=array();
            $tampung['id']=$prod->idproduk;
            $tampung['nama']=$prod->nama_produk;
            $tampung['harga_jual']=$prod->harga_jual;
            $tampung['jumlah']=$prod->jumlah;
            array_push($produk_array, $tampung);
        }
        return json_encode($produk_array);
    }

    public function create()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $list_pelanggan = Pelanggan::all();
            $list_nota_jual = NotaJual::all();
            $list_produk = Produk::all();
            return view('retur-jual.tambah-retur', ['list_pelanggan'=>$list_pelanggan,'list_nota_jual'=>$list_nota_jual,'list_produk'=>$list_produk]);
        }
        else{
            return redirect()->back();
        }
    }

    // public function fetch(Request $request)
    // {
    //     $select = $request->get('select');
    //     $value = $request->get('value');
    //     $dependent = $request->get('dependent');
    //     $data = NotaJual::select('*')
    //             ->where($select, $value)
    //             ->groupBy($dependent)
    //             ->get();
    //         $output = '<option value="">Pilih '.ucfirst($dependent).'</option>';
    //     foreach($data as $row){
    //         $output .= '<option value="'.$row->dependent.'">'.$row->$dependent.'</option>';
    //     }
    //     echo $output;
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $prodzz = $request->all();
        // dd($prodzz);
        $username = Auth::user()->username;
        $pegawai = Pegawai::select('idpegawai')
            ->where('username', '=', $username)
            ->first();
        $no_nota = $request->get('no_nota');
        $jenis_retur=$request->get('jenis_retur');

        date_default_timezone_set('Asia/Jakarta');
        $tanggal= date('dmy');
        $tanggaljam = date('Y-m-d H:i:s');

        $retur = new ReturJual([
            'no_nota_jual' => $no_nota,
            'idpegawai' => $pegawai->idpegawai,
            'tanggal' => $tanggaljam,
            'deskripsi' => $request->get('deskripsi'),
            'grand_total' => $request->get('grand_total'),
            'jenis_retur' => $jenis_retur,
        ]);
        $retur->save();


        //detail retur jual
        $nota = NotaJual::select('produk.nama_produk','detail_nota_jual.*')
                ->join('detail_nota_jual', 'detail_nota_jual.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                ->join('produk', 'produk.idproduk', '=', 'detail_nota_jual.idproduk')
                ->where('nota_jual.no_nota_jual',$no_nota)
                ->get();
        $tanggal_lama=array();
        foreach($nota as $note){
            array_push($tanggal_lama, $note->tanggal_kadaluwarsa);
        }
        $jumlah_produk = count($nota);
        $produk_toko_diretur=array();
        $tanggal_kadaluwarsa_lama=array();
        $tanggal_kadaluwarsa_baru=array();
        for($i=0;$i<$jumlah_produk;$i++){
            $tampung=array();
            $idproduk=Produk::select('idproduk')
                    ->where('nama_produk','=',$request->get('produk'.$i))
                    ->first();
            $tampung['id']=$idproduk->idproduk;
            $tampung['jumlah']=$request->get('jumlah_diretur'.$i);
            $tampung['tanggal_lama']=$tanggal_lama[$i];
            array_push($produk_toko_diretur, $tampung);
        }
        $total_stok_produk = Produk::select(DB::raw('SUM(stok_produk.stok) as total_stok'),'produk.*')
            ->leftjoin('stok_produk','stok_produk.idproduk','=','produk.idproduk')
            ->groupBy('produk.idproduk')
            ->get();

        $retur_terakhir = ReturJual::select('idretur')
                ->orderBy('idretur', 'desc')
                ->first();

        for ($i = 0; $i < count($produk_toko_diretur); $i++){
            if($produk_toko_diretur[$i]['jumlah']>0){
                if($jenis_retur=='Produk'){
                    $list_stok_produk = StokProduk::select('*')
                        ->where('stok_produk.idproduk','=',$produk_toko_diretur[$i]['id'])
                        ->where('stok_produk.stok','>',0)
                        ->orderBy('stok_produk.tanggal_kadaluwarsa','asc')
                        ->get();


                    foreach($total_stok_produk as $total_stok){
                        if($total_stok->idproduk==$produk_toko_diretur[$i]['id']){
                            $jumlah_diretur = $produk_toko_diretur[$i]['jumlah'];
                            if($jumlah_diretur<=$total_stok->total_stok){
                                foreach($list_stok_produk as $list_stok){
                                    $stok_pertanggal = $list_stok->stok;
                                    if($jumlah_diretur>0){
                                        $sisa_dibeli = $jumlah_diretur;
                                        $jumlah_diretur-=$stok_pertanggal;
                                        $diretur_pertanggal=0;
                                        if($jumlah_diretur>0){
                                            $stok_sekarang=0;
                                            $diretur_pertanggal=$stok_pertanggal;
                                        }
                                        else{
                                            $stok_sekarang=$stok_pertanggal-$sisa_dibeli;
                                            $diretur_pertanggal=$sisa_dibeli;
                                        }
                                        $list_stok->stok = $stok_sekarang;

                                        $ubah_list_stok = StokProduk::select('*')
                                            ->where('stok_produk.idproduk','=',$list_stok->idproduk)
                                            ->where('stok_produk.tanggal_kadaluwarsa','=',$list_stok->tanggal_kadaluwarsa)
                                            ->first();
                                        $ubah_list_stok->stok = $stok_sekarang;
                                        $ubah_list_stok->save();

                                        $detail_retur = new DetailReturJual([
                                            'idretur_penjualan' => $retur_terakhir->idretur,
                                            'idproduk' => $produk_toko_diretur[$i]['id'],
                                            'tanggal_kadaluwarsa_lama' => $produk_toko_diretur[$i]['tanggal_lama'],
                                            'tanggal_kadaluwarsa_baru' => $ubah_list_stok->tanggal_kadaluwarsa,
                                            'jumlah' => $diretur_pertanggal,
                                        ]);

                                        $detail_retur->save();
                                    }
                                }
                            }
                        }
                    }
                    // foreach($total_stok_produk as $total_stok){
                    //     if($total_stok->idproduk==$produk_toko_diretur[$i]['id']){
                    //         $jumlah_diretur = $produk_toko_diretur[$i]['jumlah'];
                    //         $sisa_diretur = $jumlah_diretur;
                    //         $stok_sekarang=0;
                    //         if($jumlah_diretur<=$total_stok->total_stok){
                    //             foreach($list_stok_produk as $list_stok){
                    //                 //list1
                    //                 //list2
                    //                 $stok_pertanggal = $list_stok->stok;
                                    
                    //                 if($stok_pertanggal>0 && $sisa_diretur>0)
                    //                 {
                    //                     $jumlah_diretur-=$stok_pertanggal;
                    //                     $diretur_pertanggal=0;
                    //                     if($jumlah_diretur>0){
                    //                         $stok_sekarang=0;
                    //                         $diretur_pertanggal=$stok_pertanggal;

                    //                     }
                    //                     else{
                    //                         $stok_sekarang=$stok_pertanggal-$sisa_diretur;
                    //                         $diretur_pertanggal=$sisa_diretur;

                    //                     //1,1,0,1,0
                    //                     }
                    //                     $list_stok->stok = $stok_sekarang;
                    //                     $ubah_list_stok = StokProduk::select('*')
                    //                         ->where('stok_produk.idproduk','=',$list_stok->idproduk)
                    //                         ->where('stok_produk.tanggal_kadaluwarsa','=',$list_stok->tanggal_kadaluwarsa)
                    //                         ->first();
                    //                     $ubah_list_stok->stok = $stok_sekarang;
                    //                     $ubah_list_stok->save();

                    //                     $detail_retur = new DetailReturJual([
                    //                         'idretur_penjualan' => $retur_terakhir->idretur,
                    //                         'idproduk' => $produk_toko_diretur[$i]['id'],
                    //                         'tanggal_kadaluwarsa_lama' => $produk_toko_diretur[$i]['tanggal_lama'],
                    //                         'tanggal_kadaluwarsa_baru' => $ubah_list_stok->tanggal_kadaluwarsa,
                    //                         'jumlah' => $diretur_pertanggal,
                    //                     ]);//
                    //                     $detail_retur->save();
                    //                     $sisa_diretur-=$diretur_pertanggal;
 
                    //                 }
                                    
                    //             }
                    //         }
                    //     }
                            
                    // }
                }
                else if($jenis_retur=='Uang'){
                    $detail_retur = new DetailReturJual([
                        'idretur_penjualan' => $retur_terakhir->idretur,
                        'idproduk' => $produk_toko_diretur[$i]['id'],
                        'tanggal_kadaluwarsa_lama' => $produk_toko_diretur[$i]['tanggal_lama'],
                        'tanggal_kadaluwarsa_baru' => $produk_toko_diretur[$i]['tanggal_lama'],
                        'jumlah' => $produk_toko_diretur[$i]['jumlah'],
                    ]);
                    $detail_retur->save();
                }
            }
        }
        for ($i = 0; $i < count($produk_toko_diretur); $i++){
            if($produk_toko_diretur[$i]['jumlah']>0){
                $tambah_stok_produk = StokProduk::select('*')
                    ->where('stok_produk.idproduk','=',$produk_toko_diretur[$i]['id'])
                    ->where('stok_produk.tanggal_kadaluwarsa','=',$produk_toko_diretur[$i]['tanggal_lama'])
                    ->first();
                $tambah_stok_produk->stok+=$produk_toko_diretur[$i]['jumlah'];
                $tambah_stok_produk->save();
            }
        }
        return redirect('/retur-penjualan/'.$retur_terakhir->idretur);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $retur=ReturJual::select('jenis_retur')
                ->where('idretur','=',$id)
                ->first();
            $produk = DetailReturJual::select('detail_retur_penjualan.*', 'produk.nama_produk')
                ->join('produk', 'produk.idproduk', '=', 'detail_retur_penjualan.idproduk')
                ->where('detail_retur_penjualan.idretur_penjualan', '=', $id)
                ->get();
            return view('retur-jual.detail-retur-jual', ['retur'=>$retur, 'produk'=>$produk, 'id'=>$id]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
