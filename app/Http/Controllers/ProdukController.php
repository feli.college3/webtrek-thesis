<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Produk;
use App\Kategori;
use App\StokProduk;
use App\DetailToko;
use App\DetailNotaJual;
use App\DetailNotaBeli;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }
    
    public function index()
    {
        $stok = Produk::select(DB::raw('SUM(stok_produk.stok) as total_stok'))
            ->leftjoin('stok_produk','stok_produk.idproduk','=','produk.idproduk')
            ->groupBy('produk.idproduk')
            ->where('produk.status','=','Dijual')
            ->get();
        $list_produk = Produk::select('*')
                    ->join('kategori', 'produk.idkategori', '=', 'kategori.idkategori')
                    ->where('produk.status','=','Dijual')
                    ->get();
        return view('produk.index-produk', ['list_produk'=>$list_produk, 'stok'=>$stok]);
    }

    // public function index_cust()
    // {
    //     $stok = Produk::select(DB::raw('SUM(stok_produk.stok) as total_stok'))
    //         ->leftjoin('stok_produk','stok_produk.idproduk','=','produk.idproduk')
    //         ->groupBy('produk.idproduk')
    //         ->get();
    //     $list_produk = Produk::select('*')
    //                ->join('kategori', 'produk.idkategori', '=', 'kategori.idkategori')
    //                ->get();
    //     return view('produk.cust-produk', ['list_produk'=>$list_produk, 'stok'=>$stok]);
    // }

    public function perlu_restock()
    {
        $list_produk = Produk::select(DB::raw('SUM(stok_produk.stok) as total_stok'),'produk.*','kategori.*')
            ->leftjoin('stok_produk','stok_produk.idproduk','=','produk.idproduk')
            ->join('kategori', 'produk.idkategori', '=', 'kategori.idkategori')
            ->where('produk.status','=','Dijual')
            ->groupBy('produk.idproduk')
            ->get();
        $perlu_restock=array();
        foreach($list_produk as $produk){
            if($produk->total_stok<=$produk->stok_minimal){
                array_push($perlu_restock, $produk);
            }
        }
        return view('produk.produk-restock', ['list_produk'=>$perlu_restock]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $list_kategori = Kategori::all();
            return view('produk.tambah-produk', ['list_kategori'=>$list_kategori]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required | string',
            'satuan' => 'required | string',
            'deskripsi' => 'required | string',
        ]);
        
        if($validator->fails()){
            return redirect()
                ->action('ProdukController@create')
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $detail_toko=DetailToko::select('*')->first();
            $idKat = $request->get('id_kategori');
            $kategori = Kategori::find($idKat);
            $kodeKat = $kategori->kode_kategori;
            //dd($kodeKat);
            $totalProd = Produk::where('produk.idproduk','like', $kodeKat.'%')
                         ->count();
            $idBaru=$totalProd+1;
            if($idBaru < 10){
                $idBaru = $kodeKat.'00'.$idBaru;
            }
            if($idBaru >= 10){
                $idBaru = $kodeKat.'0'.$idBaru;
            }
            else if($idBaru >= 100){
                $idBaru = $kodeKat.$idBaru;
            }
            if($request->hasFile('foto')){
                $this->validate($request, [
                  'foto'  => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                 ]);
                $filefoto = $request->file('foto');
                $nama_foto = $idBaru;
                $ekstensi = $filefoto->getClientOriginalExtension();
                Storage::disk('public')->put($nama_foto.'.'.$ekstensi, File::get($filefoto));
            }
            else{
                $nama_foto = 'standar';
                $ekstensi = 'png';
            }

            $stok_minimal=0;
            if($detail_toko->fitur_stok_minimal_perproduk==1){
                $stok_minimal=$request->get('stok_minimal');
            }
            else{
                $stok_minimal=$detail_toko->stok_minimal;
            }
            // $harga_jual_eceran=$request->get('harga_jual_eceran');
            // $harga_jual_grosir=$harga_jual_eceran;
            // if($detail_toko->fitur_harga_grosir==1){
            //     $harga_jual_grosir=$request->get('harga_jual_grosir');
            // }
            $produk = new Produk([
            'idproduk' => $idBaru,
            'nama_produk' => $request->get('nama'),
            'satuan' => $request->get('satuan'),
            'deskripsi' => $request->get('deskripsi'),
            'stok_minimal' => $stok_minimal,
            'status' => 'Dijual',
            'nama_foto' => $nama_foto,
            'ekstensi' => $ekstensi,
            'hpp' => 0,
            'harga_jual_eceran' => 0,
            'harga_jual_grosir' => 0,
            'idkategori' => $idKat,
            ]);

            $produk->save();

            $stok_produk = new StokProduk([
                'idproduk' => $idBaru,
                'tanggal_kadaluwarsa' => '2000-01-01 00:00:00',
                'stok' => 0,
                'lokasi' => '-',
            ]);
            $stok_produk->save();
            return redirect('/produk');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $stok_produk = StokProduk::select('*')
            ->join('produk', 'produk.idproduk', '=', 'stok_produk.idproduk')
            ->where('stok_produk.idproduk', '=', $id)
            ->where('stok_produk.stok', '>', 0)
            ->get();
        $produk = Produk::find($id);
        return view('produk.detail-produk', ['stok_produk'=>$stok_produk, 'id'=>$id, 'produk'=>$produk]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $produk = Produk::find($id);

            $idKat = DB::table('produk')
                ->where('idproduk', '=', $id)
                ->value('idkategori');
            
            $selectedKat = Kategori::find($idKat);
            $list_kategori = Kategori::select()->get();
            $full_foto = $produk->nama_foto.$produk->ekstensi;
            return view('produk.edit-produk', compact('produk','selectedKat','list_kategori','id', 'full_foto'));
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required | string',
            'satuan' => 'required | string',
            'deskripsi' => 'required | string',
            'harga_jual_eceran' => 'required | numeric | min:'.$request->get('hpp'),
        ]);
        
        if($validator->fails()){
            return redirect()
                ->action('ProdukController@edit', $id)
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $detail_toko=DetailToko::select('*')->first();
            
            if($request->hasFile('foto')){
                $exists = Storage::disk('public')->exists($id.'.jpg');
                $exists2 = Storage::disk('public')->exists($id.'.png');

                if($exists=='1'){
                    Storage::disk('public')->delete($id.'.jpg');
                }
                else if($exists2=='1'){
                    Storage::disk('public')->delete($id.'.png');
                }
                $filefoto = $request->file('foto');
                $nama_foto = $id;
                $ekstensi = $filefoto->getClientOriginalExtension();
                Storage::disk('public')->put($nama_foto.'.'.$ekstensi, File::get($filefoto));
            }

            $stok_minimal=0;
            if($detail_toko->fitur_stok_minimal_perproduk==1){
                $stok_minimal=$request->get('stok_minimal');
            }
            else{
                $stok_minimal=$detail_toko->stok_minimal;
            }
            
            $harga_jual_eceran=$request->get('harga_jual_eceran');
            $harga_jual_grosir=$harga_jual_eceran;
            if($detail_toko->fitur_harga_grosir==1){
                $harga_jual_grosir=$request->get('harga_jual_grosir');
            }
            $produk = Produk::find($id);
            $produk->nama_produk = $request->get('nama');
            $produk->satuan = $request->get('satuan');
            $produk->stok_minimal = $stok_minimal;
            $produk->deskripsi = $request->get('deskripsi');
            $produk->harga_jual_eceran = $harga_jual_eceran;
            $produk->harga_jual_grosir = $harga_jual_grosir;
            $produk->status = $request->get('status');
            if($request->hasFile('foto')){
                $produk->nama_foto = $nama_foto;
                $produk->ekstensi = $ekstensi;
            }
            $produk->save();
            return redirect('/produk');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $jual=DetailNotaJual::where('idproduk','=',$id)->first();
            $beli=DetailNotaBeli::where('idproduk','=',$id)->first();
            if($jual==null&&$beli==null){
                $produk=Produk::where('idproduk',$id)->first();
                Storage::disk('public')->delete($id.'.'.$produk->ekstensi);
                $stok_produk=StokProduk::where('idproduk',$id)->first();
                $stok_produk->delete();
                $produk->delete();
                return redirect('/produk')->with('status', 'Produk terhapus');
            }
            else{
                $produk=Produk::where('idproduk',$id)->first();
                $produk->status='Tidak Dijual';
                $produk->save();
                return redirect('/produk')->with('status', 'Produk tidak dijual lagi');
            }
        }
        else{
            return redirect()->back();
        }
        
    }
}
