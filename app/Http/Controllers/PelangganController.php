<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pelanggan;
use App\DetailToko;
use Validator;

class PelangganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }
    
    public function index()
    {
        $list_pelanggan = Pelanggan::all();
        return view('pelanggan.index-pelanggan', ['list_pelanggan'=>$list_pelanggan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            return view('pelanggan.tambah-pelanggan');
        }
        else{
            return redirect()->back();
        }
    }

    public function index_profile($username)
    {
        $username = Auth::user()->username;
        $pelanggan = Pelanggan::where('username', '=', $username)->first();
        return view('customer.profile-pelanggan',['pelanggan'=>$pelanggan]);
    }

    public function simpan_data(Request $request)
    {
        /*$lanjot=$request->get('lanjot');
        dd($lanjot);
        if($lanjot==1)
        {*/
            $idpelanggan=$request->get('idpelanggan');
            $plg = Pelanggan::find($idpelanggan);
            $plg->nama_pelanggan = $request->get('nama');
            $plg->alamat_pelanggan = $request->get('alamat');
            $plg->telepon_pelanggan = $request->get('telepon');
            $plg->save();
            return redirect('/home')->with('alert', 'Data telah tersimpan!');
        /*}
        else{
            return redirect()->back();
        }
*/
       
        // return redirect()->back()->with('alert', 'Deleted!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required | string',
        ]);
        
        if($validator->fails()){
            return redirect()
                ->action('PelangganController@create')
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $detail_toko=DetailToko::select('*')->first();
            $batas_hutang=0;
            if($detail_toko->fitur_batas_hutang==1){
                $batas_hutang=$request->get('batas_hutang');
            }
            $plg = new Pelanggan([
            'nama_pelanggan' => $request->get('nama'),
            'alamat_pelanggan' => $request->get('alamat'),
            'telepon_pelanggan' => $request->get('telepon'),
            'status' => 'Aktif',
            'total_hutang' => 0,
            'batas_hutang' => $batas_hutang,
            'username' => '-',
            ]);

            $plg->save();
            return redirect('/pelanggan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $plg = Pelanggan::find($id);    
            return view('pelanggan.edit-pelanggan', ['plg'=>$plg, 'id'=>$id]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required | string',
            'status' => 'required | string',
        ]);
        
        if($validator->fails()){
            return redirect()
                ->action('PelangganController@edit', $id)
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $detail_toko=DetailToko::select('*')->first();
            $batas_hutang=0;
            if($detail_toko->pembayaran_piutang==1){
                $batas_hutang=$request->get('batas_hutang');
            }

            $plg = Pelanggan::find($id);
            $plg->nama_pelanggan = $request->get('nama');
            $plg->alamat_pelanggan = $request->get('alamat');
            $plg->telepon_pelanggan = $request->get('telepon');
            $plg->status = $request->get('status');
            $plg->total_hutang = $request->get('total_hutang');
            $plg->batas_hutang = $batas_hutang;

            $plg->save();
            return redirect('/pelanggan');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            Pelanggan::where('idpelanggan',$id)->first()->delete();
            return redirect('/pelanggan')->with('status', 'Pelanggan terhapus');
        }
        else{
            return redirect()->back();
        }
    }
}
