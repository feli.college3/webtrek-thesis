<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\ProdukKonsinyasi;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ProdukKonsinyasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }
    
    public function index()
    {
        $list_produk = ProdukKonsinyasi::all();
        return view('produk-konsinyasi.index-produk', ['list_produk'=>$list_produk]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            return view('produk-konsinyasi.tambah-produk');
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required | string',
            'deskripsi' => 'required | string',
            'satuan' => 'required | string',
            'lama_penitipan' => 'required | numeric',
            'hpp' => 'required | numeric | between:0.000,1000000.000',
            'harga_jual' => 'required | numeric | min:'.$request->get('hpp'),
        ]);
        
        if($validator->fails()){
            return redirect()
                ->action('ProdukKonsinyasiController@create')
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $idTerakhir = ProdukKonsinyasi::max('idproduk');
            $idBaru = $idTerakhir+1;
            if($request->hasFile('foto')){
                $filefoto = $request->file('foto');
                $nama_foto = $idBaru;
                $ekstensi = $filefoto->getClientOriginalExtension();
                Storage::disk('public')->put($nama_foto.'.'.$ekstensi, File::get($filefoto));
            }
            else{
                $nama_foto = 'standar';
                $ekstensi = 'png';
            }

            $satuan = $request->get('satuan');
            $produk = new ProdukKonsinyasi([
            'nama_produk' => $request->get('nama'),
            'deskripsi' => $request->get('deskripsi'),
            'stok' => 0,
            'satuan' => strtolower($satuan),
            'lama_penitipan' => $request->get('lama_penitipan'),
            'nama_foto' => $nama_foto,
            'ekstensi' => $ekstensi,
            'hpp' => $request->get('hpp'),
            'harga_jual' => $request->get('harga_jual'),
            ]);

            $produk->save();
            return redirect('/produk-konsinyasi');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $produk = ProdukKonsinyasi::find($id);
            return view('produk-konsinyasi.edit-produk', ['produk'=>$produk, 'id'=>$id]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required | string',
            'deskripsi' => 'required | string',
            'satuan' => 'required | string',
            'lama_penitipan' => 'required | numeric',
            'hpp' => 'required | numeric | between:0.000,1000000.000',
            'harga_jual' => 'required | numeric | min:'.$request->get('hpp'),
        ]);
        
        if($validator->fails()){
            return redirect()
                ->action('ProdukKonsinyasiController@edit', $id)
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $exists = Storage::disk('public')->exists($id.'.jpg');
            $exists2 = Storage::disk('public')->exists($id.'.png');

            if($exists=='1'){
                Storage::disk('public')->delete($id.'.jpg');
            }
            else if($exists2=='1'){
                Storage::disk('public')->delete($id.'.png');
            }
            if($request->hasFile('foto')){
                $filefoto = $request->file('foto');
                $nama_foto = $id;
                $ekstensi = $filefoto->getClientOriginalExtension();
                Storage::disk('public')->put($nama_foto.'.'.$ekstensi, File::get($filefoto));
            }
            $satuan = $request->get('satuan');
            $produk = ProdukKonsinyasi::find($id);
            $produk->nama_produk = $request->get('nama');
            $produk->deskripsi = $request->get('deskripsi');
            $produk->satuan = strtolower($satuan);
            $produk->lama_penitipan = $request->get('lama_penitipan');
            $produk->hpp = $request->get('hpp');
            $produk->harga_jual = $request->get('harga_jual');
            if($request->hasFile('foto')){
                $produk->nama_foto = $nama_foto;
                $produk->ekstensi = $ekstensi;
            }
            else{
                $produk->nama_foto = $produk->nama_foto;
                $produk->ekstensi = $produk->ekstensi;
            }
            $produk->save();
            return redirect('/produk-konsinyasi');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            ProdukKonsinyasi::where('idproduk',$id)->first()->delete();
            $exists = Storage::disk('public')->exists($id.'.jpg');
            $exists2 = Storage::disk('public')->exists($id.'.png');

            if($exists=='1'){
                Storage::disk('public')->delete($id.'.jpg');
            }
            else if($exists2=='1'){
                Storage::disk('public')->delete($id.'.png');
            }
            return redirect('/produk-konsinyasi')->with('status', 'Produk terhapus');
        }
        else{
            return redirect()->back();
        }
    }
}
