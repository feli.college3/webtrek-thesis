<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pelanggan;
use App\KeranjangBiasa;
use App\KeranjangKonsinyasi;
use App\NotaJual;
use App\Produk;
use App\ProdukKonsinyasi;
use App\StokProduk;
use App\DetailNotaJual;
use App\DetailJualKonsinyasi;
use App\DetailToko;
use App\Pengiriman;
use DB;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //tambahan untuk update with 2 primary keys
    protected $primaryKey = ['idproduk', 'tanggal_kadaluwarsa'];
    public $incrementing = false;
    //tutup tambahan

    public function index()
    {
        $detail_toko=DetailToko::select('*')->first();
        $username = Auth::user()->username;
        $pelanggan = Pelanggan::select('*')
            ->where('username', '=', $username)
            ->get();

        $produk_toko = KeranjangBiasa::select('keranjang_biasa.*','produk.*')
        ->join('produk', 'produk.idproduk', '=', 'keranjang_biasa.idproduk')
        ->where('keranjang_biasa.idpelanggan', '=', $pelanggan[0]->idpelanggan)
        ->get();

        $produk_konsinyasi = KeranjangKonsinyasi::select('keranjang_konsinyasi.*', 'produk_konsinyasi.*')
            ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'keranjang_konsinyasi.idproduk_konsinyasi')
            ->where('keranjang_konsinyasi.idpelanggan', '=', $pelanggan[0]->idpelanggan)
            // ->orderBy('keranjang_konsinyasi.updated_at')
            ->get();

        if($detail_toko->fitur_harga_grosir==1){
            $subtotal_toko=array();
            foreach($produk_toko as $produk){
                if($produk->jumlah>=$detail_toko->jumlah_grosir){
                    $tampung=KeranjangBiasa::select(DB::raw("sum(keranjang_biasa.jumlah*produk.harga_jual_grosir) as subtotal"))
                        ->join('produk', 'produk.idproduk', '=', 'keranjang_biasa.idproduk')
                        ->where('keranjang_biasa.idpelanggan', '=', $pelanggan[0]->idpelanggan)
                        ->where('keranjang_biasa.idproduk', '=', $produk->idproduk)
                        ->first();
                }
                else{
                    $tampung=KeranjangBiasa::select(DB::raw("sum(keranjang_biasa.jumlah*produk.harga_jual_eceran) as subtotal"))
                        ->join('produk', 'produk.idproduk', '=', 'keranjang_biasa.idproduk')
                        ->where('keranjang_biasa.idpelanggan', '=', $pelanggan[0]->idpelanggan)
                        ->where('keranjang_biasa.idproduk', '=', $produk->idproduk)
                        ->first();
                }
                array_push($subtotal_toko, $tampung);
            }
        }
        else{
            $subtotal_toko = KeranjangBiasa::select(DB::raw("sum(keranjang_biasa.jumlah*produk.harga_jual_eceran) as subtotal"))
            ->join('produk', 'produk.idproduk', '=', 'keranjang_biasa.idproduk')
            ->where('keranjang_biasa.idpelanggan', '=', $pelanggan[0]->idpelanggan)
            // ->orderBy('keranjang_biasa.updated_at')
            ->groupBy('keranjang_biasa.idproduk')
            ->get();
        }
            
        $subtotal_konsinyasi = KeranjangKonsinyasi::select(DB::raw("sum(keranjang_konsinyasi.jumlah*produk_konsinyasi.harga_jual) as subtotal"))
        ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'keranjang_konsinyasi.idproduk_konsinyasi')
        ->where('keranjang_konsinyasi.idpelanggan', '=', $pelanggan[0]->idpelanggan)
        ->groupBy('keranjang_konsinyasi.idproduk_konsinyasi')
        ->get();

        $total_awal=0;
        foreach($subtotal_toko as $produk){
            $total_awal+=$produk->subtotal;
        }
        foreach($subtotal_konsinyasi as $produk){
            $total_awal+=$produk->subtotal;
        }

        $grand_total = $total_awal;
        return view('customer.checkout', ['produk_toko'=>$produk_toko,'produk_konsinyasi'=>$produk_konsinyasi,'subtotal_toko'=>$subtotal_toko,'subtotal_konsinyasi'=>$subtotal_konsinyasi,'total_keranjang'=>$total_awal,'pelanggan'=>$pelanggan[0],'grand_total'=>$grand_total]);
    }

    public function cek_ongkir($alamat, $kodepos)
    {
        $ongkir=10000;
        if($alamat=='Jl. Panglima Sudirman No. 30'){
            $ongkir=25000;
        }
        return view('customer.keranjang', ['ongkir'=>$ongkir]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $detail_toko=DetailToko::select('*')->first();
        $username = Auth::user()->username;
        $pelanggan = Pelanggan::select('*')
            ->where('username', '=', $username)
            ->get();
        $idpelanggan = $pelanggan[0]->idpelanggan;

        date_default_timezone_set('Asia/Jakarta');
        $tanggal= date('dmy');
        $tanggaljam = date('Y-m-d H:i:s');

        $total_trans = NotaJual::where('nota_jual.no_nota_jual','like', $tanggal.'%')
                     ->count();
        $idBaru=$total_trans+1;
        if($idBaru < 10){
            $idBaru = $tanggal.'J00'.$idBaru;
        }
        else if($idBaru >= 10){
            $idBaru = $tanggal.'J0'.$idBaru;
        }
        else if($idBaru >= 100){
            $idBaru = $tanggal.'J'.$idBaru;
        }

        $metode = $request->get('metode');
        $status_transaksi ='';
        if($metode=='COD'){
            $status_transaksi='Belum Diproses';
        }
        else if($metode=='Transfer'){
            $status_transaksi='Belum Dibayar';
        }

        //////////////////////////////////NOTA JUAL//////////////////////////////////
        $produk_toko_dibeli = KeranjangBiasa::select('keranjang_biasa.*', 'produk.*')
        ->join('produk', 'produk.idproduk', '=', 'keranjang_biasa.idproduk')
        ->where('keranjang_biasa.idpelanggan', '=', $idpelanggan)
        ->get();

        $produk_konsinyasi_dibeli = KeranjangKonsinyasi::select('keranjang_konsinyasi.*')
            ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'keranjang_konsinyasi.idproduk_konsinyasi')
            ->where('keranjang_konsinyasi.idpelanggan', '=', $idpelanggan)
            ->get();

        if($detail_toko->fitur_harga_grosir==1){
            $subtotal_toko=array();
            foreach($produk_toko_dibeli as $produk){
                if($produk->jumlah>=$detail_toko->jumlah_grosir){
                    $tampung=KeranjangBiasa::select(DB::raw("sum(keranjang_biasa.jumlah*produk.harga_jual_grosir) as subtotal"))
                        ->join('produk', 'produk.idproduk', '=', 'keranjang_biasa.idproduk')
                        ->where('keranjang_biasa.idpelanggan', '=', $pelanggan[0]->idpelanggan)
                        ->where('keranjang_biasa.idproduk', '=', $produk->idproduk)
                        ->first();
                }
                else{
                    $tampung=KeranjangBiasa::select(DB::raw("sum(keranjang_biasa.jumlah*produk.harga_jual_eceran) as subtotal"))
                        ->join('produk', 'produk.idproduk', '=', 'keranjang_biasa.idproduk')
                        ->where('keranjang_biasa.idpelanggan', '=', $pelanggan[0]->idpelanggan)
                        ->where('keranjang_biasa.idproduk', '=', $produk->idproduk)
                        ->first();
                }
                array_push($subtotal_toko, $tampung);
            }
        }
        else{
            $subtotal_toko = KeranjangBiasa::select(DB::raw("sum(keranjang_biasa.jumlah*produk.harga_jual_eceran) as subtotal"))
            ->join('produk', 'produk.idproduk', '=', 'keranjang_biasa.idproduk')
            ->where('keranjang_biasa.idpelanggan', '=', $pelanggan[0]->idpelanggan)
            // ->orderBy('keranjang_biasa.updated_at')
            ->groupBy('keranjang_biasa.idproduk')
            ->get();
        }
            
        $subtotal_konsinyasi = KeranjangKonsinyasi::select(DB::raw("sum(keranjang_konsinyasi.jumlah*produk_konsinyasi.harga_jual) as subtotal"))
        ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'keranjang_konsinyasi.idproduk_konsinyasi')
        ->where('keranjang_konsinyasi.idpelanggan', '=', $idpelanggan)
        ->groupBy('keranjang_konsinyasi.idproduk_konsinyasi')
        ->get();

        $total_awal=0;
        foreach($subtotal_toko as $produk){
            $total_awal+=$produk->subtotal;
        }
        foreach($subtotal_konsinyasi as $produk){
            $total_awal+=$produk->subtotal;
        }

        $kirim=$request->get('pengiriman');
        $total_keranjang=$request->get('total_keranjang');
        
        $notajual = new NotaJual([
        'no_nota_jual' => $idBaru,
        'idpelanggan' => $idpelanggan,
        'idpegawai_kasir' => 0,
        'tanggal' => $tanggaljam,
        'grand_total' => $total_keranjang,
        'status_pembayaran' => 'Belum Lunas',
        'metode_pembayaran' => $metode,
        'tanggal_jatuh_tempo' => $tanggaljam,
        'sisa_piutang' => $total_keranjang,
        'status_transaksi' => $status_transaksi,
        'keterangan' => '-',
        'kirim' => $kirim,
        ]);

        $notajual->save();

        if($kirim=="1"){
            $ongkir=$request->get('ongkir');
            $keterangan=$request->get('keterangan');
            if($keterangan==""){
                $keterangan="-";
            }
            $pengiriman = new Pengiriman([
            'no_nota_jual' => $idBaru,
            'idpegawai' => 0,
            'tanggal_kirim' => $tanggaljam,
            'alamat' => $request->get('alamat'),
            'ongkos_kirim' => $ongkir,
            'nama_penerima' => '-',
            'status_kirim' => '-',
            'keterangan' => $keterangan,
            ]);
            $pengiriman->save();
        }

        //hapus dari keranjang
        foreach($produk_toko_dibeli as $produk){
            $idproduk = $produk->idproduk;
            $panjang_id = strlen($idproduk);
            KeranjangBiasa::where(['idproduk' => $idproduk, 'idpelanggan'=>$idpelanggan])->delete();
        }
        foreach($produk_konsinyasi_dibeli as $produk){
            $idproduk = $produk->idproduk_konsinyasi;
            $panjang_id = strlen($idproduk);
            KeranjangKonsinyasi::where(['idproduk_konsinyasi' => $idproduk, 'idpelanggan'=>$idpelanggan])->delete();
        }

        ////////////////////DETAIL NOTA JUAL////////////////////
        // $produk = Produk::select()->get();
        
        // $idProdukTerpilih = "";
        // $totaldata = $request->get('totaldata');
        // $stokLama = 0;
        // for($i=1;$i<=$totaldata;$i++)
        // {
        //     foreach($produk as $prod)
        //     {
        //         if($prod->namaProduk == $request->get('produk'.$i)){
        //             $idProdukTerpilih = $prod->idProduk;
        //             //UPDATE STOK PRODUK
        //             $stokLama = $prod->stok;
        //             $prodTerpilih = Produk::find($idProdukTerpilih);
        //             $prodTerpilih->stok -= (float)($request->get('jumlah'.$i));
        //             $prodTerpilih->save();
        //         }
        //     }
        //     $detailnotajual = new DetailNotaJual([
        //         'no_nota_jual' => $idBaru,
        //         'idProduk' => $idProdukTerpilih,
        //         'hargaJual' => (float)($request->get('harga_numb'.$i)),
        //         'jumlah' => (float)($request->get('jumlah'.$i))
        //     ]);
        //     $detailnotajual->save();
        // }


        /////kurangi stok & detail nota
                
        foreach($produk_konsinyasi_dibeli as $produk_dibeli){
            $produk_terpilih = ProdukKonsinyasi::find($produk_dibeli->idproduk_konsinyasi);
            $produk_terpilih->stok -= (float)($produk_dibeli->jumlah);
            $produk_terpilih->save();

            $detail_konsi = new DetailJualKonsinyasi([
            'no_nota_jual' => $idBaru,
            'idproduk_konsinyasi' => $produk_dibeli->idproduk_konsinyasi,
            'harga_jual' => $produk_terpilih->harga_jual,
            'jumlah' => $produk_dibeli->jumlah,
            ]);

            $detail_konsi->save();
        }

        $total_stok_produk = Produk::select(DB::raw('SUM(stok_produk.stok) as total_stok'),'produk.*')
            ->leftjoin('stok_produk','stok_produk.idproduk','=','produk.idproduk')
            ->groupBy('produk.idproduk')
            ->get();

        for ($i = 0; $i < count($produk_toko_dibeli); $i++){
            $list_stok_produk = StokProduk::select('*')
                ->where('stok_produk.idproduk','=',$produk_toko_dibeli[$i]->idproduk)
                ->where('stok_produk.stok','>',0)
                ->orderBy('stok_produk.tanggal_kadaluwarsa','asc')
                ->get();

            $harga_jual=$produk_toko_dibeli[$i]->harga_jual_eceran;
            if($detail_toko->fitur_harga_grosir==1){
                if($produk_toko_dibeli[$i]->jumlah>=$detail_toko->jumlah_grosir){
                    $harga_jual=$produk_toko_dibeli[$i]->harga_jual_grosir;
                }
            }
            foreach($total_stok_produk as $total_stok){
                if($total_stok->idproduk==$produk_toko_dibeli[$i]->idproduk){
                    $jumlah_dibeli = $produk_toko_dibeli[$i]->jumlah;
                    if($jumlah_dibeli<=$total_stok->total_stok){
                        foreach($list_stok_produk as $list_stok){
                            $stok_pertanggal = $list_stok->stok;
                            if($jumlah_dibeli>0){
                                $sisa_dibeli = $jumlah_dibeli;
                                $jumlah_dibeli-=$stok_pertanggal;
                                $dibeli_pertanggal=0;
                                if($jumlah_dibeli>0){
                                    $stok_sekarang=0;
                                    $dibeli_pertanggal=$stok_pertanggal;
                                }
                                else{
                                    $stok_sekarang=$stok_pertanggal-$sisa_dibeli;
                                    $dibeli_pertanggal=$sisa_dibeli;
                                }
                                $list_stok->stok = $stok_sekarang;

                                $ubah_list_stok = StokProduk::select('*')
                                    ->where('stok_produk.idproduk','=',$list_stok->idproduk)
                                    ->where('stok_produk.tanggal_kadaluwarsa','=',$list_stok->tanggal_kadaluwarsa)
                                    ->first();
                                $ubah_list_stok->stok = $stok_sekarang;
                                $ubah_list_stok->save();

                                $detail_jual = new DetailNotaJual([
                                    'no_nota_jual' => $idBaru,
                                    'idproduk' => $produk_toko_dibeli[$i]->idproduk,
                                    'harga_jual' => $harga_jual,
                                    'tanggal_kadaluwarsa' => $ubah_list_stok->tanggal_kadaluwarsa,
                                    'jumlah' => $dibeli_pertanggal,
                                ]);

                                $detail_jual->save();
                            }
                        }
                    }
                }
            }
        }

        if($metode=='COD'){
            return redirect('pesanan/diproses');
        }
        else{
            return view('customer.pembayaran',['nota_jual'=>$notajual]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
