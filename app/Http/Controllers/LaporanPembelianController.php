<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\DetailToko;
use App\NotaBeli;
use PDF;

class LaporanPembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }
    
    public function index()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $list_nota_beli = NotaBeli::select('nota_beli.*', 'supplier.idsupplier', 'supplier.nama_supplier', 'pegawai.idpegawai', 'pegawai.nama_pegawai')
                        ->join('supplier', 'supplier.idsupplier', '=', 'nota_beli.idsupplier')
                        ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_beli.idpegawai')
                        ->orderBy('nota_beli.tanggal_dipesan', 'desc')
                        ->get();
            $bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
            $tahun = NotaBeli::selectRaw('substr(tanggal_dipesan,1,4) as tahun')->pluck('tahun')->unique();
            return view('laporan.index-pembelian', ['list_nota_beli'=>$list_nota_beli,'bulan'=>$bulan,'tahun'=>$tahun]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function get_nota($bulan,$tahun,$jenis){
        $bulan_index=$bulan;
        $bulanmin=$bulan;
        $bulanplus=$bulan+1;
        $tahunmin=$tahun;
        $tahunplus=$tahun;
        if($bulan==12){
            $bulanplus=1;
            $tahunplus++;
        }
        if($bulan<10){
            $bulan='0'.$bulan;
        }
        if($bulanmin<10){
            $bulanmin='0'.$bulanmin;
        }
        if($bulanplus<10){
            $bulanplus='0'.$bulanplus;
        }
        $from = date($tahunmin.$bulanmin.'01');
        $to = date($tahunplus.$bulanplus.'01');
        if($jenis=='Belum Lunas')
            $list_nota_beli = NotaBeli::select('nota_beli.*', 'supplier.idsupplier', 'supplier.nama_supplier', 'pegawai.idpegawai', 'pegawai.nama_pegawai')
                    ->join('supplier', 'supplier.idsupplier', '=', 'nota_beli.idsupplier')
                    ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_beli.idpegawai')
                    ->where('nota_beli.status_pembayaran','=','Belum Lunas')
                    ->whereBetween('tanggal_dipesan', [$from, $to])
                    ->orderBy('nota_beli.tanggal_dipesan', 'asc')
                    ->get();
        else if($jenis=='Jatuh Tempo'){
            $list_nota_beli = NotaBeli::select('nota_beli.*', 'supplier.idsupplier', 'supplier.nama_supplier', 'pegawai.idpegawai', 'pegawai.nama_pegawai')
                    ->join('supplier', 'supplier.idsupplier', '=', 'nota_beli.idsupplier')
                    ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_beli.idpegawai')
                    ->where('nota_beli.status_pembayaran','=','Belum Lunas')
                    ->whereBetween('tanggal_jatuh_tempo', [$from, $to])
                    ->orderBy('nota_beli.tanggal_dipesan', 'asc')
                    ->get();
        }
        else if($jenis=='Lunas'){
            $list_nota_beli = NotaBeli::select('nota_beli.*', 'supplier.idsupplier', 'supplier.nama_supplier', 'pegawai.idpegawai', 'pegawai.nama_pegawai')
                    ->join('supplier', 'supplier.idsupplier', '=', 'nota_beli.idsupplier')
                    ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_beli.idpegawai')
                    ->where('nota_beli.status_pembayaran','=','Lunas')
                    ->whereBetween('tanggal_dipesan', [$from, $to])
                    ->orderBy('nota_beli.tanggal_dipesan', 'asc')
                    ->get();
        }
        else if($jenis=='Semua'){
            $list_nota_beli = NotaBeli::select('nota_beli.*', 'supplier.idsupplier', 'supplier.nama_supplier', 'pegawai.idpegawai', 'pegawai.nama_pegawai')
                    ->join('supplier', 'supplier.idsupplier', '=', 'nota_beli.idsupplier')
                    ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_beli.idpegawai')
                    ->whereBetween('tanggal_dipesan', [$from, $to])
                    ->orderBy('nota_beli.tanggal_dipesan', 'asc')
                    ->get();
        }
        return $list_nota_beli;
    }

    public function get_total_pembelian($list_nota_beli){
        $total_beli=0;
        foreach ($list_nota_beli as $nota) {
            $total_beli+=$nota->grand_total;
        }
        return $total_beli;
    }

    public function get_tanpa_hutang($list_nota_beli){
        $tanpa_hutang=0;
        foreach ($list_nota_beli as $nota) {
            $tanpa_hutang+=$nota->grand_total;
            $tanpa_hutang-=$nota->sisa_hutang;
        }
        return $tanpa_hutang;
    }

    public function store(Request $request)
    {
        $detail_toko=DetailToko::select('*')->first();
        $bulan=$request->get('bulan');
        $tahun=$request->get('tahun');
        $jenis='Semua';
        if($detail_toko->pembayaran_hutang==1){
            $jenis=$request->get('jenis');
        }
        $list_nota_beli=$this->get_nota($bulan,$tahun,$jenis);
        $total_beli=$this->get_total_pembelian($list_nota_beli);
        $tanpa_hutang=0;
        if($detail_toko->pembayaran_hutang==1){
            $tanpa_hutang=$this->get_tanpa_hutang($list_nota_beli);
        }
        $list_bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        return view('laporan.laporan-pembelian', ['list_nota_beli'=>$list_nota_beli,'list_bulan'=>$list_bulan,'tahun'=>$tahun,'bulan'=>$bulan,'total_beli'=>$total_beli,'tanpa_hutang'=>$tanpa_hutang,'jenis'=>$jenis]);
    }

    public function cetak($tahun,$bulan,$jenis)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $detail_toko=DetailToko::select('*')->first();
            $list_nota_beli=$this->get_nota($bulan,$tahun,$jenis);
            $list_bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
            $total_beli=$this->get_total_pembelian($list_nota_beli);
            $tanpa_hutang=0;
            if($detail_toko->pembayaran_hutang==1){
                $tanpa_hutang=$this->get_tanpa_hutang($list_nota_beli);
            }
            $pdf = PDF::loadview('laporan.cetak-pembelian',['list_nota_beli'=>$list_nota_beli,'list_bulan'=>$list_bulan,'tahun'=>$tahun,'bulan'=>$bulan,'total_beli'=>$total_beli,'tanpa_hutang'=>$tanpa_hutang,'jenis'=>$jenis])->setPaper('a4', 'landscape');;

            return $pdf->stream();
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
