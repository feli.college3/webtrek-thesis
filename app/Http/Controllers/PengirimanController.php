<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Pengiriman;
use App\NotaJual;
use App\DetailToko;
use App\Pegawai;
use Validator;
use DB;
use PDF;
use FarhanWazir\GoogleMaps\GMaps;

class PengirimanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected $sensor=true;
    public $directionsWaypointsOptimize = true;


    public function index()
    {
        $pegawai=$this->get_id_pegawai();
        $role = Auth::user()->roles->first()->nama;
        if($role=='kurir'){
            $list_pengiriman = Pengiriman::select('nota_jual.no_nota_jual','nota_jual.metode_pembayaran','nota_jual.grand_total','pelanggan.*','pengiriman.*')
                ->join('nota_jual', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
                ->where('pengiriman.status_kirim','=','Akan Dikirim')
                ->where('pengiriman.idpegawai','=',$pegawai)
                ->get();
            return view('kurir.index-pengiriman', ['list_pengiriman'=>$list_pengiriman]);
        }
        else if($role=='kasir'||$role=='manajer'){
            $list_nota_jual = NotaJual::select('nota_jual.*','pengiriman.alamat','pelanggan.*','pengiriman.keterangan as keterangan_pengiriman')
                ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
                ->leftJoin('pengiriman', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                ->where('nota_jual.status_transaksi','=','Siap Dikirim')
                ->get();
            $list_kurir = Pegawai::select('*')
                ->join('jabatan', 'jabatan.idjabatan', '=', 'pegawai.idjabatan')
                ->where('jabatan.nama_jabatan','=','Kurir')
                ->get();
            return view('pengiriman.index-pengiriman', ['list_nota_jual'=>$list_nota_jual,'list_kurir'=>$list_kurir]);
        }
        else{
            return redirect()->back();
        }
    }

    public function get_id_pegawai(){
        $username = Auth::user()->username;
        $pegawai = Pegawai::select('*')
            ->where('username', '=', $username)
            ->first();
        return $pegawai->idpegawai;
    }
    
    public function berlangsung()
    {
        $pegawai=$this->get_id_pegawai();
        $list_pengiriman = Pengiriman::select('nota_jual.no_nota_jual','nota_jual.metode_pembayaran','nota_jual.grand_total','pelanggan.nama_pelanggan','pengiriman.alamat','pengiriman.idpegawai','pengiriman.keterangan')
            ->join('nota_jual', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
            ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
            ->where('pengiriman.status_kirim','Sedang Dikirim')
            ->where('pengiriman.idpegawai','=',$pegawai)
            ->get();
        return view('kurir.kirim-berlangsung', ['list_pengiriman'=>$list_pengiriman]);
    }

    public function selesai()
    {
        $pegawai=$this->get_id_pegawai();
        $list_pengiriman = Pengiriman::select('nota_jual.no_nota_jual','nota_jual.metode_pembayaran','nota_jual.grand_total','pelanggan.nama_pelanggan','pengiriman.alamat','pengiriman.idpegawai','pengiriman.keterangan','pengiriman.nama_penerima')
            ->join('nota_jual', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
            ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
            ->where('pengiriman.status_kirim','Sudah Dikirim')
            ->where('pengiriman.idpegawai','=',$pegawai)
            ->orderBy('pengiriman.tanggal_kirim','desc')
            ->get();
        // dd($list_pengiriman);
        return view('kurir.kirim-selesai', ['list_pengiriman'=>$list_pengiriman]);
    }

    public function terima_kirim()
    {
        $pegawai=$this->get_id_pegawai();
        $list_nota = Pengiriman::select('*')
            ->where('pengiriman.status_kirim','Akan Dikirim')
            ->where('pengiriman.idpegawai','=',$pegawai)
            ->get();
        date_default_timezone_set('Asia/Jakarta');
        $tanggal = date('Y-m-d H:i:s');
        foreach($list_nota as $nota){
            $nota->status_kirim='Sedang Dikirim';
            $nota->tanggal_kirim=$tanggal;
            $nota->save();
        }
        return $this->rute_pengiriman();
    }

    public function sukses_dikirim($no_nota,$penerima)
    {
        $nota = Pengiriman::select('*')
            ->where('pengiriman.no_nota_jual',$no_nota)
            ->first();
        $nota->status_kirim='Sudah Dikirim';
        $nota->save();
        $pengiriman=Pengiriman::find($no_nota);
        $pengiriman->nama_penerima=$penerima;
        $pengiriman->save();
        return $this->rute_pengiriman();
    }

    public function rute_pengiriman()
    {
        $pegawai=$this->get_id_pegawai();
        $list_nota = Pengiriman::select('nota_jual.no_nota_jual','nota_jual.metode_pembayaran','nota_jual.grand_total','pelanggan.nama_pelanggan','pengiriman.alamat','pengiriman.idpegawai','pengiriman.keterangan')
            ->join('nota_jual', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
            ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
            ->where('pengiriman.status_kirim','Sedang Dikirim')
            ->where('pengiriman.idpegawai','=',$pegawai)
            ->get();
        $detail_toko=DetailToko::all();
        $alamat_toko=$detail_toko[0]->alamat_toko;
        $config['center'] = $alamat_toko;
        $config['zoom'] = 'auto';
        $config['map_height'] = '400px';
        $gmap = new GMaps();
        $gmap->initialize($config);

        foreach($list_nota as $nota){
            $marker['position']=$nota->alamat;
            $marker['infowindow_content']=$nota->no_nota_jual;
            $marker['icon']="http://maps.google.com/mapfiles/kml/paddle/red-circle.png";
            $marker['icon_scaledSize']='50,50';
            $gmap->add_marker($marker);
        }

        $marker['position']=$alamat_toko;
        $marker['infowindow_content']='Posisi Sekarang\n'.$alamat_toko;
        $marker['icon']="http://maps.google.com/mapfiles/kml/paddle/blu-circle.png";
        $marker['icon_scaledSize']='50,50';
        $gmap->add_marker($marker);

        $map = $gmap->create_map();
        return view('kurir.rute-pengiriman', ['list_nota'=>$list_nota,'map'=>$map]);
    }

    public function kirim_sekarang($no_nota)
    {
        $nota = Pengiriman::select('nota_jual.no_nota_jual','nota_jual.metode_pembayaran','pelanggan.nama_pelanggan','nota_jual.grand_total','pengiriman.alamat','pengiriman.tanggal_kirim','pengiriman.status_kirim','pengiriman.keterangan')
            ->join('nota_jual', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
            ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
            ->where('pengiriman.no_nota_jual',$no_nota)
            ->first();
        $config['zoom'] = '14';
        $config['map_height'] = '400px';
        $config['directions'] = true;
        $config['directionsStart'] = 'auto';
        $config['directionsEnd'] = $nota->alamat;
        $config['directionsDivID'] =  'directions';
        $gmap = new GMaps();
        $gmap->initialize($config);

        $map = $gmap->create_map();
        return view('kurir.detail-rute', ['nota'=>$nota,'map'=>$map]);
    }

    public function pengiriman_hari_ini(){
        date_default_timezone_set('Asia/Jakarta');
        $tanggal = date('Y-m-d');
        $list_nota = Pengiriman::select('nota_jual.no_nota_jual','pelanggan.nama_pelanggan','nota_jual.grand_total','pegawai.nama_pegawai','pengiriman.alamat','pengiriman.tanggal_kirim','pengiriman.status_kirim')
            ->join('nota_jual', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
            ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
            ->join('pegawai', 'pegawai.idpegawai', '=', 'pengiriman.idpegawai')
            ->where('pengiriman.tanggal_kirim', 'like', $tanggal.'%')
            ->orderBy('pengiriman.status_kirim','asc')
            ->get();
        
        return view('pengiriman.hari-ini', ['list_nota'=>$list_nota]);
    }

    public function selesai_dikirim_kurir(){
        $list_nota = Pengiriman::select('nota_jual.no_nota_jual','pelanggan.nama_pelanggan','nota_jual.grand_total','pegawai.nama_pegawai','pengiriman.alamat','pengiriman.tanggal_kirim','pengiriman.status_kirim')
            ->join('nota_jual', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
            ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
            ->join('pegawai', 'pegawai.idpegawai', '=', 'pengiriman.idpegawai')
            ->where('pengiriman.status_kirim', '=', 'Sudah Dikirim')
            ->where('nota_jual.status_transaksi','=','Dikirim')
            ->get();
        
        return view('pengiriman.selesai-dikirim', ['list_nota'=>$list_nota]);
    }

    public function pesanan_selesai($id){
        $nota_jual=NotaJual::find($id);
        $nota_jual->status_pembayaran='Lunas';
        $nota_jual->status_transaksi='Selesai';
        $nota_jual->save();
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $total_nota=$request->get('total_nota');
        $array_nota=array();
        $kurir=$request->get('kurir');
        date_default_timezone_set('Asia/Jakarta');
        $tanggal = date('Y-m-d H:i:s');
        for($i=0;$i<$total_nota;$i++){
            $index='nota'.$i;
            if (isset($request->$index)) {
                $nomor_nota = $request->input($index);
                $nota=NotaJual::select('*')
                    ->leftJoin('pengiriman', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                    ->where('nota_jual.no_nota_jual','=',$nomor_nota)
                    ->first();
                $nota->status_transaksi='Dikirim';
                $nota->save();
                $nota=Pengiriman::select('*')
                    ->join('nota_jual', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                    ->where('nota_jual.no_nota_jual','=',$nomor_nota)
                    ->first();
                $nota->status_kirim='Akan Dikirim';
                $nota->tanggal_kirim=$tanggal;
                $nota->idpegawai=$kurir;
                $nota->save();
            }
        }
        $produk_toko=NotaJual::select('detail_nota_jual.idproduk','produk.nama_produk','detail_nota_jual.tanggal_kadaluwarsa', 'stok_produk.lokasi', DB::raw('sum(detail_nota_jual.jumlah) as jumlah_toko'))
            ->join('pengiriman', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
            ->join('detail_nota_jual', 'nota_jual.no_nota_jual', '=', 'detail_nota_jual.no_nota_jual')
            ->join('produk', 'produk.idproduk', '=', 'detail_nota_jual.idproduk')
            ->join('stok_produk', function($join)
                {
                    $join->on('detail_nota_jual.idproduk', '=',  'stok_produk.idproduk');
                    $join->on('detail_nota_jual.tanggal_kadaluwarsa','=', 'stok_produk.tanggal_kadaluwarsa');
                })
            ->where('pengiriman.status_kirim','=','Akan Dikirim')
            ->groupBy('detail_nota_jual.idproduk')
            ->groupBy('detail_nota_jual.tanggal_kadaluwarsa')
            ->get();
        $produk_konsinyasi=NotaJual::select('detail_jual_konsinyasi.idproduk_konsinyasi','produk_konsinyasi.nama_produk', DB::raw('sum(detail_jual_konsinyasi.jumlah) as jumlah_konsi'))
            ->join('pengiriman', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
            ->join('detail_jual_konsinyasi', 'nota_jual.no_nota_jual', '=', 'detail_jual_konsinyasi.no_nota_jual')
            ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'detail_jual_konsinyasi.idproduk_konsinyasi')
            ->where('pengiriman.status_kirim','=','Akan Dikirim')
            ->groupBy('detail_jual_konsinyasi.idproduk_konsinyasi')
            ->get();
        $pegawai=$this->get_id_pegawai();
        $detail_toko=DetailToko::first();
        $pdf = PDF::loadview('pengiriman.cetak-produk',['produk_toko'=>$produk_toko,'produk_konsinyasi'=>$produk_konsinyasi,'pegawai'=>$pegawai,'tanggal'=>$tanggal,'detail_toko'=>$detail_toko])->setPaper('a4', 'portrait');
        return $pdf->stream();
        // return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
