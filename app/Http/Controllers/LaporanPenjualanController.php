<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\DetailToko;
use App\NotaJual;
use PDF;

class LaporanPenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }
    
    public function index()
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $list_nota_jual = NotaJual::select('nota_jual.*', 'pelanggan.idpelanggan', 'pelanggan.nama_pelanggan', 'pegawai.idpegawai', 'pegawai.nama_pegawai')
                        ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
                        ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_jual.idpegawai_kasir')
                        ->orderBy('nota_jual.tanggal', 'desc')
                        ->get();
            $bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
            $tahun = NotaJual::selectRaw('substr(tanggal,1,4) as tahun')->pluck('tahun')->unique();
            return view('laporan.index-penjualan', ['list_nota_jual'=>$list_nota_jual,'bulan'=>$bulan,'tahun'=>$tahun]);
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // protected $list_nota_jual=null;

    public function get_nota($bulan,$tahun,$jenis){
        $bulan_index=$bulan;
        $bulanmin=$bulan;
        $bulanplus=$bulan+1;
        $tahunmin=$tahun;
        $tahunplus=$tahun;
        if($bulan==12){
            $bulanplus=1;
            $tahunplus++;
        }
        if($bulan<10){
            $bulan='0'.$bulan;
        }
        if($bulanmin<10){
            $bulanmin='0'.$bulanmin;
        }
        if($bulanplus<10){
            $bulanplus='0'.$bulanplus;
        }
        $from = date($tahunmin.$bulanmin.'01');
        $to = date($tahunplus.$bulanplus.'01');

        if($jenis=='Semua'){
            $list_nota_jual = NotaJual::select('nota_jual.*', 'pelanggan.idpelanggan', 'pelanggan.nama_pelanggan', 'pegawai.idpegawai', 'pegawai.nama_pegawai')
                    ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
                    ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_jual.idpegawai_kasir')
                    ->whereBetween('tanggal', [$from, $to])
                    ->orderBy('nota_jual.tanggal', 'asc')
                    ->get();
        }
        else if($jenis=='Lunas'||$jenis=='Belum Lunas'){
            $list_nota_jual = NotaJual::select('nota_jual.*', 'pelanggan.idpelanggan', 'pelanggan.nama_pelanggan', 'pegawai.idpegawai', 'pegawai.nama_pegawai')
                    ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
                    ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_jual.idpegawai_kasir')
                    ->where('nota_jual.status_pembayaran','=',$jenis)
                    ->whereBetween('tanggal', [$from, $to])
                    ->orderBy('nota_jual.tanggal', 'asc')
                    ->get();
        }
        else if($jenis=='Jatuh Tempo'){
            $list_nota_jual = NotaJual::select('nota_jual.*', 'pelanggan.idpelanggan', 'pelanggan.nama_pelanggan', 'pegawai.idpegawai', 'pegawai.nama_pegawai')
                    ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
                    ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_jual.idpegawai_kasir')
                    ->where('nota_jual.status_pembayaran','=','Belum Lunas')
                    ->whereBetween('tanggal_jatuh_tempo', [$from, $to])
                    ->orderBy('nota_jual.tanggal', 'asc')
                    ->get();
        }
        return $list_nota_jual;
    }

    public function get_total_penjualan($list_nota_jual){
        $total_jual=0;
        foreach ($list_nota_jual as $nota) {
            $total_jual+=$nota->grand_total;
        }
        return $total_jual;
    }

    public function get_tanpa_piutang($list_nota_jual){
        $tanpa_piutang=0;
        foreach ($list_nota_jual as $nota) {
            $tanpa_piutang+=$nota->grand_total;
            $tanpa_piutang-=$nota->sisa_piutang;
        }
        return $tanpa_piutang;
    }

    public function store(Request $request)
    {
        $detail_toko=DetailToko::select('*')->first();
        $bulan=$request->get('bulan');
        $tahun=$request->get('tahun');
        $jenis='Semua';
        if($detail_toko->pembayaran_piutang==1){
            $jenis=$request->get('jenis');
        }
        $list_nota_jual=$this->get_nota($bulan,$tahun,$jenis);
        $total_jual=$this->get_total_penjualan($list_nota_jual);
        $tanpa_piutang=0;
        if($detail_toko->pembayaran_piutang==1){
            $tanpa_piutang=$this->get_tanpa_piutang($list_nota_jual);
        }
        $list_bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        return view('laporan.laporan-penjualan', ['list_nota_jual'=>$list_nota_jual,'list_bulan'=>$list_bulan,'tahun'=>$tahun,'bulan'=>$bulan,'total_jual'=>$total_jual,'tanpa_piutang'=>$tanpa_piutang,'jenis'=>$jenis]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cetak($tahun,$bulan,$jenis)
    {
        $role=$this->get_role();
        if($role=='manajer'){
            $detail_toko=DetailToko::select('*')->first();
            $list_nota_jual=$this->get_nota($bulan,$tahun,$jenis);
            $list_bulan=array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
            $total_jual=$this->get_total_penjualan($list_nota_jual);
            $tanpa_piutang=0;
            if($detail_toko->pembayaran_piutang==1){
                $tanpa_piutang=$this->get_tanpa_piutang($list_nota_jual);
            }
            $pdf = PDF::loadview('laporan.cetak-penjualan',['list_nota_jual'=>$list_nota_jual,'list_bulan'=>$list_bulan,'tahun'=>$tahun,'bulan'=>$bulan,'total_jual'=>$total_jual,'tanpa_piutang'=>$tanpa_piutang,'jenis'=>$jenis])->setPaper('a4', 'landscape');;

            return $pdf->stream();
        }
        else{
            return redirect()->back();
        }
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
