<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use DB;
use PDF;
use App\NotaJual;
use App\DetailNotaJual;
use App\DetailJualKonsinyasi;
use App\Supplier;
use App\Pegawai;
use App\Pelanggan;
use App\Produk;
use App\ProdukKonsinyasi;
use App\StokProduk;
use App\DetailToko;
use App\Pengiriman;
use App\PenyesuaianStok;
use Validator;


class NotaJualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_role(){
        $role = Auth::user()->roles->first()->nama;
        return $role;
    }

    public function index()
    {
        $list_nota_jual = NotaJual::select('nota_jual.*', 'pelanggan.idpelanggan', 'pelanggan.nama_pelanggan', 'pegawai.idpegawai', 'pegawai.nama_pegawai')
                    ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
                    ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_jual.idpegawai_kasir')
                    // ->orderBy('nota_jual.tanggal_jatuh_tempo','asc')
                    ->orderBy('nota_jual.tanggal', 'desc')
                    ->get();
        // dd($list_nota_jual);
        return view('nota-jual.index-nota-jual', ['list_nota_jual'=>$list_nota_jual]);
    }

    public function nota_pelanggan($kategori)
    {
        $username = Auth::user()->username;
        $role = Auth::user()->roles->first()->nama;
        if($role!='kasir'&&$role!='kurir'&&$role!='manajer'){
            $pelanggan = Pelanggan::select('idpelanggan')
                ->where('username', '=', $username)
                ->get();
            $idpelanggan=$pelanggan[0]->idpelanggan;
            $status='';
            $status1='';
            $status2='';
            $status3='';
            if($kategori=='diproses'){
                $status='Belum Diproses';
                $status1='Belum Divalidasi';
                $status2='Diproses';
                $status3='Siap Dikirim';
            }
            else if($kategori=='dikirim'){
                $status='Dikirim';
            }
            else if($kategori=='siap-diambil'){
                $status='Siap Diambil';
            }
            else if($kategori=='selesai'){
                $status='Selesai';
            }
            else if($kategori=='dibatalkan'){
                $status='Dibatalkan';
            }
            else if($kategori=='belum-bayar'){
                $status='Belum Dibayar';
                $status1='Bukti Pembayaran Tidak Valid';
            }
            // $list_nota_jual = NotaJual::select(DB::raw("nota_jual.*, (SELECT p.* FROM detail_nota_jual d inner join produk p on d.idproduk =p.idproduk where d.no_nota_jual=nota_jual.no_nota_jual LIMIT 1)"))
            $list_nota_jual = NotaJual::select('nota_jual.*',DB::raw('(select produk.nama_foto from detail_nota_jual inner join produk on detail_nota_jual.idproduk=produk.idproduk where detail_nota_jual.no_nota_jual = nota_jual.no_nota_jual limit 1) as foto_toko, (select produk.ekstensi from detail_nota_jual inner join produk on detail_nota_jual.idproduk=produk.idproduk where detail_nota_jual.no_nota_jual = nota_jual.no_nota_jual limit 1) as ekstensi_toko, (select produk_konsinyasi.nama_foto from detail_jual_konsinyasi inner join produk_konsinyasi on detail_jual_konsinyasi.idproduk_konsinyasi=produk_konsinyasi.idproduk where detail_jual_konsinyasi.no_nota_jual = nota_jual.no_nota_jual limit 1) as foto_konsinyasi, (select produk_konsinyasi.ekstensi from detail_jual_konsinyasi inner join produk_konsinyasi on detail_jual_konsinyasi.idproduk_konsinyasi=produk_konsinyasi.idproduk where detail_jual_konsinyasi.no_nota_jual = nota_jual.no_nota_jual limit 1) as ekstensi_konsinyasi'))
                ->where('nota_jual.idpelanggan','=',$idpelanggan)
                ->where(function($con) use ($status, $status1, $status2, $status3) {
                      $con->where('nota_jual.status_transaksi', $status)
                        ->orWhere('nota_jual.status_transaksi', $status1)
                        ->orWhere('nota_jual.status_transaksi', $status2)
                        ->orWhere('nota_jual.status_transaksi', $status3);
                  })
                ->orderBy('nota_jual.tanggal', 'desc')
                ->get();
            //tanpa produk
            // $list_nota_jual = NotaJual::select('nota_jual.*')
            //         ->where('nota_jual.idpelanggan','=',$idpelanggan)
            //         ->where(function($con) use ($status, $status1, $status2, $status3) {
            //               $con->where('nota_jual.status_transaksi', $status)
            //                 ->orWhere('nota_jual.status_transaksi', $status1)
            //                 ->orWhere('nota_jual.status_transaksi', $status2)
            //                 ->orWhere('nota_jual.status_transaksi', $status3);
            //           })
            //         ->orderBy('nota_jual.tanggal', 'desc')
            //         ->get(); 
            //tanpa produk
            // $produk_konsi = DetailJualKonsinyasi::select('produk_konsinyasi.*')
            //     ->join('nota_jual', 'produk.idproduk', '=', 'detail_nota_jual.idproduk')

            // $results = DB::select( DB::raw("SELECT n.no_nota_jual, min(dk.idproduk_konsinyasi), pk.nama_produk FROM nota_jual n left join detail_jual_konsinyasi dk on n.no_nota_jual = dk.no_nota_jual left join produk_konsinyasi pk on dk.idproduk_konsinyasi = pk.idproduk where pk.nama_produk is not null group by n.no_nota_jual") );
            // dd($results);
            // $list_nota_jual = NotaJual::select('nota_jual.*', 'pelanggan.nama_pelanggan', 'detail_nota_jual.*', 'detail_jual_konsinyasi.*')
            //         ->leftJoin('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
            //         ->leftJoin('detail_nota_jual', 'detail_nota_jual.no_nota_jual', '=', 'nota_jual.no_nota_jual')
            //         ->leftJoin('detail_jual_konsinyasi', 'detail_jual_konsinyasi.no_nota_jual', '=', 'nota_jual.no_nota_jual')
            //         ->where('nota_jual.idpelanggan','=',$pelanggan[0]->idpelanggan)
            //         ->where('nota_jual.keterangan','=', $status)
            //         ->orderBy('nota_jual.tanggal', 'desc')
            //         ->get(); 

            // $list_nota_jual = NotaJual::select('*')
            //             ->get();
            // $idproduk='';
            // foreach($list_nota_jual as $nota){
            //     $idproduk = $nota->$idproduk[0];
            // }
            // dd($list_nota_jual);
            return view('customer.pesanan', ['list_nota_jual'=>$list_nota_jual, 'status'=>$status]);
        }
        else{
            return redirect()->back();
        }
    }

    public function nota_admin($kategori){
        $status='';
        $status1='';
        $status2='';
        $status3='';
        if($kategori=='belum-diproses'){
            $status='Belum Diproses';
            $status1='Belum Dibayar';
            $status2='Belum Divalidasi';
            $status3='Bukti Pembayaran Tidak Valid';
        }
        else if($kategori=='diproses'){
            $status='Diproses';
        }
        else if($kategori=='siap-dikirim'){
            $status='Siap Dikirim';
        }
        else if($kategori=='dikirim'){
            $status='Dikirim';
        }
        else if($kategori=='selesai'){
            $status='Selesai';
        }
        else if($kategori=='belum-diambil'){
            $status='Siap Diambil';
        }
        else if($kategori=='dibatalkan'){
            $status='Dibatalkan';
        }
        // else if($kategori=='belum-bayar'){
        //     $status='Belum Dibayar';
        // }
        $list_nota_jual = NotaJual::select('*',DB::raw('nota_jual.no_nota_jual as nomor_nota'))
                ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
                ->join('pegawai', 'pegawai.idpegawai', '=', 'nota_jual.idpegawai_kasir')
                ->where('nota_jual.status_transaksi','=', $status)
                ->orWhere('nota_jual.status_transaksi','=', $status1)
                ->orWhere('nota_jual.status_transaksi','=', $status2)
                ->orWhere('nota_jual.status_transaksi','=', $status3)
                ->orderBy('nota_jual.tanggal', 'asc')
                ->get();
        if($kategori=='dikirim'){
            $list_nota_jual = NotaJual::select('*',DB::raw('nota_jual.no_nota_jual as nomor_nota'),'pegawai.nama_pegawai','pengiriman.status_kirim','pengiriman.tanggal_kirim')
                ->join('pengiriman', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                ->join('pelanggan', 'pelanggan.idpelanggan', '=', 'nota_jual.idpelanggan')
                ->join('pegawai', 'pegawai.idpegawai', '=', 'pengiriman.idpegawai')
                ->where('nota_jual.status_transaksi','=', 'Dikirim')
                ->where('pengiriman.status_kirim','=', 'Sedang Dikirim')
                ->orderBy('nota_jual.tanggal', 'asc')
                ->get();
            return view('transaksi-online.index-transaksi', ['list_nota_jual'=>$list_nota_jual, 'status'=>$status]);
        }
        else{
            return view('transaksi-online.index-transaksi', ['list_nota_jual'=>$list_nota_jual, 'status'=>$status]);
        }
    }

    public function detail_pesanan($nonota)
    {
        $produk_toko = DetailNotaJual::select('produk.idproduk','produk.nama_produk', 'produk.deskripsi', 'produk.nama_foto', 'produk.ekstensi')
            ->join('produk', 'produk.idproduk', '=', 'detail_nota_jual.idproduk')
            ->where('detail_nota_jual.no_nota_jual', '=', $nonota)
            ->groupBy('detail_nota_jual.idproduk')
            ->orderBy('detail_nota_jual.idproduk')
            ->get();
        $jumlah_produk_toko = DetailNotaJual::select(DB::raw("sum(detail_nota_jual.jumlah) as jumlah_produk"))
            ->where('detail_nota_jual.no_nota_jual', '=', $nonota)
            ->groupBy('detail_nota_jual.idproduk')
            ->orderBy('detail_nota_jual.idproduk')
            ->get();
        $harga_jual = DetailNotaJual::select(DB::raw("distinct detail_nota_jual.idproduk, detail_nota_jual.harga_jual"))
            ->where('detail_nota_jual.no_nota_jual', '=', $nonota)
            ->orderBy('detail_nota_jual.idproduk')
            ->get();
        $produk_konsinyasi = DetailJualKonsinyasi::select('detail_jual_konsinyasi.*', 'produk_konsinyasi.nama_produk', 'produk_konsinyasi.deskripsi', 'produk_konsinyasi.nama_foto', 'produk_konsinyasi.ekstensi')
            ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'detail_jual_konsinyasi.idproduk_konsinyasi')
            ->where('detail_jual_konsinyasi.no_nota_jual', '=', $nonota)
            ->get();

        $subtotal_toko = DetailNotaJual::select(DB::raw("sum(detail_nota_jual.jumlah*detail_nota_jual.harga_jual) as subtotal"))
            ->join('produk', 'produk.idproduk', '=', 'detail_nota_jual.idproduk')
            ->where('detail_nota_jual.no_nota_jual', '=', $nonota)
            ->groupBy('detail_nota_jual.idproduk')
            ->get();

        $subtotal_konsinyasi = DetailJualKonsinyasi::select(DB::raw("sum(detail_jual_konsinyasi.jumlah*detail_jual_konsinyasi.harga_jual) as subtotal"))
        ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'detail_jual_konsinyasi.idproduk_konsinyasi')
        ->where('detail_jual_konsinyasi.no_nota_jual', '=', $nonota)
        ->groupBy('detail_jual_konsinyasi.idproduk_konsinyasi')
        ->get();
        $total_awal=0;
        foreach($subtotal_toko as $produk){
            $total_awal+=$produk->subtotal;
        }
        foreach($subtotal_konsinyasi as $produk){
            $total_awal+=$produk->subtotal;
        }
        $nota_jual = NotaJual::select('nota_jual.*','pengiriman.*','nota_jual.keterangan as keterangan_pesanan')
                    ->leftJoin('pengiriman', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                    ->where('nota_jual.no_nota_jual','=',$nonota)
                    ->get();
        if(count($produk_toko)<1){
            $produk_toko=null;
            // return view('customer.detail-pesanan', ['produk_konsinyasi'=>$produk_konsinyasi, 'nonota'=>$nonota,'subtotal_konsinyasi'=>$subtotal_konsinyasi,'nota_jual'=>$nota_jual[0],'total_keranjang'=>$total_awal]);
        }
        else if(count($produk_konsinyasi)<1){
            $produk_konsinyasi=null;
        }
        return view('customer.detail-pesanan', ['produk_toko'=>$produk_toko, 'jumlah_produk_toko'=>$jumlah_produk_toko, 'harga_jual'=>$harga_jual, 'produk_konsinyasi'=>$produk_konsinyasi,'nonota'=>$nonota,'subtotal_toko'=>$subtotal_toko,'subtotal_konsinyasi'=>$subtotal_konsinyasi,'nota_jual'=>$nota_jual[0],'total_keranjang'=>$total_awal]);
    }

    public function dashboard_admin(){
        $detail_toko=DetailToko::select('*')->first();
        if($detail_toko==null){
            return view('index.pengaturan-awal');
        }
        else{
            $nota_belum_diproses=NotaJual::select(DB::raw('count(nota_jual.no_nota_jual) as jumlah_nota'))
                    ->where('status_transaksi','=','Belum Diproses')
                    ->orWhere('status_transaksi','=','Belum Dibayar')
                    ->orWhere('status_transaksi','=','Belum Divalidasi')
                    ->first();
            $nota_belum_diproses=$nota_belum_diproses->jumlah_nota;
            $nota_belum_dikirim=NotaJual::select(DB::raw('count(nota_jual.no_nota_jual) as jumlah_nota'))
                        ->where('status_transaksi','=','Siap Dikirim')
                        ->first();
            $nota_belum_dikirim=$nota_belum_dikirim->jumlah_nota;
            $nota_belum_diambil=NotaJual::select(DB::raw('count(nota_jual.no_nota_jual) as jumlah_nota'))
                        ->where('status_transaksi','=','Siap Diambil')
                        ->first();
            $nota_belum_diambil=$nota_belum_diambil->jumlah_nota;

            // date_default_timezone_set('Asia/Jakarta');
            // $tanggal = date('Y-m-d');
            // $tanggal = date('Y-m-d', strtotime("+6 months", strtotime($tanggal)));
            
            $list_produk=Produk::select('*')
                        ->leftJoin('stok_produk', 'stok_produk.idproduk', '=', 'produk.idproduk')
                        ->where('stok_produk.stok','>',0)
                        ->orderBy('stok_produk.tanggal_kadaluwarsa','ASC')
                        ->get();
            $jumlah_list_produk=count($list_produk);
            $jumlah_produk=Produk::select(DB::raw('count(produk.idproduk) as total'))->first();
            $jumlah_produk=$jumlah_produk->total;
            $jum=$jumlah_produk;
            if($jumlah_produk>10){
                $jum=10;
            }
            $produk_terlaris=Produk::select(DB::raw('sum(detail_nota_jual.jumlah) as jumlah_terjual'),'produk.nama_produk')
                        ->leftJoin('detail_nota_jual', 'detail_nota_jual.idproduk', '=', 'produk.idproduk')
                        ->groupBy('produk.idproduk')
                        ->orderBy('jumlah_terjual','DESC')
                        ->take($jum)
                        ->get();

            $produk_sedikit = Produk::select(DB::raw('SUM(stok_produk.stok) as total_stok'), 'produk.nama_produk')
                ->leftjoin('stok_produk','stok_produk.idproduk','=','produk.idproduk')
                ->groupBy('produk.idproduk')
                ->orderBy('total_stok','ASC')
                ->get();

            $produk_restock = Produk::select(DB::raw('SUM(stok_produk.stok) as total_stok'),'produk.stok_minimal')
                ->leftjoin('stok_produk','stok_produk.idproduk','=','produk.idproduk')
                ->join('kategori', 'produk.idkategori', '=', 'kategori.idkategori')
                ->groupBy('produk.idproduk')
                ->get();
            $perlu_restock=array();
            foreach($produk_restock as $produk){
                if($produk->total_stok<=$produk->stok_minimal){
                    array_push($perlu_restock, $produk);
                }
            }

            //pesanan dibatalkan
            $batas_waktu=(int)$detail_toko->maksimal_pembatalan;
            $batas_detik=$batas_waktu*60*60;
            date_default_timezone_set('Asia/Jakarta');
            $sekarang = date('Y-m-d H:i:s');
            $pesanan_dibatalkan=NotaJual::where(function($con){
                    $con->where('nota_jual.status_transaksi', 'Belum Dibayar')
                        ->orWhere('nota_jual.status_transaksi', 'Bukti Pembayaran Tidak Valid')
                        ->orWhere('nota_jual.status_transaksi', 'Siap Diambil');
                    })
                ->count();

            $validasi_penyesuaian = PenyesuaianStok::select(DB::raw('count(idpenyesuaian) as total'))
                ->where('status','=','Menunggu Validasi')
                ->first();

            $jumlah_panel=3;
            if($detail_toko->fitur_kirim==1){
                $jumlah_panel++;
            }
            if($detail_toko->fitur_pembatalan_pesanan==1){
                $jumlah_panel++;
            }
            return view('index.dashboard',['nota_belum_diproses'=>$nota_belum_diproses,'nota_belum_dikirim'=>$nota_belum_dikirim,'nota_belum_diambil'=>$nota_belum_diambil,'list_produk'=>$list_produk,'jumlah_list_produk'=>$jumlah_list_produk,'produk_terlaris'=>$produk_terlaris,'produk_sedikit'=>$produk_sedikit,'perlu_restock'=>$perlu_restock,'pesanan_dibatalkan'=>$pesanan_dibatalkan,'validasi_penyesuaian'=>$validasi_penyesuaian,'jumlah_panel'=>$jumlah_panel]);
        }
    }

    // public function checkout()
    // {
    //     return view('customer.checkout');
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function validitas_bukti($nota, $valid)
    {
        $nota = NotaJual::find($nota);
        if($valid=='ya'){
            $nota->status_pembayaran = 'Lunas';
            $nota->status_transaksi = 'Belum Diproses';
        }
        else if($valid=='tidak'){
            $nota->status_transaksi = 'Bukti Pembayaran Tidak Valid';
        }
        $nota->save();
        return redirect()->back();
    }

    public function terima_pesanan($nota)
    {
        $username = Auth::user()->username;
        $pegawai = Pegawai::select('idpegawai')
            ->where('username', '=', $username)
            ->first();
        $nota = NotaJual::find($nota);
        $nota->status_transaksi = 'Diproses';
        $nota->idpegawai_kasir = $pegawai->idpegawai;
        $nota->save();
        return redirect()->back();
    }

    public function tolak_pesanan($nota)
    {
        $nota = NotaJual::select('nota_jual.*','pelanggan.nama_pelanggan')
            ->join('pelanggan','pelanggan.idpelanggan','=','nota_jual.idpelanggan')
            ->where('nota_jual.no_nota_jual','=',$nota)
            ->first();
        return view('transaksi-online.penolakan-pesanan',['nota'=>$nota]);
        // $nota = NotaJual::find($nota);
        // $nota->status_transaksi = 'Dibatalkan';
        // $nota->keterangan = 'Dibatalkan oleh penjual karena stok tidak mencukupi.';
        // $nota->save();
        // return redirect()->back();
    }
    public function final_tolak_pesanan(Request $request)
    {
        $username = Auth::user()->username;
        $pegawai = Pegawai::select('idpegawai')
            ->where('username', '=', $username)
            ->first();
        $no_nota=$request->get('no_nota');
        $keterangan=$request->get('alasan');
        $nota = NotaJual::find($no_nota);
        if($keterangan==1){
            $keterangan='Dibatalkan oleh penjual karena stok tidak mencukupi.';
        }
        else if($keterangan==2||$keterangan==4){
            $keterangan='Dibatalkan oleh penjual karena toko sedang tidak menerima pesanan.';
        }
        else if($keterangan==3){
            $keterangan='Dibatalkan oleh penjual karena terdapat kendala dalam pengiriman pesanan.';
        }
        else if($keterangan==5){
            $keterangan='Dibatalkan atas permintaan pelanggan.';
        }
        else if($keterangan==0){
            $keterangan='Dibatalkan oleh penjual. (Penyebab: '.$request->get('alasan_lainnya').')';
        }
        $nota->status_transaksi = 'Dibatalkan';
        $nota->keterangan = $keterangan;
        $nota->idpegawai_kasir=$pegawai->idpegawai;
        $nota->save();

        //kembalikan stok
        $list_toko = DetailNotaJual::where('no_nota_jual','=',$no_nota)->get();
        $list_konsi=DetailJualKonsinyasi::where('no_nota_jual','=',$no_nota)->get();
        if(count($list_toko)>0){
            foreach($list_toko as $toko){
                $stok=StokProduk::where('idproduk',$toko->idproduk)
                    ->where('tanggal_kadaluwarsa',$toko->tanggal_kadaluwarsa)
                    ->first();
                $stok->stok+=$toko->jumlah;
                $stok->save();
            }
        }
        if(count($list_konsi)>0){
            foreach($list_konsi as $konsi){
                $stok=ProdukKonsinyasi::where('idproduk',$konsi->idproduk_konsinyasi)
                    ->first();
                $stok->stok+=$konsi->jumlah;
                $stok->save();
            }
        }
        return redirect('detail-transaksi/'.$no_nota);
    }

    public function kirim_pesanan($nota)
    {
        $nota = NotaJual::find($nota);
        $nota->status_transaksi = 'Siap Dikirim';
        $nota->save();
        // $pengiriman=new Pengiriman([
        // 'no_nota_jual' => $nomor_nota,
        // 'idpelanggan' => $request->get('pelanggan'),
        // 'idpegawai_kasir' => $idpegawai,
        // 'tanggal' => $tanggaljam,
        // 'grand_total' => $grand_total,
        // 'status_pembayaran' => $status_pembayaran,
        // 'metode_pembayaran' => $metode_pembayaran,
        // 'tanggal_jatuh_tempo' => $tgl_kadaluwarsa,
        // 'sisa_piutang' => $sisa_piutang,
        // 'nomor_kartu' => $nomor_kartu,
        // 'status_transaksi' => 'Selesai',
        // 'keterangan' => $request->get('keterangan'),
        // 'kirim' => $kirim,
        // ]);
        // $notajual->save();
        return redirect()->back();
    }

    public function selesai_diproses($nota)
    {
        $nota = NotaJual::find($nota);
        $nota->status_transaksi = 'Siap Diambil';
        $nota->save();
        return redirect()->back();
    }

    public function sudah_diambil($nota)
    {
        $nota = NotaJual::find($nota);
        $nota->status_transaksi = 'Selesai';
        $nota->save();
        return redirect()->back();
    }

    public function perlu_dibatalkan(){
        $detail_toko=DetailToko::first();
        if($detail_toko->fitur_pembatalan_pesanan==1){
            $batas_waktu=(int)($detail_toko->maksimal_pembatalan);
            $batas_detik=$batas_waktu*60*60;
            date_default_timezone_set('Asia/Jakarta');
            $sekarang = date('Y-m-d H:i:s');
            $nota_jual=NotaJual::where(function($con){
                    $con->where('nota_jual.status_transaksi', 'Belum Dibayar')
                        ->orWhere('nota_jual.status_transaksi', 'Bukti Pembayaran Tidak Valid')
                        ->orWhere('nota_jual.status_transaksi', 'Siap Diambil');
                    })
                ->orderBy('nota_jual.tanggal','asc')
                ->get();
            $perlu_dibatalkan=array();
            foreach($nota_jual as $nota){
                $db=NotaJual::join('pelanggan','pelanggan.idpelanggan','=','nota_jual.idpelanggan')
                    ->where('nota_jual.no_nota_jual','=',$nota->no_nota_jual)
                    ->first();
                $tampung=array();
                $tampung['no_nota_jual']=$db->no_nota_jual;
                $tampung['tanggal']=$db->tanggal;
                $tampung['nama_pelanggan']=$db->nama_pelanggan;
                $tampung['grand_total']=$db->grand_total;
                $tampung['metode_pembayaran']=$db->metode_pembayaran;
                $keterangan="";
                if($nota->status_transaksi=='Belum Dibayar'||$nota->status_transaksi=='Bukti Pembayaran Tidak Valid'){
                    $keterangan='Tidak ada pembayaran valid dalam '.$batas_waktu.' jam.';
                }
                else if($nota->status_transaksi=='Siap Diambil'){
                    $keterangan='Pesanan tidak diambil dalam '.$batas_waktu.' jam.';
                }
                $tampung['keterangan']=$keterangan;
                array_push($perlu_dibatalkan, $tampung);
            }
        }
        return view('dashboard.index-dibatalkan',['perlu_dibatalkan'=>$perlu_dibatalkan]);
    }

    public function batalkan_pesanan($nota){
        $nota=NotaJual::find($nota);
        $detail_toko=DetailToko::first();
        $batas_waktu=(int)($detail_toko->maksimal_pembatalan);
        if($nota->status_transaksi=='Siap Diambil'){
            $nota->keterangan='Dibatalkan karena pesanan tidak diambil dalam '.$batas_waktu.' jam.';
        }
        else{
            $nota->keterangan='Dibatalkan karena tidak ada pembayaran valid dalam '.$batas_waktu.' jam.';
        }
        $nota->status_transaksi='Dibatalkan';
        $nota->save();

        //kembalikan produk
        $detail_nota_toko=DetailNotaJual::where('no_nota_jual','=',$nota->no_nota_jual)->get();
        $detail_nota_konsi=DetailJualKonsinyasi::where('no_nota_jual','=',$nota->no_nota_jual)->get();
        if(count($detail_nota_toko)>0){
            foreach($detail_nota_toko as $detail){
                $stok_produk=StokProduk::where('idproduk','=',$detail->idproduk)
                    ->where('tanggal_kadaluwarsa','=',$detail->tanggal_kadaluwarsa)->first();
                $stok_produk->stok+=$detail->jumlah;
                $stok_produk->save();
            }
        }
        if(count($detail_nota_konsi)>0){
            foreach($detail_nota_konsi as $konsi){
                $produk_konsi=ProdukKonsinyasi::where('idproduk','=',$konsi->idproduk_konsinyasi)->first();
                $produk_konsi->stok+=$konsi->jumlah;
                $produk_konsi->save();
            }
        }
        return redirect()->back();
    }

    public function create()
    {
        $list_pelanggan = Pelanggan::all();
        $list_produk = Produk::select(DB::raw('SUM(stok_produk.stok) as total_stok'),'produk.*')
            ->leftjoin('stok_produk','stok_produk.idproduk','=','produk.idproduk')
            ->where('produk.status','=','Dijual')
            ->groupBy('produk.idproduk')
            ->orderBy('produk.nama_produk')
            ->get();
        $list_produk_konsi = ProdukKonsinyasi::orderBy('produk_konsinyasi.nama_produk')->get();
        date_default_timezone_set('Asia/Jakarta');
        $tanggal= date('dmy');

        $total_trans = NotaJual::where('nota_jual.no_nota_jual','like', $tanggal.'%')
                     ->count();
        $id_baru=$total_trans+1;
        if($total_trans < 10){
            $id_baru = $tanggal."J00".$id_baru;
        }
        else if($total_trans >= 10){
            $id_baru = $tanggal."J0".$id_baru;
        }
        else if($total_trans >= 100){
            $id_baru = $tanggal."J".$id_baru;
        }
        $detail_toko = DetailToko::select('*')->first();
        return view('nota-jual.tambah-nota-jual', ['list_pelanggan'=>$list_pelanggan, 'list_produk'=>$list_produk, 'list_produk_konsi'=>$list_produk_konsi, 'id_baru'=>$id_baru,'detail_toko'=>$detail_toko]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $detail_toko = DetailToko::select('*')->first();
        $status_pembayaran='Lunas';
        if($detail_toko->pembayaran_piutang==1){
            $status_pembayaran=$request->get('status');
        }
        $nomor_nota=$request->get('no_nota');
        $grand_total=$request->get('grand_total');
        $grand_total_asli=$grand_total;
        $username = Auth::user()->username;
        $pegawai = Pegawai::select('idpegawai')
            ->where('username', '=', $username)
            ->first();
        $idpegawai=$pegawai->idpegawai;

        date_default_timezone_set('Asia/Jakarta');
        $tanggal= date('dmy');
        $tanggaljam = date('Y-m-d H:i:s');

        $tgl_kadaluwarsa=$tanggaljam;
        $sisa_piutang=0;
        if($status_pembayaran=='Belum Lunas'){
            $tgl_kadaluwarsa=$request->get('jatuh_tempo');
            $sisa_piutang=$grand_total;
        }
        $metode_pembayaran=$request->get('metode_pembayaran');
        $nomor_kartu="-";
        if($metode_pembayaran=='Debit'||$metode_pembayaran=='Kredit'){
            $nomor_kartu=$request->get('nomor_kartu');
        }
        
        $ongkir=0;
        $status_transaksi='Selesai';

        $kirim=0;
        if($detail_toko->fitur_kirim==1){
            $kirim=$request->get('kirim');
        }
        if($kirim=='1'){
            $ongkir=$request->get('ongkir');
            $grand_total_asli-=$ongkir;
            // $grand_total+=$ongkir;
            $status_transaksi='Siap Dikirim';
        }
        $pelanggan=$request->get('pelanggan');
        $notajual = new NotaJual([
        'no_nota_jual' => $nomor_nota,
        'idpelanggan' => $pelanggan,
        'idpegawai_kasir' => $idpegawai,
        'tanggal' => $tanggaljam,
        'grand_total' => $grand_total_asli,
        'status_pembayaran' => $status_pembayaran,
        'metode_pembayaran' => $metode_pembayaran,
        'tanggal_jatuh_tempo' => $tgl_kadaluwarsa,
        'sisa_piutang' => $sisa_piutang,
        'nomor_kartu' => $nomor_kartu,
        'status_transaksi' => $status_transaksi,
        'keterangan' => $request->get('keterangan'),
        'kirim' => $kirim,
        ]);
        $notajual->save();

        //tambah piutang
        $plg=Pelanggan::find($pelanggan);
        $plg->total_hutang+=$sisa_piutang;
        $plg->save();

        $keterangan_pengiriman=$request->get('keterangan_pengiriman');
        if($keterangan_pengiriman==""){
            $keterangan_pengiriman="-";
        }
        if($kirim=='1'){
            $pengiriman = new Pengiriman([
            'no_nota_jual' => $nomor_nota,
            'idpegawai' => 0,
            'tanggal_kirim' => $tanggaljam,
            'alamat' => $request->get('alamat'),
            'ongkos_kirim' => $ongkir,
            'nama_penerima' => '-',
            'status_kirim' => '-',
            'keterangan' => $keterangan_pengiriman,
            ]);
            $pengiriman->save();
        }

        $total_data=$request->get('total_data');
        $semua_produk_toko=Produk::all();
        $semua_produk_konsi=ProdukKonsinyasi::all();
        $produk_konsinyasi_dibeli=array();
        $produk_toko_dibeli=array();
        for($i=1;$i<=$total_data;$i++){
            $produk=$request->get('produk'.$i);
            $jumlah=$request->get('jumlah'.$i);
            $harga=$request->get('harga'.$i);
            foreach($semua_produk_toko as $toko){
                if($produk==$toko->nama_produk){
                    $tampung=array();
                    $tampung['id']=$toko->idproduk;
                    $tampung['nama']=$toko->nama_produk;
                    $tampung['jumlah']=$jumlah;
                    $tampung['harga']=$harga;
                    array_push($produk_toko_dibeli, $tampung);
                    break;
                }
            }
            foreach($semua_produk_konsi as $konsi){
                if($produk==$konsi->nama_produk){
                    $tampung=array();
                    $tampung['id']=$konsi->idproduk;
                    $tampung['jumlah']=$jumlah;
                    array_push($produk_konsinyasi_dibeli, $tampung);
                    break;
                }
            }
        }
        $semua_toko_dibeli=$produk_toko_dibeli;
        $semua_konsi_dibeli=array();
        foreach($produk_konsinyasi_dibeli as $produk_dibeli){
            $produk_terpilih = ProdukKonsinyasi::find($produk_dibeli['id']);
            $produk_terpilih->stok -= (float)($produk_dibeli['jumlah']);
            $produk_terpilih->save();
            $detail_konsi = new DetailJualKonsinyasi([
                'no_nota_jual' => $nomor_nota,
                'idproduk_konsinyasi' => $produk_dibeli['id'],
                'harga_jual' => $produk_terpilih->harga_jual,
                'jumlah' => $produk_dibeli['jumlah'],
            ]);
            $detail_konsi->save();
            $tampung=array();
            $tampung['id']=$produk_dibeli['id'];
            $tampung['nama']=$produk_terpilih->nama_produk;
            $tampung['jumlah']=$produk_terpilih->harga_jual;
            $tampung['harga']=$produk_dibeli['jumlah'];
            array_push($semua_konsi_dibeli, $tampung);
        }

        $total_stok_produk = Produk::select(DB::raw('SUM(stok_produk.stok) as total_stok'),'produk.*')
            ->leftjoin('stok_produk','stok_produk.idproduk','=','produk.idproduk')
            ->groupBy('produk.idproduk')
            ->get();

        for ($i = 0; $i < count($produk_toko_dibeli); $i++){
            $list_stok_produk = StokProduk::select('*')
                ->where('stok_produk.idproduk','=',$produk_toko_dibeli[$i]['id'])
                ->where('stok_produk.stok','>',0)
                ->orderBy('stok_produk.tanggal_kadaluwarsa','asc')
                ->get();
            foreach($total_stok_produk as $total_stok){
                if($total_stok->idproduk==$produk_toko_dibeli[$i]['id']){
                    $jumlah_dibeli = $produk_toko_dibeli[$i]['jumlah'];
                    if($jumlah_dibeli<=$total_stok->total_stok){
                        foreach($list_stok_produk as $list_stok){
                            $stok_pertanggal = $list_stok->stok;
                            if($jumlah_dibeli>0){
                                $sisa_dibeli = $jumlah_dibeli;
                                $jumlah_dibeli-=$stok_pertanggal;
                                $dibeli_pertanggal=0;
                                if($jumlah_dibeli>0){
                                    $stok_sekarang=0;
                                    $dibeli_pertanggal=$stok_pertanggal;
                                }
                                else{
                                    $stok_sekarang=$stok_pertanggal-$sisa_dibeli;
                                    $dibeli_pertanggal=$sisa_dibeli;
                                }
                                $list_stok->stok = $stok_sekarang;

                                $ubah_list_stok = StokProduk::select('*')
                                    ->where('stok_produk.idproduk','=',$list_stok->idproduk)
                                    ->where('stok_produk.tanggal_kadaluwarsa','=',$list_stok->tanggal_kadaluwarsa)
                                    ->first();
                                $ubah_list_stok->stok = $stok_sekarang;
                                $ubah_list_stok->save();

                                $detail_jual = new DetailNotaJual([
                                    'no_nota_jual' => $nomor_nota,
                                    'idproduk' => $produk_toko_dibeli[$i]['id'],
                                    'harga_jual' => $produk_toko_dibeli[$i]['harga'],
                                    'tanggal_kadaluwarsa' => $ubah_list_stok->tanggal_kadaluwarsa,
                                    'jumlah' => $dibeli_pertanggal,
                                ]);

                                $detail_jual->save();
                            }
                        }
                    }
                }
            }
        }
        // $this->cetak($nomor_nota,$semua_toko_dibeli,$semua_konsi_dibeli,$pelanggan,$grand_total_asli,$ongkir);
        return redirect('nota-jual/'.$nomor_nota);
    }

    public function cetak($nota,$toko,$konsi,$pelanggan,$grand_total,$ongkir)
    {
        //mbo ah capek huuuuuffff
        date_default_timezone_set('Asia/Jakarta');
        $tanggal= date('d m Y H:i');
        $customPaper = array(0,0,567.00,283.80);
        $pdf = PDF::loadview('nota-jual.cetak-nota-jual',['nota'=>$nota,'toko'=>$toko,'konsi'=>$konsi,'pelanggan'=>$pelanggan,'grand_total'=>$grand_total,'ongkir'=>$ongkir,'tanggal'=>$tanggal])->setPaper('a4', 'landscape');
        // return $pdf->stream();
        return redirect('/nota-jual/cetak/'.$nota.'/'.$toko.'/'.$konsi.'/'.$pelanggan.'/'.$grand_total.'/'.$ongkir);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail_toko=DetailToko::select('*')->first();
        $nota_jual = NotaJual::select('*')
            ->where('nota_jual.no_nota_jual', '=', $id)
            ->get();
        $table='detail_nota_jual';
        $produk_toko = DetailNotaJual::select('detail_nota_jual.*', 'produk.*')
            ->join('produk', 'produk.idproduk', '=', 'detail_nota_jual.idproduk')
            ->where('detail_nota_jual.no_nota_jual', '=', $id)
            ->get();
        $lokasi_produk=array();
        foreach($produk_toko as $produk){
            $lokasi = StokProduk::select('lokasi')
            ->where('idproduk', '=', $produk->idproduk)
            ->where('tanggal_kadaluwarsa', '=', $produk->tanggal_kadaluwarsa)
            ->first();
            array_push($lokasi_produk, $lokasi);
        }
        $produk_konsinyasi = DetailJualKonsinyasi::select('detail_jual_konsinyasi.*', 'produk_konsinyasi.*')
            ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'detail_jual_konsinyasi.idproduk_konsinyasi')
            ->where('detail_jual_konsinyasi.no_nota_jual', '=', $id)
            ->get();

        $role = Auth::user()->roles->first()->nama;
        if($detail_toko->fitur_stok_fifo==1){
            if($role=='kurir'){
                return view('kurir.detail-nota-jual', ['produk_toko'=>$produk_toko, 'produk_konsinyasi'=>$produk_konsinyasi, 'id'=>$id]);
            }
            else if($role=='kasir'){
                return view('nota-jual.detail-nota-jual', ['nota_jual'=>$nota_jual[0], 'produk_toko'=>$produk_toko, 'lokasi_produk'=>$lokasi_produk, 'produk_konsinyasi'=>$produk_konsinyasi, 'id'=>$id]);
            }
            else{
                return view('nota-jual.detail-nota-jual', ['nota_jual'=>$nota_jual[0], 'produk_toko'=>$produk_toko, 'lokasi_produk'=>$lokasi_produk, 'produk_konsinyasi'=>$produk_konsinyasi, 'id'=>$id]);
            }
        }

        $produk_toko = DetailNotaJual::select('produk.idproduk','produk.nama_produk', 'produk.deskripsi', 'produk.nama_foto', 'produk.ekstensi')
            ->join('produk', 'produk.idproduk', '=', 'detail_nota_jual.idproduk')
            ->where('detail_nota_jual.no_nota_jual', '=', $id)
            ->groupBy('detail_nota_jual.idproduk')
            ->orderBy('detail_nota_jual.idproduk')
            ->get();
        $jumlah_produk_toko = DetailNotaJual::select(DB::raw("sum(detail_nota_jual.jumlah) as jumlah_produk"))
            ->where('detail_nota_jual.no_nota_jual', '=', $id)
            ->groupBy('detail_nota_jual.idproduk')
            ->orderBy('detail_nota_jual.idproduk')
            ->get();
        $harga_jual = DetailNotaJual::select(DB::raw("distinct detail_nota_jual.idproduk, detail_nota_jual.harga_jual"))
            ->where('detail_nota_jual.no_nota_jual', '=', $id)
            ->orderBy('detail_nota_jual.idproduk')
            ->get();
        $produk_konsinyasi = DetailJualKonsinyasi::select('detail_jual_konsinyasi.*', 'produk_konsinyasi.nama_produk', 'produk_konsinyasi.deskripsi', 'produk_konsinyasi.nama_foto', 'produk_konsinyasi.ekstensi')
            ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'detail_jual_konsinyasi.idproduk_konsinyasi')
            ->where('detail_jual_konsinyasi.no_nota_jual', '=', $id)
            ->get();

        $nota_jual = NotaJual::select('nota_jual.*','pengiriman.*')
                    ->leftJoin('pengiriman', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
                    ->where('nota_jual.no_nota_jual','=',$id)
                    ->get();
        if(count($produk_toko)<1){
            $produk_toko=array();
        }
        else if(count($produk_konsinyasi)<1){
            $produk_konsinyasi=array();
        }
        if($detail_toko->fitur_stok_fifo==0){
            if($role=='kurir'){
                return view('kurir.detail-nota-jual', ['nota_jual'=>$nota_jual[0], 'produk_toko'=>$produk_toko,'jumlah_produk_toko'=>$jumlah_produk_toko,'harga_jual'=>$harga_jual,'produk_konsinyasi'=>$produk_konsinyasi, 'id'=>$id]);
            }
            else if($role=='kasir'){
                return view('nota-jual.detail-nota-jual', ['nota_jual'=>$nota_jual[0], 'produk_toko'=>$produk_toko,'jumlah_produk_toko'=>$jumlah_produk_toko,'harga_jual'=>$harga_jual,'produk_konsinyasi'=>$produk_konsinyasi, 'id'=>$id]);
            }
            else{
                return view('nota-jual.detail-nota-jual', ['nota_jual'=>$nota_jual[0], 'produk_toko'=>$produk_toko,'jumlah_produk_toko'=>$jumlah_produk_toko,'harga_jual'=>$harga_jual,'produk_konsinyasi'=>$produk_konsinyasi, 'id'=>$id]);
            }
        }
    }

    public function detail_transaksi($id)
    {
        $nota_jual = NotaJual::select('*')
            ->where('nota_jual.no_nota_jual', '=', $id)
            ->get();

        $produk_toko = DetailNotaJual::select('detail_nota_jual.*', 'produk.*','stok_produk.lokasi')
            ->join('produk', 'produk.idproduk', '=', 'detail_nota_jual.idproduk')
            ->join('stok_produk', function($join)
             {
               $join->on('stok_produk.idproduk', '=', 'detail_nota_jual.idproduk');
               $join->on('stok_produk.tanggal_kadaluwarsa', '=', 'detail_nota_jual.tanggal_kadaluwarsa');
             })
            ->where('detail_nota_jual.no_nota_jual', '=', $id)
            ->get();
        
        $produk_konsinyasi = DetailJualKonsinyasi::select('detail_jual_konsinyasi.*', 'produk_konsinyasi.*')
            ->join('produk_konsinyasi', 'produk_konsinyasi.idproduk', '=', 'detail_jual_konsinyasi.idproduk_konsinyasi')
            ->where('detail_jual_konsinyasi.no_nota_jual', '=', $id)
            ->get();

        return view('transaksi-online.detail-transaksi', ['nota_jual'=>$nota_jual[0], 'produk_toko'=>$produk_toko, 'produk_konsinyasi'=>$produk_konsinyasi, 'id'=>$id]);
    }

    public function detail_pengiriman($id)
    {
        $nota_jual = NotaJual::select('pengiriman.*','pegawai.*')
            ->join('pengiriman', 'pengiriman.no_nota_jual', '=', 'nota_jual.no_nota_jual')
            ->join('pegawai', 'pengiriman.idpegawai', '=', 'pegawai.idpegawai')
            ->where('nota_jual.no_nota_jual', '=', $id)
            ->first();
        return view('nota-jual.detail-pengiriman', ['nota_jual'=>$nota_jual, 'id'=>$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
