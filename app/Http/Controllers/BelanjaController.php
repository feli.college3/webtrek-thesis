<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\ProdukKonsinyasi;
use App\Kategori;
use DB;

class BelanjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk_toko = Produk::all();
        $produk_konsinyasi = ProdukKonsinyasi::all();
        $kategori = Kategori::select('*')
        ->orderBy('nama_kategori','asc')
        ->get();
        $kategori_terpilih='Semua';
        return view('customer.belanja', ['produk_toko'=>$produk_toko, 'produk_konsinyasi'=>$produk_konsinyasi, 'kategori'=>$kategori, 'kategori_terpilih'=>$kategori_terpilih]);
    }

    public function perkategori($kategori)
    {
        $produk_toko = Produk::select('*')
            ->join('kategori','kategori.idkategori','=','produk.idkategori')
            ->where('kategori.nama_kategori', '=', ucfirst($kategori))
            ->get();
        $list_kategori = Kategori::select('*')
        ->orderBy('nama_kategori','asc')
        ->get();
        $kategori_terpilih = Kategori::select('*')
        ->where('kategori.nama_kategori', '=', ucfirst($kategori))
        ->first();
        return view('customer.belanja', ['produk_toko'=>$produk_toko, 'kategori'=>$list_kategori, 'kategori_terpilih'=>$kategori_terpilih]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produk_toko = Produk::find($id);
        $produk_konsinyasi = ProdukKonsinyasi::find($id);
        $produk = null;
        if($produk_toko!=null)
            $produk = $produk_toko;
        else if($produk_konsinyasi!=null)
            $produk=$produk_konsinyasi;
        $panjang_id = strlen((string)$produk->idproduk);
        $stok = DB::table('produk')
            ->select(DB::raw('SUM(stok_produk.stok) as total_stok'))
            ->leftjoin('stok_produk','stok_produk.idproduk','=','produk.idproduk')
            ->where('produk.idproduk', '=', $id)
            ->get();
        $total_stok = $stok[0]->total_stok;
        return view('customer.detail-produk', ['produk'=>$produk, 'id'=>$id, 'panjang_id'=>$panjang_id, 'stok'=>$total_stok]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
