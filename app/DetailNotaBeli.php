<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class DetailNotaBeli extends Model
{
    protected $table = 'detail_nota_beli';
    protected $primaryKey = ['no_nota_beli','idproduk','tanggal_kadaluwarsa'];
    protected $fillable = ['no_nota_beli','idproduk','tanggal_kadaluwarsa','harga_beli','jumlah'];
    public $incrementing = false;
    public $timestamps = false;

    public function notabeli()
    {
        return $this->belongsTo('App\NotaBeli');
    }

    public function produk()
    {
        return $this->belongsTo('App\Produk');
    }

    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if(!is_array($keys)){
            return parent::setKeysForSaveQuery($query);
        }

        foreach($keys as $keyName){
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if(is_null($keyName)){
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }
}
