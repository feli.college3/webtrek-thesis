<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder; //tambahan untuk update with 2 primary keys

class DetailBeliKonsinyasi extends Model
{
    protected $table = 'detail_beli_konsinyasi';
    protected $primaryKey = ['no_nota_beli_konsinyasi','idproduk_konsinyasi'];
    protected $fillable = ['no_nota_beli_konsinyasi','idproduk_konsinyasi','harga_beli','jumlah'];
    public $incrementing = false;
    public $timestamps = false;

    public function nota_beli_konsinyasi()
    {
        return $this->belongsTo('App\NotaBeliKonsinyasi');
    }

    public function produk_konsinyasi()
    {
        return $this->belongsTo('App\ProdukKonsinyasi');
    }

    //tambahan untuk update with 2 primary keys
    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if(!is_array($keys)){
            return parent::setKeysForSaveQuery($query);
        }

        foreach($keys as $keyName){
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if(is_null($keyName)){
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }
    //tutup tambahan
}
