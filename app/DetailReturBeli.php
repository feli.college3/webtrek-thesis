<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailReturBeli extends Model
{
    protected $table = 'detail_retur_pembelian';
    protected $fillable = ['idretur_pembelian','idproduk','tanggal_kadaluwarsa_lama','jumlah','tanggal_kadaluwarsa_baru'];
    public $incrementing = false;
    public $timestamps = false;

    public function returbeli()
    {
        return $this->belongsTo('App\ReturBeli');
    }

    public function produk()
    {
        return $this->belongsTo('App\Produk');
    }
}
