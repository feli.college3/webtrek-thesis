<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class StokProduk extends Model
{
    protected $table = 'stok_produk';
    protected $primaryKey = ['tanggal_kadaluwarsa','idproduk'];
    protected $fillable = ['tanggal_kadaluwarsa','idproduk','stok','lokasi'];
    public $timestamps = false;
    public $incrementing = false;

    public function produk()
    {
        return $this->belongsTo('App\Produk');
    }


    //tambahan untuk update with 2 primary keys
    protected function setKeysForSaveQuery(Builder $query)
	{
	    $keys = $this->getKeyName();
	    if(!is_array($keys)){
	        return parent::setKeysForSaveQuery($query);
	    }

	    foreach($keys as $keyName){
	        $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
	    }

	    return $query;
	}

	/**
	 * Get the primary key value for a save query.
	 *
	 * @param mixed $keyName
	 * @return mixed
	 */
	protected function getKeyForSaveQuery($keyName = null)
	{
	    if(is_null($keyName)){
	        $keyName = $this->getKeyName();
	    }

	    if (isset($this->original[$keyName])) {
	        return $this->original[$keyName];
	    }

	    return $this->getAttribute($keyName);
	}
    //tutup tambahan
}
