<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaKembaliKonsinyasi extends Model
{
    protected $table = 'nota_kembali_konsinyasi';
    protected $primaryKey = 'no_nota_kembali';
    protected $fillable = ['no_nota_kembali','no_nota_beli_konsinyasi','idkonsinyator','idpegawai','tanggal','keterangan','total_dibayar','total_keuntungan'];
    public $timestamps = false;
    public $incrementing = false;

    public function detail_kembali_konsinyasi()
    {
        return $this->hasMany('App\DetailKembaliKonsinyasi');
    }
}
