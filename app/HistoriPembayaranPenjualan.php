<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoriPembayaranPenjualan extends Model
{
    protected $table = 'histori_pembayaran_penjualan';
    protected $primaryKey = 'idhistori';
    protected $fillable = ['no_nota_jual','idpegawai','tanggal','jumlah_dibayar','metode_pembayaran'];
    public $timestamps = false;

    public function notajual()
    {
        return $this->belongsTo('App\NotaJual');
    }
}
