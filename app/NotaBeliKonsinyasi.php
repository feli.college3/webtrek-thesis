<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaBeliKonsinyasi extends Model
{
    protected $table = 'nota_beli_konsinyasi';
    protected $primaryKey = 'no_nota_beli_konsinyasi';
    protected $fillable = ['no_nota_beli_konsinyasi','idkonsinyator','idpegawai','tanggal_dipesan','lama_penitipan','keterangan','tanggal_diterima','status_transaksi'];
    public $timestamps = false;
    public $incrementing = false;

    public function detail_beli_konsinyasi()
    {
        return $this->hasMany('App\DetailBeliKonsinyasi');
    }
}
