<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaJual extends Model
{
	protected $table = 'nota_jual';
    protected $primaryKey = 'no_nota_jual';
    protected $fillable = ['no_nota_jual','idpegawai_kasir','idpelanggan','tanggal','grand_total','status_pembayaran','metode_pembayaran','tanggal_jatuh_tempo','sisa_piutang','keterangan','status_transaksi','nomor_kartu','kirim','bukti_transfer'];
    public $timestamps = false;
    public $incrementing = false;

    public function detail_nota_jual()
    {
        return $this->hasMany('App\DetailNotaBeli');
    }

    public function detail_jual_konsinyasi()
    {
        return $this->hasMany('App\DetailJualKonsinyasi');
    }
}
