<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengiriman extends Model
{
    protected $table = 'pengiriman';
    protected $primaryKey = 'no_nota_jual';
    protected $fillable = ['no_nota_jual','idpegawai','tanggal_kirim','alamat','ongkos_kirim','nama_penerima','status_kirim','keterangan'];
    public $incrementing = false;
    public $timestamps = false;

    public function nota_jual()
    {
        return $this->belongsTo('App\NotaJual');
    }
}
