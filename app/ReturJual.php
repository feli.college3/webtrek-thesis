<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturJual extends Model
{
    protected $table = 'retur_penjualan';
    protected $primaryKey = 'idretur';
    protected $fillable = ['no_nota_jual','idpegawai','tanggal','deskripsi','jenis_retur','grand_total'];
    public $timestamps = false;
}
