<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailJualKonsinyasi extends Model
{
    protected $table = 'detail_jual_konsinyasi';
    protected $fillable = ['no_nota_jual','idproduk_konsinyasi','harga_jual','jumlah'];
    public $incrementing = false;
    public $timestamps = false;

    public function nota_jual()
    {
        return $this->belongsTo('App\NotaJual');
    }

    public function produk_konsinyasi()
    {
        return $this->belongsTo('App\ProdukKonsinyasi');
    }
}
