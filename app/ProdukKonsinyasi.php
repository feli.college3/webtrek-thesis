<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdukKonsinyasi extends Model
{
    protected $table = 'produk_konsinyasi';
    protected $primaryKey = 'idproduk';
    protected $fillable = ['nama_produk','stok','satuan','deskripsi','lama_penitipan','hpp','harga_jual','nama_foto','ekstensi'];
    public $timestamps = false;
}
