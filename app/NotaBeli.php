<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaBeli extends Model
{
    protected $table = 'nota_beli';
    protected $primaryKey = 'no_nota_beli';
    protected $fillable = ['no_nota_beli','idpegawai','idsupplier','tanggal_dipesan','grand_total','status_pembayaran','metode_pembayaran','tanggal_jatuh_tempo','sisa_hutang','keterangan','tanggal_diterima','status_transaksi'];
    public $timestamps = false;
    public $incrementing = false;

    public function detail_nota_beli()
    {
        return $this->hasMany('App\DetailNotaBeli');
    }
}
