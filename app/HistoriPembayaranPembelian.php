<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoriPembayaranPembelian extends Model
{
    protected $table = 'histori_pembayaran_pembelian';
    protected $primaryKey = 'idhistori';
    protected $fillable = ['no_nota_beli','idpegawai','tanggal','jumlah_dibayar','metode_pembayaran'];
    public $timestamps = false;

    public function notabeli()
    {
        return $this->belongsTo('App\NotaBeli');
    }
}
