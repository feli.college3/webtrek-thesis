<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class KeranjangBiasa extends Model
{
    protected $table = 'keranjang_biasa';
    protected $primaryKey = ['idpelanggan','idproduk'];
    protected $fillable = ['idpelanggan','idproduk','jumlah'];
    public $incrementing = false;
    public $timestamps = false;

    public function pelanggan()
    {
        return $this->belongsTo('App\Pelanggan');
    }

    public function produk()
    {
        return $this->belongsTo('App\Produk');
    }

    //tambahan untuk update with 2 primary keys
    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if(!is_array($keys)){
            return parent::setKeysForSaveQuery($query);
        }

        foreach($keys as $keyName){
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if(is_null($keyName)){
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }
    //tutup tambahan
}
