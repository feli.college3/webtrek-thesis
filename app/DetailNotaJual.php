<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailNotaJual extends Model
{
    protected $table = 'detail_nota_jual';
    protected $fillable = ['no_nota_jual','idproduk','tanggal_kadaluwarsa','harga_jual','jumlah'];
    public $incrementing = false;
    public $timestamps = false;

    public function notajual()
    {
        return $this->belongsTo('App\NotaJual');
    }

    public function produk()
    {
        return $this->belongsTo('App\Produk');
    }
}
