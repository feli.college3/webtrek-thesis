<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturBeli extends Model
{
    protected $table = 'retur_pembelian';
    protected $primaryKey = 'idretur';
    protected $fillable = ['no_nota_beli','idpegawai','tanggal','deskripsi','jenis_retur','grand_total'];
    public $timestamps = false;
}
