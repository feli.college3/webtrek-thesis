<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenyesuaianStok extends Model
{
    protected $table = 'penyesuaian_stok';
    protected $primaryKey = 'idpenyesuaian';
    protected $fillable = ['idproduk','tanggal_kadaluwarsa','tanggal','jenis','jumlah','keterangan','status'];
    public $timestamps = false;
}
