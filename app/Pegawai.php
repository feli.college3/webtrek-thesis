<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawai';
    protected $primaryKey = 'idpegawai';
    protected $fillable = ['nama_pegawai','username','status','idjabatan'];
    public $timestamps = false;

    public function jabatan()
    {
        return $this->belongsTo('App\Jabatan');
    }
}
