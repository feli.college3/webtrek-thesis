<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk';
    protected $primaryKey = 'idproduk';
    protected $fillable = ['idproduk','nama_produk','satuan','stok_minimal','deskripsi','hpp','harga_jual_eceran','harga_jual_grosir','status','nama_foto','ekstensi','idkategori'];
    public $timestamps = false;
    public $incrementing = false;

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }

    public function stok_produk()
    {
        return $this->hasMany('App\StokProduk');
    }
}
