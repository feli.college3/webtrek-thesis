@extends('layouts.index-admin', ['title' => 'Retur Pembelian'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DETAIL RETUR PEMBELIAN
      </b></h1>
        <!-- <button class="btn btn-success btn-tambah"><a href="{{ route('produk.create') }}" class="font-white">Tambah Produk Baru</a></button> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @if(!empty($produk))
            <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"><b>No. Retur: {{$id}}</b></h3>
            </div>
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                  <tr>
                  <th>ID Produk</th>
                  <th>Nama Produk</th>
                  <th class="no-sort">Tanggal Kadaluwarsa Lama</th>
                  @if($retur->jenis_retur=='Produk')
                    <th class="no-sort">Tanggal Kadaluwarsa Baru</th>
                  @endif
                  <th class="no-sort right-aligned">Jumlah</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($produk as $post)
                    <tr>
                        <td><a href="{{route('produk.show', $post->idproduk)}}">{{$post->idproduk}}</a></td>
                        <td>{{$post->nama_produk}}</td>
                        <td>{{date('d-m-Y', strtotime($post->tanggal_kadaluwarsa_lama))}}</td>
                        @if($retur->jenis_retur=='Produk')
                          <td>{{date('d-m-Y', strtotime($post->tanggal_kadaluwarsa_baru))}}</td>
                        @endif
                        <td class="right-aligned">{{$post->jumlah}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          @endif

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection