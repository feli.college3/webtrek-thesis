@extends('layouts.index-admin', ['title' => 'Retur Pembelian'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DAFTAR RETUR PEMBELIAN
      </b></h1>
        <button class="btn btn-success btn-tambah"><a href="{{ route('retur-pembelian.create') }}" class="font-white">Buat Retur Pembelian</a></button>
    </section>

    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID Retur</th>
                  <th>No. Nota</th>
                  <th>Waktu Retur</th>
                  <th class="no-sort">Supplier</th>
                  <th class="no-sort">Pegawai</th>
                  <th class="right-aligned">Grand Total (Rp)</th>
                  <th class="no-sort">Jenis Retur</th>
                  <th class="no-sort">Deskripsi</th>
                  <!-- <th class="no-sort"></th>
                  <th class="no-sort"></th> -->
                </tr>
                </thead>
                <tbody>
                @if(!empty($list_retur))
                    @foreach($list_retur as $post)
                        <tr>
                          <td><a href="{{route('retur-pembelian.show', $post->idretur)}}">{{$post->idretur}}</a></td>
                          <td><a href="{{route('nota-beli.show', $post->no_nota_beli)}}">{{$post->no_nota_beli}}</a></td>
                          <td>{{date('d-m-Y H:i', strtotime($post->tanggal))}}</td>
                          <td>{{$post->nama_supplier}}</td>
                          <td>{{$post->nama_pegawai}}</td>
                          <td class="right-aligned">{{number_format($post->grand_total,0,".",",")}}</td>
                          <td>{{$post->jenis_retur}}</td>
                          <td>{{$post->deskripsi}}</td>
                          <!-- <td class="center-aligned">
                            <button class="btn btn-warning"><a href="{{ route('retur-pembelian.edit', $post->idretur) }}" class="font-white">Edit</a></button>
                          </td>
                          <td class="center-aligned">
                            <form method="post" action="{{ route('retur-pembelian.destroy', $post->idretur) }}">
                              {{ method_field('delete')}}
                              {{ csrf_field() }}
                              <button class="btn btn-danger" type="submit" name="delete" onclick="return confirm('Anda yakin untuk menghapus data retur?')">Hapus</button>
                            </form>         
                          </td> -->
                        </tr>
                    @endforeach
                  @endif 
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection