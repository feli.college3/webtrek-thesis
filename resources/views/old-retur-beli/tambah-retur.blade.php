@extends('layouts.index-admin', ['title' => 'Retur Pembelian'])
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Buat Retur Pembelian
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <form role="form" method="POST" action="{{ url('retur-pembelian') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-warning">
              <!-- /.box-header -->
              <div class="box-body">

                  <div class="form-group">
                    <label>Supplier(*)</label>
                    <select id="supplier" name="supplier" class="form-control dynamic" required>
                      <option disabled selected value>Pilih Supplier</option>
                      @foreach($list_supplier as $sup)
                      <option value="{{$sup->idsupplier}}">{{$sup->nama_supplier}}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="form-group display-products">
                    <label>No. Nota Beli(*)</label>
                    <select id="no_nota" name="no_nota" class="form-control" required>
                      <option disabled selected value>Pilih Nomor Nota</option>
                      
                    </select>
                  </div>

                  <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                    <label>Keterangan(*)</label>
                    <textarea name="deskripsi" class="form-control" placeholder="Alasan retur..."  value="{{ old('deskripsi') }}" required></textarea>
                    @if ($errors->has('deskripsi'))
                      <span class="help-block">
                          <strong>{{ $errors->first('deskripsi') }}</strong>
                      </span>
                    @endif
                  </div>

                  <div class="form-group display-products">
                    <label>Jenis Retur(*)</label>
                    <select id="jenis_retur" name="jenis_retur" class="form-control" required>
                      <option value="Produk">Produk</option>
                      <option value="Uang">Uang</option>
                    </select>
                  </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

          </div>
        <!-- /.col -->

        <!-- <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Produk yang Diretur(*)</h3>
            </div>
            <div class="box-body">
              @for($i = 0; $i < count($list_produk); $i++)
                <div class="form-group">
                  <select id="produk{{$i}}" name="produk{{$i}}" class="form-control">
                    <option disabled selected value>Pilih Produk</option>
                    @foreach($list_produk as $prod)
                    <option value="{{$prod->idproduk}}">{{$prod->nama_produk}}</option>
                    @endforeach
                  </select>
                </div>
              @endfor
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Jumlah yang Diretur(*)</h3>
            </div>
            <div class="box-body">
              @for($i = 0; $i < count($list_produk); $i++)
                <div class="form-group{{ $errors->has('jumlah') ? ' has-error' : '' }}">
                  <input type="number" name="jumlah{{$i}}" class="form-control" placeholder="1"  value="{{ old('jumlah') }}" required/>
                  @if ($errors->has('jumlah'))
                    <span class="help-block">
                      <strong>{{ $errors->first('jumlah') }}</strong>
                    </span>
                  @endif
                </div>
              @endfor
            </div>
          </div>
        </div> -->
        </div>

        <h4><label name="grand" id="grand"><b>Grand Total: Rp0</b></label></h4>
        <input type="hidden" name="grand_total" id="grand_total"/>

        <table class="table" id="produk_retur">
          <thead id="table-head">
            
          </thead>
          <tbody>
            <!-- <tr>
              <td>John</td>
              <td>Doe</td>
            </tr>
            <tr>
              <td>Mary</td>
              <td>Moe</td>
            </tr>
            <tr>
              <td>July</td>
              <td>Dooley</td>
            </tr> -->
          </tbody>
        </table>
        <p><b>(*) wajib diisi</b></p>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </form>
    </section>

<!-- Ajax 3.4.1 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  var jumproduk=0;
  jQuery(document).ready(function(){

    jQuery('#supplier').on('change',function(){
      var idsupplier=jQuery(this).val();
      if(idsupplier){
        jQuery.ajax({
          url:'create/getnotabeli/'+idsupplier,
          type:"GET",
          dataType:"json",
          success:function(nota)
          {
            jQuery('#no_nota').empty();
            $('#no_nota').append('<option disabled selected value>Pilih Nomor Nota</option>');
            jQuery.each(nota, function(key,value){
              $('#no_nota').append('<option value="'+value+'">'+value+'</option>');
            });
          }
        });
      }
      else{
        $('select[name="no_nota"]').empty();
      }
    });
    jQuery('#no_nota').on('change',function(){
      var no_nota=jQuery(this).val();
      var jenis=jQuery('#jenis_retur').val();
      if(no_nota){
        $('#produk_retur > tbody:last-child').empty();
        jQuery.ajax({
          url:'create/getprodukbeli/'+no_nota,
          type:"GET",
          dataType:"json",
          success:function(produk_array)
          {
            $("#table-head").empty();
            if(jenis=='Produk'){
              // $('#produk_retur > thead').append('<tr><th>Produk yang Diretur</th><th>Tanggal Kadaluwarsa Baru (dd/mm/yyyy)</th><th>Lokasi Baru(*)</th><th class="right-aligned">Harga Beli (Rp)</th><th class="right-aligned">Jumlah yang Dibeli</th><th class="right-aligned">Jumlah yang Diretur(*)</th><th class="right-aligned">Subtotal (Rp)</th></tr>');
              $('#produk_retur > thead').append('<tr><th>Produk yang Diretur</th><th>Tanggal Kadaluwarsa Baru (dd/mm/yyyy)</th><th>Lokasi Baru(*)</th><th class="right-aligned">Harga Beli (Rp)</th><th class="right-aligned">Jumlah Dibeli</th><th class="right-aligned">Jumlah Diretur(*)</th><th class="right-aligned">Subtotal (Rp)</th></tr>');
            }
            else{
              $('#produk_retur > thead').append('<tr><th>Produk yang Diretur</th><th class="right-aligned">Harga Beli (Rp)</th><th class="right-aligned">Jumlah Dibeli</th><th class="right-aligned">Jumlah Diretur(*)</th><th class="right-aligned">Subtotal (Rp)</th></tr>');
            }
            jumproduk=produk_array.length;
            // jQuery.each(produk_array, function(key,value){
            //   $('#produk_retur > tbody:last-child').append('<tr><td><input type="text" name="halo" class="form-control" value="'+value+'" disabled></td>'+'<td><input type="number" name="jumlah" class="form-control" value="0"/></td></tr>');
            // });
            for (let i = 0; i < produk_array.length; ++i) {
              if(jenis=='Produk'){
                
                var formattedDate = new Date(produk_array[i]['tanggal']);
                var d = formattedDate.getDate();
                var m =  formattedDate.getMonth();
                m += 1;  // JavaScript months are 0-11
                var y = formattedDate.getFullYear();

                // $('#produk_retur > tbody:last-child').append('<tr><td><input type="text" name="textproduk'+[i]+'" class="form-control" value="('+produk_array[i]['id']+') '+produk_array[i]['nama']+'" readonly/></td>'+
                //   '<td><input type="date" name="tanggal'+[i]+'" id="tanggal'+[i]+'" class="form-control"/></td>'+
                //   '<td><input type="text" name="lokasi'+[i]+'" id="lokasi'+[i]+'" class="form-control" value="-" required/></td>'+
                //   '<td><input type="text" name="harga_beli'+[i]+'" id="harga_beli'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['harga_beli']+'" readonly/></td>'+
                //   '<td><input type="text" name="jumlah_dibeli'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['jumlah']+'" readonly/></td>'+
                // '<td><input type="number" name="jumlah_diretur'+[i]+'" id="jumlah_diretur'+[i]+'" class="form-control right-aligned" value="0" min="0" max="'+produk_array[i]['jumlah']+'" onchange="getsubtotal('+i+');getgrandtotal()" onkeyup="getsubtotal('+i+');getgrandtotal()" required/></td>'+
                // '<td><input type="text" name="subtotal'+[i]+'" id="subtotal'+[i]+'" class="form-control right-aligned" value="0" readonly/><input type="hidden" name="produk'+[i]+'" id="produk'+[i]+'" value="'+produk_array[i]['nama']+'"/></td></tr>');

                $('#produk_retur > tbody:last-child').append('<tr><td><input type="text" name="textproduk'+[i]+'" class="form-control" value="'+produk_array[i]['id']+' ('+d+'/'+m+'/'+y+')" readonly/></td>'+
                  '<td><input type="date" name="tanggal'+[i]+'" id="tanggal'+[i]+'" class="form-control"/></td>'+
                  '<td><input type="text" name="lokasi'+[i]+'" id="lokasi'+[i]+'" class="form-control" value="-" required/></td>'+
                  '<td><input type="text" name="harga_beli'+[i]+'" id="harga_beli'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['harga_beli']+'" readonly/></td>'+
                  '<td><input type="text" name="jumlah_dibeli'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['jumlah']+'" readonly/></td>'+
                '<td><input type="number" name="jumlah_diretur'+[i]+'" id="jumlah_diretur'+[i]+'" class="form-control right-aligned" value="0" min="0" max="'+produk_array[i]['jumlah']+'" onchange="getsubtotal('+i+');getgrandtotal()" onkeyup="getsubtotal('+i+');getgrandtotal()" required/></td>'+
                '<td><input type="text" name="subtotal'+[i]+'" id="subtotal'+[i]+'" class="form-control right-aligned" value="0" readonly/><input type="hidden" name="produk'+[i]+'" id="produk'+[i]+'" value="'+produk_array[i]['nama']+'"/></td></tr>');
                // $('#produk_retur > tbody:last-child').append('<tr><td>'+'<select name="produk'+[i]+'" class="form-control"><option disabled selected value>Pilih Produk</option>'+'<option value="'+produk_array[i]['nama']+'">('+produk_array[i]['id']+') '+produk_array[i]['nama']+' ('+d+'/'+m+'/'+y+')</option>'+
                //   '<td><input type="date" name="tanggal'+[i]+'" id="tanggal'+[i]+'" class="form-control"/></td>'+
                //   '<td><input type="text" name="lokasi'+[i]+'" id="lokasi'+[i]+'" class="form-control" value="-" required/></td>'+
                //   '<td><input type="text" name="harga_beli'+[i]+'" id="harga_beli'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['harga_beli']+'" readonly/></td>'+
                //   '<td><input type="text" name="jumlah_dibeli'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['jumlah']+'" readonly/></td>'+
                // '<td><input type="number" name="jumlah_diretur'+[i]+'" id="jumlah_diretur'+[i]+'" class="form-control right-aligned" value="0" min="0" max="'+produk_array[i]['jumlah']+'" onchange="getsubtotal('+i+');getgrandtotal()" onkeyup="getsubtotal('+i+');getgrandtotal()" required/></td>'+
                // '<td><input type="text" name="subtotal'+[i]+'" id="subtotal'+[i]+'" class="form-control right-aligned" value="0" readonly/><input type="hidden" name="produk'+[i]+'" id="produk'+[i]+'" value="'+produk_array[i]['nama']+'"/></td></tr>');
              // });
                document.getElementById("tanggal"+[i]).valueAsDate = new Date();
              }
              else{
                $('#produk_retur > tbody:last-child').append('<tr><td><input type="text" name="textproduk'+[i]+'" class="form-control" value="('+produk_array[i]['id']+') '+produk_array[i]['nama']+'" readonly/></td>'+
                    '<td><input type="text" name="harga_beli'+[i]+'" id="harga_beli'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['harga_beli']+'" readonly/></td>'+
                    '<td><input type="text" name="jumlah_dibeli'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['jumlah']+'" readonly/></td>'+
                  '<td><input type="number" name="jumlah_diretur'+[i]+'" id="jumlah_diretur'+[i]+'" class="form-control right-aligned" value="0" min="0" max="'+produk_array[i]['jumlah']+'" onchange="getsubtotal('+i+');getgrandtotal()" onkeyup="getsubtotal('+i+');getgrandtotal()" required/></td>'+
                  '<td><input type="text" name="subtotal'+[i]+'" id="subtotal'+[i]+'" class="form-control right-aligned" value="0" readonly/><input type="hidden" name="produk'+[i]+'" id="produk'+[i]+'" value="'+produk_array[i]['nama']+'"/></td></tr>');
              }
            }
          }
        });
      }
      else{
        // $('table[name="produk_retur"]').empty();
      }
    });
    
    jQuery('#jenis_retur').on('change',function(){
      var no_nota=jQuery('#no_nota').val();
      var jenis=jQuery(this).val();
      if(no_nota){
        $('#produk_retur > tbody:last-child').empty();
        jQuery.ajax({
          url:'create/getproduk/'+no_nota,
          type:"GET",
          dataType:"json",
          success:function(produk_array)
          {
            $("#table-head").empty();
            if(jenis=='Produk'){
              $('#produk_retur > thead').append('<tr><th>Produk yang Diretur</th><th>Tanggal Kadaluwarsa Baru (dd/mm/yyyy)</th><th>Lokasi Baru(*)</th><th class="right-aligned">Harga Beli (Rp)</th><th class="right-aligned">Jumlah yang Dibeli</th><th class="right-aligned">Jumlah yang Diretur(*)</th><th class="right-aligned">Subtotal (Rp)</th></tr>');
            }
            else{
              $('#produk_retur > thead').append('<tr><th>Produk yang Diretur</th><th class="right-aligned">Harga Beli (Rp)</th><th class="right-aligned">Jumlah yang Dibeli</th><th class="right-aligned">Jumlah yang Diretur(*)</th><th class="right-aligned">Subtotal (Rp)</th></tr>');
            }
            jumproduk=produk_array.length;
            // jQuery.each(produk_array, function(key,value){
            //   $('#produk_retur > tbody:last-child').append('<tr><td><input type="text" name="halo" class="form-control" value="'+value+'" disabled></td>'+'<td><input type="number" name="jumlah" class="form-control" value="0"/></td></tr>');
            // });
            for (let i = 0; i < produk_array.length; ++i) {
              if(jenis=='Produk'){
                $('#produk_retur > tbody:last-child').append('<tr><td><input type="text" name="textproduk'+[i]+'" class="form-control" value="('+produk_array[i]['id']+') '+produk_array[i]['nama']+'" readonly/></td>'+
                  '<td><input type="date" name="tanggal'+[i]+'" id="tanggal'+[i]+'" class="form-control"/></td>'+
                  '<td><input type="text" name="lokasi'+[i]+'" id="lokasi'+[i]+'" class="form-control" value="-" required/></td>'+
                  '<td><input type="text" name="harga_beli'+[i]+'" id="harga_beli'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['harga_beli']+'" readonly/></td>'+
                  '<td><input type="text" name="jumlah_dibeli'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['jumlah']+'" readonly/></td>'+
                '<td><input type="number" name="jumlah_diretur'+[i]+'" id="jumlah_diretur'+[i]+'" class="form-control right-aligned" value="0" min="0" max="'+produk_array[i]['jumlah']+'" onchange="getsubtotal('+i+');getgrandtotal()" onkeyup="getsubtotal('+i+');getgrandtotal()" required/></td>'+
                '<td><input type="text" name="subtotal'+[i]+'" id="subtotal'+[i]+'" class="form-control right-aligned" value="0" readonly/><input type="hidden" name="produk'+[i]+'" id="produk'+[i]+'" value="'+produk_array[i]['nama']+'"/></td></tr>');
              // });
                document.getElementById("tanggal"+[i]).valueAsDate = new Date();
              }
              else{
                $('#produk_retur > tbody:last-child').append('<tr><td><input type="text" name="textproduk'+[i]+'" class="form-control" value="('+produk_array[i]['id']+') '+produk_array[i]['nama']+'" readonly/></td>'+
                    '<td><input type="text" name="harga_beli'+[i]+'" id="harga_beli'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['harga_beli']+'" readonly/></td>'+
                    '<td><input type="text" name="jumlah_dibeli'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['jumlah']+'" readonly/></td>'+
                  '<td><input type="number" name="jumlah_diretur'+[i]+'" id="jumlah_diretur'+[i]+'" class="form-control right-aligned" value="0" min="0" max="'+produk_array[i]['jumlah']+'" onchange="getsubtotal('+i+');getgrandtotal()" onkeyup="getsubtotal('+i+');getgrandtotal()" required/></td>'+
                  '<td><input type="text" name="subtotal'+[i]+'" id="subtotal'+[i]+'" class="form-control right-aligned" value="0" readonly/><input type="hidden" name="produk'+[i]+'" id="produk'+[i]+'" value="'+produk_array[i]['nama']+'"/></td></tr>');
              }
            }
          }
        });
      }
      else{
        // $('table[name="produk_retur"]').empty();
      }
    });
    // jQuery('#jenis_retur').on('change',function(){
    //   var jenis=jQuery(this).val();
    // };
  });
  function getsubtotal(index){
    var prod = "produk"+index;
    var jml = "jumlah_diretur"+index;
    var hrg = "harga_beli"+index;
    var sub = "subtotal"+index;
    var jum = document.getElementById(jml).value;
    var harga_beli = parseFloat(document.getElementById(hrg).value);
    var total=jum*harga_beli;
    total = Math.round(total);
    document.getElementById(sub).value = total;
  }

  function getgrandtotal(){
    var grand = 0;
    var data = 0;
    for(i=0;i<jumproduk;i++){
      var sub = "subtotal"+i;
      var subtotal = parseFloat(document.getElementById(sub).value);
      grand+=subtotal;
    }
    grand = Math.round(grand);
    document.getElementById('grand').innerHTML = "Grand Total: Rp"+separator(grand);
    document.getElementById('grand_total').value = grand;
  }

  function separator(number) {
      var parts=number.toString().split(".");
      return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
  }


  // $('#add').click(function () {
  //   var table = $(this).closest('table');
  //   console.log(table.find('input:text').length);
  //   if (table.find('input:text').length < 6) {
  //       var x = $(this).closest('tr').nextAll('tr');
  //       $.each(x, function (i, val) {
  //           val.remove();
  //       });
  //       table.append('<tr><td style="width:200px;" align="right">First Name <td> <input type="text" id="current Name" value="" /> </td><td style="width:200px;" align="right">Last Name <td> <input type="text" id="current Name" value="" /> </td></tr>');
  //       $.each(x, function (i, val) {
  //           table.append(val);
  //       });
  //   }
  // });
</script>
@endsection