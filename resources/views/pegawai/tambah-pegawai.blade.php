@extends('layouts.index-admin', ['title' => 'Pegawai'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Tambah Pegawai Baru
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{ url('pegawai') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="form-group">
                  <label>Nama Pegawai(*)</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Pegawai" value="{{ old('nama') }}" required autofocus/>
                </div>

                <div class="form-group">
                  <label>Jabatan(*)</label>
                  <select id="id_jabatan" name="id_jabatan" class="form-control" required>
                    <option disabled selected value>Pilih Jabatan</option>
                    @foreach($list_jabatan as $jab)
                    <option value="{{$jab->idjabatan}}">{{$jab->nama_jabatan}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                  <label>Username(*)</label>
                  <input type="text" name="username" class="form-control" placeholder="username" value="{{ old('username') }}" required/>
                  @if ($errors->has('username'))
                    <span class="help-block">
                      <strong>{{ $errors->first('username') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <label>Password(*)</label>
                  <input type="password" name="password" class="form-control" placeholder="password"  value="{{ old('password') }}" required/>
                  @if ($errors->has('password'))
                    <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('ulang_pass') ? ' has-error' : '' }}">
                  <label>Ulangi Password(*)</label>
                  <input type="password" name="ulang_pass" class="form-control" placeholder="password"  value="{{ old('ulang_pass') }}" required/>
                  @if ($errors->has('ulang_pass'))
                    <span class="help-block">
                        <strong>{{ $errors->first('ulang_pass') }}</strong>
                    </span>
                  @endif
                </div>

                <p><b>(*) wajib diisi</b></p>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection