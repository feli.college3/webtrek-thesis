@extends('layouts.index-admin', ['title' => 'Pegawai'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Ubah Data Pegawai
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{ route('pegawai.update', $id) }}" enctype="multipart/form-data">
                {{ method_field("PUT") }}
                {{ csrf_field() }}
                <!-- text input -->
                <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                  <label>Nama Pegawai(*)</label>
                  <input type="text" name="nama" class="form-control" value="{{$peg->nama_pegawai}}" required autofocus/>
                  @if ($errors->has('nama'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nama') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group">
                  <label>Jabatan(*)</label>
                  <select id="id_jabatan" name="id_jabatan" class="form-control" required>
                    <option hidden> {{ $selectedJab->namaJabatan }} </option>
                      @foreach($list_jabatan as $jab)
                        @if($selectedJab->nama_jabatan == $jab->nama_jabatan)
                          <option selected value="{{$jab->idjabatan}}">{{$jab->nama_jabatan}}</option>
                        @else
                          <option value="{{$jab->idjabatan}}">{{$jab->nama_jabatan}}</option>
                        @endif
                      @endforeach
                  </select>
                </div>

                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                  <label>Username</label>
                  <input type="text" name="username" class="form-control" value="{{$peg->username}}"/>
                  @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group">
                  <label>Status(*)</label>
                    <select id="status" name="status" class="form-control" required>
                      @if($peg->status=="Aktif")
                          <option value="Aktif" selected>Aktif</option>
                          <option value="Non-aktif">Non-aktif</option>
                      @else
                          <option value="Aktif">Aktif</option>
                          <option value="Non-aktif" selected>Non-aktif</option>
                      @endif
                    </select>
                    @if ($errors->has('status'))
                      <span class="help-block">
                        <strong>{{ $errors->first('status') }}</strong>
                      </span>
                    @endif
                </div>

                <input type="hidden" name="password"value="{{$peg->password}}"/>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
  <script>
    alert('{{ session('status') }}')
  </script>
@endif
@endsection