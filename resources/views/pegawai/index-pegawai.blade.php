@extends('layouts.index-admin', ['title' => 'Pegawai'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DATA PEGAWAI
      </b></h1>
        <button class="btn btn-success btn-tambah"><a href="{{ route('pegawai.create') }}" class="font-white">Tambah Pegawai Baru</a></button>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama Pegawai</th>
                  <th class="no-sort">Username</th>
                  <th class="no-sort">Status</th>
                  <th class="no-sort">Jabatan</th>
                  <th class="no-sort"></th>
                  <th class="no-sort"></th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($list_pegawai))
                    @foreach($list_pegawai as $post)
                        <tr>
                            <td>{{$post->idpegawai}}</td>
                            <td>{{$post->nama_pegawai}}</td>
                            <td>{{$post->username}}</td>
                            <td>{{$post->status}}</td>
                            <td>{{$post->nama_jabatan}}</td>
                          <td class="center-aligned">
                            <button class="btn btn-warning"><a href="{{ route('pegawai.edit', $post->idpegawai) }}" class="font-white">Edit</a></button>
                          </td>
                          <td class="center-aligned">
                            <form method="post" action="{{ route('pegawai.destroy', $post->idpegawai) }}">
                              {{ method_field('delete')}}
                              {{ csrf_field() }}
                              <button class="btn btn-danger" type="submit" name="delete" onclick="return confirm('Anda yakin untuk menghapus pegawai?')">Hapus</button>
                            </form>         
                          </td>
                        </tr>
                     @endforeach
                    @endif 
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection