@extends('layouts.index-admin', ['title' => 'Produk'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        SEMUA PRODUK
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="container mt-2">
            @if(!empty($list_produk))
              @for ($i = 0; $i < count($list_produk); $i++)
                @if($i%6 == 0)
                  <div class="row">
                @endif
                <div class="col-md-2 col-sm-6">
                  <div class="card card-block border-primary">
                    <img src="images/{{$list_produk[$i]->nama_foto}}.{{$list_produk[$i]->ekstensi}}" alt="Foto produk">
                    <h5 class="card-title mt-3 mb-3"><b>{{$list_produk[$i]->nama_produk}}</b></h5>
                    <h5 class="card-text">Rp{{number_format($list_produk[$i]->hpp,0,".",",")}}</h5>
                  </div>
                </div>
                @if($i%6 == 0)
                  </div>
                @endif
              @endfor
            @endif

            <div class="row">
  <div class="col-sm-2">
    <div class="card border-primary">
      <div class="card-body">
        <h5 class="card-title">Special title treatment</h5>
        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
        <a href="#" class="btn btn-primary">Go somewhere</a>
      </div>
    </div>
  </div>
  <div class="col-sm-2">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Special title treatment</h5>
        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
        <a href="#" class="btn btn-primary">Go somewhere</a>
      </div>
    </div>
  </div>
</div>
            <!-- <div class="row">
              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                  <img src="https://static.pexels.com/photos/7096/people-woman-coffee-meeting.jpg" alt="Photo of sunset">
                  <h5 class="card-title mt-3 mb-3">Sierra Web Development • Ownerdsd dgc gdghd fghfg</h5>
                </div>
              </div>
              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                  <img src="https://static.pexels.com/photos/7357/startup-photos.jpg" alt="Photo of sunset">
                  <h5 class="card-title  mt-3 mb-3">ProVyuh</h5>
                </div>
              </div>
              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                  <img src="https://static.pexels.com/photos/262550/pexels-photo-262550.jpeg" alt="Photo of sunset">
                  <h5 class="card-title  mt-3 mb-3">ProVyuh</h5>
                </div>
              </div>
              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                <img src="https://static.pexels.com/photos/326424/pexels-photo-326424.jpeg" alt="Photo of sunset">
                  <h5 class="card-title  mt-3 mb-3">ProVyuh</h5>
                  <p class="card-text">This is a company that builds websites, web apps and e-commerce solutions.</p> 
                </div>
              </div>

              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                <img src="https://static.pexels.com/photos/326424/pexels-photo-326424.jpeg" alt="Photo of sunset">
                  <h5 class="card-title  mt-3 mb-3">ProVyuh</h5>
                  <p class="card-text">This is a company that builds websites, web apps and e-commerce solutions.</p> 
                </div>
              </div>   

              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                <img src="https://static.pexels.com/photos/326424/pexels-photo-326424.jpeg" alt="Photo of sunset">
                  <h5 class="card-title  mt-3 mb-3">ProVyuh</h5>
                  <p class="card-text">This is a company that builds websites, web apps and e-commerce solutions.</p> 
                </div>
              </div>   
            </div>
            <div class="row">
              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                <img src="https://static.pexels.com/photos/326424/pexels-photo-326424.jpeg" alt="Photo of sunset">
                  <h5 class="card-title  mt-3 mb-3">ProVyuh</h5>
                  <p class="card-text">This is a company that builds websites, web apps and e-commerce solutions.</p> 
                </div>
              </div>   

              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                <img src="https://static.pexels.com/photos/326424/pexels-photo-326424.jpeg" alt="Photo of sunset">
                  <h5 class="card-title  mt-3 mb-3">ProVyuh</h5>
                  <p class="card-text">This is a company that builds websites, web apps and e-commerce solutions.</p> 
                </div>
              </div>   

              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                  <img src="https://static.pexels.com/photos/7096/people-woman-coffee-meeting.jpg" alt="Photo of sunset">
                  <h5 class="card-title mt-3 mb-3">Sierra Web Development • Owner</h5>
                  <p class="card-text">This is a company that builds websites, web apps and e-commerce solutions.</p> 
                </div>
              </div>
              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                  <img src="https://static.pexels.com/photos/7357/startup-photos.jpg" alt="Photo of sunset">
                  <h5 class="card-title  mt-3 mb-3">ProVyuh</h5>
                  <p class="card-text">This is a company that builds websites, web .</p> 
                </div>
              </div>
              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                  <img src="https://static.pexels.com/photos/262550/pexels-photo-262550.jpeg" alt="Photo of sunset">
                  <h5 class="card-title  mt-3 mb-3">ProVyuh</h5>
                  <p class="card-text">This is a company that builds websites, web apps and e-commerce solutions.</p> 
                </div>
              </div>
              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                <img src="https://static.pexels.com/photos/326424/pexels-photo-326424.jpeg" alt="Photo of sunset">
                  <h5 class="card-title  mt-3 mb-3">ProVyuh</h5>
                  <p class="card-text">This is a company that builds websites, web apps and e-commerce solutions.</p> 
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                <img src="https://static.pexels.com/photos/326424/pexels-photo-326424.jpeg" alt="Photo of sunset">
                  <h5 class="card-title  mt-3 mb-3">ProVyuh</h5>
                  <p class="card-text">This is a company that builds websites, web apps and e-commerce solutions.</p> 
                </div>
              </div>   

              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                <img src="https://static.pexels.com/photos/326424/pexels-photo-326424.jpeg" alt="Photo of sunset">
                  <h5 class="card-title  mt-3 mb-3">ProVyuh</h5>
                  <p class="card-text">This is a company that builds websites, web apps and e-commerce solutions.</p> 
                </div>
              </div>   

              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                <img src="https://static.pexels.com/photos/326424/pexels-photo-326424.jpeg" alt="Photo of sunset">
                  <h5 class="card-title  mt-3 mb-3">ProVyuh</h5>
                  <p class="card-text">This is a company that builds websites, web apps and e-commerce solutions.</p> 
                </div>
              </div>   

              <div class="col-md-2 col-sm-6">
                <div class="card card-block">
                <img src="https://static.pexels.com/photos/326424/pexels-photo-326424.jpeg" alt="Photo of sunset">
                  <h5 class="card-title  mt-3 mb-3">ProVyuh</h5>
                  <p class="card-text">This is a company that builds websites, web apps and e-commerce solutions.</p> 
                </div>
              </div>

            </div> -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection