@extends('layouts.index-admin', ['title' => 'Produk'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DETAIL PRODUK
      </b></h1>
        <!-- <button class="btn btn-success btn-tambah"><a href="{{ route('produk.create') }}" class="font-white">Tambah Produk Baru</a></button> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><b>{{$produk->idproduk}} - {{$produk->nama_produk}}</b></h3><br>
              @if($detail_toko->fitur_stok_minimal_perproduk==0)
                <h5 class="box-title">Stok Minimal: {{$detail_toko->stok_minimal}}</h5>
              @else
                <h5 class="box-title">Stok Minimal: {{$produk->stok_minimal}}</h5>
              @endif
            </div>
            <!-- Minyak berkualitas, bisa diminum.Kopi mari kita berlayar menuju angkasa. -->
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Tanggal Kadaluwarsa</th>
                  <th>Stok</th>
                  <th>Lokasi</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($stok_produk))
                    @foreach($stok_produk as $post)
                        <tr>
                            <td>{{date('d-m-Y', strtotime($post->tanggal_kadaluwarsa))}}</td>
                            <td>{{$post->stok}}</td>
                            <td>{{$post->lokasi}}</td>
                        </tr>
                     @endforeach
                    @endif  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection