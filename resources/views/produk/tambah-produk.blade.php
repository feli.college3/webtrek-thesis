@extends('layouts.index-admin', ['title' => 'Produk'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Tambah Produk Baru
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{ url('produk') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                  <label>Nama Produk(*)</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Produk" value="{{ old('nama') }}" required autofocus/>
                  @if ($errors->has('nama'))
                    <span class="help-block">
                      <strong>{{ $errors->first('nama') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group">
                  <label>Kategori(*) &nbsp;&nbsp;<a href="{{ route('kategori.create') }}" target="_blank">Tambah Kategori Baru</a></label>
                  <select id="id_kategori" name="id_kategori" class="form-control" required>
                    <option disabled selected value>Pilih Kategori</option>
                    @foreach($list_kategori as $kat)
                    <option value="{{$kat->idkategori}}">{{$kat->nama_kategori}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group{{ $errors->has('satuan') ? ' has-error' : '' }}">
                  <label>Satuan(*)</label>
                  <input type="text" name="satuan" class="form-control" placeholder="satuan" value="{{ old('satuan') }}" required/>
                  @if ($errors->has('satuan'))
                    <span class="help-block">
                      <strong>{{ $errors->first('satuan') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                  <label>Deskripsi(*)</label>
                  <textarea name="deskripsi" class="form-control" placeholder="Deskripsi Produk"  value="{{ old('deskripsi') }}" maxlength="500" required></textarea>
                  @if ($errors->has('deskripsi'))
                    <span class="help-block">
                        <strong>{{ $errors->first('deskripsi') }}</strong>
                    </span>
                  @endif
                </div>
                @if($detail_toko->fitur_stok_minimal_perproduk==1)
                  <div class="form-group{{ $errors->has('stok_minimal') ? ' has-error' : '' }}">
                    <label>Stok Minimal(*)</label>
                    <input type="number" name="stok_minimal" class="form-control" placeholder="5"  value="{{ old('stok_minimal') }}" required/>
                    @if ($errors->has('stok_minimal'))
                      <span class="help-block">
                        <strong>{{ $errors->first('stok_minimal') }}</strong>
                      </span>
                    @endif
                  </div>
                @endif

                <!-- <div class="form-group{{ $errors->has('hpp') ? ' has-error' : '' }}">
                  <label>HPP(*)</label>
                  <input type="number" name="hpp" class="form-control" placeholder="1000"  value="{{ old('hpp') }}" required/>
                  @if ($errors->has('hpp'))
                    <span class="help-block">
                      <strong>{{ $errors->first('hpp') }}</strong>
                    </span>
                  @endif
                </div>

                @if($detail_toko->fitur_harga_grosir==1)
                  <div class="form-group{{ $errors->has('harga_jual_eceran') ? ' has-error' : '' }}">
                    <label>Harga Jual Eceran(*)</label>
                    <input type="number" name="harga_jual_eceran" class="form-control" placeholder="1000"  value="{{ old('harga_jual_eceran') }}" required/>
                    @if ($errors->has('harga_jual_eceran'))
                      <span class="help-block">
                        <strong>{{ $errors->first('harga_jual_eceran') }}</strong>
                      </span>
                    @endif
                  </div>

                  <div class="form-group{{ $errors->has('harga_jual_grosir') ? ' has-error' : '' }}">
                    <label>Harga Jual Grosir(*)</label>
                    <input type="number" name="harga_jual_grosir" class="form-control" placeholder="1000"  value="{{ old('harga_jual_grosir') }}" required/>
                    @if ($errors->has('harga_jual_grosir'))
                      <span class="help-block">
                        <strong>{{ $errors->first('harga_jual_grosir') }}</strong>
                      </span>
                    @endif
                  </div>
                @else
                  <div class="form-group{{ $errors->has('harga_jual_eceran') ? ' has-error' : '' }}">
                    <label>Harga Jual(*)</label>
                    <input type="number" name="harga_jual_eceran" class="form-control" placeholder="1000"  value="{{ old('harga_jual_eceran') }}" required/>
                    @if ($errors->has('harga_jual_eceran'))
                      <span class="help-block">
                        <strong>{{ $errors->first('harga_jual_eceran') }}</strong>
                      </span>
                    @endif
                  </div>
                @endif -->

                <div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
                  <label>Foto:</label>
                  <input type="file" name="foto"/>
                  @if ($errors->has('foto'))
                    <span class="help-block">
                      <strong>{{ $errors->first('foto') }}</strong>
                    </span>
                  @endif
                </div>

                <p><b>(*) wajib diisi</b></p>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection