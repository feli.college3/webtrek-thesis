@extends('layouts.index-admin', ['title' => 'Produk Perlu Direstock'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DATA PRODUK TOKO
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- Minyak berkualitas, bisa diminum.Kopi mari kita berlayar menuju angkasa. -->
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama Produk</th>
                  <th class="no-sort">Kategori</th>
                  <th class="right-aligned">Total Stok</th>
                  <th class="right-aligned">Stok Minimal</th>
                  <!-- <th class="right-aligned">HPP</th>
                  <th class="right-aligned">Harga Jual Eceran</th>
                  <th class="right-aligned">Harga Jual Grosir</th> -->
                  <th class="no-sort center-aligned">Foto</th>
                </tr>
                </thead>
                <tbody>
                @if(count($list_produk)>0)
                    @for ($i = 0; $i < count($list_produk); $i++)
                        <tr>
                            <td><a href="{{route('produk.show', $list_produk[$i]->idproduk)}}">{{$list_produk[$i]->idproduk}}</a></td>
                            <td>{{$list_produk[$i]->nama_produk}}</td>
                            <td>{{$list_produk[$i]->nama_kategori}}</td>
                            @if($list_produk[$i]->total_stok==null)
                              <td class="right-aligned">0</td>
                            @else
                              <td class="right-aligned">{{$list_produk[$i]->total_stok}}</td>
                            @endif
                            @if($detail_toko->fitur_stok_minimal_perproduk==0)
                              <td class="right-aligned">{{$detail_toko->stok_minimal}}</td>
                            @else
                              <td class="right-aligned">{{$list_produk[$i]->stok_minimal}}</td>
                            @endif
                            <!-- <td class="right-aligned">{{number_format($list_produk[$i]->hpp,2,".",",")}}</td>
                            <td class="right-aligned">{{number_format($list_produk[$i]->harga_jual_eceran,2,".",",")}}</td>
                            <td class="right-aligned">{{number_format($list_produk[$i]->harga_jual_grosir,2,".",",")}}</td> -->
                            <td class="center-aligned"><img src="images/{{$list_produk[$i]->nama_foto}}.{{$list_produk[$i]->ekstensi}}" style="width: 100px; height: 100px;"></td>
                        </tr>
                    @endfor
                  @endif 
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection