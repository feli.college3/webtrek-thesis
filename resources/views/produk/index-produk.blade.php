@extends('layouts.index-admin', ['title' => 'Produk'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DATA PRODUK TOKO
      </b></h1>
      @if($role=='manajer')
        <button class="btn btn-success btn-tambah"><a href="{{ route('produk.create') }}" class="font-white">Tambah Produk Baru</a></button>
      @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama Produk</th>
                  <th class="no-sort">Kategori</th>
                  <th class="right-aligned">Total Stok</th>
                  <th class="no-sort">Satuan</th>
                  <th class="no-sort">Deskripsi</th>
                  @if($role=='manajer')
                    <th class="right-aligned">HPP (Rp)</th>
                  @endif
                  @if($detail_toko->fitur_harga_grosir==1)
                    <th class="right-aligned">Jual Eceran (Rp)</th>
                    <th class="right-aligned">Jual Grosir (Rp)</th>
                  @else
                    <th class="right-aligned">Harga Jual (Rp)</th>
                  @endif
                  <th class="no-sort">Status</th>
                  <th class="no-sort center-aligned">Foto</th>
                  @if($role=='manajer')
                    <th class="no-sort"></th>
                    <th class="no-sort"></th>
                  @endif
                </tr>
                </thead>
                <tbody>
                @if(!empty($list_produk))
                    @for ($i = 0; $i < count($list_produk); $i++)
                        <tr>
                          @if($detail_toko->fitur_stok_fifo==1)
                            <td><a href="{{route('produk.show', $list_produk[$i]->idproduk)}}">{{$list_produk[$i]->idproduk}}</a></td>
                          @else
                            <td>{{$list_produk[$i]->idproduk}}</td>
                          @endif
                          
                          <td>{{$list_produk[$i]->nama_produk}}</td>
                          <td>{{$list_produk[$i]->nama_kategori}}</td>

                          <td class="right-aligned"><a href="{{route('penyesuaian-stok.show', $list_produk[$i]->idproduk)}}">
                          @if($stok[$i]->total_stok!=null)
                            {{$stok[$i]->total_stok}}
                          @else
                            0
                          @endif
                          </td>
                          <td>{{$list_produk[$i]->satuan}}</td>
                          <td>{{$list_produk[$i]->deskripsi}}</td>
                          <td class="right-aligned">{{number_format($list_produk[$i]->hpp,0,".",",")}}</td>

                          @if($detail_toko->fitur_harga_grosir==1)
                            @if($role=='manajer')
                              <td class="right-aligned">{{number_format($list_produk[$i]->harga_jual_eceran,0,".",",")}}</td>
                            @endif
                            <td class="right-aligned">{{number_format($list_produk[$i]->harga_jual_grosir,0,".",",")}}</td>
                          @else
                            <td class="right-aligned">{{number_format($list_produk[$i]->harga_jual_eceran,0,".",",")}}</td>
                          @endif
                          
                          <td>{{$list_produk[$i]->status}}</td>
                          <td><img src="images/{{$list_produk[$i]->nama_foto}}.{{$list_produk[$i]->ekstensi}}" style="width: 100px; height: 100px;"></td>
                          
                          @if($role=='manajer')
                            <td class="center-aligned">
                              <button class="btn btn-warning"><a href="{{ route('produk.edit', $list_produk[$i]->idproduk) }}" class="font-white">Edit</a></button>
                            </td>
                            <td class="center-aligned">
                              <form method="post" action="{{ route('produk.destroy', $list_produk[$i]->idproduk) }}">
                                {{ method_field('delete')}}
                                {{ csrf_field() }}
                                <button class="btn btn-danger" type="submit" name="delete" onclick="return confirm('Anda yakin untuk menghapus produk?')">Hapus</button>
                              </form>         
                            </td>
                          @endif
                        </tr>
                    @endfor
                  @endif 
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection