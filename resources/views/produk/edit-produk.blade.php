@extends('layouts.index-admin', ['title' => 'Produk'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Ubah Data Produk
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{ route('produk.update', $id) }}" enctype="multipart/form-data">
                {{ method_field("PUT") }}
                {{ csrf_field() }}
                <!-- text input -->
                <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                  <label>Nama Produk(*)</label>
                  <input type="text" name="nama" class="form-control" value="{{$produk->nama_produk}}" required autofocus/>
                  @if ($errors->has('nama'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nama') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group">
                  <label>Kategori: {{ $selectedKat->nama_kategori }}</label>
                </div>

                <div class="form-group{{ $errors->has('satuan') ? ' has-error' : '' }}">
                  <label>Satuan(*)</label>
                  <input type="text" name="satuan" class="form-control" value="{{$produk->satuan}}" required/>
                  @if ($errors->has('satuan'))
                    <span class="help-block">
                        <strong>{{ $errors->first('satuan') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                  <label>Deskripsi(*)</label>
                  <input type="text" name="deskripsi" class="form-control" value="{{$produk->deskripsi}}" maxlength="200" required/>
                  @if ($errors->has('deskripsi'))
                    <span class="help-block">
                        <strong>{{ $errors->first('deskripsi') }}</strong>
                    </span>
                  @endif
                </div>

                @if($detail_toko->fitur_stok_minimal_perproduk==1)
                  <div class="form-group{{ $errors->has('stok_minimal') ? ' has-error' : '' }}">
                    <label>Stok Minimal(*)</label>
                    <input type="text" name="stok_minimal" class="form-control" value="{{$produk->stok_minimal}}"/>
                    @if ($errors->has('stok_minimal'))
                      <span class="help-block">
                          <strong>{{ $errors->first('stok_minimal') }}</strong>
                      </span>
                    @endif
                  </div>
                @endif

                @if($detail_toko->fitur_harga_grosir==1)
                  <div class="form-group{{ $errors->has('harga_jual_eceran') ? ' has-error' : '' }}">
                    <label>Harga Jual Eceran(*)</label>
                    <input type="text" name="harga_jual_eceran" class="form-control" value="{{$produk->harga_jual_eceran}}"/>
                    @if ($errors->has('harga_jual_eceran'))
                      <span class="help-block">
                          <strong>{{ $errors->first('harga_jual_eceran') }}</strong>
                      </span>
                    @endif
                  </div>

                  <div class="form-group{{ $errors->has('harga_jual_grosir') ? ' has-error' : '' }}">
                    <label>Harga Jual Grosir(*)</label>
                    <input type="text" name="harga_jual_grosir" class="form-control" value="{{$produk->harga_jual_grosir}}"/>
                    @if ($errors->has('harga_jual_grosir'))
                      <span class="help-block">
                          <strong>{{ $errors->first('harga_jual_grosir') }}</strong>
                      </span>
                    @endif
                  </div>
                @else
                  <div class="form-group{{ $errors->has('harga_jual_eceran') ? ' has-error' : '' }}">
                    <label>Harga Jual(*)</label>
                    <input type="text" name="harga_jual_eceran" class="form-control" value="{{$produk->harga_jual_eceran}}"/>
                    @if ($errors->has('harga_jual_eceran'))
                      <span class="help-block">
                          <strong>{{ $errors->first('harga_jual_eceran') }}</strong>
                      </span>
                    @endif
                  </div>
                @endif

                <div class="form-group">
                  <label>Status(*)</label>
                    <select id="status" name="status" class="form-control" required>
                      @if($produk->status=="Dijual")
                          <option value="Dijual" selected>Dijual</option>
                          <option value="Tidak Dijual">Tidak Dijual</option>
                      @else
                          <option value="Dijual">Dijual</option>
                          <option value="Tidak Dijual" selected>Tidak Dijual</option>
                      @endif
                    </select>
                    @if ($errors->has('status'))
                      <span class="help-block">
                        <strong>{{ $errors->first('status') }}</strong>
                      </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
                  <label>Foto:</label>
                  <!-- @if($full_foto!='standar.png')
                    {{$title=$full_foto}}
                  
                  @else
                    {{$title='No file chosen'}}
                  @endif -->
                  <input type="file" name="foto"/>
                  <!-- <label>{{$title}}</label> -->
                  @if ($errors->has('foto'))
                    <span class="help-block">
                      <strong>{{ $errors->first('foto') }}</strong>
                    </span>
                  @endif
                </div>

                <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
  <script>
    alert('{{ session('status') }}')
  </script>
@endif
@endsection