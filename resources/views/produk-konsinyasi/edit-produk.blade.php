@extends('layouts.index-admin', ['title' => 'Produk'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Ubah Data Produk Konsinyasi
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{ route('produk-konsinyasi.update', $id) }}" enctype="multipart/form-data">
                {{ method_field("PUT") }}
                {{ csrf_field() }}
                <!-- text input -->
                <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                  <label>Nama Produk(*)</label>
                  <input type="text" name="nama" class="form-control" value="{{$produk->nama_produk}}" required autofocus/>
                  @if ($errors->has('nama'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nama') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                  <label>Deskripsi(*)</label>
                  <input type="text" name="deskripsi" class="form-control" value="{{$produk->deskripsi}}" maxlength="200" required/>
                  @if ($errors->has('deskripsi'))
                    <span class="help-block">
                        <strong>{{ $errors->first('deskripsi') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('satuan') ? ' has-error' : '' }}">
                  <label>Satuan(*)</label>
                  <input type="text" name="satuan" class="form-control" value="{{$produk->satuan}}" required/>
                  @if ($errors->has('satuan'))
                    <span class="help-block">
                        <strong>{{ $errors->first('satuan') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('lama_penitipan') ? ' has-error' : '' }}">
                  <label>Lama Penitipan(*)</label>
                  <input type="text" name="lama_penitipan" class="form-control" value="{{$produk->lama_penitipan}}" required/>
                  @if ($errors->has('lama_penitipan'))
                    <span class="help-block">
                        <strong>{{ $errors->first('lama_penitipan') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('hpp') ? ' has-error' : '' }}">
                  <label>HPP(*)</label>
                  <input type="text" name="hpp" class="form-control" value="{{$produk->hpp}}" required/>
                  @if ($errors->has('hpp'))
                    <span class="help-block">
                        <strong>{{ $errors->first('hpp') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('harga_jual') ? ' has-error' : '' }}">
                  <label>Harga Jual(*)</label>
                  <input type="text" name="harga_jual" class="form-control" value="{{$produk->harga_jual}}" required/>
                  @if ($errors->has('harga_jual'))
                    <span class="help-block">
                        <strong>{{ $errors->first('harga_jual') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
                  <label>Foto:</label>
                  <input type="file" name="foto"/>
                  @if ($errors->has('foto'))
                    <span class="help-block">
                      <strong>{{ $errors->first('foto') }}</strong>
                    </span>
                  @endif
                </div>

                <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
  <script>
    alert('{{ session('status') }}')
  </script>
@endif
@endsection