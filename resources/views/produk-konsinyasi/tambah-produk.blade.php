@extends('layouts.index-admin', ['title' => 'Produk'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Tambah Produk Konsinyasi Baru
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{ url('produk-konsinyasi') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                  <label>Nama Produk(*)</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Produk" value="{{ old('nama') }}" required autofocus/>
                  @if ($errors->has('nama'))
                    <span class="help-block">
                      <strong>{{ $errors->first('nama') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                  <label>Deskripsi(*)</label>
                  <textarea name="deskripsi" class="form-control" placeholder="Deskripsi Produk" maxlength="200" required>{{ old('deskripsi') }}</textarea>
                  @if ($errors->has('deskripsi'))
                    <span class="help-block">
                        <strong>{{ $errors->first('deskripsi') }}</strong>
                    </span>
                  @endif
                </div>

                <!-- <div class="form-group{{ $errors->has('stok') ? ' has-error' : '' }}">
                  <label>Stok(*)</label>
                  <input type="number" name="stok" class="form-control" placeholder="3" value="{{ old('stok') }}" required/>
                  @if ($errors->has('stok'))
                    <span class="help-block">
                        <strong>{{ $errors->first('stok') }}</strong>
                    </span>
                  @endif
                </div> -->

                <div class="form-group{{ $errors->has('satuan') ? ' has-error' : '' }}">
                  <label>Satuan(*)</label>
                  <input type="text" name="satuan" class="form-control" placeholder="bungkus" value="{{ old('satuan') }}" required/>
                  @if ($errors->has('satuan'))
                    <span class="help-block">
                        <strong>{{ $errors->first('satuan') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('lama_penitipan') ? ' has-error' : '' }}">
                  <label>Lama Penitipan (hari)(*)</label>
                  <input type="number" name="lama_penitipan" class="form-control" placeholder="3" value="{{ old('lama_penitipan') }}" required/>
                  @if ($errors->has('lama_penitipan'))
                    <span class="help-block">
                      <strong>{{ $errors->first('lama_penitipan') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('hpp') ? ' has-error' : '' }}">
                  <label>HPP(*)</label>
                  <input type="number" name="hpp" class="form-control" placeholder="1000"  value="{{ old('hpp') }}" required/>
                  @if ($errors->has('hpp'))
                    <span class="help-block">
                      <strong>{{ $errors->first('hpp') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('harga_jual') ? ' has-error' : '' }}">
                  <label>Harga Jual(*)</label>
                  <input type="number" name="harga_jual" class="form-control" placeholder="1000"  value="{{ old('harga_jual') }}" required/>
                  @if ($errors->has('harga_jual'))
                    <span class="help-block">
                      <strong>{{ $errors->first('harga_jual') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
                  <label>Foto:</label>
                  <input type="file" name="foto"/>
                  @if ($errors->has('foto'))
                    <span class="help-block">
                      <strong>{{ $errors->first('foto') }}</strong>
                    </span>
                  @endif
                </div>

                <p><b>(*) wajib diisi</b></p>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection