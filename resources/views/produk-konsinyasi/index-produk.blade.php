@extends('layouts.index-admin', ['title' => 'Produk'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DATA PRODUK KONSINYASI
      </b></h1>
      @if($role=='manajer')
        <button class="btn btn-success btn-tambah"><a href="{{ route('produk-konsinyasi.create') }}" class="font-white">Tambah Produk Baru</a></button>
      @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- Minyak berkualitas, bisa diminum.Kopi mari kita berlayar menuju angkasa. -->
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama Produk</th>
                  <th>Deskripsi</th>
                  <th class="right-aligned">Stok</th>
                  <th class="right-aligned">Lama Penitipan (hari)</th>
                  @if($role=='manajer')
                    <th class="right-aligned">HPP</th>
                  @endif
                  <th class="right-aligned">Harga Jual</th>
                  <th>Foto</th>
                  @if($role=='manajer')
                    <th class="no-sort"></th>
                    <th class="no-sort"></th>
                  @endif
                </tr>
                </thead>
                <tbody>
                @if(!empty($list_produk))
                    @foreach($list_produk as $post)
                        <tr>
                          <td>{{$post->idproduk}}</td>
                          <td>{{$post->nama_produk}}</td>
                          <td>{{$post->deskripsi}}</td>
                          <td class="right-aligned">{{number_format($post->stok)}}</td>
                          <td class="right-aligned">{{$post->lama_penitipan}}</td>
                          @if($role=='manajer')
                            <td class="right-aligned">{{number_format($post->hpp,0,".",",")}}</td>
                          @endif
                          <td class="right-aligned">{{number_format($post->harga_jual,0,".",",")}}</td>
                          <td><img src="images/{{$post->nama_foto}}.{{$post->ekstensi}}" style="width: 100px; height: 100px;"></td>
                          @if($role=='manajer')
                            <td class="center-aligned">
                              <button class="btn btn-warning"><a href="{{ route('produk-konsinyasi.edit', $post->idproduk) }}" class="font-white">Edit</a></button>
                            </td>
                            <td class="center-aligned">
                              <form method="post" action="{{ route('produk-konsinyasi.destroy', $post->idproduk) }}">
                                {{ method_field('delete')}}
                                {{ csrf_field() }}
                                <button class="btn btn-danger" type="submit" name="delete" onclick="return confirm('Anda yakin untuk menghapus produk?')">Hapus</button>
                              </form>         
                            </td>
                          @endif
                        </tr>
                     @endforeach
                    @endif 
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection