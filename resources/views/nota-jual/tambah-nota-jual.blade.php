@extends('layouts.index-admin', ['title' => 'Penjualan'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1 class="center-aligned"><b>Pembuatan Nota Jual</b></h1>
      <h1><b>
        No. Nota: {{$id_baru}}
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form id="form-jual" method="POST" action="{{ url('nota-jual') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <input type="hidden" name="no_nota" value="{{$id_baru}}"/>
                    <div class="form-group">
                        <div class="row bottom-row">
                            <div class="col-sm-3">
                                <label class="span-tambah-baru">Pelanggan <a class="btn-tambah-baru" href="{{ route('pelanggan.create') }}" target="_blank">Tambah Baru</a></label>
                                <select id="pelanggan" name="pelanggan" class="form-control select2 margin-top-2" onchange="getadress()" required>
                                    @foreach($list_pelanggan as $plg)
                                    <option value="{{$plg->idpelanggan}}">{{$plg->nama_pelanggan}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label class="">Alamat</label>
                                <textarea id="alamat"class="form-control" name="alamat"></textarea>
                            </div>
                            <div class="col-sm-3 hide" id="div-ongkir">
                                <label class="">Ongkos Kirim</label><br>
                                <label id="label-ongkir">Rp0</label><br>
                                <input type="hidden" id="ongkir" name="ongkir" value="0">
                                <a class="" id="update-ongkir" onclick="panggil_ongkir('cek')"><b><i class="fa fa-refresh"></i> Update</b></a>
                            </div>
                            <div>
                                <label id="label-grand">Rp0</label>
                                <input type="hidden" id="grand-total" name="grand_total"/>
                                <input type="hidden" id="total-data" name="total_data"/>
                            </div>
                        </div>
                    </div>

                    <div class="narrow-rows scrollable-37">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="center-aligned">No.</th>
                                    <th>Nama Produk</th>
                                    <th class="right-aligned">Jumlah</th>
                                    @if($detail_toko->fitur_harga_grosir==1)
                                        <th class="right-aligned">Ecer/Grosir</th>
                                    @endif
                                    <th class="right-aligned">Harga</th>
                                    <th class="right-aligned">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                @for($i=1;$i<=20;$i++)
                                    <tr>
                                        <td class="center-aligned">{{$i}}</td>
                                        <!-- <td><input type="text" class="small-input form-control" id="{{ 'produk'.$i }}" name= "{{ 'produk'.$i }}" onkeyup="getproduct('{{$i}}')"/></td> -->
                                        <td><select id="{{ 'produk'.$i }}" name="{{ 'produk'.$i }}" class="form-control" onchange="checkdouble();getproduct('{{$i}}');checkproduct('{{$i}}');getsubtotal('{{$i}}'); getgrandtotal('{{$i}}'); getstok('{{$i}}');">
                                            <option selected value>Pilih Produk</option>
                                            @foreach($list_produk as $prod)
                                            <option value="{{$prod->nama_produk}}">{{$prod->nama_produk}}</option>
                                            @endforeach
                                            @foreach($list_produk_konsi as $prod)
                                            <option value="{{$prod->nama_produk}}">{{$prod->nama_produk}} [K]</option>
                                            @endforeach
                                        </select></td>
                                        <td class="right-aligned"><input type="number" class="small-input right-aligned" id="{{ 'jumlah'.$i }}" name="{{ 'jumlah'.$i }}" onkeyup="checkproduct('{{$i}}');getsubtotal('{{$i}}'); getgrandtotal('{{$i}}'); getstok('{{$i}}');" onchange="checkproduct('{{$i}}');getsubtotal('{{$i}}'); getgrandtotal('{{$i}}'); getstok('{{$i}}');"/></td>
                                        
                                        @if($detail_toko->fitur_harga_grosir==1)
                                            <td class="center-aligned"><label id="{{ 'jenis'.$i }}" class="">Ecer</label></td>
                                        @endif
                                        <td class="right-aligned"><input type="number" class="small-input right-aligned" id="{{ 'label-harga'.$i }}" name="{{ 'label_harga'.$i }}" readonly/></td>
                                        <input type="hidden" id="{{ 'harga'.$i }}" name="{{ 'harga'.$i }}"/>
                                        <td class="right-aligned"><label id="{{ 'subtotal'.$i }}"></label></td>
                                    </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div class="form-group">
                        <div class="row bottom-row margin-1-br">
                            <div class="col-sm-2">
                                <label class="control-label bold">Metode Pembayaran</label>
                                <div>
                                    <select id="metode_pembayaran" name="metode_pembayaran" class="form-control" onchange="metode_toggle()"  required>
                                        @if($detail_toko->pembayaran_tunai==0&&$detail_toko->pembayaran_debit==0&&$detail_toko->pembayaran_kredit==0&&$detail_toko->pembayaran_transfer==0)
                                            <option value="Tunai" selected>Cash</option>
                                        @else
                                            @if($detail_toko->pembayaran_tunai==1)
                                                <option value="Tunai" selected>Cash</option>
                                            @endif
                                            @if($detail_toko->pembayaran_debit==1)
                                                <option value="Debit">Debit</option>
                                            @endif
                                            @if($detail_toko->pembayaran_kredit==1)
                                                <option value="Kredit">Kredit</option>
                                            @endif
                                            @if($detail_toko->pembayaran_transfer==1)
                                                <option value="Transfer">Transfer</option>
                                            @endif
                                        @endif
                                    </select>
                                </div>
                            </div>
                            @if($detail_toko->pembayaran_piutang==1)
                                <div class="col-sm-2">
                                    <label class="control-label bold">Status Pembayaran</label>
                                    <div class="">
                                        <select id="status" name="status" class="form-control" onchange="tempo()" required>
                                            <option value="Lunas" id="lunas" selected>Lunas</option>
                                            <option value="Belum Lunas" id="belum-lunas">Belum Lunas</option>
                                        </select>
                                    </div>
                                </div>
                            @endif

                            <div class="col-sm-2 hide" id="tempo">
                                <label class="control-label bold">Jatuh Tempo</label>
                                <input id="jatuh_tempo" type="date" class="form-control" name="jatuh_tempo">
                            </div>

                            @if($detail_toko->fitur_kirim==1)
                                <div class="col-sm-2">
                                    <label class="control-label bold">Kirim</label>
                                    <div class="">
                                        <select id="kirim" name="kirim" class="form-control" onchange="pengiriman_toggle()" required>
                                            <option value="0" selected>Tidak</option>
                                            <option value="1">Ya</option>
                                        </select>
                                    </div>
                                </div>
                            @endif

                            <div class="col-sm-4">
                                <label class="control-label bold">Keterangan Penjualan</label>
                                <div class="">
                                    <textarea id="keterangan"class="form-control" name="keterangan">-</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row bottom-row margin-1-br">
                            <div class="col-sm-4 hide" id="nomor-kartu">
                                <label class="control-label bold">No. Kartu</label>
                                <div class="">
                                    <input type="text" id="nomor_kartu"class="form-control" name="nomor_kartu" maxlength="19" onkeyup="addDashes(this)">
                                </div>
                            </div>
                            <div class="col-sm-4 hide" id="div-keterangan-pengiriman">
                                <label class="control-label bold">Keterangan Pengiriman</label>
                                <div class="">
                                    <textarea id="keterangan_pengiriman" class="form-control" name="keterangan_pengiriman">-</textarea>
                                </div>
                            </div>
                            <!-- <div class="col-sm-7 float-right">
                                <label></label><br><br>
                                <div class="">
                                    <button type="submit" name="simpan" value="simpan" id="btn-submit" class="btn btn-success">Buat Nota Jual</button>
                                </div>
                            </div> -->
                        </div>
                        <div class="row bottom-row margin-1-br">
                            <div class="col-sm-12 center-aligned">
                                <label></label><br><br>
                                <div class="">
                                    <button type="submit" name="simpan" value="simpan" id="btn-submit" class="btn btn-success">Buat Nota Jual</button>
                                </div>
                            </div>
                        </div>

                    </div>
                    <input type="hidden" name="jarak" id="jarak">
                    <input type="hidden" name="jarak_aman" id="jarak_aman">
                    <input type="hidden" name="total_hutang" id="total_hutang">
                    <input type="hidden" name="batas_hutang" id="batas_hutang">
                    <div id="map" style="width: 400px; height: 300px;" class="hide"></div> 
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<script src="{{ asset('customer/js/jquery.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyC2UlF6oXbQDWRWv4CPbphCK0U8sKlUzJE"></script>
<script>
    // $(function () {
    //     //Initialize Select2 Elements
    //     // $('.select2').select2()
    // })
    window.onload = function() {
        getadress();
    };

    var detail_toko = {!! json_encode($detail_toko) !!};

    $('#form-jual').on('submit', (event) => {
        event.preventDefault();
        var jumlah_produk = cek_jumlah_produk();
        if(jumlah_produk>0){
            if(detail_toko['fitur_kirim']==1){
                panggil_ongkir('submit');
            }
            var idpelanggan=document.getElementById('pelanggan').value;
            var aman = cek_piutang(idpelanggan);
            var batas_hutang = parseFloat(document.getElementById('batas_hutang').value);
            var batas_hutang = parseFloat(document.getElementById('batas_hutang').value);
            var jarak_aman=document.getElementById('jarak_aman').value;

            if(detail_toko['fitur_kirim']==1){
                panggil_ongkir('submit');
                var e = document.getElementById("kirim");
                var value = e.options[e.selectedIndex].value;
                if(value==0){
                    jarak_aman=1;
                }
            }
            if(detail_toko['fitur_kirim']==0){
                jarak_aman=1;
            }
            if(detail_toko['fitur_batas_hutang']==0){
                aman=1;
            }
            if(aman==0){
                Swal.fire({
                    title: 'Piutang Melampaui Batas',
                    text: "Batas piutang untuk pelanggan ini adalah Rp"+separator(batas_hutang)+". Piutang pelanggan akan/telah melampaui batas piutang.",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#44b839',
                    cancelButtonText: 'Batalkan',
                    confirmButtonText: 'Tetap Lanjutkan',
                    reverseButtons: true,
                }).then((result) => {
                    if (result.value) {
                        if(jarak_aman==1){
                            var form = $('#form-jual');
                            form.trigger('submit');
                        }
                    }
                })
            }
            else{
                if(jarak_aman==1){
                    var form = $('#form-jual');
                    form.trigger('submit');
                }
            }
            console.log('aman:'+aman+',jarak_aman:'+jarak_aman);
            if(aman==1&&jarak_aman==1){
                var form = $('#form-jual');
                form.trigger('submit');
            }
        }
    });

    function cek_piutang(idpelanggan){
        var aman=1;
        if(detail_toko['pembayaran_piutang']==0){
            aman=1;
        }
        else{
            var status = document.getElementById('status').value;
            var total_hutang = parseFloat(document.getElementById('total_hutang').value);
            var batas_hutang = parseFloat(document.getElementById('batas_hutang').value);
            var grand_total = parseFloat(document.getElementById('grand-total').value);
            if(status=='Belum Lunas'){
                var total_sekarang=total_hutang+grand_total;
                console.log('total_sekarang'+total_sekarang);
                if(total_sekarang>batas_hutang){
                    aman=0;
                }
            }
        }
        return aman;
    }

    function checkdouble() {
        d=document;
        var produk = {!! json_encode($list_produk->toArray()) !!};
        var produk_konsi = {!! json_encode($list_produk_konsi->toArray()) !!};
        var total_produk=produk.length+produk_konsi.length;
        myArray = [];
        for (h=0;h<20;h++) {
            myArray[h] = d.getElementById('produk'+(h+1)).value;
        }
        for (a=0;a<20;a++) {
            for (b=1;b<=total_produk;b++) {
                d.getElementById('produk'+(a+1)).options[b].style.display = "block";
                for (c=0;c<20;c++) {
                    if(d.getElementById('produk'+(a+1)).options[b].value == myArray[c]) {
                        d.getElementById('produk'+(a+1)).options[b].style.display = "none";
                    }
                }
            }
        }
    }

    function cek_jumlah_produk(){
        var total_produk = document.getElementById('total-data').value;
        console.log(total_produk);
        if(total_produk==null||total_produk==""||total_produk==undefined||total_produk==0){
            total_produk=0;
            Swal.fire({
                title: 'Masukkan Produk Terlebih Dahulu',
                icon: 'warning',
                showConfirmButton: false,
                timer:1000,
            })
        }
        return total_produk;
    }

    function pengiriman_toggle(){
        var e = document.getElementById("kirim");
        var value = e.options[e.selectedIndex].value;
        var div_keterangan=document.getElementById('div-keterangan-pengiriman');
        if(value==1){
          $('#div-ongkir').toggleClass('hide');
          panggil_ongkir('cek');
          div_keterangan.classList.remove('hide');
        }
        else{
          $('#div-ongkir').toggleClass('hide');
          div_keterangan.classList.add('hide');
        }
    }
    function getadress(){
        var pelanggan = {!! json_encode($list_pelanggan->toArray()) !!};
        var id = document.getElementById('pelanggan').value;
        if(detail_toko['fitur_kirim']==1){
            var e = document.getElementById("kirim");
            var value = e.options[e.selectedIndex].value;
        }
        for(i=0;i<pelanggan.length;i++){
            if(id==pelanggan[i]['idpelanggan'])
            {
                document.getElementById('alamat').value = pelanggan[i]['alamat_pelanggan'];
                document.getElementById('total_hutang').value = pelanggan[i]['total_hutang'];
                document.getElementById('batas_hutang').value = pelanggan[i]['batas_hutang'];
                if(detail_toko['fitur_kirim']==1){
                    if(value==1){
                        panggil_ongkir('cek');
                    }
                }
                // cek_piutang(id);
            }
        }
    }

    function panggil_ongkir(aksi){
        var e = document.getElementById("kirim");
        var kirim_value = e.options[e.selectedIndex].value;
        if(kirim_value==1){
            getongkir(document.getElementById('alamat').value,aksi);
        }
        else{
            // if(aksi=='submit'){
            //     var form = $('#form-jual');
            //     form.trigger('submit');
            // }
        }
    }

    function getongkir(tujuan,aksi){
        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer();

        var myOptions = {
         zoom:7,
         mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        var map = new google.maps.Map(document.getElementById("map"), myOptions);
        directionsDisplay.setMap(map);
        var request = {
           origin: detail_toko['alamat_toko'], 
           destination: tujuan,
           travelMode: google.maps.DirectionsTravelMode.DRIVING
        };

        directionsService.route(request, function(response, status) {
          if (status == google.maps.DirectionsStatus.OK) {
            var distance= 0;
            for(i = 0; i < response.routes[0].legs.length; i++){
               distance += parseFloat(response.routes[0].legs[i].distance.value);
            }
            distance/=1000;
            distance=distance.toFixed(1);
            interval=detail_toko['interval_jarak'];
            ongkir_interval=detail_toko['ongkir_interval'];
            ongkir_perkm=ongkir_interval/interval;
            ongkir=ongkir_perkm*distance;
            if(ongkir>0){
                ongkir=pembulatan_ribuan(ongkir);
            }
            document.getElementById('jarak').value = distance;
            document.getElementById('ongkir').value = ongkir;
            document.getElementById('label-ongkir').innerHTML ="Rp"+separator(ongkir)+" ("+distance+" km)";
            getgrandtotal(1);
            directionsDisplay.setDirections(response);
          }
        });

        setTimeout(function(){
            var jarak=parseFloat(document.getElementById('jarak').value);
            var maksimal_jarak=parseFloat(detail_toko['maksimal_jarak']);
            if(jarak<=maksimal_jarak){
                document.getElementById('jarak_aman').value=1;
              // if(aksi=='submit'){
              //   setTimeout(function(){
              //       var form = $('#form-jual');
              //       form.trigger('submit');
              //   },1000);
              // }
            }
            else{
                document.getElementById('jarak_aman').value=0;
              Swal.fire({
                title: 'Alamat Kirim Berada di Luar Wilayah Pengiriman',
                text: "Jarak maksimal pengiriman adalah "+maksimal_jarak+" km. Silahkan ubah alamat dalam batas jarak pengiriman.",
                icon: 'warning',
                confirmButtonColor: '#44b839',
                confirmButtonText: 'OK',
              })
            }
        }, 1000);
    }

    // function pembulatan_ribuan(jumlah){
    //     var jum=Math.ceil(jumlah);
    //     jum=jum.toString();
    //     var jum_sub=jum.substr(-3);
    //     jum_sub=parseInt(jum_sub,10);
    //     if (jum_sub>499){
    //         jum=Math.round(jum,-3);
    //     } else {
    //         jum=Math.round(jum,-3)+1000;
    //     } 
         
    //     return jum;
    // }
    function pembulatan_ribuan(jumlah){
        var jum=Math.ceil(jumlah);
        jum=jum.toString();
        var jum_sub=jum.substr(-2);
        jum_sub=parseInt(jum_sub,10);
        var length=jum.length;
        var jumlah_diambil=length-3;
        if (jum_sub==0){
            jum=Math.round(jum,-2);
        } else {
            var dibulatkan=jum.substr(-3,1);
            dibulatkan++;
            var awalan=jum.substr(0,jumlah_diambil);
            var hasil=awalan+dibulatkan+"00";
            jum=parseInt(hasil,10);
        }
        return jum;
    }

    function getproduct(index){
        var produk = {!! json_encode($list_produk->toArray()) !!};
        var produk_konsi = {!! json_encode($list_produk_konsi->toArray()) !!};
        for (i = 0; i < produk.length; i++) {
            var prod = "produk"+index;
            var hrg = "label-harga"+index;
            var harga = "harga"+index;
            var jum = "jumlah"+index;
            var sub = "subtotal"+index;
            // if(document.getElementById(hrg).length!=0)
            // {
                var nama = document.getElementById(prod).value;
                document.getElementById(hrg).value = "";
                document.getElementById(harga).value = "";
                document.getElementById(sub).value = "";
                if(nama===produk[i]['nama_produk'])
                {
                    // return produk[i]['hargaJual'];
                    var total_stok=produk[i]['total_stok'];
                    if(total_stok==null){
                        total_stok=0;
                    }
                    document.getElementById(hrg).value = produk[i]['harga_jual_eceran'];
                    document.getElementById(harga).value = produk[i]['harga_jual_eceran'];
                    document.getElementById(jum).setAttribute('max', total_stok);
                    break;
                }
            // }
        }
        for (i = 0; i < produk_konsi.length; i++) {
            var prod = "produk"+index;
            var hrg = "label-harga"+index;
            var harga = "harga"+index;
            if(document.getElementById(hrg).length!=0)
            {
                var nama = document.getElementById(prod).value;
                if(nama===produk_konsi[i]['nama_produk'])
                {
                    // return produk[i]['hargaJual'];
                    document.getElementById(hrg).value = produk_konsi[i]['harga_jual'];
                    document.getElementById(harga).value = produk_konsi[i]['harga_jual'];
                    break;
                }
            }
        }
    }

    function getstok(index){
        var produk = {!! json_encode($list_produk->toArray()) !!};
        var produk_konsi = {!! json_encode($list_produk_konsi->toArray()) !!};
        
        var jum_grosir=0;
        if(detail_toko['fitur_harga_grosir']==1){
            jum_grosir=detail_toko['jumlah_grosir'];
        }
        for (i = 0; i < produk.length; i++) {
            var produk_element = document.getElementById("produk"+index);
            var jumlah_element = document.getElementById("jumlah"+index);
            var jenis_element = document.getElementById("jenis"+index);
            var label_harga = document.getElementById("label-harga"+index);
            var harga_element = document.getElementById("harga"+index);
            var nama = produk_element.value;

            if(nama===produk[i]['nama_produk'])
            {
                var total_stok=produk[i]['total_stok'];
                if(total_stok==null){
                    total_stok=0;
                }
                if(detail_toko['fitur_harga_grosir']==1){
                    if(jumlah_element.value>=jum_grosir){
                        jenis_element.innerHTML="Grosir";
                        label_harga.value = produk[i]['harga_jual_grosir'];
                        harga_element.value = produk[i]['harga_jual_grosir'];
                    }
                    else{
                        jenis_element.innerHTML="Ecer";
                        label_harga.value = produk[i]['harga_jual_eceran'];
                        harga_element.value = produk[i]['harga_jual_eceran'];
                    }
                    getsubtotal(index);
                    getgrandtotal(index);
                }
                if(jumlah_element.value>total_stok){
                    jumlah_element.value=total_stok;
                    getsubtotal(index);
                    getgrandtotal(index);
                    break;
                }
            }
        }
        for (i = 0; i < produk_konsi.length; i++) {
            var produk_element = document.getElementById("produk"+index);
            var jumlah_element = document.getElementById("jumlah"+index);
            var nama = produk_element.value;
            if(nama===produk_konsi[i]['nama_produk'])
            {
                var total_stok=produk_konsi[i]['stok'];
                if(total_stok==null){
                    total_stok=0;
                }
                console.log(total_stok);
                if(jumlah_element.value>total_stok){
                    jumlah_element.value=total_stok;
                    break;
                }
            }
        }
    }

    function checkproduct(index){
        var prod = "produk"+index;
        var jum = "jumlah"+index;
        if(document.getElementById(prod).length==0 || document.getElementById(prod).value == null){
            alert('Masukkan produk terlebih dahulu');
            document.getElementById(jum).value = null;
            return 0;
        }
        else{
            return 1;
        }
    }

    function getsubtotal(index){
        if(checkproduct(index)!=0){
            for(i=1;i<=20;i++)
            {
                var jml = "jumlah"+index;
                var hrg = "label-harga"+index;
                var sub = "subtotal"+index;
                if(document.getElementById(hrg).length!=0 && document.getElementById(jml).length!=0)
                {
                    var jum = document.getElementById(jml).value;
                    var harga_jual = document.getElementById(hrg).value;
                    var total = jum*harga_jual;
                    total = Math.round(total);
                    // return total;
                    document.getElementById(sub).innerHTML = total;
                }
            }
        }
    }

    function getgrandtotal(index){
        var grand = 0;
        if(checkproduct(index)!=0){
            var data = 0;
            for(i=1;i<=20;i++){
                var sub = "subtotal"+i;
                var hrg = "label-harga"+i;
                var jum = "jumlah"+i;
                if(document.getElementById(hrg).value.length!=0)
                {
                    // alert(document.getElementById(hrg).innerHTML.length);
                    var helper = parseFloat(document.getElementById(sub).innerHTML);
                    // alert(helper);
                    grand += helper;
                    grand = Math.round(grand);
                    if(document.getElementById(jum).value>0){
                        data++;
                    }
                }
            }
        }
        ongkir=document.getElementById('ongkir').value;
        // console.log(ongkir);
        ongkir=parseInt(ongkir,10);
        grand+=ongkir;
        document.getElementById('label-grand').innerHTML = "Rp"+separator(grand);
        document.getElementById('grand-total').value = grand;
        document.getElementById('total-data').value = data;
    }

    function separator(number) {
        var parts=number.toString().split(".");
        return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
    }

    function tempo(){
        if(document.getElementById('belum-lunas').selected){
            $('#tempo').toggleClass('hide');
        }
        else{
            $('#tempo').toggleClass('hide');
        }
    }

    function metode_toggle(){
        console.log(document.getElementById('metode_pembayaran').value);
        var no_kartu=document.getElementById('nomor-kartu');
        if(document.getElementById('metode_pembayaran').value=='Debit' || document.getElementById('metode_pembayaran').value=='Kredit'){
            no_kartu.classList.remove('hide');
        }
        else{
            no_kartu.classList.add('hide');
        }
    }

    function addDashes(f)
    {
        var r = /(\D+)/g,
        one = '',
        two = '',
        three = '',
        four = '';
        f.value = f.value.replace(r, '');
        one = f.value.substr(0, 4);
        two = f.value.substr(4, 4);
        three = f.value.substr(8, 4);
        four = f.value.substr(12, 4);
        f.value = one + '-' + two + '-' + three + '-' + four;
    }
</script>

@endsection
<script>
</script>