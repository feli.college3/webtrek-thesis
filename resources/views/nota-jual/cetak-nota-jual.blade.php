<!DOCTYPE html>
<html>
<head>
	<title>Nota Jual</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<script type="text/javascript"> try { this.print(); } catch (e) { window.onload = window.print; } </script>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
		@page { margin: 50px 25px; }
	    footer { position: fixed; bottom: -20px; left: 0px; right: 0px; height: 20px; font-size: 15px }
	    p { page-break-after: always; }
	    p:last-child { page-break-after: never; }
	    .center-aligned{
	    	text-align: center;
	    }
	    .right-aligned{
	    	text-align: right;
	    }
	    .green{
	    	color: green;
	    }
	    .red{
	    	color: red;
	    }
	    .vertical-middle{
		  vertical-align: middle !important;
		}
	</style>
	<center>
		<h5>{{$detail_toko->nama_toko}}</h5>
		<h6>{{$detail_toko->alamat_toko}}</h6>
		<h6>{{$detail_toko->telepon_toko}}</h6>
		<h6>{{$tanggal}}}</h6>
		<hr>
	</center>
 	

	<table class='table table-bordered'>
		<thead>
			<tr>
	            <th class="vertical-middle center-aligned">ID Produk</th>
	            <th class="vertical-middle">Nama Produk</th>
	            <!-- <th rowspan="2" class="vertical-middle right-aligned">Total Pembelian</th>
	            <th rowspan="2" class="vertical-middle right-aligned">Total Penjualan (Rp)</th>
	            <th colspan="2" class="vertical-middle center-aligned">Penyesuaian</th>
	            <th rowspan="2" class="vertical-middle right-aligned">Keuntungan/Kerugian (Rp)</th> -->
	            <!-- <th rowspan="2" class="vertical-middle right-aligned">Jumlah Beli (Rp)</th> -->
	            <!-- <th rowspan="2" class="vertical-middle right-aligned">Jumlah Jual</th> -->
          	</tr>
          	<tr>
	            <!-- <th class="vertical-middle right-aligned">Penambahan</th> -->
	            <th class="vertical-middle right-aligned">Penambahan Stok (Rp)</th>
	            <!-- <th class="vertical-middle right-aligned">Pengeluaran</th> -->
	            <th class="vertical-middle right-aligned">Pengeluaran Stok (Rp)</th>
          	</tr>
		</thead>
		<tbody>
			@if(count($toko)>0)
	            @foreach($toko as $post)
	                <tr>
	                    <td class="center-aligned"><b>{{$post['id']}}</b></td>
	                    <td>{{$post['nama']}}</td>
	                </tr>
	            @endforeach
          	@endif 
		</tbody>
	</table>
 	<footer>Dibuat oleh <b>WEBTREK</b></footer>
</body>
</html>