@extends('layouts.index-admin', ['title' => 'Detail Pengiriman'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DETAIL PENGIRIMAN - {{$id}}
      </b></h1>
        <!-- <button class="btn btn-success btn-tambah"><a href="{{ route('produk.create') }}" class="font-white">Tambah Produk Baru</a></button> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-xs-12">
          <div class="box box-primary">
          <div class="box-body">
            <table id="tabeldata" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th class="no-sort">Tanggal Kirim</th>
                  <th class="no-sort">Alamat Pengiriman</th>
                  <th class="no-sort">Nama Penerima</th>
                  <th class="no-sort right-aligned">Ongkos Kirim</th>
                  <th class="no-sort">Kurir</th>
                  <th class="no-sort">Status</th>
                  <th class="no-sort">Keterangan</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{date('d-m-Y, H:i', strtotime($nota_jual->tanggal_kirim))}}</td>
                  <td>{{$nota_jual->alamat}}</td>
                  <td>{{$nota_jual->nama_penerima}}</td>
                    <td class="right-aligned">{{number_format($nota_jual->ongkos_kirim,0,".",",")}}</td>
                  <td>{{$nota_jual->nama_pegawai}}</td>
                  <td>{{$nota_jual->status_kirim}}</td>
                  <td>{{$nota_jual->keterangan}}</td>
                </tr>
              </tbody>
            </table>
          </div>

          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>

@if (session('status'))
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection