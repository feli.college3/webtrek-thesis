@extends('layouts.index-admin', ['title' => 'Retur Pembelian'])
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Buat Retur Pembelian
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <form role="form" method="POST" action="{{ url('retur-pembelian') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-warning">
              <!-- /.box-header -->
              <div class="box-body">

                  <div class="form-group">
                    <label>Produk(*)</label>
                    <select id="produk" name="produk" class="form-control dynamic" required>
                      <option disabled selected value>Pilih Produk</option>
                      @foreach($list_produk as $produk)
                      <option value="{{$produk->idproduk}}">{{$produk->nama_produk}}</option>
                      @endforeach
                    </select>
                  </div>

                  @if($detail_toko->fitur_stok_fifo==1)
                    <div class="form-group display-products">
                  @else
                    <div class="form-group display-products hide">
                  @endif
                      <label>Tanggal Kedaluwarsa(*)</label>
                      <select id="tanggal_kedaluwarsa" name="tanggal_kedaluwarsa" class="form-control" required>
                        <option disabled selected value>Pilih Tanggal Kedaluwarsa</option>
                      </select>
                    </div>

                  <div class="form-group display-products">
                    <label>Nota Beli(*)</label>
                    <select id="nota_beli" name="nota_beli" class="form-control" required>
                      <option disabled selected value>Pilih Nota Beli</option>
                    </select>
                  </div>

                  <div class="form-group{{ $errors->has('jumlah_retur') ? ' has-error' : '' }}">
                    <label>Jumlah Diretur(*)</label>
                    <input type="number" id="jumlah_retur" name="jumlah_retur" class="form-control" placeholder="1" value="{{ old('jumlah_retur') }}" required onchange="getgrandtotal()" onkeyup="getgrandtotal()" />
                    @if ($errors->has('jumlah_retur'))
                      <span class="help-block">
                        <strong>{{ $errors->first('jumlah_retur') }}</strong>
                      </span>
                    @endif
                  </div>

                  <input type="hidden" name="harga_beli" id="harga_beli"/>
                  <h4><label name="grand" id="grand"><b>Grand Total: Rp0</b></label></h4>
                  <input type="hidden" name="grand_total" id="grand_total"/>

                  <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
                    <label>Keterangan(*)</label>
                    <textarea name="deskripsi" class="form-control" placeholder="Alasan retur..."  value="{{ old('deskripsi') }}" required></textarea>
                    @if ($errors->has('deskripsi'))
                      <span class="help-block">
                          <strong>{{ $errors->first('deskripsi') }}</strong>
                      </span>
                    @endif
                  </div>

                  <div class="form-group display-products">
                    <label>Jenis Retur(*)</label>
                    <select id="jenis_retur" name="jenis_retur" class="form-control" required onchange="retur_toggle()">
                      <option value="Uang">Uang</option>
                      <option value="Produk">Produk</option>
                    </select>
                  </div>

                  <div id="retur-produk" class="hide">
                    <div class="form-group{{ $errors->has('tanggal_baru') ? ' has-error' : '' }}">
                      <label>Tanggal Kedaluwarsa Baru(*)</label>
                      <input type="date" id="tanggal_baru" name="tanggal_baru" class="form-control" value="{{ old('tanggal_baru') }}"/>
                      @if ($errors->has('tanggal_baru'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tanggal_baru') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="form-group{{ $errors->has('lokasi') ? ' has-error' : '' }}">
                      <label>Lokasi(*)</label>
                      <input type="text" id="lokasi" name="lokasi" class="form-control" placeholder="Lokasi" value="{{ old('lokasi') }}"/>
                      @if ($errors->has('lokasi'))
                        <span class="help-block">
                          <strong>{{ $errors->first('lokasi') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

          </div>
        <!-- /.col -->
        </div>

        <p><b>(*) wajib diisi</b></p>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </form>
    </section>

<!-- Ajax 3.4.1 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  var detail_toko = {!! json_encode($detail_toko) !!};
  jQuery(document).ready(function(){
    var jumlah_dibeli=0;
    jQuery('#produk').on('change',function(){
      var idproduk=jQuery(this).val();
      if(idproduk){
        jQuery.ajax({
          url:'create/gettanggalkedaluwarsa/'+idproduk,
          type:"GET",
          dataType:"json",
          success:function(tanggal)
          {
            jQuery('#tanggal_kedaluwarsa').empty();
            jQuery('#nota_beli').empty();
            $('#nota_beli').append('<option disabled selected value>Pilih Nota Beli</option>');
            $('#tanggal_kedaluwarsa').append('<option disabled selected value>Pilih Tanggal Kedaluwarsa</option>');
            jQuery.each(tanggal, function(key,value){
              $('#tanggal_kedaluwarsa').append('<option value="'+value+'">'+value+'</option>');
            });
            if(detail_toko['fitur_stok_fifo']==0){
              $("#tanggal_kedaluwarsa").prop("selectedIndex", 1).val();
              var tanggal=jQuery('#tanggal_kedaluwarsa').val();

                jQuery.ajax({
                  url:'create/getnotabeli/'+idproduk+'/'+tanggal,
                  type:"GET",
                  dataType:"json",
                  success:function(nota)
                  {
                    jQuery('#nota_beli').empty();
                    $('#nota_beli').append('<option disabled selected value>Pilih Nota Beli</option>');
                    jQuery.each(nota, function(key,value){
                      console.log(value.substring(0,10));
                      $('#nota_beli').append('<option value="'+value.substring(0,10)+'">'+value+'</option>');
                    });
                  }
                });
            }
          }
        });
      }
      else{
        jQuery('#tanggal_kedaluwarsa').empty();
      }
    });

    jQuery('#tanggal_kedaluwarsa').on('change',function(){
      var tanggal=jQuery(this).val();
      var idproduk=jQuery('#produk').val();
      if(tanggal){
        jQuery.ajax({
          url:'create/getnotabeli/'+idproduk+'/'+tanggal,
          type:"GET",
          dataType:"json",
          success:function(nota)
          {
            jQuery('#nota_beli').empty();
            $('#nota_beli').append('<option disabled selected value>Pilih Nota Beli</option>');
            jQuery.each(nota, function(key,value){
              console.log(value.substring(0,10));
              $('#nota_beli').append('<option value="'+value.substring(0,10)+'">'+value+'</option>');
            });
          }
        });
      }
      else{
        jQuery('#tanggal_kedaluwarsa').empty();
      }
    });

    jQuery('#nota_beli').on('change',function(){
      var nomor_nota=jQuery(this).val();
      var idproduk=jQuery('#produk').val();
      var tanggal=jQuery('#tanggal_kedaluwarsa').val();
      if(nomor_nota){
        jQuery.ajax({
          url:'create/getprodukbeli/'+nomor_nota+'/'+idproduk+'/'+tanggal,
          type:"GET",
          dataType:"json",
          success:function(jumlah_dibeli)
          {
            var array_beli=jumlah_dibeli.split(',');
            $('#jumlah_retur').attr({
               'max' : array_beli[1],
               'min' : 1          
            });
            $('#harga_beli').val(array_beli[0]);
            jumlah_dibeli=array_beli[1];
          }
        });
      }
    });

    // jQuery('#jumlah_retur').on('change',function(){
    //   if($('#jumlah_retur').val()>jumlah_dibeli){
    //     $('#jumlah_retur').val(jumlah_dibeli);
    //   }
    //   else if($('#jumlah_retur').val()<0){
    //     $('#jumlah_retur').val(0); 
    //   }
    // });
  })

  function retur_toggle(){
    if(document.getElementById('jenis_retur').value=='Produk'){
      $('#retur-produk').toggleClass('hide');
      document.getElementById("tanggal_baru").required = true;
      document.getElementById("lokasi").required = true;
    }
    else{
      $('#retur-produk').toggleClass('hide');
      document.getElementById("tanggal_baru").required = false;
      document.getElementById("lokasi").required = false;
    }
  }

  function getgrandtotal(){
    var harga_beli = document.getElementById('harga_beli').value;
    var jumlah = document.getElementById('jumlah_retur').value;
    var grand_total=harga_beli*jumlah;
    grand_total = Math.ceil(grand_total);
    document.getElementById('grand').innerHTML = "Grand Total: Rp"+separator(grand_total);
    document.getElementById('grand_total').value = grand_total;
  }

  function separator(number) {
      var parts=number.toString().split(".");
      return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
  }
</script>
@endsection