<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  @if($detail_toko!=null)
    <title>{{$detail_toko->nama_toko}} | Registrasi</title>
  @else
    <title>WEBTREK | Registrasi</title>
  @endif
  
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('dist/css/webtrek-admin.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('plugins/iCheck/square/blue.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    @if($detail_toko!=null)
      <a href="{{url('/home')}}"><b>{{strtoupper($detail_toko->nama_toko)}}</b></a>
    @else
      <a href="{{url('/home')}}"><b>WEBTREK</b></a>
    @endif
    
  </div>

  <div class="register-box-body">

    <form role="form" action="{{url('register')}}" method="post" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
        <input type="text" id="ucfirst" class="form-control" name="nama" placeholder="Nama Lengkap" value="{{ old('nama') }}" required autofocus>
        @if ($errors->has('nama'))
          <span class="help-block">
            <strong>{{ $errors->first('nama') }}</strong>
          </span>
        @endif
      </div>
      <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
        <input type="username" class="form-control" name="username" placeholder="username" value="{{ old('username') }}" required>
        @if ($errors->has('username'))
          <span class="help-block">
            <strong>{{ $errors->first('username') }}</strong>
          </span>
        @endif
      </div>
      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <input type="password" id="password-field1" class="form-control" name="password" placeholder="password" value="{{ old('password') }}" required>
        @if ($errors->has('password'))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
        @endif
        <span toggle="#password-field1" class="fa fa-fw fa-eye field-icon toggle-password"></span>
      </div>
      <div class="form-group{{ $errors->has('ulang_pass') ? ' has-error' : '' }}">
        <input type="password" id="password-field2" class="form-control" name="ulang_pass" placeholder="ulangi password" value="{{ old('ulang_pass') }}" required>
        @if ($errors->has('ulang_pass'))
          <span class="help-block">
            <strong>{{ $errors->first('ulang_pass') }}</strong>
          </span>
        @endif
        <span toggle="#password-field2" class="fa fa-fw fa-eye field-icon toggle-password"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-12">
          <button type="submit" class="btn btn-success btn-block btn-flat">Daftar</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <br>
    <a href="{{url('login')}}" class="text-center">Sudah punya akun?</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });

  $("#ucfirst").on('input', function(){
    
    var start = this.selectionStart,
        end = this.selectionEnd;
    
    this.value = ucfirst(this.value);
    this.setSelectionRange(start, end);
});
  $(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
</script>
</body>
</html>
