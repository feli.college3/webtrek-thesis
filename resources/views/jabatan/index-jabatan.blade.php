@extends('layouts.index-admin', ['title' => 'Jabatan'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        DATA JABATAN
      </h1>
      <button class="btn btn-success btn-tambah"><a href="{{ route('jabatan.create') }}" class="font-white">Tambah Jabatan Baru</a></button>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Jabatan</h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama Jabatan</th>
                  <th class="no-sort"></th>
                  <th class="no-sort"></th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($list_jabatan))
                    @foreach($list_jabatan as $post)
                        <tr>
                          <td>{{$post->idjabatan}}</td>
                          <td>{{$post->nama_jabatan}}</td>
                          <td class="center-aligned">
                            <button class="btn btn-warning"><a href="{{ route('jabatan.edit', $post->idjabatan) }}" class="font-white">Edit</a></button>
                          </td>
                          <td class="center-aligned">
                            <form method="post" action="{{ route('jabatan.destroy', $post->idjabatan) }}">
                              {{ method_field('delete')}}
                              {{ csrf_field() }}
                              <button class="btn btn-danger" type="submit" name="delete" onclick="return confirm('Anda yakin untuk menghapus jabatan?')">Hapus</button>
                            </form>         
                          </td>
                        </tr>
                     @endforeach
                    @endif  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection