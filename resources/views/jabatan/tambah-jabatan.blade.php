@extends('layouts.index-admin', ['title' => 'Jabatan'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah Jabatan Baru
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{ url('jabatan') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="form-group">
                  <label>Nama Jabatan(*)</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Jabatan" required autofocus>
                </div>
                <p><b>(*) wajib diisi</b></p>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection