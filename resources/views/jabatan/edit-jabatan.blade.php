@extends('layouts.index-admin', ['title' => 'Jabatan'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Ubah Data Jabatan
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{ route('jabatan.update', $id) }}" enctype="multipart/form-data">
                {{ method_field("PUT") }}
                {{ csrf_field() }}
                <!-- text input -->
                <div class="form-group">
                  <label>Nama Jabatan(*)</label>
                  <input type="text" name="nama" class="form-control" value="{{$jab->nama_jabatan}}" required autofocus/>
                  @if ($errors->has('Nama Jabatan(*)'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nama') }}</strong>
                    </span>
                  @endif
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
  <script>
    alert('{{ session('status') }}')
  </script>
@endif
@endsection