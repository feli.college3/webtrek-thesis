@extends('layouts.index-admin', ['title' => 'Penyesuaian Stok'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        PENYESUAIAN STOK PRODUK
      </b></h1>
      <button class="btn btn-success btn-tambah"><a href="{{ route('penyesuaian-stok.createid', $id) }}" class="font-white">Tambah Penyesuaian</a></button>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"><b>{{$id}} - {{$produk->nama_produk}}</b></h3><br>
            </div>
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                  <tr>
                  <th class="no-sort center-aligned">No</th>
                  @if($detail_toko->fitur_stok_fifo==1)
                    <th>Tanggal Kedaluwarsa</th>
                  @endif
                  <th>Waktu Penyesuaian</th>
                  <th class="no-sort">Jenis</th>
                  <th class="no-sort right-aligned">Jumlah</th>
                  <th class="no-sort">Keterangan</th>
                  <th class="no-sort">Status</th>
                </tr>
                </thead>
                <tbody>
                  @for ($i = 0; $i < count($list_penyesuaian); $i++)
                    <tr>
                      <td class="center-aligned">{{$i+1}}</td>
                      @if($detail_toko->fitur_stok_fifo==1)
                        <td>{{date('d-m-Y', strtotime($list_penyesuaian[$i]->tanggal_kadaluwarsa))}}</td>
                      @endif
                      <td>{{date('d-m-Y, H:i', strtotime($list_penyesuaian[$i]->tanggal))}}</td>
                      @if($list_penyesuaian[$i]->jenis=='Masuk')
                        <td>Penambahan</td>
                      @else
                        <td>Pengeluaran</td>
                      @endif
                      <td class="right-aligned">{{number_format($list_penyesuaian[$i]->jumlah,0,".",",")}}</td>
                      <td>{{$list_penyesuaian[$i]->keterangan}}</td>
                      <td>{{$list_penyesuaian[$i]->status}}</td>
                    </tr>
                  @endfor
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection