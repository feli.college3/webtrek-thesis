@extends('layouts.index-admin', ['title' => 'Atur Pengiriman'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        VALIDASI PENYESUAIAN STOK
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <form role="form" id="form-validasi-stok" method="POST" action="{{route('penyesuaian-stok.validasi')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <!-- /.box-header -->
              <!-- <div class="box-header">
              </div> -->
              <div class="box-body">
                <table id="tabeldata" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th class="no-sort center-aligned">Cek</th>
                    <th>ID Produk</th>
                    <th>Nama Produk</th>
                    <th class="no-sort">Tanggal Kedaluwarsa</th>
                    <th class="no-sort">Waktu Penyesuaian</th>
                    <th class="no-sort">Jenis</th>
                    <th class="right-aligned">Jumlah</th>
                    <th class="no-sort">Keterangan</th>
                  </tr>
                  </thead>
                  <tbody>
                  @if(count($list_penyesuaian)>0)
                    <?php $index=0; ?>
                      @foreach($list_penyesuaian as $post)
                          <tr>
                            <td class="center-aligned"><input type="checkbox" class="cek_produk" name="produk{{$index}}" value="{{$post->idpenyesuaian}}"></td>
                            <td>{{$post->idproduk}}</td>
                            <td>{{$post->nama_produk}}</td>
                            <td>{{date('d-m-Y', strtotime($post->tanggal_kadaluwarsa))}}</td>
                            <td>{{date('d-m-Y, H:i', strtotime($post->tanggal))}}</td>
                            @if($post->jenis=='Masuk')
                              <td>Penambahan</td>
                            @else
                              <td>Pengeluaran</td>
                            @endif
                            <td class="right-aligned">{{number_format($post->jumlah,0,".",",")}}</td>
                            <td>{{$post->keterangan}}</td>
                          </tr>
                        <?php $index++; ?>
                      @endforeach
                    @endif 
                  </tbody>
                </table>
                @if(count($list_penyesuaian)>0)
                  <label><input type="checkbox" name="cek_all" id="cek_all" value="cek_all">&nbsp;&nbsp;Cek Semua</label><br>
                  <input type="hidden" name="btn-action">
                  <input type="submit" name="action" id="btn-setuju" class="btn btn-success" value="Setujui">
                  <input type="submit" name="action" id="btn-tolak" class="btn btn-danger" value="Tolak">
                @endif
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

          </div>
          <!-- /.col -->
        </div>
      <!-- /.row -->
      </form>
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif

<script src="{{ asset('customer/js/jquery.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('customer/js/popper.min.js') }}"></script>
<script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
<script type="text/javascript">

  $('#cek_all').on('click', function(e) {
    $('.cek_produk').prop('checked', $(e.target).prop('checked'));
  });

  $('#form-validasi-stok').on('submit', (event) => {
    event.preventDefault();
    if ($("#form-validasi-stok input:checkbox:checked").length > 0){
       // var btn = $(this).find("input[type=submit]:focus");
       var action = $("input[type=submit][clicked=true]").val();
       var text="Anda yakin menyetujui penyesuaian?";
       $('input[name="btn-action"]').val('setuju');
       if(action=='Tolak'){
          text="Anda yakin menolak penyesuaian?";
          $('input[name="btn-action"]').val('tolak');
       }
       Swal.fire({
        title: text,
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#44b839',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Batalkan',
        confirmButtonText: 'Ya',
        reverseButtons: true,
      }).then((result) => {
        if (result.value) {
          Swal.fire({
            title: 'Penyesuaian Stok Berhasil Disimpan',
            icon: 'success',
            showConfirmButton: false,
          })
          setTimeout(function(){
            var form = $('#form-validasi-stok');
            form.trigger('submit');
          },500);
        }
        else{
          
        }
      })
    }
    else{
      Swal.fire({
        title: 'Pilih minimal 1 penyesuaian!',
        icon: 'warning',
        showConfirmButton: false,
        timer:1000,
      })
    }
    
  })
  $("form input[type=submit]").click(function() {
    $("input[type=submit]", $(this).parents("form")).removeAttr("clicked");
    $(this).attr("clicked", "true");
  });
</script>
@endsection