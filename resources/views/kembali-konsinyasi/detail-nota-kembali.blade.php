@extends('layouts.index-admin', ['title' => 'Nota Kembali Konsinyasi'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DETAIL KEMBALI KONSINYASI
      </b></h1>
        <!-- <button class="btn btn-success btn-tambah"><a href="{{ route('produk.create') }}" class="font-white">Tambah Produk Baru</a></button> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @if(count($produk)>0)
            <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"><b>No. Nota: {{$id}}</b></h3>
            </div>
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                  <tr>
                  <th>ID Produk</th>
                  <th>Nama Produk</th>
                  <th class="right-aligned">Jumlah Terjual</th>
                  <th class="right-aligned">Jumlah Kembali</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($produk as $post)
                    <tr>
                        <td>{{$post->idproduk_konsinyasi}}</td>
                        <td>{{$post->nama_produk}}</td>
                        <td class="right-aligned">{{number_format($post->jumlah_terjual,0,".",",")}}</td>
                        <td class="right-aligned">{{number_format($post->jumlah_kembali,0,".",",")}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          @endif

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection