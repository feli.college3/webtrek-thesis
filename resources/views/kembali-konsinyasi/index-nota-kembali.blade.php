@extends('layouts.index-admin', ['title' => 'Nota Kembali Konsinyasi'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DAFTAR NOTA KEMBALI KONSINYASI
      </b></h1>
        <button class="btn btn-success btn-tambah"><a href="{{ route('kembali-konsinyasi.create') }}" class="font-white">Buat Nota Kembali</a></button>
    </section>

    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No. Nota Kembali</th>
                  <th>No. Nota Beli</th>
                  <th>Waktu Kembali</th>
                  <th class="no-sort">Konsinyator</th>
                  <th class="no-sort">Pegawai</th>
                  <th class="right-aligned">Total Dibayar (Rp)</th>
                  <th class="right-aligned">Total Keuntungan (Rp)</th>
                  <th class="no-sort">Keterangan</th>
                </tr>
                </thead>
                <tbody>
                @if(count($list_nota)>0)
                    @foreach($list_nota as $post)
                        <tr>
                          <td><a href="{{route('kembali-konsinyasi.show', $post->no_nota_kembali)}}">{{$post->no_nota_kembali}}</a></td>
                          <td><a href="{{route('beli-konsinyasi.show', $post->no_nota_beli_konsinyasi)}}">{{$post->no_nota_beli_konsinyasi}}</a></td>
                          <td>{{date('d-m-Y H:i', strtotime($post->tanggal))}}</td>
                          <td>{{$post->nama_supplier}}</td>
                          <td>{{$post->nama_pegawai}}</td>
                          <td class="right-aligned">{{number_format($post->total_dibayar,0,".",",")}}</td>
                          <td class="right-aligned">{{number_format($post->total_keuntungan,0,".",",")}}</td>
                          <td>{{$post->keterangan}}</td>
                        </tr>
                    @endforeach
                  @endif 
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection