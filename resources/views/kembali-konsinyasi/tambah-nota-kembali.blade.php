@extends('layouts.index-admin', ['title' => 'Nota Kembali Konsinyasi'])
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Buat Nota Kembali Konsinyasi
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <form role="form" method="POST" action="{{ url('kembali-konsinyasi') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-xs-12">
            <div class="box box-warning">
              <!-- /.box-header -->
              <div class="box-body">

                  <div class="form-group">
                    <label>Konsinyator(*)</label>
                    <select id="konsinyator" name="konsinyator" class="form-control" required>
                      <option disabled selected value>Pilih Konsinyator</option>
                      @foreach($list_supplier as $plg)
                      <option value="{{$plg->idsupplier}}">{{$plg->nama_supplier}}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="form-group">
                    <label>No. Nota Beli Konsinyasi(*)</label>
                    <select id="no_nota" name="no_nota" class="form-control" required>
                      <option disabled selected value>Pilih Nomor Nota</option>
                    </select>
                  </div>

                  <div class="form-group{{ $errors->has('keterangan') ? ' has-error' : '' }}">
                    <label>Keterangan(*)</label>
                    <textarea name="keterangan" class="form-control" value="{{ old('keterangan') }}" required>-</textarea>
                    @if ($errors->has('keterangan'))
                      <span class="help-block">
                          <strong>{{ $errors->first('keterangan') }}</strong>
                      </span>
                    @endif
                  </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

          </div>
        <!-- /.col -->
        </div>

        <h4><label name="dibayar" id="dibayar"><b>Total Dibayar: Rp0</b></label></h4>
        <h5><label name="untung" id="untung"><b>Total Keuntungan: Rp0</b></label></h5>
        <input type="hidden" name="total_dibayar" id="total_dibayar"/>
        <input type="hidden" name="total_untung" id="total_untung"/>
        <input type="hidden" name="total_produk" id="total_produk"/>

        <table class="table" id="produk_konsi">
          <thead>
            <tr>
              <th>Produk Konsinyasi</th>
              <th class="right-aligned">Harga Beli (Rp)</th>
              <th class="right-aligned">Jumlah Titipan</th>
              <th class="right-aligned">Jumlah Terjual</th>
              <th class="right-aligned">Jumlah Kembali</th>
              <th class="right-aligned">Subtotal (Rp)</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
        <p><b>(*) wajib diisi</b></p>
        <div class="center-aligned"><button type="submit" class="btn btn-success">Buat Nota</button></div>
      </form>
    </section>

<!-- Ajax 3.4.1 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  var jumproduk=0;
  jQuery(document).ready(function(){

    jQuery('#konsinyator').on('change',function(){
      var idsupplier=jQuery(this).val();
      if(idsupplier){
        jQuery.ajax({
          url:'create/getnotabelikonsi/'+idsupplier,
          type:"GET",
          dataType:"json",
          success:function(nota)
          {
            jQuery('#no_nota').empty();
            $('#no_nota').append('<option disabled selected value>Pilih Nomor Nota</option>');
            jQuery.each(nota, function(key,value){
              $('#no_nota').append('<option value="'+value+'">'+value+'</option>');
            });
          }
        });
      }
      else{
        $('select[name="no_nota"]').empty();
      }
    });

    jQuery('#no_nota').on('change',function(){
      var no_nota=jQuery(this).val();
      if(no_nota){
        $('#produk_konsi > tbody:last-child').empty();
        jQuery.ajax({
          url:'create/getprodukkonsinyasi/'+no_nota,
          type:"GET",
          dataType:"json",
          success:function(produk_array)
          {
            jumproduk=produk_array.length;
            // console.log(jumproduk);
            // jQuery.each(produk_array, function(key,value){
            //   $('#produk_konsi > tbody:last-child').append('<tr><td><input type="text" name="halo" class="form-control" value="'+value+'" disabled></td>'+'<td><input type="number" name="jumlah" class="form-control" value="0"/></td></tr>');
            // });

            for (let i = 0; i < jumproduk; ++i) {
              $('#produk_konsi > tbody:last-child').append('<tr><td><input type="text" name="textproduk'+[i]+'" class="form-control" value="('+produk_array[i]['id']+') '+produk_array[i]['nama']+'" readonly/></td>'+
                '<td><input type="text" name="harga_beli'+[i]+'" id="harga_beli'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['harga_beli']+'" readonly/></td>'+
                '<td><input type="text" name="jumlah_dibeli'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['jumlah_dibeli']+'" readonly/></td>'+
                '<td><input type="text" name="jumlah_terjual'+[i]+'" id="jumlah_terjual'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['jumlah_terjual']+'" readonly/></td>'+
              '<td><input type="text" name="jumlah_kembali'+[i]+'" name="jumlah_kembali'+[i]+'" class="form-control right-aligned" value="'+produk_array[i]['jumlah_kembali']+'" readonly/></td>'+
              '<td><input type="text" name="subtotal_dibayar'+[i]+'" id="subtotal_dibayar'+[i]+'" class="form-control right-aligned" value="0" readonly/><input type="hidden" name="produk'+[i]+'" id="produk'+[i]+'" value="'+produk_array[i]['id']+'"/></td></tr>');
            // });
            }
            var total_dibayar=0;
            var total_untung=0;
            $('#total_produk').val(jumproduk);
            for (let i = 0; i < jumproduk; ++i) {
              //dibayar
              var jum_jual=$('#jumlah_terjual'+[i]).val();
              var harga_beli=$('#harga_beli'+[i]).val();
              var sub_dibayar=jum_jual*harga_beli;
              total_dibayar+=sub_dibayar;
              $('#subtotal_dibayar'+[i]).val(sub_dibayar);

              //untung
              var harga_jual=produk_array[i]['harga_jual'];
              var sub_untung=harga_jual*jum_jual;
              total_untung+=sub_untung;
              total_untung-=sub_dibayar;
            }
            $("#dibayar").text("Total Dibayar: Rp"+separator(total_dibayar));
            $('#total_dibayar').val(total_dibayar);
            $('#total_untung').val(total_untung);
            if(total_untung>=0){
              $("#untung").toggleClass('green', true);
              $("#untung").toggleClass('red', false);
              $("#untung").text("Total Keuntungan: Rp"+separator(total_untung));
            }
            else{
              total_untung*=-1;
              $("#untung").toggleClass('red', true);
              $("#untung").toggleClass('green', false);
              $("#untung").text("Total Kerugian: Rp"+separator(total_untung));
            }
          }
        });
      }
      else{
        // $('table[name="produk_konsi"]').empty();
      }
    });
  });
  // $("#no_nota").change(function(){
  // });
  //waktu page load
  // $(function() {
  //   console.log('halo');
  //   
  // });
  
  // function getsubtotal_dibayar(){
  //   for (var i = 1; i <= jumproduk; i++) {
  //     console.log(i);
  //     var prod = "produk"+index;
  //     var jml = "jumlah_terjual"+index;
  //     var hrg = "harga_beli"+index;
  //     var sub = "subtotal_dibayar"+index;
  //     var jum = document.getElementById(jml).value;
  //     var harga_beli = parseFloat(document.getElementById(hrg).value);
  //     var total=jum*harga_beli;
  //     total = Math.round(total);
  //     document.getElementById(sub).value = total;
  //   }
  // }

  function getsubtotal_untung(index){
    var prod = "produk"+index;
    var jml = "jumlah_kembali"+index;
    var hrg = "harga_beli"+index;
    var sub = "subtotal_dibayar"+index;
    var jum = document.getElementById(jml).value;
    var harga_beli = parseFloat(document.getElementById(hrg).value);
    var total=jum*harga_beli;
    total = Math.round(total);
    document.getElementById(sub).value = total;
  }

  function gettotaldibayar(){
    var total = 0;
    var data = 0;
    for(i=0;i<jumproduk;i++){
      var sub = "subtotal_dibayar"+i;
      var subtotal_dibayar = parseFloat(document.getElementById(sub).value);
      total+=subtotal_dibayar;
    }
    total = Math.round(total);
    document.getElementById('dibayar').innerHTML = "Grand Total: Rp"+separator(total);
    document.getElementById('total_dibayar').value = total;
  }

  function getkeuntungan(){
    var untung = 0;
    var data = 0;
    for(i=0;i<jumproduk;i++){
      var sub = "subtotal_dibayar"+i;
      var subtotal_dibayar = parseFloat(document.getElementById(sub).value);
      untung+=subtotal_dibayar;
    }
    untung = Math.round(untung);
    document.getElementById('dibayar').innerHTML = "Grand Total: Rp"+separator(untung);
    document.getElementById('total_dibayar').value = untung;
  }

  function separator(number) {
      var parts=number.toString().split(".");
      return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
  }

</script>
@endsection