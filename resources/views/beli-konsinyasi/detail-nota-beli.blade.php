@extends('layouts.index-admin', ['title' => 'Nota Beli Konsinyasi'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DETAIL NOTA TITIP KONSINYASI
      </b></h1>
        <!-- <button class="btn btn-success btn-tambah"><a href="{{ route('produk.create') }}" class="font-white">Tambah Produk Baru</a></button> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @if(count($produk)>0)
            <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"><b>No. Nota: {{$id}}</b></h3>
            </div>
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                  <tr>
                  <th>ID Produk</th>
                  <th>Nama Produk</th>
                  <th class="no-sort right-aligned">Harga Beli (Rp)</th>
                  <th class="no-sort right-aligned">Jumlah</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($produk as $post)
                    <tr>
                        <td>{{$post->idproduk_konsinyasi}}</td>
                        <td>{{$post->nama_produk}}</td>
                        <td class="right-aligned">{{number_format($post->harga_beli,0,".",",")}}</td>
                        <td class="right-aligned">{{$post->jumlah}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          @endif  

          <!-- @if(!empty($produk_konsinyasi))
            <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Produk Konsinyasi</h3>
            </div>
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                  <tr>
                  <th>ID Produk</th>
                  <th>Nama Produk</th>
                  <th>Harga Beli</th>
                  <th>Jumlah</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($produk_konsinyasi as $post)
                    <tr>
                        <td>{{$post->idproduk}}</td>
                        <td>{{$post->nama_produk}}</td>
                        <td>{{$post->harga_beli}}</td>
                        <td>{{$post->jumlah}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          @endif   -->
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection