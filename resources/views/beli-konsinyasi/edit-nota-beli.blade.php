@extends('layouts.index-admin', ['title' => 'Pembelian'])
@section('content')

    <!-- Content Header (Page header) -->
    <form role="form" method="POST" action="{{ route('beli-konsinyasi.update',$id) }}" enctype="multipart/form-data">
        {{ method_field("PUT") }}
        {{ csrf_field() }}
        <section class="content-header">
            <h1 class="center-aligned"><b>Penerimaan Produk Konsinyasi</b></h1>
            <h1><b>
                No. Nota: {{$id}}
            </b></h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <!-- /.box-header -->
                <div class="box-body">
                  
                    <!-- text input -->
                        <div class="form-group">
                            <div class="row bottom-row">
                                <div class="col-sm-6">
                                    <h4>Konsinyator: <b>{{$nota_titip->nama_supplier}}</b></h4>
                                    <h4>{{$nota_titip->alamat_supplier}}</h4>

                                </div>

                                <div>
                                    <label id="label-grand">Rp0</label>
                                    <input type="hidden" id="grand-total" name="grand_total"/>
                                    <input type="hidden" id="total-data" name="total_data"/>
                                    <input type="hidden" id="jumlah_baris" name="jumlah_baris"/>
                                </div>
                            </div>
                        </div>

                        <a id="update-ongkir" onclick="tambah_baris()">Tambah Baris Baru</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a id="cursor-pointer" onclick="hapus_baris()">Hapus Baris Terakhir</a>
                        <div class="narrow-rows scrollable-40">
                            <table class="table" id="edit-nota">
                                <thead>
                                    <tr>
                                        <th class="center-aligned">No.</th>
                                        <th>Nama Produk</th>
                                        <th class="right-aligned">Jumlah</th>
                                        <th class="right-aligned">Harga</th>
                                        <th class="right-aligned">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for($i=1;$i<=$total_data;$i++)
                                        <tr>
                                            <td class="center-aligned">{{$i}}</td>
                                            <td><h4>{{ $detail_nota[$i-1]->nama_produk }}</h4></td>
                                            <input type="hidden" id="{{ 'produk'.$i }}" name="{{ 'produk'.$i }}" value="{{ $detail_nota[$i-1]->idproduk }}"/>
                                            <td class="right-aligned"><input type="number" class="small-input right-aligned form-control font-20px" id="{{ 'jumlah'.$i }}" name="{{ 'jumlah'.$i }}" onkeyup="getsubtotal(); getgrandtotal()" onchange="getsubtotal(); getgrandtotal()" value="{{ $detail_nota[$i-1]->jumlah }}" required/></td>
                                            <td class="right-aligned"><input type="number" class="small-input right-aligned form-control font-20px" id="{{ 'harga'.$i }}" name="{{ 'harga'.$i }}" onkeyup="getsubtotal(); getgrandtotal()" min="0" onchange="getsubtotal(); getgrandtotal()" value="{{ $detail_nota[$i-1]->harga_beli }}" required/></td>
                                            <td class="right-aligned"><label id="{{ 'subtotal'.$i }}" value="{{ old('subtotal'.$i) }}"></label></td>
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="row bottom-row margin-1-br">
                                <div class="col-sm-2">
                                    <label class="control-label bold">Lama Penitipan (hari)</label>
                                    <input id="lama_penitipan" type="number" class="form-control right-aligned" name="lama_penitipan" min="1" value="7">
                                </div>

                                <div class="col-sm-5">
                                    <label class="control-label bold">Keterangan</label>
                                    <div class="">
                                        <textarea id="keterangan"class="form-control" name="keterangan">{{$nota_titip->keterangan}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                                <!-- <button type="button" onclick="klik()" id="yuhu">Klik aku mas!</button> -->
                        <div class="form-group">
                            <div class="center-aligned">
                                <button type="submit" name="action" class="btn btn-success" value="simpan">Simpan</button>
                            </div>
                        </div>
                  </form>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
    </form>
@if (session('alert'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
        swal("{{ session('alert') }}");
    </script>
@endif
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
  // $(function () {
  //   //Initialize Select2 Elements
  //   // $('.select2').select2()
  // })

    window.onload = function() {
        var total_data = {!! json_encode($total_data) !!};
        document.getElementById('total-data').value=total_data;
        document.getElementById('jumlah_baris').value = total_data;
        getsubtotal();
        getgrandtotal();
    };

    function getsubtotal(){
        var jumlah_baris = document.getElementById('jumlah_baris').value;
        console.log('subtotal: '+jumlah_baris);
        for(i=1;i<=jumlah_baris;i++)
        {
            var jml = "jumlah"+i;
            var hrg = "harga"+i;
            var sub = "subtotal"+i;
            var element_harga=document.getElementById(hrg);
            var element_jumlah=document.getElementById(jml);
            if((element_harga.length!=0 && element_jumlah.length!=0) && (element_harga.value!="" && element_jumlah.value!=""))
            {
                var jum = document.getElementById(jml).value;
                var harga_beli = document.getElementById(hrg).value;
                var total = jum*harga_beli;
                total = Math.round(total);
                // return total;
                document.getElementById(sub).innerHTML = total;
            }
        }
    }

    function getgrandtotal(){
        var grand = 0;
        var data = 0;
        var jumlah_baris = document.getElementById('jumlah_baris').value;
        for(i=1;i<=jumlah_baris;i++){
            var sub = "subtotal"+i;
            var hrg = "harga"+i;
            if(document.getElementById(hrg).value.length!=0)
            {
                // alert(document.getElementById(hrg).innerHTML.length);
                var helper = parseFloat(document.getElementById(sub).innerHTML);
                // alert(helper);
                grand += helper;
                grand = Math.round(grand);
                document.getElementById('label-grand').innerHTML = "Rp"+separator(grand);
                document.getElementById('grand-total').value = grand;

                data++;
                document.getElementById('total-data').value = data;
            }
        }
    }

    function separator(number) {
        var parts=number.toString().split(".");
        return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
    }

    function tambah_baris(){
        var jumlah_baris=document.getElementById('jumlah_baris').value;
        var total_data=parseInt(document.getElementById('total-data').value);
        var semua_produk = {!! json_encode($semua_produk) !!};
        var index=total_data+1;
        if(jumlah_baris==total_data){
            $('#edit-nota > tbody:last-child').append(
                '<tr id="no'+index+'"><td class="center-aligned">'+index+'</td>'+'<td><select id="'+"produk"+index+'" name="'+"produk"+index+'" class="form-control" required>'+
                '<option disabled selected value="bambang">Pilih Produk</option>'+
                    
                '</select></td>'+
                
                  '<td><input type="number" name="jumlah'+[i]+'" id="jumlah'+[index]+'" class="form-control right-aligned" min="0" required onchange="getsubtotal();getgrandtotal()" onkeyup="getsubtotal();getgrandtotal()"/></td>'+
                    '<td><input type="number" name="harga'+[index]+'" id="harga'+[index]+'" class="form-control right-aligned" required onchange="getsubtotal();getgrandtotal()" onkeyup="getsubtotal();getgrandtotal()"/></td>'+
                  '<td class="right-aligned"><label name="subtotal'+[i]+'" id="subtotal'+[index]+'" value="0"></label></td></tr>');
            for(i=0;i<semua_produk.length;i++){
                $('#produk'+index).append('<option value="'+semua_produk[i]['idproduk']+'">'+semua_produk[i]['nama_produk']+'</option>' );
                // var option_produk = new Option(semua_produk[i]['nama_produk'], semua_produk[i]['idproduk']);
                // /// jquerify the DOM object 'o' so we can use the html method
                // $(option_produk).html(semua_produk[i]['nama_produk']);
                // $("#produk"+index).append(option_produk);
            }
            document.getElementById('jumlah_baris').value++;
        }
    }

    function hapus_baris(){
        var jumlah_baris=document.getElementById('jumlah_baris').value;
        var total_data_awal = {!! json_encode($total_data) !!};
        if(jumlah_baris>total_data_awal){
            var subtotal=document.getElementById('subtotal'+jumlah_baris).innerHTML;
            if(subtotal!=""){
                document.getElementById('total-data').value--;
            }
            $("#no"+jumlah_baris).remove();
            document.getElementById('jumlah_baris').value--;
        }
    }
</script>