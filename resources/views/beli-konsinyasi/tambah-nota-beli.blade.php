@extends('layouts.index-admin', ['title' => 'Nota Beli Konsinyasi'])
@section('content')
    <!-- Content Header (Page header) -->
    <form role="form" method="POST" action="{{ url('beli-konsinyasi') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <section class="content-header">
            <h1 class="center-aligned"><b>Order Titip Konsinyasi</b></h1>
            <h1><b>
                No. Nota: {{$id_baru}}
            </b></h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <!-- /.box-header -->
                <div class="box-body">
                  
                    <!-- text input -->
                        <!-- <div class="form-group">
                            <div class="row">
                                <div class="col-sm-5">
                                    <label class="no-nota">No. Nota: {{$id_baru}}</label>
                                </div>
                            </div>
                        </div> -->
                        <input type="hidden" name="no_nota" value="{{$id_baru}}"/>
                        <div class="form-group">
                            <div class="row bottom-row">
                                <div class="col-sm-3">
                                    <label class="span-tambah-baru">Konsinyator<a class="btn-tambah-baru3" href="{{ route('supplier.create') }}" target="_blank">Tambah Baru</a></label>
                                    <select id="konsinyator" name="konsinyator" class="form-control" required>
                                        @foreach($list_supplier as $sup)
                                        <option value="{{$sup->idsupplier}}">{{$sup->nama_supplier}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div>
                                    <label id="label-grand">Rp0</label>
                                    <input type="hidden" id="grand-total" name="grand_total"/>
                                    <input type="hidden" id="total-data" name="total_data"/>
                                </div>
                            </div>
                        </div>

                        <div class="narrow-rows scrollable-40">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="center-aligned">No.</th>
                                        <th>Nama Produk</th>
                                        <th class="right-aligned">Jumlah</th>
                                        <th class="right-aligned">Harga</th>
                                        <th class="right-aligned">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for($i=1;$i<=20;$i++)
                                        <tr>
                                            <td class="center-aligned vertical-middle">{{$i}}</td>
                                            <!-- <td><input type="text" class="small-input form-control" id="{{ 'produk'.$i }}" name= "{{ 'produk'.$i }}"/></td> -->
                                            <td><select id="{{ 'produk'.$i }}" name="{{ 'produk'.$i }}" class="form-control" onchange="checkdouble();getdetail('{{$i}}')">
                                                <option selected value>Pilih Produk</option>
                                                @foreach($list_produk as $prod)
                                                <option value="{{$prod->nama_produk}}">{{$prod->nama_produk}}</option>
                                                @endforeach
                                            </select></td>
                                            <td class="right-aligned"><input type="number" class="small-input right-aligned form-control font-20px" id="{{ 'jumlah'.$i }}" name="{{ 'jumlah'.$i }}" onkeyup="getsubtotal('{{$i}}'); getgrandtotal('{{$i}}')" onchange="getsubtotal('{{$i}}'); getgrandtotal('{{$i}}')"/></td>
                                            <td class="right-aligned"><input type="number" class="small-input right-aligned form-control font-20px" id="{{ 'harga'.$i }}" name="{{ 'harga'.$i }}" onkeyup="getsubtotal('{{$i}}'); getgrandtotal('{{$i}}')" min="0" onchange="getsubtotal('{{$i}}'); getgrandtotal('{{$i}}')"/></td>
                                            <td class="right-aligned"><label id="{{ 'subtotal'.$i }}"></label></td>
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="row bottom-row margin-1-br">
                                <!-- <div class="col-sm-2">
                                    <label class="control-label bold">Lama Penitipan (hari)</label>
                                    <input id="lama_penitipan" type="number" class="form-control right-aligned" name="lama_penitipan" min="1" value="7">
                                </div> -->

                                <div class="col-sm-5">
                                    <label class="control-label bold">Keterangan</label>
                                    <div class="">
                                        <textarea id="keterangan"class="form-control" name="keterangan">-</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="center-aligned">
                                <button type="submit" name="action" class="btn btn-success" value="simpan">Buat Nota Titip Konsinyasi</button>
                            </div>
                        </div>
                  </form>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
    </form>
@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert("{{ session('status') }}");
    </script>
@endif
@endsection

<script type="text/javascript">

    function checkdouble() {
        d=document;
        var produk = {!! json_encode($list_produk->toArray()) !!};
        var total_produk=produk.length;
        myArray = [];
        for (h=0;h<20;h++) {
            myArray[h] = d.getElementById('produk'+(h+1)).value;
        }
        for (a=0;a<20;a++) {
            for (b=1;b<=total_produk;b++) {
                d.getElementById('produk'+(a+1)).options[b].style.display = "block";
                for (c=0;c<20;c++) {
                    if(d.getElementById('produk'+(a+1)).options[b].value == myArray[c]) {
                        d.getElementById('produk'+(a+1)).options[b].style.display = "none";
                    }
                }
            }
        }
    }

    function getdetail(index){
        var idproduk=document.getElementById('produk'+index).value;
        if(idproduk!=""){
            jQuery.ajax({
                url:'create/getdetailproduk/'+idproduk,
                type:"GET",
                dataType:"json",
                success:function(detail)
                {
                    var harga_terakhir=parseFloat(detail);
                    document.getElementById("harga"+index).value=harga_terakhir;
                }
            });
        }
    }

    function getsubtotal(index){
        for(i=1;i<=20;i++)
        {
            var jml = "jumlah"+index;
            var hrg = "harga"+index;
            var sub = "subtotal"+index;
            if(document.getElementById(hrg).length!=0 && document.getElementById(jml).length!=0)
            {
                var jum = document.getElementById(jml).value;
                var harga_beli = document.getElementById(hrg).value;
                var total = jum*harga_beli;
                total = Math.round(total);
                // return total;
                document.getElementById(sub).innerHTML = total;
            }
        }
    }

    function getgrandtotal(index){
        var grand = 0;
        var data = 0;
        for(i=1;i<=20;i++){
            var sub = "subtotal"+i;
            var hrg = "harga"+i;
            if(document.getElementById(hrg).value.length!=0)
            {
                // alert(document.getElementById(hrg).innerHTML.length);
                var helper = parseFloat(document.getElementById(sub).innerHTML);
                // alert(helper);
                grand += helper;
                grand = Math.round(grand);
                document.getElementById('label-grand').innerHTML = "Rp"+separator(grand);
                document.getElementById('grand-total').value = grand;

                data++;
                document.getElementById('total-data').value = data;
            }
        }
    }

    function separator(number) {
        var parts=number.toString().split(".");
        return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
    }

    function tempo(){
        if(document.getElementById('belum-lunas').selected) $('#tempo').toggleClass('hide');
        else $('#tempo').toggleClass('hide');
    }
</script>