@extends('layouts.index-admin', ['title' => 'Nota Beli Konsinyasi'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DAFTAR NOTA KONSINYASI DIPESAN
      </b></h1>
    </section>

    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No. Nota</th>
                  <th class="no-sort">Konsinyator</th>
                  <th>Tanggal Dipesan</th>
                  <th class="no-sort">Pegawai</th>
                  <th>Keterangan</th>
                  <th class="no-sort center-aligned">Sudah Datang</th>
                </tr>
                </thead>
                <tbody>
                  @if(count($list_nota)>0)
                    @foreach($list_nota as $post)
                      <tr>
                        <td><a href="{{route('beli-konsinyasi.show', $post->no_nota_beli_konsinyasi)}}">{{$post->no_nota_beli_konsinyasi}}</a></td>
                        <td>{{$post->nama_supplier}}</td>
                        <td>{{date('d-m-Y', strtotime($post->tanggal_dipesan))}}</td>
                        <td>{{$post->nama_pegawai}}</td>
                        <td>{{$post->keterangan}}</td>
                        <td class="center-aligned">
                          <button class="btn btn-warning"><a href="{{ route('beli-konsinyasi.edit', $post->no_nota_beli_konsinyasi) }}" class="font-white">Edit</a></button>
                        </td>
                      </tr>
                    @endforeach
                  @endif 
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert("{{ session('status') }}")
    </script>
@endif
@endsection