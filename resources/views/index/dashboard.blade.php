@extends('layouts.index-admin', ['title' => 'Dashboard'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DASHBOARD
      </b></h1>
      <div class="right-aligned">
        <a href="{{route('dashboard')}}" class="btn-tambah"><i class="fa fa-refresh"></i> Refresh</a>
      </div>
    </section>

    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        @if($jumlah_panel==3||$jumlah_panel==5)
          <div class="col-lg-4 col-xs-6">
        @elseif($jumlah_panel==4)
          <div class="col-lg-3 col-xs-6">
        @endif
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$nota_belum_diproses}}</h3>

              <p>Pesanan Belum Diproses</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{{route('transaksi', 'belum-diproses')}}" class="small-box-footer">Lihat daftar <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        @if($detail_toko->fitur_kirim==1)
          @if($jumlah_panel==3||$jumlah_panel==5)
            <div class="col-lg-4 col-xs-6">
          @elseif($jumlah_panel==4)
            <div class="col-lg-3 col-xs-6">
          @endif
            <!-- small box -->
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3>{{$nota_belum_dikirim}}</h3>

                <p>Pesanan Perlu Dikirim</p>
              </div>
              <div class="icon">
                <i class="fa fa-truck"></i>
              </div>
              <a href="{{url('atur-pengiriman')}}" class="small-box-footer">Atur pengiriman <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        @endif
        <!-- ./col -->
        
        @if($jumlah_panel==3||$jumlah_panel==5)
          <div class="col-lg-4 col-xs-6">
        @elseif($jumlah_panel==4)
          <div class="col-lg-3 col-xs-6">
        @endif
          <!-- small box -->
          @if($role=='kasir')
            <div class="small-box bg-green">
              <div class="inner">
                <h3>{{$nota_belum_diambil}}</h3>

                <p>Pesanan Belum Diambil</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-box"></i>
              </div>
              <a href="{{route('transaksi', 'belum-diambil')}}" class="small-box-footer">Lihat daftar <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          @elseif($role=='manajer')
            <div class="small-box bg-green">
              <div class="inner">
                <h3>{{$validasi_penyesuaian->total}}</h3>

                <p>Menunggu Verifikasi Stok</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-box"></i>
              </div>
              <a href="{{route('penyesuaian-stok-validasi')}}" class="small-box-footer">Lihat daftar <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          @endif
        </div>

        @if($jumlah_panel==5)
          <div class="col-lg-2 col-xs-6">
          </div>
        @endif

        @if($jumlah_panel==3||$jumlah_panel==5)
          <div class="col-lg-4 col-xs-6">
        @elseif($jumlah_panel==4)
          <div class="col-lg-3 col-xs-6">
        @endif
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{count($perlu_restock)}}</h3>

              <p>Produk Perlu Direstock</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-cart"></i>
            </div>
            <a href="{{route('perlu-restock')}}" class="small-box-footer">Lihat daftar <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        @if($detail_toko->fitur_pembatalan_pesanan==1)
          @if($jumlah_panel==3||$jumlah_panel==5)
            <div class="col-lg-4 col-xs-6">
          @elseif($jumlah_panel==4)
            <div class="col-lg-3 col-xs-6">
          @endif
            <!-- small box -->
            <div class="small-box bg-purple">
              <div class="inner">
                <h3>{{$pesanan_dibatalkan}}</h3>

                <p>Pesanan Perlu Dibatalkan</p>
              </div>
              <div class="icon">
                <i class="ion ion-android-cancel"></i>
              </div>
              <a href="{{route('perlu-dibatalkan')}}" class="small-box-footer">Lihat daftar <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
        @endif
        @if($jumlah_panel==5)
          <div class="col-lg-2 col-xs-6">
          </div>
        @endif

      </div>

      <!-- /.row -->
      <div class="row">
        @if($detail_toko->fitur_stok_fifo==1)
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title"><b>Produk dengan Kedaluwarsa Terdekat</b></h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table class="table table-bordered">
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Produk</th>
                    <th>Tanggal Kedaluwarsa</th>
                    <th style="width: 40px" class="right-aligned">Jumlah</th>
                  </tr>
                  @if(count($list_produk)>0)
                    @if($jumlah_list_produk>10)
                      @for ($i = 0; $i < 10; $i++)
                        <tr>
                          <td>{{$i+1}}</td>
                          <td><a href="{{route('produk.show', $list_produk[$i]->idproduk)}}">{{$list_produk[$i]->nama_produk}}</a></td>
                          <td>{{date("d-m-Y", strtotime($list_produk[$i]->tanggal_kadaluwarsa))}}</td>
                          <td class="right-aligned">{{number_format($list_produk[$i]->stok,0,".",",")}}</td>
                        </tr>
                      @endfor
                    @else
                      @for ($i = 0; $i < $jumlah_list_produk; $i++)
                        <tr>
                          <td>{{$i+1}}</td>
                          <td><a href="{{route('produk.show', $list_produk[$i]->idproduk)}}">{{$list_produk[$i]->nama_produk}}</a></td>
                          <td>{{date("d-m-Y", strtotime($list_produk[$i]->tanggal_kadaluwarsa))}}</td>
                          <td class="right-aligned">{{number_format($list_produk[$i]->stok,0,".",",")}}</td>
                        </tr>
                      @endfor
                    @endif
                  @endif
                </table>
              </div>
                <!-- /.box-body -->
            </div>
          </div>
        @endif
        <div class="col-xs-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Produk Toko Terlaris</b></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Produk</th>
                  <th style="width: 40px" class="right-aligned">Jumlah</th>
                </tr>
                @if(count($produk_terlaris)>0)
                  @for ($i = 0; $i < count($produk_terlaris); $i++)
                    <tr>
                      <td>{{$i+1}}</td>
                      <td>{{$produk_terlaris[$i]->nama_produk}}</td>
                      @if($produk_terlaris[$i]->jumlah_terjual==null)
                        <td class="right-aligned">0</td>
                      @else
                        <td class="right-aligned">{{$produk_terlaris[$i]->jumlah_terjual}}</td>
                      @endif
                    </tr>
                  @endfor
                @endif
              </table>
            </div>
              <!-- /.box-body -->
          </div>
        </div>
        <div class="col-xs-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Stok Produk Terendah</b></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Produk</th>
                  <th style="width: 40px" class="right-aligned">Jumlah</th>
                </tr>
                @if(count($produk_sedikit)>0)
                  @for ($i = 0; $i < count($produk_sedikit); $i++)
                    <tr>
                      <td>{{$i+1}}</td>
                      <td>{{$produk_sedikit[$i]->nama_produk}}</td>
                      @if($produk_sedikit[$i]->total_stok==null)
                        <td class="right-aligned">0</td>
                      @else
                        <td class="right-aligned">{{$produk_sedikit[$i]->total_stok}}</td>
                      @endif
                    </tr>
                  @endfor
                @endif
              </table>
            </div>
              <!-- /.box-body -->
          </div>
        </div>
      </div>

    </section>
@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection