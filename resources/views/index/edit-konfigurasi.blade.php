@extends('layouts.index-admin', ['title' => 'Ubah Konfigurasi'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
          UBAH KONFIGURASI SISTEM
      </b></h1>
    </section>
<style type="text/css">
  
.switch {
  position: relative;
  display: inline-block;
  width: 35px;
  height: 20px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 12px;
  width: 12px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(12px);
  -ms-transform: translateX(12px);
  transform: translateX(15px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 20px;
}

.slider.round:before {
  border-radius: 50%;
}

hr {
  border: 0;
  clear:both;
  display:block;
  width: 100%;               
  background-color:black;
  height: 1px;
  margin-left:0px;
  margin-right:-10px;
}
</style>
<!-- Halo. Kami Alfamart melayani Anda dengan sepenuh hati. Apapun yang Anda butuhkan, kami menyediakannya. Anda pilih produknya, kami kirim sampai ke rumah. -->
    <!-- Main content -->
    <section class="content">
      <form role="form" method="POST" action="{{ route('edit-konfigurasi',$detail_toko->nama_toko) }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title"><b>Data Toko</b></h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  <!-- text input -->
                  <div class="form-group">
                    <label>Nama Toko(*) (maks. 20 karakter)</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama Toko" value="{{$detail_toko->nama_toko}}" maxlength="20" required autofocus/>
                  </div>
                  <div class="form-group">
                    <label>Alamat Toko(*)</label><br>
                    <label class="orange-text"><b>Keterangan: Masukkan nama kabupaten/kota, contoh: Jl. ABC No. 1 Surabaya</b></label>
                    <textarea class="form-control" name="alamat" required>{{$detail_toko->alamat_toko}}</textarea>
                  </div>
                  <div class="form-group">
                    <label>Telepon Toko(*)</label>
                    <input type="number" name="telepon" class="form-control" value="{{$detail_toko->telepon_toko}}" required/>
                  </div>
                  <div class="form-group">
                    <label>Deskripsi Singkat(*) (maks. 200 karakter)</label>
                    <textarea class="form-control" name="deskripsi" maxlength="200" required>{{$detail_toko->deskripsi_toko}}</textarea>
                  </div>
                  <p><b>(*) wajib diisi</b></p>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

          </div>
          <!-- /.col -->
        </div>
      <!-- /.row -->
      <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title"><b>Pengaturan Sistem</b></h3><br>
                <label class="red warning-label"><b>(*)Pengaturan sistem ini tidak dapat diubah!</b></label>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  <div class="form-group">
                    <label><u>PEMBAYARAN</u></label>
                  </div>
                  <div class="col-xs-12">
                    <div class="">
                      <div class="col-xs-2 form-group">
                        <label>&#9679;&nbsp;&nbsp;Cash</label>
                      </div>
                      <div class="col-xs-2 form-group right-aligned">
                        <label class="switch">
                          @if($detail_toko->pembayaran_tunai==1)
                            <input type="checkbox" name="tunai" checked>
                          @else
                            <input type="checkbox" name="tunai">
                          @endif
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                    <div class="">
                      <div class="col-xs-2 form-group">
                        <label>&#9679;&nbsp;&nbsp;Kartu Debit</label>
                      </div>
                      <div class="col-xs-2 form-group right-aligned">
                        <label class="switch">
                          @if($detail_toko->pembayaran_debit==1)
                            <input type="checkbox" name="debit" checked>
                          @else
                            <input type="checkbox" name="debit">
                          @endif
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                    <div class="col-xs-2 form-group">
                      <label>&#9679;&nbsp;&nbsp;Hutang (Pembelian)</label>
                    </div>
                    <div class="col-xs-2 form-group right-aligned">
                      <label class="switch">
                        @if($detail_toko->pembayaran_hutang==1)
                          <input type="checkbox" name="hutang" id="hutang" checked>
                        @else
                          <input type="checkbox" name="hutang" id="hutang">
                        @endif
                        <span class="slider round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <div class="col-xs-2 form-group">
                      <label>&#9679;&nbsp;&nbsp;Cash on Delivery (COD)</label>
                    </div>
                    <div class="col-xs-2 form-group right-aligned">
                      <label class="switch">
                        @if($detail_toko->pembayaran_cod==1)
                        <input type="checkbox" name="cod" checked>
                        @else
                        <input type="checkbox" name="cod">
                        @endif
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div class="col-xs-2 form-group">
                      <label>&#9679;&nbsp;&nbsp;Kartu Kredit</label>
                    </div>
                    <div class="col-xs-2 form-group right-aligned">
                      <label class="switch">
                        @if($detail_toko->pembayaran_kredit==1)
                          <input type="checkbox" name="kredit" checked>
                        @else
                          <input type="checkbox" name="kredit">
                        @endif
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div class="col-xs-2 form-group">
                      <label>&#9679;&nbsp;&nbsp;Piutang (Penjualan)</label>
                    </div>
                    <div class="col-xs-2 form-group right-aligned">
                      <label class="switch">
                        @if($detail_toko->pembayaran_piutang==1)
                          <input type="checkbox" name="piutang" id="piutang" checked onchange="hutang_toggle()">
                        @else
                          <input type="checkbox" name="piutang" id="piutang" onchange="hutang_toggle()">
                        @endif
                        <span class="slider round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <div class="col-xs-2 form-group">
                      <label>&#9679;&nbsp;&nbsp;Transfer</label>
                    </div>
                    <div class="col-xs-2 form-group right-aligned">
                      <label class="switch">
                        @if($detail_toko->pembayaran_transfer==1)
                          <input type="checkbox" name="transfer" id="tf" checked onchange="transfer_toggle()">
                        @else
                          <input type="checkbox" name="transfer" id="tf" onchange="transfer_toggle()">
                        @endif
                        <span class="slider round"></span>
                      </label>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div id="transfer" class="hide">
                      <div class="col-xs-2 form-group">
                        <label>Nama Bank</label>
                      </div>
                      <div class="col-xs-2 form-group">
                        <input type="text" name="nama_bank" id="nama_bank" class="form-control" value="{{$detail_toko->nama_bank}}"/>
                      </div>
                      <div class="col-xs-2 form-group">
                        <label>No. Rekening</label>
                      </div>
                      <div class="col-xs-2 form-group">
                        <input type="number" name="norek" id="norek" class="form-control" value="{{$detail_toko->nomor_rekening}}"/>
                      </div>
                      <div class="col-xs-2 form-group">
                        <label>Nama Pemilik Rek.</label>
                      </div>
                      <div class="col-xs-2 form-group">
                        <input type="text" name="pemilik_rek" id="pemilik_rek" class="form-control" value="{{$detail_toko->pemilik_rekening}}"/>
                      </div>
                    </div>
                  </div>

                  <hr width="1">
                  
                  <div class="form-group">
                    <label><u>FITUR</u></label>
                  </div>
                  <div class="col-xs-12">
                    <div class="col-xs-3 form-group">
                      <label>&#9679;&nbsp;&nbsp;Retur&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko menerima retur pembelian atau penjualan"></i></label>
                    </div>
                    <div class="col-xs-1 form-group right-aligned">
                      <label class="switch">
                        @if($detail_toko->fitur_retur==1)
                          <input type="checkbox" name="retur" checked>
                        @else
                          <input type="checkbox" name="retur">
                        @endif
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div class="col-xs-3 form-group">
                      <label>&#9679;&nbsp;&nbsp;Konsinyasi&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko menjual produk konsinyasi (titipan)"></i></label>
                    </div>
                    <div class="col-xs-1 form-group right-aligned">
                      <label class="switch">
                        @if($detail_toko->fitur_konsinyasi==1)
                          <input type="checkbox" name="konsinyasi" checked>
                        @else
                          <input type="checkbox" name="konsinyasi">
                        @endif
                        <span class="slider round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <div class="col-xs-3 form-group">
                      <label>&#9679;&nbsp;&nbsp;Pengiriman&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko melakukan pengiriman pesanan ke pelanggan"></i></label>
                    </div>
                    <div class="col-xs-1 form-group right-aligned">
                      <label class="switch">
                        @if($detail_toko->fitur_kirim==1)
                          <input type="checkbox" name="pengiriman" id="pengiriman" checked onchange="pengiriman_toggle()">
                        @else
                          <input type="checkbox" name="pengiriman" id="pengiriman" onchange="pengiriman_toggle()">
                        @endif
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div class="col-xs-3 form-group">
                      <label>&#9679;&nbsp;&nbsp;Tanggal Kedaluwarsa (Stok FIFO)<span class="red"><b>(*)</b></span>&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko menjual produk dengan tanggal kedaluwarsa"></i></label>
                    </div>
                    <div class="col-xs-1 form-group right-aligned">
                      <label class="switch">
                        @if($detail_toko->fitur_stok_fifo==1)
                          <input type="checkbox" name="kadaluwarsa" checked disabled>
                        @else
                          <input type="checkbox" name="kadaluwarsa" disabled>
                        @endif
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div id="hutang_max" class="hide">
                      <div class="col-xs-3 form-group">
                        <label>&#9679;&nbsp;&nbsp;Penentuan Batas Hutang Maksimal<span class="red"><b>(*)</b></span><i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko memiliki batas hutang bagi pelanggan"></i></label>
                      </div>
                      <div class="col-xs-1 form-group right-aligned">
                        <label class="switch">
                          @if($detail_toko->fitur_batas_hutang==1)
                            <input type="checkbox" name="batas_hutang" checked disabled>
                          @else
                            <input type="checkbox" name="batas_hutang" disabled>
                          @endif
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div id="div-pengiriman" class="hide">
                      <div class="col-xs-2 form-group">
                        <label>Jarak Maksimal Pengiriman (km)&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Jarak maksimal toko bersedia mengirimkan pesanan."></i></label>
                      </div>
                      <div class="col-xs-2 form-group">
                        @if($detail_toko->maksimal_jarak!=0)
                          <input type="number" name="maksimal_jarak" id="maksimal_jarak" value="{{$detail_toko->maksimal_jarak}}" class="form-control" min="1"/>
                        @else
                          <input type="number" name="maksimal_jarak" id="maksimal_jarak" class="form-control" min="1"/>
                        @endif
                      </div>
                      <div class="col-xs-2 form-group">
                        <label>Interval Jarak Ongkir (km)&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Interval jarak pengiriman. Misalkan 3 km, maka biaya ongkos kirim naik setiap 3 km, yaitu 3, 6, 9 km, dst."></i></label>
                      </div>
                      <div class="col-xs-2 form-group">
                        @if($detail_toko->interval_jarak!=0)
                          <input type="number" name="interval_jarak" id="interval_jarak" class="form-control" value="{{$detail_toko->interval_jarak}}" min="1"/>
                        @else
                          <input type="number" name="interval_jarak" id="interval_jarak" class="form-control" min="1"/>
                        @endif
                      </div>
                      <div class="col-xs-2 form-group">
                        <label>Ongkir Setiap Interval (Rp)&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Ongkir untuk setiap interval jarak. Misalkan 5000, maka jika interval diatur 3 km, maka ongkir untuk 3 km = Rp5.000, 6 km = Rp10.000, dst."></i></label>
                      </div>
                      <div class="col-xs-2 form-group">
                        @if($detail_toko->ongkir_interval!=0)
                        <input type="number" name="ongkir" id="ongkir" class="form-control" value="{{$detail_toko->ongkir_interval}}"/>
                        @else
                          <input type="number" name="ongkir" id="ongkir" class="form-control"/>
                        @endif
                      </div>
                    </div>
                  </div>

                  <hr width="1">

                  <div class="col-xs-12">
                    <div class="col-xs-3 form-group">
                      <label>&#9679;&nbsp;&nbsp;Menyediakan Harga Grosir&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko menjual produk dengan harga eceran dan grosir"></i></label>
                    </div>
                    <div class="col-xs-1 form-group right-aligned">
                      <label class="switch">
                        @if($detail_toko->fitur_harga_grosir==1)
                          <input type="checkbox" name="grosir" id="grosir" checked onchange="grosir_toggle()">
                        @else
                          <input type="checkbox" name="grosir" id="grosir" onchange="grosir_toggle()">
                        @endif
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div id="penentuan_grosir" class="hide">
                      <div class="col-xs-2 form-group">
                        <label>Minimal Jumlah Grosir</label>
                      </div>
                      <div class="col-xs-5 form-group">
                        <input type="number" name="jumlah_grosir" id="jumlah_grosir" class="right-aligned" value="6" step="1" min="1"/>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="col-xs-3 form-group">
                      <label>&#9679;&nbsp;&nbsp;Penentuan Waktu Maksimal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pembatalan Pesanan&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko ingin membatalkan pesanan secara otomatis setelah waktu tertentu"></i></label>
                    </div>
                    <div class="col-xs-1 form-group right-aligned">
                      <label class="switch">
                        @if($detail_toko->fitur_pembatalan_pesanan==1)
                          <input type="checkbox" name="maksimal_toggle" id="maksimal_toggle" checked onchange="pembatalan_toggle()">
                        @else
                          <input type="checkbox" name="maksimal_toggle" id="maksimal_toggle" onchange="pembatalan_toggle()">
                        @endif
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div id="pembatalan" class="hide">
                      <div class="col-xs-2 form-group">
                        <label>Waktu Maksimal (jam)</label>
                      </div>
                      <div class="col-xs-5 form-group">
                        @if($detail_toko->maksimal_pembatalan!=0)
                          <input type="number" name="waktu_maksimal" id="waktu_maksimal" class="right-aligned" value="{{$detail_toko->maksimal_pembatalan}}" step="1" min="1"/>
                        @else
                          <input type="number" name="waktu_maksimal" id="waktu_maksimal" class="right-aligned" step="1" min="1"/>
                        @endif
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="col-xs-3 form-group">
                      <label>&#9679;&nbsp;&nbsp;Penentuan Stok Minimal per &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Produk&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko menerapkan stok minimal berbeda untuk setiap produk"></i></label>
                    </div>
                    <div class="col-xs-1 form-group right-aligned">
                      <label class="switch">
                        @if($detail_toko->fitur_stok_minimal_perproduk==1)
                          <input type="checkbox" name="stok_global_toggle" id="stok_global_toggle" checked onchange="stok_minimal_toggle()">
                        @else
                          <input type="checkbox" name="stok_global_toggle" id="stok_global_toggle" onchange="stok_minimal_toggle()">
                        @endif
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div id="penentuan_stok">
                      <div class="col-xs-2 form-group">
                        <label>Stok Minimal Global</label>
                      </div>
                      <div class="col-xs-5 form-group">
                        @if($detail_toko->fitur_stok_minimal_perproduk==0)
                          <input type="number" name="stok_minimal" id="stok_minimal" class="right-aligned" value="{{$detail_toko->stok_minimal}}" step="1" min="1"/>
                        @else
                          <input type="number" name="stok_minimal" id="stok_minimal" class="right-aligned" step="1" min="1"/>
                        @endif
                      </div>
                    </div>
                  </div>
                  <label class="red warning-label"><b>(*)Pengaturan sistem ini tidak dapat diubah!</b></label>
                  <br>
                  <div class="form-group">
                    <div class="center-aligned">
                        <button type="submit" class="btn btn-success center-aligned form-control"><b>SIMPAN</b></button>
                    </div>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

          </div>
          <!-- /.col -->
        </div>
      </form>
    </section>
@endsection

<script type="text/javascript">
  window.onload = function() {
    transfer_toggle();
    grosir_toggle();
    pembatalan_toggle();
    stok_minimal_toggle();
    hutang_toggle();
    pengiriman_toggle();
  };

  function transfer_toggle(){
    var element=document.getElementById('transfer');
    if(document.getElementById('tf').checked){
      element.classList.remove('hide');
      document.getElementById("nama_bank").required = true;
      document.getElementById("norek").required = true;
      document.getElementById("pemilik_rek").required = true;
    }
    else{
      element.classList.add('hide');
      document.getElementById("nama_bank").required = false;
      document.getElementById("norek").required = false;
      document.getElementById("pemilik_rek").required = false;
    }
  }
  function grosir_toggle(){
    var element=document.getElementById('penentuan_grosir');
    if(document.getElementById('grosir').checked){
      element.classList.remove('hide');
      document.getElementById("jumlah_grosir").required = true;
    }
    else{
      element.classList.add('hide');
      document.getElementById("jumlah_grosir").required = false;
    }
  }
  function pembatalan_toggle(){
    var element=document.getElementById('pembatalan');
    if(document.getElementById('maksimal_toggle').checked){
      console.log(document.getElementById("waktu_maksimal").value);
      element.classList.remove('hide');
      document.getElementById("waktu_maksimal").required = true;
    }
    else{
      element.classList.add('hide');
      document.getElementById("waktu_maksimal").required = false;
    }
  }
  function stok_minimal_toggle(){
    var element=document.getElementById('penentuan_stok');
    if(document.getElementById('stok_global_toggle').checked){
      element.classList.add('hide');
      document.getElementById("stok_minimal").required = false;
    }
    else{
      element.classList.remove('hide');
      document.getElementById("stok_minimal").required = true;
    }
  }
  function hutang_toggle(){
    var element=document.getElementById('hutang_max');
    if(document.getElementById('piutang').checked){
      element.classList.remove('hide');
    }
    else{
      element.classList.add('hide');
    }
  }
  function pengiriman_toggle(){
    var element=document.getElementById('div-pengiriman');
    if(document.getElementById('pengiriman').checked){
      element.classList.remove('hide');
      document.getElementById("maksimal_jarak").required = true;
      document.getElementById("interval_jarak").required = true;
      document.getElementById("ongkir").required = true;
    }
    else{
      element.classList.add('hide');
      document.getElementById("maksimal_jarak").required = false;
      document.getElementById("interval_jarak").required = false;
      document.getElementById("ongkir").required = false;
    }
  }
</script>