@extends('layouts.index-admin', ['title' => 'Pelanggan'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
          PENGATURAN AWAL SISTEM
      </b></h1>
    </section>
<style type="text/css">
  
.switch {
  position: relative;
  display: inline-block;
  width: 35px;
  height: 20px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 12px;
  width: 12px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(12px);
  -ms-transform: translateX(12px);
  transform: translateX(15px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 20px;
}

.slider.round:before {
  border-radius: 50%;
}

hr {
  border: 0;
  clear:both;
  display:block;
  width: 100%;               
  background-color:black;
  height: 1px;
  margin-left:0px;
  margin-right:-10px;
}
</style>
<!-- Halo. Kami Alfamart melayani Anda dengan sepenuh hati. Apapun yang Anda butuhkan, kami menyediakannya. Anda pilih produknya, kami kirim sampai ke rumah. -->
    <!-- Main content -->
    <section class="content">
      <form role="form" method="POST" action="{{ url('pengaturan-awal') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title"><b>Data Toko</b></h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  <!-- text input -->
                  <div class="form-group">
                    <label>Nama Toko(*) (maks. 20 karakter)</label>
                    <input type="text" name="nama" class="form-control" placeholder="Nama Toko" maxlength="20" required autofocus/>
                  </div>
                  <div class="form-group">
                    <label>Alamat Toko(*)</label><br>
                    <label class="orange-text"><b>Keterangan: Masukkan nama kabupaten/kota, contoh: Jl. ABC No. 1 Surabaya</b></label>
                    <textarea class="form-control" name="alamat" placeholder="Jl..." required></textarea>
                  </div>
                  <div class="form-group">
                    <label>Telepon Toko(*)</label>
                    <input type="number" name="telepon" class="form-control" placeholder="0311234567" required/>
                  </div>
                  <div class="form-group">
                    <label>Deskripsi Singkat(*) (maks. 200 karakter)</label>
                    <textarea class="form-control" name="deskripsi" placeholder="Deskripsi" maxlength="200" required></textarea>
                  </div>
                  <p><b>(*) wajib diisi</b></p>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

          </div>
          <!-- /.col -->
        </div>
      <!-- /.row -->
      <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title"><b>Pengaturan Sistem</b></h3><br>
                <label class="red warning-label"><b>(*)Pengaturan sistem ini tidak dapat diganti setelah disimpan!</b></label>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  <div class="form-group">
                    <label><u>PEMBAYARAN</u></label>
                  </div>
                  <div class="col-xs-12">
                    <div class="">
                      <div class="col-xs-2 form-group">
                        <label>&#9679;&nbsp;&nbsp;Cash</label>
                      </div>
                      <div class="col-xs-2 form-group right-aligned">
                        <label class="switch">
                          <input type="checkbox" name="tunai">
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                    <div class="">
                      <div class="col-xs-2 form-group">
                        <label>&#9679;&nbsp;&nbsp;Kartu Debit</label>
                      </div>
                      <div class="col-xs-2 form-group right-aligned">
                        <label class="switch">
                          <input type="checkbox" name="debit">
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                    <div class="col-xs-2 form-group">
                      <label>&#9679;&nbsp;&nbsp;Hutang (Pembelian)</label>
                    </div>
                    <div class="col-xs-2 form-group right-aligned">
                      <label class="switch">
                        <input type="checkbox" name="hutang" id="hutang">
                        <span class="slider round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <div class="col-xs-2 form-group">
                      <label>&#9679;&nbsp;&nbsp;Cash on Delivery (COD)</label>
                    </div>
                    <div class="col-xs-2 form-group right-aligned">
                      <label class="switch">
                        <input type="checkbox" name="cod">
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div class="col-xs-2 form-group">
                      <label>&#9679;&nbsp;&nbsp;Kartu Kredit</label>
                    </div>
                    <div class="col-xs-2 form-group right-aligned">
                      <label class="switch">
                        <input type="checkbox" name="kredit">
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div class="col-xs-2 form-group">
                      <label>&#9679;&nbsp;&nbsp;Piutang (Penjualan)</label>
                    </div>
                    <div class="col-xs-2 form-group right-aligned">
                      <label class="switch">
                        <input type="checkbox" name="piutang" id="piutang" onchange="hutang_toggle()">
                        <span class="slider round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <div class="col-xs-2 form-group">
                      <label>&#9679;&nbsp;&nbsp;Transfer</label>
                    </div>
                    <div class="col-xs-2 form-group right-aligned">
                      <label class="switch">
                        <input type="checkbox" name="transfer" id="tf" onchange="transfer_toggle()">
                        <span class="slider round"></span>
                      </label>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div id="transfer" class="hide">
                      <div class="col-xs-2 form-group">
                        <label>Nama Bank</label>
                      </div>
                      <div class="col-xs-2 form-group">
                        <input type="text" name="nama_bank" id="nama_bank" class="form-control" placeholder="Nama Bank"/>
                      </div>
                      <div class="col-xs-2 form-group">
                        <label>No. Rekening</label>
                      </div>
                      <div class="col-xs-2 form-group">
                        <input type="number" name="norek" id="norek" class="form-control" placeholder="Nomor Rekening"/>
                      </div>
                      <div class="col-xs-2 form-group">
                        <label>Nama Pemilik Rek.</label>
                      </div>
                      <div class="col-xs-2 form-group">
                        <input type="text" name="pemilik_rek" id="pemilik_rek" class="form-control" placeholder="Nama Pemilik Rek."/>
                      </div>
                    </div>
                  </div>

                  <hr width="1">
                  
                  <div class="form-group">
                    <label><u>FITUR</u></label>
                  </div>
                  <div class="col-xs-12">
                    <div class="col-xs-3 form-group">
                      <label>&#9679;&nbsp;&nbsp;Retur&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko menerima retur pembelian atau penjualan"></i></label>
                    </div>
                    <div class="col-xs-1 form-group right-aligned">
                      <label class="switch">
                        <input type="checkbox" name="retur">
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div class="col-xs-3 form-group">
                      <label>&#9679;&nbsp;&nbsp;Konsinyasi&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko menjual produk konsinyasi (titipan)"></i></label>
                    </div>
                    <div class="col-xs-1 form-group right-aligned">
                      <label class="switch">
                        <input type="checkbox" name="konsinyasi">
                        <span class="slider round"></span>
                      </label>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <div class="col-xs-3 form-group">
                      <label>&#9679;&nbsp;&nbsp;Pengiriman&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko melakukan pengiriman pesanan ke pelanggan"></i></label>
                    </div>
                    <div class="col-xs-1 form-group right-aligned">
                      <label class="switch">
                        <input type="checkbox" name="pengiriman" id="pengiriman" onchange="pengiriman_toggle()">
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div class="col-xs-3 form-group">
                      <label>&#9679;&nbsp;&nbsp;Tanggal Kedaluwarsa (Stok FIFO)<span class="red"><b>(*)</b></span>&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko menjual produk dengan tanggal kedaluwarsa"></i></label>
                    </div>
                    <div class="col-xs-1 form-group right-aligned">
                      <label class="switch">
                        <input type="checkbox" name="kadaluwarsa">
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div id="hutang_max" class="hide">
                      <div class="col-xs-3 form-group">
                        <label>&#9679;&nbsp;&nbsp;Penentuan Batas Hutang Maksimal<span class="red"><b>(*)</b></span><i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko memiliki batas hutang bagi pelanggan"></i></label>
                      </div>
                      <div class="col-xs-1 form-group right-aligned">
                        <label class="switch">
                          <input type="checkbox" name="batas_hutang">
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div id="div-pengiriman" class="hide">
                      <div class="col-xs-2 form-group">
                        <label>Jarak Maksimal Pengiriman (km)&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Jarak maksimal toko bersedia mengirimkan pesanan."></i></label>
                      </div>
                      <div class="col-xs-2 form-group">
                        <input type="number" name="maksimal_jarak" id="maksimal_jarak" class="form-control" placeholder="10" min="1"/>
                      </div>
                      <div class="col-xs-2 form-group">
                        <label>Interval Jarak Ongkir (km)&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Interval jarak pengiriman. Misalkan 3 km, maka biaya ongkos kirim naik setiap 3 km, yaitu 3, 6, 9 km, dst."></i></label>
                      </div>
                      <div class="col-xs-2 form-group">
                        <input type="number" name="interval_jarak" id="interval_jarak" class="form-control" placeholder="3" min="1"/>
                      </div>
                      <div class="col-xs-2 form-group">
                        <label>Ongkir Setiap Interval (Rp)&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Ongkir untuk setiap interval jarak. Misalkan 5000, maka jika interval diatur 3 km, maka ongkir untuk 3 km = Rp5.000, 6 km = Rp10.000, dst."></i></label>
                      </div>
                      <div class="col-xs-2 form-group">
                        <input type="number" name="ongkir" id="ongkir" class="form-control" placeholder="5000"/>
                      </div>
                    </div>
                  </div>

                  <hr width="1">

                  <div class="col-xs-12">
                    <div class="col-xs-3 form-group">
                      <label>&#9679;&nbsp;&nbsp;Menyediakan Harga Grosir&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko menjual produk dengan harga eceran dan grosir"></i></label>
                    </div>
                    <div class="col-xs-1 form-group right-aligned">
                      <label class="switch">
                        <input type="checkbox" name="grosir" id="grosir" onchange="grosir_toggle()">
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div id="penentuan_grosir" class="hide">
                      <div class="col-xs-2 form-group">
                        <label>Minimal Jumlah Grosir</label>
                      </div>
                      <div class="col-xs-5 form-group">
                        <input type="number" name="jumlah_grosir" id="jumlah_grosir" class="right-aligned" value="6" step="1" min="1"/>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="col-xs-3 form-group">
                      <label>&#9679;&nbsp;&nbsp;Penentuan Waktu Maksimal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pembatalan Pesanan&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko ingin membatalkan pesanan secara otomatis setelah waktu tertentu"></i></label>
                    </div>
                    <div class="col-xs-1 form-group right-aligned">
                      <label class="switch">
                        <input type="checkbox" name="maksimal_toggle" id="maksimal_toggle" onchange="pembatalan_toggle()">
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div id="pembatalan" class="hide">
                      <div class="col-xs-2 form-group">
                        <label>Waktu Maksimal (jam)</label>
                      </div>
                      <div class="col-xs-5 form-group">
                        <input type="number" name="waktu_maksimal" id="waktu_maksimal" class="right-aligned" value="24" step="1" min="1"/>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12">
                    <div class="col-xs-3 form-group">
                      <label>&#9679;&nbsp;&nbsp;Penentuan Stok Minimal per &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Produk&nbsp; <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Aktifkan jika toko menerapkan stok minimal berbeda untuk setiap produk"></i></label>
                    </div>
                    <div class="col-xs-1 form-group right-aligned">
                      <label class="switch">
                        <input type="checkbox" name="stok_global_toggle" id="stok_global_toggle" onchange="stok_minimal_toggle()">
                        <span class="slider round"></span>
                      </label>
                    </div>
                    <div id="penentuan_stok">
                      <div class="col-xs-2 form-group">
                        <label>Stok Minimal Global</label>
                      </div>
                      <div class="col-xs-5 form-group">
                        <input type="number" name="stok_minimal" id="stok_minimal" class="right-aligned" value="1" step="1" min="1"/>
                      </div>
                    </div>
                  </div>

                  <label class="red warning-label"><b>(*)Pengaturan sistem ini tidak dapat diganti setelah disimpan!</b></label>
                  <br>
                  <div class="form-group">
                    <div class="center-aligned">
                        <button type="submit" class="btn btn-success center-aligned form-control"><b>SIMPAN</b></button>
                    </div>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

          </div>
          <!-- /.col -->
        </div>
      </form>
    </section>
@endsection

<script type="text/javascript">
  function transfer_toggle(){
    if(document.getElementById('tf').checked){
      $('#transfer').toggleClass('hide');
      document.getElementById("nama_bank").required = true;
      document.getElementById("norek").required = true;
      document.getElementById("pemilik_rek").required = true;
    }
    else{
      $('#transfer').toggleClass('hide');
      document.getElementById("nama_bank").required = false;
      document.getElementById("norek").required = false;
      document.getElementById("pemilik_rek").required = false;
    }
  }
  function grosir_toggle(){
    if(document.getElementById('grosir').checked){
      $('#penentuan_grosir').toggleClass('hide');
      document.getElementById("jumlah_grosir").required = true;
    }
    else{
      $('#penentuan_grosir').toggleClass('hide');
      document.getElementById("jumlah_grosir").required = false;
    }
  }
  function pembatalan_toggle(){
    if(document.getElementById('maksimal_toggle').checked){
      $('#pembatalan').toggleClass('hide');
      document.getElementById("waktu_maksimal").required = true;
    }
    else{
      $('#pembatalan').toggleClass('hide');
      document.getElementById("waktu_maksimal").required = false;
    }
  }
  function stok_minimal_toggle(){
    if(document.getElementById('stok_global_toggle').checked){
      $('#penentuan_stok').toggleClass('hide');
      document.getElementById("stok_minimal").required = false;
    }
    else{
      $('#penentuan_stok').toggleClass('hide');
      document.getElementById("stok_minimal").required = true;
    }
  }
  function hutang_toggle(){
    if(document.getElementById('piutang').checked){
      $('#hutang_max').toggleClass('hide');
    }
    else{
      $('#hutang_max').toggleClass('hide');
    }
  }
  function pengiriman_toggle(){
    if(document.getElementById('pengiriman').checked){
      console.log('halo');
      $('#div-pengiriman').toggleClass('hide');
      document.getElementById("maksimal_jarak").required = true;
      document.getElementById("interval_jarak").required = true;
      document.getElementById("ongkir").required = true;
    }
    else{
      console.log('else');
      $('#div-pengiriman').toggleClass('hide');
      document.getElementById("maksimal_jarak").required = true;
      document.getElementById("interval_jarak").required = false;
      document.getElementById("ongkir").required = false;
    }
  }
</script>