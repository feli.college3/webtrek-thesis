@extends('layouts.index-admin', ['title' => 'Pembelian'])
@section('content')

    <!-- Content Header (Page header) -->
    <form role="form" method="POST" action="{{ route('nota-beli.update',$id) }}" enctype="multipart/form-data">
        {{ method_field("PUT") }}
        {{ csrf_field() }}
        <section class="content-header">
            <h1 class="center-aligned"><b>Penerimaan Produk Pembelian</b></h1>
            <h1><b>
                No. Nota: {{$id}}
            </b></h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <!-- /.box-header -->
                <div class="box-body">
                  
                    <!-- text input -->
                        <div class="form-group">
                            <div class="row bottom-row">
                                <div class="col-sm-6">
                                    <h4>Supplier: <b>{{$nota_beli->nama_supplier}}</b></h4>
                                    <h4>{{$nota_beli->alamat_supplier}}</h4>

                                </div>

                                <div>
                                    <label id="label-grand">Rp0</label>
                                    <input type="hidden" id="grand-total" name="grand_total"/>
                                    <input type="hidden" id="total-data" name="total_data"/>
                                    <input type="hidden" id="jumlah_baris" name="jumlah_baris"/>
                                </div>
                            </div>
                        </div>

                        <a id="update-ongkir" onclick="tambah_baris()">Tambah Baris Baru</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a id="cursor-pointer" onclick="hapus_baris()">Hapus Baris Terakhir</a>
                        <div class="narrow-rows scrollable-40">
                            <table class="table" id="edit-nota">
                                <thead>
                                    <tr>
                                        <th class="center-aligned">No.</th>
                                        <th>Nama Produk</th>
                                        @if($detail_toko->fitur_stok_fifo==1)
                                            <th>Tanggal Kadaluwarsa</th>
                                            <th>Lokasi</th>
                                        @endif
                                        <th class="right-aligned">Jumlah</th>
                                        <th class="right-aligned">Harga</th>
                                        <th class="right-aligned">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for($i=1;$i<=$total_data;$i++)
                                        <tr>
                                            <td class="center-aligned">{{$i}}</td>
                                            <td><h4>{{ $list_produk[$i-1]->nama_produk }}</h4></td>
                                            <input type="hidden" id="{{ 'produk'.$i }}" name="{{ 'produk'.$i }}" value="{{ $list_produk[$i-1]->idproduk }}"/>
                                            @if($detail_toko->fitur_stok_fifo==1)
                                                <td><input type="date" class="small-input form-control" name= "{{ 'tanggal_kadaluwarsa'.$i }}" value="{{ old('tanggal_kadaluwarsa'.$i) }}" required/></td>
                                                <td><input type="text" class="small-input form-control" id="{{ 'lokasi'.$i }}" name= "{{ 'lokasi'.$i }}" value="{{ old('lokasi'.$i) }}" required/></td>
                                            @endif
                                            <td class="right-aligned"><input type="number" class="small-input right-aligned form-control font-20px" id="{{ 'jumlah'.$i }}" name="{{ 'jumlah'.$i }}" onkeyup="getsubtotal(); getgrandtotal()" onchange="getsubtotal(); getgrandtotal()" value="{{ $list_produk[$i-1]->jumlah }}" required/></td>
                                            <td class="right-aligned"><input type="number" class="small-input right-aligned form-control font-20px" id="{{ 'harga'.$i }}" name="{{ 'harga'.$i }}" onkeyup="getsubtotal(); getgrandtotal()" min="0" onchange="getsubtotal(); getgrandtotal()" value="{{ $list_produk[$i-1]->harga_beli }}" required/></td>
                                            <td class="right-aligned"><label id="{{ 'subtotal'.$i }}" value="{{ old('subtotal'.$i) }}"></label></td>
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="row bottom-row margin-1-br">
                                <div class="col-sm-2">
                                    <label class="control-label bold">Metode Pembayaran</label>
                                    <div>
                                        <select id="metode_pembayaran" name="metode_pembayaran" class="form-control" required>
                                            @if($nota_beli->metode_pembayaran == 'Tunai')
                                                <option value="Tunai" selected>Cash</option>
                                                <option value="Transfer">Transfer</option>
                                            @else
                                                <option value="Tunai">Cash</option>
                                                <option value="Transfer" selected>Transfer</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                @if($detail_toko->pembayaran_piutang==1)
                                    <div class="col-sm-2">
                                        <label class="control-label bold">Status Pembayaran</label>
                                        <div class="">
                                            <select id="status" name="status" class="form-control" onchange="tempo()" required>
                                                @if(old('status', null) != null)
                                                    @if(old('status') == 'Lunas' )
                                                        <option value="Lunas" id="lunas" selected>Lunas</option>
                                                        <option value="Belum Lunas" id="belum-lunas">Belum Lunas</option>
                                                    @elseif(old('status') == 'Belum Lunas' )
                                                        <option value="Lunas" id="lunas">Lunas</option>
                                                        <option value="Belum Lunas" id="belum-lunas" selected>Belum Lunas</option>
                                                    @endif
                                                @else
                                                    <option value="Lunas" id="lunas" selected>Lunas</option>
                                                    <option value="Belum Lunas" id="belum-lunas">Belum Lunas</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2 hide" id="tempo">
                                        <label class="control-label bold">Jatuh Tempo</label>
                                        <input id="jatuh_tempo" type="date" class="form-control" name="jatuh_tempo" value="{{ old('jatuh_tempo') }}">
                                    </div>
                                @endif

                                <div class="col-sm-5">
                                    <label class="control-label bold">Keterangan</label>
                                    <div class="">
                                        <textarea id="keterangan"class="form-control" name="keterangan">{{ $nota_beli->keterangan }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                                <!-- <button type="button" onclick="klik()" id="yuhu">Klik aku mas!</button> -->
                        <div class="form-group">
                            <div class="center-aligned">
                                <button type="submit" name="action" class="btn btn-success" value="simpan">Simpan</button>
                            </div>
                        </div>
                  </form>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
    </form>
@if (session('alert'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
        swal("{{ session('alert') }}");
    </script>
@endif
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
  // $(function () {
  //   //Initialize Select2 Elements
  //   // $('.select2').select2()
  // })

    var detail_toko = {!! json_encode($detail_toko) !!};
    window.onload = function() {
        var total_data = {!! json_encode($total_data) !!};
        if(detail_toko['pembayaran_piutang']==1){
            tempo();
        }
        document.getElementById('total-data').value=total_data;
        document.getElementById('jumlah_baris').value = total_data;
        getsubtotal();
        getgrandtotal();
    };

    function getsubtotal(){
        var jumlah_baris = document.getElementById('jumlah_baris').value;
        console.log('subtotal: '+jumlah_baris);
        for(i=1;i<=jumlah_baris;i++)
        {
            var jml = "jumlah"+i;
            var hrg = "harga"+i;
            var sub = "subtotal"+i;
            var element_harga=document.getElementById(hrg);
            var element_jumlah=document.getElementById(jml);
            if((element_harga.length!=0 && element_jumlah.length!=0) && (element_harga.value!="" && element_jumlah.value!=""))
            {
                var jum = document.getElementById(jml).value;
                var harga_beli = document.getElementById(hrg).value;
                var total = jum*harga_beli;
                total = Math.round(total);
                // return total;
                document.getElementById(sub).innerHTML = total;
            }
        }
    }

    function getgrandtotal(){
        var grand = 0;
        var data = 0;
        var jumlah_baris = document.getElementById('jumlah_baris').value;
        for(i=1;i<=jumlah_baris;i++){
            var sub = "subtotal"+i;
            var hrg = "harga"+i;
            if(document.getElementById(hrg).value.length!=0)
            {
                // alert(document.getElementById(hrg).innerHTML.length);
                var helper = parseFloat(document.getElementById(sub).innerHTML);
                // alert(helper);
                grand += helper;
                grand = Math.round(grand);
                document.getElementById('label-grand').innerHTML = "Rp"+separator(grand);
                document.getElementById('grand-total').value = grand;

                data++;
                document.getElementById('total-data').value = data;
            }
        }
    }

    function separator(number) {
        var parts=number.toString().split(".");
        return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
    }

    function tambah_baris(){
        var jumlah_baris=document.getElementById('jumlah_baris').value;
        var total_data=parseInt(document.getElementById('total-data').value);
        var produk_toko = {!! json_encode($produk_toko) !!};
        var index=total_data+1;
        if(jumlah_baris==total_data){
            if(detail_toko['fitur_stok_fifo']==1){
                $('#edit-nota > tbody:last-child').append(
                    '<tr id="no'+index+'"><td class="center-aligned">'+index+'</td>'+'<td><select id="'+"produk"+index+'" name="'+"produk"+index+'" class="form-control" required>'+
                    '<option disabled selected value="bambang">Pilih Produk</option>'+
                        
                    '</select></td>'+
                    
                    '<td><input type="date" name="tanggal_kadaluwarsa'+[index]+'" id="tanggal_kadaluwarsa'+[index]+'" class="form-control" required/></td>'+
                    '<td><input type="text" name="lokasi'+[index]+'" id="lokasi'+[index]+'" class="form-control" required/></td>'+
                      '<td><input type="number" name="jumlah'+[i]+'" id="jumlah'+[index]+'" class="form-control right-aligned" min="0" required onchange="getsubtotal();getgrandtotal()" onkeyup="getsubtotal();getgrandtotal()"/></td>'+
                        '<td><input type="number" name="harga'+[index]+'" id="harga'+[index]+'" class="form-control right-aligned" required onchange="getsubtotal();getgrandtotal()" onkeyup="getsubtotal();getgrandtotal()"/></td>'+
                      '<td class="right-aligned"><label name="subtotal'+[i]+'" id="subtotal'+[index]+'" value="0"></label></td></tr>');
            }
            else{
                $('#edit-nota > tbody:last-child').append(
                    '<tr id="no'+index+'"><td class="center-aligned">'+index+'</td>'+'<td><select id="'+"produk"+index+'" name="'+"produk"+index+'" class="form-control" required>'+
                    '<option disabled selected value="bambang">Pilih Produk</option>'+
                    '</select></td>'+
                      '<td><input type="number" name="jumlah'+[i]+'" id="jumlah'+[index]+'" class="form-control right-aligned" min="0" required onchange="getsubtotal();getgrandtotal()" onkeyup="getsubtotal();getgrandtotal()"/></td>'+
                        '<td><input type="number" name="harga'+[index]+'" id="harga'+[index]+'" class="form-control right-aligned" required onchange="getsubtotal();getgrandtotal()" onkeyup="getsubtotal();getgrandtotal()"/></td>'+
                      '<td class="right-aligned"><label name="subtotal'+[i]+'" id="subtotal'+[index]+'" value="0"></label></td></tr>');
            }
            for(i=0;i<produk_toko.length;i++){
                $('#produk'+index).append('<option value="'+produk_toko[i]['idproduk']+'">'+produk_toko[i]['nama_produk']+'</option>' );
                // var option_produk = new Option(produk_toko[i]['nama_produk'], produk_toko[i]['idproduk']);
                // /// jquerify the DOM object 'o' so we can use the html method
                // $(option_produk).html(produk_toko[i]['nama_produk']);
                // $("#produk"+index).append(option_produk);
            }
            document.getElementById('jumlah_baris').value++;
        }
    }

    function hapus_baris(){
        var jumlah_baris=document.getElementById('jumlah_baris').value;
        var total_data_awal = {!! json_encode($total_data) !!};
            console.log(jumlah_baris+'bro');
        if(jumlah_baris>total_data_awal){
            var subtotal=document.getElementById('subtotal'+jumlah_baris).innerHTML;
            if(subtotal!=""){
                document.getElementById('total-data').value--;
            }
            $("#no"+jumlah_baris).remove();
            document.getElementById('jumlah_baris').value--;
        }
    }

    function tempo(){

        var tempo=document.getElementById('tempo');
        if(document.getElementById('belum-lunas').selected){
            tempo.classList.remove('hide');
        }
        else{
            tempo.classList.add('hide');
        }
    }

    jQuery(document).ready(function(){
        jQuery('#yuhu').on('click',function(){
            var total_data=jQuery('#total-data').val();
            if(total_data!=undefined||total_data!=""||total_data!=null||total_data!=0){
                var kembar="";
                    jQuery.ajax({
                    url:'create/getkembar/'+total_data,
                    type:"GET",
                    dataType:"json",
                    success:function(produk_sama)
                    {
                        console.log(produk_sama);
                        if(produk_sama!=0){
                            jQuery.each(tanggal, function(key,value){
                              kembar=kembar+value+", ";
                            });
                            console.log(value);
                            console.log(kembar);
                        }
                        else{
                            console.log('aman');
                        }
                    }
                });
            }
            else{
                console.log('bener');
            }
        });
  })
</script>