@extends('layouts.index-admin', ['title' => 'Pembelian'])
@section('content')

    <!-- Content Header (Page header) -->
    <form role="form" method="POST" action="{{ url('nota-beli') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <section class="content-header">
            <h1 class="center-aligned"><b>Order Pembelian</b></h1>
            <h1><b>
                No. Nota: {{$id_baru}}
                @if (\Session::has('status'))
                    <!-- <div class="alert alert-success">
                        <ul>
                            <li>{!! \Session::get('status') !!}</li>
                        </ul>
                    </div> -->
                @endif
            </b></h1>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <!-- /.box-header -->
                <div class="box-body">
                  
                    <!-- text input -->
                        <!-- <div class="form-group">
                            <div class="row">
                                <div class="col-sm-5">
                                    <label class="no-nota">No. Nota: {{$id_baru}}</label>
                                </div>
                            </div>
                        </div> -->
                        <input type="hidden" name="no_nota" value="{{$id_baru}}"/>
                        <div class="form-group">
                            <div class="row bottom-row">
                                <div class="col-sm-3">
                                    <label class="span-tambah-baru">Supplier<a class="btn-tambah-baru2" href="{{ route('supplier.create') }}" target="_blank">Tambah Baru</a></label>
                                    <select id="supplier" name="supplier" class="form-control select2" required>
                                        @foreach($list_supplier as $sup)
                                            @if(old('supplier') == $sup->idsupplier )
                                                <option value="{{ $sup->idsupplier }}" selected>{{ $sup->nama_supplier }}</option>
                                            @else
                                                <option value="{{ $sup->idsupplier }}">{{ $sup->nama_supplier }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>

                                <!-- <div class="col-sm-2 offset-3">
                                    <label class="">Pegawai</label>
                                    <select id="idPgw" name="idPgw" class="form-control select2" required>
                                        @foreach($list_pegawai as $peg)
                                        <option value="{{$peg->idPegawai}}">{{$peg->nama_pegawai}}</option>
                                        @endforeach
                                    </select>
                                </div> -->

                                <div>
                                    @if(old('label-grand-hidden', null) != null)
                                        <label id="label-grand">{{ old('label-grand-hidden') }}</label>
                                    @else
                                        <label id="label-grand">Rp0</label>
                                    @endif
                                    <input type="hidden" id="label-grand-hidden" name="label-grand-hidden" value="{{ old('label-grand-hidden') }}"/>
                                    <input type="hidden" id="grand-total" name="grand_total" value="{{ old('grand_total') }}"/>
                                    <input type="hidden" id="total-data" name="total_data" value="{{ old('total_data') }}"/>
                                </div>
                            </div>
                        </div>

                        <div class="narrow-rows scrollable-40">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="center-aligned">No.</th>
                                        <th>Nama Produk</th>
                                        <th class="right-aligned">Jumlah Sudah Dipesan <i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" title="Jumlah produk yang sudah dipesan namun belum datang"></i></th>
                                        <th class="right-aligned">Jumlah</th>
                                        <th class="right-aligned">Harga</th>
                                        <th class="right-aligned">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for($i=1;$i<=20;$i++)
                                        <tr>
                                            <td class="center-aligned">{{$i}}</td>
                                            <!-- <td><input type="text" class="small-input form-control" id="{{ 'produk'.$i }}" name= "{{ 'produk'.$i }}"/></td> -->
                                            <td><select id="{{ 'produk'.$i }}" name="{{ 'produk'.$i }}" class="form-control" onchange="checkdouble();getdetail('{{$i}}')">

                                                <option selected value="">Pilih Produk</option>
                                                @foreach($list_produk as $prod)
                                                    @if(old('produk'.$i) == $prod->nama_produk )
                                                        <option value="{{ $prod->nama_produk }}" selected>{{ $prod->nama_produk }}</option>
                                                    @else
                                                        <option value="{{ $prod->nama_produk }}">{{ $prod->nama_produk }}</option>
                                                    @endif
                                                @endforeach
                                            </select></td>
                                            <td class="right-aligned"><label id="{{ 'jumlah-dipesan'.$i }}"></label></td>
                                            <td class="right-aligned"><input type="number" class="small-input right-aligned form-control font-20px" id="{{ 'jumlah'.$i }}" name="{{ 'jumlah'.$i }}" onkeyup="getsubtotal(); getgrandtotal()" onchange="getsubtotal(); getgrandtotal()" value="{{ old('jumlah'.$i) }}"/></td>
                                            <td class="right-aligned"><input type="number" class="small-input right-aligned form-control font-20px" id="{{ 'harga'.$i }}" name="{{ 'harga'.$i }}" onkeyup="getsubtotal(); getgrandtotal()" min="0" onchange="getsubtotal(); getgrandtotal()" value="{{ old('harga'.$i) }}"/></td>
                                            @if(old('label-subtotal'.$i, null) != null)
                                                <td class="right-aligned"><label id="{{ 'subtotal'.$i }}">{{ old('label-subtotal'.$i) }}</label></td>
                                            @else
                                            <td class="right-aligned"><label id="{{ 'subtotal'.$i }}"></label></td>
                                            @endif
                                            <input type="hidden" id="{{ 'label-subtotal'.$i }}" name="{{'label-subtotal'.$i }}" value="{{ old('label-subtotal'.$i) }}"/>
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="row bottom-row margin-1-br">
                                <div class="col-sm-2">
                                    <label class="control-label bold">Metode Pembayaran</label>
                                    <div>
                                        <select id="metode_pembayaran" name="metode_pembayaran" class="form-control" required>
                                            @if(old('metode_pembayaran', null) != null)
                                                @if(old('metode_pembayaran') == 'Tunai' )
                                                    <option value="Tunai" selected>Cash</option>
                                                    <option value="Transfer">Transfer</option>
                                                @elseif(old('metode_pembayaran') == 'Transfer' )
                                                    <option value="Tunai">Cash</option>
                                                    <option value="Transfer" selected>Transfer</option>
                                                @endif
                                            @else
                                                <option value="Tunai" selected>Cash</option>
                                                <option value="Transfer">Transfer</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-5">
                                    <label class="control-label bold">Keterangan</label>
                                    <div class="">
                                        <textarea id="keterangan"class="form-control" name="keterangan">{{ old('keterangan') }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                                <!-- <button type="button" onclick="klik()" id="yuhu">Klik aku mas!</button> -->
                                <!-- <button type="button" id="yuhu" onclick="klik()">Klik aku mas!</button> -->
                        <div class="form-group">
                            <div class="center-aligned">
                                <!-- <button class="btn btn-primary" name="action" value="cek_dobel">Cek Kedobelan Pembelian</button> -->
                                <button type="submit" name="action" class="btn btn-success" value="simpan">Buat Nota Beli</button>
                            </div>
                        </div>
                  </form>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
    </form>
@if (session('alert'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
        swal("{{ session('alert') }}");
    </script>
@endif
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
  // $(function () {
  //   //Initialize Select2 Elements
  //   // $('.select2').select2()
  // })
    window.onload = function() {
        // tempo();
        getsubtotal();
        getgrandtotal();
    };

    function checkdouble() {
        d=document;
        var produk = {!! json_encode($list_produk->toArray()) !!};
        var total_produk=produk.length;
        myArray = [];
        for (h=0;h<20;h++) {
            myArray[h] = d.getElementById('produk'+(h+1)).value;
        }
        for (a=0;a<20;a++) {
            for (b=1;b<=total_produk;b++) {
                d.getElementById('produk'+(a+1)).options[b].style.display = "block";
                for (c=0;c<20;c++) {
                    if(d.getElementById('produk'+(a+1)).options[b].value == myArray[c]) {
                        d.getElementById('produk'+(a+1)).options[b].style.display = "none";
                    }
                }
            }
        }
    }

    function getsubtotal(){
        for(i=1;i<=20;i++)
        {
            var jml = "jumlah"+i;
            var hrg = "harga"+i;
            var sub = "subtotal"+i;
            var element_harga=document.getElementById(hrg);
            var element_jumlah=document.getElementById(jml);
            var element_sub_label=document.getElementById("label-subtotal"+i);
            if((element_harga.length!=0 && element_jumlah.length!=0) && (element_harga.value!="" && element_jumlah.value!=""))
            {
                var jum = document.getElementById(jml).value;
                var harga_beli = document.getElementById(hrg).value;
                var total = jum*harga_beli;
                total = Math.round(total);
                // return total;
                document.getElementById(sub).innerHTML = total;
                element_sub_label.value=total;
            }
        }
    }

    function getgrandtotal(){
        var grand = 0;
        var data = 0;
        for(i=1;i<=20;i++){
            var sub = "subtotal"+i;
            var hrg = "harga"+i;
            if(document.getElementById(hrg).value.length!=0)
            {
                // alert(document.getElementById(hrg).innerHTML.length);
                var helper = parseFloat(document.getElementById(sub).innerHTML);
                // alert(helper);
                grand += helper;
                grand = Math.round(grand);
                document.getElementById('label-grand').innerHTML = "Rp"+separator(grand);
                document.getElementById('label-grand-hidden').value = "Rp"+separator(grand);
                document.getElementById('grand-total').value = grand;

                data++;
                document.getElementById('total-data').value = data;
            }
        }
    }

    function separator(number) {
        var parts=number.toString().split(".");
        return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
    }

    // function tempo(){

    //     var tempo=document.getElementById('tempo');
    //     if(document.getElementById('belum-lunas').selected){
    //         tempo.classList.remove('hide');
    //     }
    //     else{
    //         tempo.classList.add('hide');
    //     }
    // }
    function klik(){
        
        swal({
              title: "Git URL is empty",
              text: "You left the Git URL field empty. You can go back and enter a URL now, or view a HeatMap for a random repository.",
              icon: "warning",
              buttons: ["Kembali", "Tetap Buat Nota Beli"],
        });
    }

    function getdetail(index){
        var idproduk=document.getElementById('produk'+index).value;
        if(idproduk!=""){
            jQuery.ajax({
                url:'create/getdetailproduk/'+idproduk,
                type:"GET",
                dataType:"json",
                success:function(detail)
                {
                    var parts=detail.toString().split(",");
                    var sudah_dipesan=parts[0];
                    var harga_terakhir=parts[1];
                    document.getElementById("jumlah-dipesan"+index).innerHTML=sudah_dipesan;
                    document.getElementById("harga"+index).value=harga_terakhir;
                }
            });
        }
    }

    jQuery(document).ready(function(){
        jQuery('#yuhu').on('click',function(){
            var total_data=jQuery('#total-data').val();
            if(total_data!=undefined||total_data!=""||total_data!=null||total_data!=0){
                var kembar="";
                    jQuery.ajax({
                    url:'create/getkembar/'+total_data,
                    type:"GET",
                    dataType:"json",
                    success:function(produk_sama)
                    {
                        if(produk_sama!=0){
                            jQuery.each(tanggal, function(key,value){
                              kembar=kembar+value+", ";
                            });
                            console.log(value);
                            console.log(kembar);
                        }
                        else{
                            console.log('aman');
                        }
                    }
                });
            }
            else{
                console.log('bener');
            }
        });
        jQuery('#produk').on('change',function(){
            var idproduk=jQuery(this).val();
            if(idproduk){
                console.log(idproduk);
                jQuery.ajax({
                    url:'create/gettanggalkedaluwarsa/'+idproduk,
                    type:"GET",
                    dataType:"json",
                    success:function(tanggal)
                    {
                    jQuery('#tanggal_kedaluwarsa').empty();
                    jQuery('#nota_beli').empty();
                    $('#nota_beli').append('<option disabled selected value>Pilih Nota Beli</option>');
                    $('#tanggal_kedaluwarsa').append('<option disabled selected value>Pilih Tanggal Kedaluwarsa</option>');
                    jQuery.each(tanggal, function(key,value){
                      $('#tanggal_kedaluwarsa').append('<option value="'+value+'">'+value+'</option>');
                    });
                    }
                });
            }
            else{
                jQuery('#tanggal_kedaluwarsa').empty();
            }
        });
    })
</script>