@extends('layouts.index-admin', ['title' => 'Atur Pengiriman'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        VALIDASI PENYESUAIAN STOK
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <form role="form" method="POST" action="{{route('penyesuaian-stok.validasi')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <!-- /.box-header -->
              <div class="box-body">
                <table id="tabeldata" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th class="no-sort center-aligned">Cek</th>
                    <th>ID Produk</th>
                    <th>Nama Produk</th>
                    <th class="no-sort">Tanggal Kedaluwarsa</th>
                    <th class="no-sort">Waktu Penyesuaian</th>
                    <th class="no-sort">Jenis</th>
                    <th class="right-aligned">Jumlah</th>
                    <th class="no-sort">Keterangan</th>
                  </tr>
                  </thead>
                  <tbody>
                  @if(count($list_penyesuaian)>0)
                    <?php $index=0; ?>
                      @foreach($list_penyesuaian as $post)
                          <tr>
                            <td class="center-aligned"><input type="checkbox" name="produk{{$index}}" value="{{$post->idproduk}}"></td>
                            <td>{{$post->idproduk}}</td>
                            <td>{{$post->nama_produk}}</td>
                            <td>{{date('d-m-Y', strtotime($post->tanggal_kadaluwarsa))}}</td>
                            <td>{{date('d-m-Y, H:i', strtotime($post->tanggal))}}</td>
                            @if($post->jenis=='Masuk')
                              <td>Penambahan</td>
                            @else
                              <td>Pengeluaran</td>
                            @endif
                            <td class="right-aligned">{{number_format($post->jumlah,0,".",",")}}</td>
                            <td>{{$post->keterangan}}</td>
                          </tr>
                        <?php $index++; ?>
                      @endforeach
                    @endif 
                  </tbody>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

          </div>
          <!-- /.col -->
        </div>
      <!-- /.row -->
      </form>
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection