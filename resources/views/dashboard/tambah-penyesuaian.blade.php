@extends('layouts.index-admin', ['title' => 'Penyesuaian Stok'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Penyesuaian Stok - {{$id}} ({{$nama_produk->nama_produk}})
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{route('penyesuaian-stok.store')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                @if($detail_toko->fitur_stok_fifo==1)
                  <div class="form-group">
                    <label>Tanggal Kedaluwarsa(*)</label>
                    <select id="tanggal_kadaluwarsa" name="tanggal_kadaluwarsa" class="form-control" required>
                      @foreach($produk as $prod)
                        <option value="{{$prod->tanggal_kadaluwarsa}}">{{date('d-m-Y', strtotime($prod->tanggal_kadaluwarsa))}}</option>
                      @endforeach
                    </select>
                  </div>
                @endif

                <div class="form-group">
                  <label>Jenis(*)</label>
                  <select id="jenis" name="jenis" class="form-control" required>
                    <option value="Masuk">Penambahan</option>
                    <option value="Keluar">Pengeluaran</option>
                  </select>
                </div>

                <div class="form-group{{ $errors->has('jumlah') ? ' has-error' : '' }}">
                  <label>Jumlah(*)</label>
                  <input type="number" name="jumlah" class="form-control" placeholder="1" value="{{ old('jumlah') }}" required/>
                  @if ($errors->has('jumlah'))
                    <span class="help-block">
                      <strong>{{ $errors->first('jumlah') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('keterangan') ? ' has-error' : '' }}">
                  <label>Keterangan(*)</label>
                  <textarea name="keterangan" class="form-control" placeholder="Keterangan Penyesuaian"  value="{{ old('keterangan') }}" maxlength="200" required></textarea>
                  @if ($errors->has('keterangan'))
                    <span class="help-block">
                        <strong>{{ $errors->first('keterangan') }}</strong>
                    </span>
                  @endif
                </div>

                <p><b>(*) wajib diisi</b></p>
                <input type="hidden" name="idproduk" value="{{$id}}"/>
                <button type="submit" class="btn btn-success">Submit</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection