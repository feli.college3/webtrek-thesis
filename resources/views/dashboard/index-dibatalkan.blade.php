@extends('layouts.index-admin', ['title' => 'Penyesuaian Stok'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        PESANAN PERLU DIBATALKAN
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <!-- <div class="box-header">
              <h3 class="box-title"><b>Jodoel</b></h3><br>
            </div> -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                  <tr>
                  <th class="no-sort center-aligned">No</th>
                  <th>No. Nota</th>
                  <th>Pelanggan</th>
                  <th>Tanggal</th>
                  <th class="no-sort">Metode Pembayaran</th>
                  <th class="no-sort right-aligned">Grand Total (Rp)</th>
                  <th class="no-sort">Keterangan</th>
                  <th class="no-sort"></th>
                </tr>
                </thead>
                <tbody>
                  @for ($i = 0; $i < count($perlu_dibatalkan); $i++)
                    <tr>
                      <td class="center-aligned">{{$i+1}}</td>
                      <td><a href="{{route('nota-jual.show', $perlu_dibatalkan[$i]['no_nota_jual'])}}">{{$perlu_dibatalkan[$i]['no_nota_jual']}}</a></td>
                      <td>{{$perlu_dibatalkan[$i]['nama_pelanggan']}}</td>
                      <td>{{date('d-m-Y, H:i', strtotime($perlu_dibatalkan[$i]['tanggal']))}}</td>
                      <td>{{$perlu_dibatalkan[$i]['metode_pembayaran']}}</td>
                      <td class="right-aligned">{{number_format($perlu_dibatalkan[$i]['grand_total'],0,".",",")}}</td>
                      <td>{{$perlu_dibatalkan[$i]['keterangan']}}</td>
                      <td class="center-aligned">
                        <button class="btn btn-danger"><a href="{{ route('batalkan-pesanan', $perlu_dibatalkan[$i]['no_nota_jual']) }}" class="font-white">Batalkan</a></button>
                      </td>
                    </tr>
                  @endfor
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection