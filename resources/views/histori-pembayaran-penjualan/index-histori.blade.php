@extends('layouts.index-admin', ['title' => 'Histori Pembayaran'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        HISTORI PEMBAYARAN PENJUALAN
      </b></h1>
      @if($nota->status_pembayaran!='Lunas')
        <button class="btn btn-success btn-tambah"><a href="{{ route('histori-pembayaran-penjualan.createid', $id) }}" class="font-white">Tambah Histori Pembayaran</a></button>
      @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"><b>No. Nota: {{$id}}</b></h3><br>
              @if($nota->sisa_piutang>0)
                <h3 class="box-title"><b>Sisa Piutang: Rp{{number_format($nota->sisa_piutang,0,".",",")}}</b></h3>
              @else
                <h3 class="box-title"><b>Nota LUNAS</b></h3>
              @endif
            </div>
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                  <tr>
                  <th>No</th>
                  <th>Tanggal</th>
                  <th class="right-aligned no-sort">Jumlah Dibayar</th>
                  <th class="no-sort">Metode Pembayaran</th>
                  <th class="no-sort">Pegawai</th>
                </tr>
                </thead>
                <tbody>
                  @for ($i = 0; $i < count($list_histori); $i++)
                    <tr>
                      <td>{{$i+1}}</td>
                      <td>{{$list_histori[$i]->tanggal}}</td>
                      <td class="right-aligned">{{number_format($list_histori[$i]->jumlah_dibayar,0,".",",")}}</td>
                      <td>{{$list_histori[$i]->metode_pembayaran}}</td>
                      <td>{{$list_histori[$i]->nama_pegawai}}</td>
                    </tr>
                  @endfor
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection