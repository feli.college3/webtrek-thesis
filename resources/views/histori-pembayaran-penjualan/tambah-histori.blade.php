@extends('layouts.index-admin', ['title' => 'Histori Pembayaran'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Histori Pembayaran - No. Nota Jual: {{$id}}
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{route('histori-pembayaran-penjualan.store')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="form-group{{ $errors->has('jumlah') ? ' has-error' : '' }}">
                  <label>Jumlah Dibayar(*)</label>
                  <input type="number" name="jumlah" class="form-control" placeholder="100000" max="{{$nota->sisa_piutang}}" value="{{ old('jumlah') }}" required autofocus/>
                  @if ($errors->has('jumlah'))
                    <span class="help-block">
                      <strong>{{ $errors->first('jumlah') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group">
                  <label>Metode Pembayaran(*)</label>
                  <select id="metode" name="metode" class="form-control" required>
                    <option value="Cash">Cash</option>
                    <option value="Transfer">Transfer</option>
                    <option value="Debit">Debit</option>
                    <option value="Kredit">Kredit</option>
                  </select>
                </div>

                <p><b>(*) wajib diisi</b></p>
                <input type="hidden" name="no_nota" value="{{$id}}"/>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection