@extends('layouts.index-admin', ['title' => 'Kategori'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Tambah Kategori Baru
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{ url('kategori') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="form-group">
                  <label>Nama Kategori(*)</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Kategori" value="{{ old('nama') }}" required autofocus/>
                </div>
                <div class="form-group{{ $errors->has('kode_kategori') ? ' has-error' : '' }}">
                  <label>Kode Kategori(*)</label>
                  <input type="text" name="kode_kategori" maxlength="3" class="form-control" value="{{ old('kode_kategori') }}" placeholder="3 DIGIT KODE" required oninput="this.value = this.value.toUpperCase()"/>
                  @if ($errors->has('kode_kategori'))
                    <span class="help-block">
                      <strong>{{ $errors->first('kode_kategori') }}</strong>
                    </span>
                  @endif
                </div>
                <p><b>(*) wajib diisi</b></p>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection