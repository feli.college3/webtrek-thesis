@extends('layouts.index-admin', ['title' => 'Kategori'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Ubah Data Kategori
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{ route('kategori.update', $id) }}" enctype="multipart/form-data">
                {{ method_field("PUT") }}
                {{ csrf_field() }}
                <!-- text input -->
                <div class="form-group">
                  <label>Nama Kategori(*)</label>
                  <input type="text" name="nama" class="form-control" value="{{$kat->nama_kategori}}" required autofocus/>
                  @if ($errors->has('Nama Kategori(*)'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nama') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Kode Kategori: </label>
                  <label>{{$kat->kode_kategori}}</label>
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
  <script>
    alert('{{ session('status') }}')
  </script>
@endif
@endsection