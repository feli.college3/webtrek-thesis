@extends('layouts.index-admin', ['title' => 'Kategori'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DATA KATEGORI
      </b></h1>
      @if($role=='manajer')
        <button class="btn btn-success btn-tambah"><a href="{{ route('kategori.create') }}" class="font-white">Tambah Kategori Baru</a></button>
      @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama Kategori</th>
                  <th>Kode Kategori</th>
                  @if($role=='manajer')
                    <th class="no-sort"></th>
                    <th class="no-sort"></th>
                  @endif
                </tr>
                </thead>
                <tbody>
                @if(!empty($list_kategori))
                    @foreach($list_kategori as $post)
                        <tr>
                          <td>{{$post->idkategori}}</td>
                          <td>{{$post->nama_kategori}}</td>
                          <td>{{$post->kode_kategori}}</td>
                          @if($role=='manajer')
                            <td class="center-aligned">
                              <button class="btn btn-warning"><a href="{{ route('kategori.edit', $post->idkategori) }}" class="font-white">Edit</a></button>
                            </td>
                            <td class="center-aligned">
                              <form method="post" action="{{ route('kategori.destroy', $post->idkategori) }}">
                                {{ method_field('delete')}}
                                {{ csrf_field() }}
                                <button class="btn btn-danger" type="submit" name="delete" onclick="return confirm('Anda yakin untuk menghapus kategori?')">Hapus</button>
                              </form>         
                            </td>
                          @endif
                        </tr>
                     @endforeach
                    @endif  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection