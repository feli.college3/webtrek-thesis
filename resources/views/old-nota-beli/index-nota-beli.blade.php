@extends('layouts.index-admin', ['title' => 'Pembelian'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DAFTAR NOTA BELI
      </b></h1>
        <button class="btn btn-success btn-tambah"><a href="{{ route('nota-beli.create') }}" class="font-white">Buat Nota Beli</a></button>
    </section>

    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Nota Lunas</h3>
            </div> -->
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No. Nota</th>
                  <th>Supplier</th>
                  <th>Tanggal Dipesan</th>
                  <th class="right-aligned">Grand Total</th>
                  <th class="no-sort">Metode Pembayaran</th>

                  @if($detail_toko->pembayaran_hutang==1)
                    <th class="no-sort">Status Pembayaran</th>
                    <th>Tanggal Jatuh Tempo</th>
                    <th class="right-aligned">Sisa Hutang</th>
                  @endif
                  <th>Tanggal Diterima</th>
                  <th class="no-sort">Pegawai</th>
                  <th class="no-sort">Status Transaksi</th>
                  <!-- <th class="no-sort"></th>
                  <th class="no-sort"></th> -->
                </tr>
                </thead>
                <tbody>
                  @if(!empty($list_nota_beli))
                    @foreach($list_nota_beli as $post)
                      <tr>
                        <!-- <td><a href="{{route('nota-beli.show', $post->no_nota_beli)}}">{{str_pad($post->no_nota_beli,10,"0", STR_PAD_RIGHT)}}</a></td> -->
                        <td><a href="{{route('nota-beli.show', $post->no_nota_beli)}}">{{$post->no_nota_beli}}</a></td>
                        <td>{{$post->nama_supplier}}</td>
                        <td>{{date('d-m-Y, H:i', strtotime($post->tanggal_dipesan))}}</td>
                        <td class="right-aligned">{{number_format($post->grand_total,0,".",",")}}</td>
                        <td>{{$post->metode_pembayaran}}</td>

                        @if($detail_toko->pembayaran_hutang==1)
                          <td><a href="{{route('histori-pembayaran-pembelian.show', $post->no_nota_beli)}}">{{$post->status_pembayaran}}</a></td>
                          @if($post->tanggal_jatuh_tempo==NULL)
                            <td>-</td>
                          @else
                            <td>{{date('d-m-Y', strtotime($post->tanggal_jatuh_tempo))}}</td>
                          @endif
                          <td class="right-aligned">{{number_format($post->sisa_hutang,0,".",",")}}</td>
                        @endif
                        
                        @if($post->status_transaksi=='Dipesan')
                          <td>-</td>
                        @else
                          <td>{{date('d-m-Y', strtotime($post->tanggal_diterima))}}</td>
                        @endif
                        <td>{{$post->nama_pegawai}}</td>
                        <td>{{$post->status_transaksi}}</td>
                        <!-- <td class="center-aligned">
                          <button class="btn btn-warning"><a href="{{ route('nota-beli.edit', $post->no_nota_beli) }}" class="font-white">Edit</a></button>
                        </td>
                        <td class="center-aligned">
                          <form method="post" action="{{ route('nota-beli.destroy', $post->no_nota_beli) }}">
                            {{ method_field('delete')}}
                            {{ csrf_field() }}
                            <button class="btn btn-danger" type="submit" name="delete" onclick="return confirm('Are you sure?')">Hapus</button>
                          </form>         
                        </td> -->
                      </tr>
                    @endforeach
                  @endif 
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert("{{ session('status') }}")
    </script>
@endif
@endsection