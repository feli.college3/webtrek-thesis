@extends('layouts.index-admin', ['title' => 'Pembelian'])
@section('content')
    <!-- Content Header (Page header) -->
    <form role="form" method="POST" action="{{ url('nota-beli') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <section class="content-header">
            <h1><b>
                No. Nota: {{$id_baru}}
                @if (\Session::has('status'))
                    <!-- <div class="alert alert-success">
                        <ul>
                            <li>{!! \Session::get('status') !!}</li>
                        </ul>
                    </div> -->
                @endif
            </b></h1>
            <button type="submit" name="action" class="btn btn-primary btn-tambah" value="cek">Cek Kedobelan Produk</button>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-warning">
                <!-- /.box-header -->
                <div class="box-body">
                  
                    <!-- text input -->
                        <!-- <div class="form-group">
                            <div class="row">
                                <div class="col-sm-5">
                                    <label class="no-nota">No. Nota: {{$id_baru}}</label>
                                </div>
                            </div>
                        </div> -->
                        <input type="hidden" name="no_nota" value="{{$id_baru}}"/>
                        <div class="form-group">
                            <div class="row bottom-row">
                                <div class="col-sm-3">
                                    <label class="span-tambah-baru">Supplier<a class="btn-tambah-baru2" href="{{ route('supplier.create') }}">Tambah Baru</a></label>
                                    <select id="supplier" name="supplier" class="form-control select2" required>
                                        @foreach($list_supplier as $sup)
                                        <option value="{{$sup->idsupplier}}">{{$sup->nama_supplier}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <!-- <div class="col-sm-2 offset-3">
                                    <label class="">Pegawai</label>
                                    <select id="idPgw" name="idPgw" class="form-control select2" required>
                                        @foreach($list_pegawai as $peg)
                                        <option value="{{$peg->idPegawai}}">{{$peg->nama_pegawai}}</option>
                                        @endforeach
                                    </select>
                                </div> -->

                                <div>
                                    <label id="label-grand">Rp0</label>
                                    <input type="hidden" id="grand-total" name="grand_total"/>
                                    <input type="hidden" id="total-data" name="total_data"/>
                                </div>
                            </div>
                        </div>

                        <div class="narrow-rows scrollable-40">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="center-aligned">No.</th>
                                        <th>Nama Produk</th>
                                        @if($detail_toko->pembayaran_hutang==1)
                                            <th>Tanggal Kadaluwarsa</th>
                                            <th>Lokasi</th>
                                        @endif
                                        <th class="right-aligned">Jumlah</th>
                                        <th class="right-aligned">Harga</th>
                                        <th class="right-aligned">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for($i=1;$i<=20;$i++)
                                        <tr>
                                            <td class="center-aligned">{{$i}}</td>
                                            <!-- <td><input type="text" class="small-input form-control" id="{{ 'produk'.$i }}" name= "{{ 'produk'.$i }}"/></td> -->
                                            <td><select id="{{ 'produk'.$i }}" name="{{ 'produk'.$i }}" class="form-control">
                                                <option disabled selected value>Pilih Produk</option>
                                                @foreach($list_produk as $prod)
                                                <option value="{{$prod->nama_produk}}">{{$prod->nama_produk}}</option>
                                                @endforeach
                                            </select></td>
                                            @if($detail_toko->pembayaran_hutang==1)
                                                <td><input type="date" class="small-input form-control" name= "{{ 'tanggal_kadaluwarsa'.$i }}"/></td>
                                                <td><input type="text" class="small-input form-control" id="{{ 'lokasi'.$i }}" name= "{{ 'lokasi'.$i }}"/></td>
                                            @endif
                                            <td class="right-aligned"><input type="number" class="small-input right-aligned form-control font-20px" id="{{ 'jumlah'.$i }}" name="{{ 'jumlah'.$i }}" onkeyup="getsubtotal('{{$i}}'); getgrandtotal('{{$i}}')" onchange="getsubtotal('{{$i}}'); getgrandtotal('{{$i}}')"/></td>
                                            <td class="right-aligned"><input type="number" class="small-input right-aligned form-control font-20px" id="{{ 'harga'.$i }}" name="{{ 'harga'.$i }}" onkeyup="getsubtotal('{{$i}}'); getgrandtotal('{{$i}}')" step="100" min="0" onchange="getsubtotal('{{$i}}'); getgrandtotal('{{$i}}')"/></td>
                                            <td class="right-aligned"><label id="{{ 'subtotal'.$i }}"></label></td>
                                        </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="row bottom-row margin-1-br">
                                <div class="col-sm-2">
                                    <label class="control-label bold">Metode Pembayaran</label>
                                    <div>
                                        <select id="metode_pembayaran" name="metode_pembayaran" class="form-control" required>
                                            <option value="Tunai" selected>Cash</option>
                                            <option value="Transfer">Transfer</option>
                                        </select>
                                    </div>
                                </div>

                                @if($detail_toko->pembayaran_hutang==1)
                                    <div class="col-sm-2">
                                        <label class="control-label bold">Status Pembayaran</label>
                                        <div class="">
                                            <select id="status" name="status" class="form-control" onchange="tempo()" required>
                                                <option value="Lunas" id="lunas" selected>Lunas</option>
                                                <option value="Belum Lunas" id="belum-lunas">Belum Lunas</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2 hide" id="tempo">
                                        <label class="control-label bold">Jatuh Tempo</label>
                                        <input id="jatuh_tempo" type="date" class="form-control" name="jatuh_tempo">
                                    </div>
                                @endif

                                <div class="col-sm-5">
                                    <label class="control-label bold">Keterangan</label>
                                    <div class="">
                                        <textarea id="keterangan"class="form-control" name="keterangan">-</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="center-aligned">
                                <button type="submit" name="action" class="btn btn-success" value="simpan">Buat Nota Beli</button>
                            </div>
                        </div>
                  </form>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
    </form>
@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert("{{ session('status') }}");
    </script>
@endif
@endsection

<script type="text/javascript">
  // $(function () {
  //   //Initialize Select2 Elements
  //   // $('.select2').select2()
  // })
  function getsubtotal(index){
        for(i=1;i<=20;i++)
        {
            var jml = "jumlah"+index;
            var hrg = "harga"+index;
            var sub = "subtotal"+index;
            if(document.getElementById(hrg).length!=0 && document.getElementById(jml).length!=0)
            {
                var jum = document.getElementById(jml).value;
                var harga_beli = document.getElementById(hrg).value;
                var total = jum*harga_beli;
                total = Math.round(total);
                // return total;
                document.getElementById(sub).innerHTML = total;
            }
        }
    }

    function getgrandtotal(index){
        var grand = 0;
        var data = 0;
        for(i=1;i<=20;i++){
            var sub = "subtotal"+i;
            var hrg = "harga"+i;
            if(document.getElementById(hrg).value.length!=0)
            {
                // alert(document.getElementById(hrg).innerHTML.length);
                var helper = parseFloat(document.getElementById(sub).innerHTML);
                // alert(helper);
                grand += helper;
                grand = Math.round(grand);
                document.getElementById('label-grand').innerHTML = "Rp"+separator(grand);
                document.getElementById('grand-total').value = grand;

                data++;
                document.getElementById('total-data').value = data;
            }
        }
    }

    function separator(number) {
        var parts=number.toString().split(".");
        return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
    }

    function tempo(){
        if(document.getElementById('belum-lunas').selected) $('#tempo').toggleClass('hide');
        else $('#tempo').toggleClass('hide');
    }
</script>