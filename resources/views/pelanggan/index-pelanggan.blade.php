@extends('layouts.index-admin', ['title' => 'Pelanggan'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DATA PELANGGAN
      </b></h1>
      @if($role=='manajer')
        <button class="btn btn-success btn-tambah"><a href="{{ route('pelanggan.create') }}" class="font-white">Tambah Pelanggan Baru</a></button>
      @endif
      <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Pelanggan Berhutang</h3>
            </div> -->

            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama Pelanggan</th>
                  <th class="no-sort">Alamat Pelanggan</th>
                  <th class="no-sort">Telepon Pelanggan</th>
                  @if($role=='manajer')
                    @if($detail_toko->pembayaran_piutang==1)
                      <th>Batas Hutang (Rp)</th>
                      <th>Total Hutang (Rp)</th>
                    @endif
                  @endif
                  <th>Status</th>
                  @if($role=='manajer')
                    <th class="no-sort"></th>
                    <th class="no-sort"></th>
                  @endif
                </tr>
                </thead>
                <tbody>
                @if(!empty($list_pelanggan))
                    @foreach($list_pelanggan as $post)
                        <tr>
                          <td>{{$post->idpelanggan}}</td>
                          <td>{{$post->nama_pelanggan}}</td>
                          <td>{{$post->alamat_pelanggan}}</td>
                          <td>{{$post->telepon_pelanggan}}</td>
                          @if($role=='manajer')
                            @if($detail_toko->pembayaran_piutang==1)
                              <td class="right-aligned">{{number_format($post->batas_hutang,0,".",",")}}</td>
                              <td class="right-aligned">{{number_format($post->total_hutang,0,".",",")}}</td>
                            @endif
                          @endif
                          <td>{{$post['status']}}</td>
                          @if($role=='manajer')
                            <td class="center-aligned">
                              <button class="btn btn-warning"><a href="{{ route('pelanggan.edit', $post->idpelanggan) }}" class="font-white">Edit</a></button>
                            </td>
                            <td class="center-aligned">
                              <form method="post" action="{{ route('pelanggan.destroy', $post->idpelanggan) }}">
                                {{ method_field('delete')}}
                                {{ csrf_field() }}
                                <button class="btn btn-danger" type="submit" name="delete" onclick="return confirm('Anda yakin untuk menghapus pelanggan?')">Hapus</button>
                              </form>         
                            </td>
                          @endif
                        </tr>
                     @endforeach
                    @endif  
                    <!-- @for ($i = 0; $i < 10; $i++)<tr>
                        <td>oi</td>
                        <td>oi</td>
                        <td>oi</td>
                        <td>oi</td>
                        <td>oi</td>
                        <td>oi</td>
                      <td class="center">
                        <button class="btn btn-warning"><a href="#">Edit</a></button>
                      </td>
                      <td>
                        <form action="#" method="post">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-danger" type="submit" onclick="return confirm('Are you sure?')">Hapus</button>
                        </form>         
                      </td>
                    </tr>
                    @endfor -->
                    
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection