@extends('layouts.index-admin', ['title' => 'Pelanggan'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Tambah Pelanggan Baru
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{ url('pelanggan') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="form-group">
                  <label>Nama Pelanggan(*)</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Pelanggan" required autofocus/>
                </div>
                <div class="form-group">
                  <label>Alamat(*)</label><br>
                  <label class="orange-text"><b>Keterangan: Masukkan nama kabupaten/kota, contoh: Jl. ABC No. 1 Surabaya</b></label>
                  <input type="text" name="alamat" class="form-control" placeholder="Jl..." value="-" required/>
                </div>
                <div class="form-group">
                  <label>Telepon(*)</label>
                  <input type="number" name="telepon" class="form-control" placeholder="0311234567" value="0" required/>
                </div>
                @if($detail_toko->fitur_batas_hutang==1)
                  <div class="form-group">
                    <label>Batas Hutang(*)</label>
                    <input type="number" name="batas_hutang" class="form-control" placeholder="1000000" required/>
                  </div>
                @endif
                <p><b>(*) wajib diisi</b></p>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection