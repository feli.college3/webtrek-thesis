@extends('layouts.index-admin', ['title' => 'Pelanggan'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Ubah Data Pelanggan
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{ route('pelanggan.update', $id) }}" enctype="multipart/form-data">
                {{ method_field("PUT") }}
                {{ csrf_field() }}
                <!-- text input -->
                <div class="form-group">
                  <label>Nama Pelanggan(*)</label>
                  <input type="text" name="nama" class="form-control" value="{{$plg->nama_pelanggan}}" required autofocus/>
                  @if ($errors->has('Nama Pelanggan(*)'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nama') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Alamat Pelanggan</label>
                  <input type="text" name="alamat" class="form-control" value="{{$plg->alamat_pelanggan}}"/>
                  @if ($errors->has('Nama Pelanggan(*)'))
                    <span class="help-block">
                        <strong>{{ $errors->first('alamat') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Telepon Pelanggan</label>
                  <input type="number" name="telepon" class="form-control" value="{{$plg->telepon_pelanggan}}"/>
                  @if ($errors->has('Telepon Pelanggan(*)'))
                    <span class="help-block">
                        <strong>{{ $errors->first('telepon') }}</strong>
                    </span>
                  @endif
                </div>
                @if($detail_toko->pembayaran_piutang==1)
                  <div class="form-group">
                    <label>Batas Hutang(*)</label>
                    <input type="number" name="batas_hutang" class="form-control" value="{{$plg->batas_hutang}}" required autofocus/>
                    @if ($errors->has('Batas Hutang(*)'))
                      <span class="help-block">
                          <strong>{{ $errors->first('batas_hutang') }}</strong>
                      </span>
                    @endif
                  </div>
                @endif
                <div class="form-group">
                  <label>Status(*)</label>
                    <select id="status" name="status" class="form-control" required>
                      @if($plg->status=="Aktif")
                          <option value="Aktif" selected>Aktif</option>
                          <option value="Non-aktif">Non-aktif</option>
                      @else
                          <option value="Aktif">Aktif</option>
                          <option value="Non-aktif" selected>Non-aktif</option>
                      @endif
                    </select>
                    @if ($errors->has('status'))
                      <span class="help-block">
                        <strong>{{ $errors->first('status') }}</strong>
                      </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
  <script>
    alert('{{ session('status') }}')
  </script>
@endif
@endsection