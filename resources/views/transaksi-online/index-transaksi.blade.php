@extends('layouts.index-admin', ['title' => 'Transaksi Online'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        @if($status=='Belum Dibayar'||$status=='Belum Divalidasi'||$status=='Belum Diproses')
          DAFTAR PESANAN BELUM DIPROSES
        @elseif($status=='Diproses')
          DAFTAR PESANAN SEDANG DIPROSES
        @elseif($status=='Siap Dikirim')
          DAFTAR PESANAN SIAP DIKIRIM
        @elseif($status=='Dikirim')
          DAFTAR PESANAN SEDANG DIKIRIM
        @elseif($status=='Siap Diambil')
          DAFTAR PESANAN BELUM DIAMBIL PELANGGAN
        @elseif($status=='Dibatalkan')
          DAFTAR PESANAN DIBATALKAN
        @endif
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="center-aligned">No.</th>
                  <th>No. Nota</th>
                  @if($status=='Dikirim')
                    <th>Waktu Kirim</th>
                  @else
                    <th>Waktu Transaksi</th>
                  @endif
                  <th class="no-sort">Pelanggan</th>
                  <th class="right-aligned">Grand Total</th>
                  <th class="no-sort">Metode Pembayaran</th>
                  <th class="no-sort">Status Pembayaran</th>
                  @if($status!='Dikirim')
                    <th class="no-sort">Kasir</th>
                  @else
                    <th class="no-sort">Kurir</th>
                  @endif
                  @if($status!='Dikirim')
                    <th class="no-sort">Status</th>
                  @endif
                  @if($status=='Dibatalkan')
                    <th class="no-sort">Keterangan</th>
                  @else
                    @if($status!='Dikirim')
                      @if($detail_toko->fitur_kirim==1)
                        <th class="no-sort">Kirim</th>
                      @endif
                    @endif
                  @endif
                </tr>
                </thead>
                <tbody>
                @if(count($list_nota_jual)>0)
                  <?php $index=0; ?>
                    @foreach($list_nota_jual as $post)
                      <tr>
                        <td class="center-aligned">{{$index+1}}</td>
                          <td><a href="{{route('detail-transaksi', $post->nomor_nota)}}">{{$post->nomor_nota}}</a></td>
                          @if($status=='Dikirim')
                            <td>{{date('d-m-Y, H:i', strtotime($post->tanggal_kirim))}}</td>
                          @else
                            <td>{{date('d-m-Y, H:i', strtotime($post->tanggal))}}</td>
                          @endif
                          <td>{{$post->nama_pelanggan}}</td>
                          <td class="right-aligned">{{number_format($post->grand_total,0,".",",")}}</td>
                          <td>{{$post->metode_pembayaran}}</td>
                          <td>{{$post->status_pembayaran}}</td>
                          @if($status!='Dikirim')
                            <td>{{$post->nama_pegawai}}</td>
                          @else
                            <td>{{$post->nama_pegawai}}</td>
                          @endif

                          @if($status=='Dikirim')
                            <!-- <td>{{$post->status_kirim}}</td> -->
                          @else
                            <td>{{$post->status_transaksi}}</td>
                          @endif
                          @if($status=='Dibatalkan')
                            <td>{{$post->keterangan}}</td>
                          @else
                            @if($status!='Dikirim')
                              @if($detail_toko->fitur_kirim==1)
                                @if($post->kirim=="1")
                                  <td>Ya</td>
                                @else
                                  <td>Tidak</td>
                                @endif
                              @endif
                            @endif
                          @endif
                      </tr>
                      <?php $index++; ?>
                    @endforeach
                  @endif 
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection