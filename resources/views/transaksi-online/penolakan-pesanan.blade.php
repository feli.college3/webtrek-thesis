@extends('layouts.index-admin', ['title' => 'Detail Pesanan'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <b>PENOLAKAN PESANAN</b>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <h3><b>Data Pesanan</b></h3>
          <h4>No. Pesanan: <a href="{{route('nota-jual.show', $nota->no_nota_jual)}}">{{$nota->no_nota_jual}}</a></h4>
          <h4>Pelanggan: {{$nota->nama_pelanggan}}</h4>
          <h4>Grand Total: Rp{{number_format($nota->grand_total,0,".",",")}}</h4>
          <h4>Metode Pembayaran: {{$nota->metode_pembayaran}}</h4>
          <h3><b>Mengapa pesanan ditolak?</b></h3>
        </div>

        <div class="col-xs-12">
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <form role="form" method="POST" id="batal" action="{{ route('nota-jual.tolak') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
          <div class="row">
            <div class="col-xs-12">
              <input type="radio" id="r1" name="alasan" value="1" checked onchange="lainnya_toggle()"/>&nbsp;&nbsp;<label for="1" class="rejection-option">Stok tidak mencukupi</label><br>
              <input type="radio" id="r5" name="alasan" value="5" onchange="lainnya_toggle()"/>&nbsp;&nbsp;<label for="5" class="rejection-option">Permintaan pelanggan</label><br>
              <input type="radio" id="r3" name="alasan" value="3" onchange="lainnya_toggle()"/>&nbsp;&nbsp;<label for="3" class="rejection-option">Terdapat kendala dalam pengiriman</label><br>
              <input type="radio" id="r4" name="alasan" value="4" onchange="lainnya_toggle()"/>&nbsp;&nbsp;<label for="4" class="rejection-option">Toko sedang tidak menerima pesanan</label><br>
              <input type="radio" id="r2" name="alasan" value="2" onchange="lainnya_toggle()"/>&nbsp;&nbsp;<label for="2" class="rejection-option">Akun pelanggan terlihat mencurigakan</label><br>
              <input type="radio" id="r0" name="alasan" value="0" onchange="lainnya_toggle()"/>&nbsp;&nbsp;<label for="0" class="rejection-option">Lainnya</label><br>
              <div id="input_alasan" class="hide">
                <input type="text" name="alasan_lainnya" id="alasan_lainnya" class="form-control" maxlength="35" /><br>
              </div>
            </div>
            <input type="hidden" name="no_nota" value="{{$nota->no_nota_jual}}">
            <div class="col-xs-12 center-aligned">
              <button type="button" class="btn btn-primary"><a href="{{route('detail-transaksi', $nota->no_nota_jual)}}" class="font-white">Kembali</a></button>
              <button type="submit" name="simpan" value="simpan" class="btn btn-danger">Konfirmasi Pembatalan</button>
            </div>
          </div>
      </form>
    </section>

<script type="text/javascript">
  window.onload = function() {
    lainnya_toggle();
  };

  function lainnya_toggle(){
    var form = document.getElementById('batal');
    var value = form.elements['alasan'].value;
    var element_input=document.getElementById('input_alasan');
    if (value==0) {
      element_input.classList.remove('hide');
      document.getElementById("alasan_lainnya").required = true;
    }
    else{
      element_input.classList.add('hide');
      document.getElementById("alasan_lainnya").required = false;
    }
  }
</script>
@endsection