@extends('layouts.index-admin', ['title' => 'Detail Pesanan'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <b>DETAIL PESANAN - {{$id}}</b>
      </h1>
      @if($nota_jual->status_transaksi=='Belum Diproses')
        @if($nota_jual->metode_pembayaran=='COD')
          <button class="btn btn-success btn-tambah2"><a href="{{route('terima-pesanan', $id)}}" class="font-white">Terima Pesanan</a></button>
          <button class="btn btn-danger btn-tambah"><a href="{{route('tolak-pesanan', $id)}}" class="font-white">Tolak Pesanan</a></button>
        @elseif($nota_jual->metode_pembayaran=='Transfer')
          @if($nota_jual->status_pembayaran=='Lunas')
            <button class="btn btn-success btn-tambah2"><a href="{{route('terima-pesanan', $id)}}" class="font-white">Terima Pesanan</a></button>
          <button class="btn btn-danger btn-tambah"><a href="{{route('tolak-pesanan', $id)}}" class="font-white">Tolak Pesanan</a></button>
          @endif
        @endif
      @elseif($nota_jual->status_transaksi=='Diproses')
        @if($nota_jual->kirim=='1')
          <button class="btn btn-success btn-tambah"><a href="{{route('kirim-pesanan', $id)}}" class="font-white">Siap Dikirim</a></button>
        @elseif($nota_jual->kirim=='0')
          <button class="btn btn-success btn-tambah"><a href="{{route('selesai-diproses', $id)}}" class="font-white">Siap Diambil</a></button>
        @endif
      @elseif($nota_jual->status_transaksi=='Siap Diambil')
          <button class="btn btn-success btn-tambah"><a href="{{route('sudah-diambil', $id)}}" class="font-white">Sudah Diambil</a></button>
      @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <h3><b>PRODUK</b></h3>
        </div>

        <div class="col-xs-12">
          @if(!empty($produk_toko))
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title"><b>Produk Toko</b></h3>
              </div>
              <div class="box-body">
                <table id="tabeldata" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th class="no-sort center-aligned"></th>
                      <th>ID Produk</th>
                      <th>Nama Produk</th>
                      <th class="no-sort">Tanggal Kadaluwarsa</th>
                      <th class="no-sort right-aligned">Harga Jual (Rp)</th>
                      <th class="no-sort right-aligned">Jumlah</th>
                      <th class="no-sort">Lokasi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($produk_toko as $post)
                      <tr>
                        <td class="center-aligned"><img src="../images/{{$post->nama_foto}}.{{$post->ekstensi}}" style="width: 100px; height: 100px;"></td>
                        <td>{{$post->idproduk}}</td>
                        <td>{{$post->nama_produk}}</td>
                        <td>{{date("d-m-Y", strtotime($post->tanggal_kadaluwarsa))}}</td>
                        <td class="right-aligned">{{number_format($post->harga_jual,0,".",",")}}</td>
                        <td class="right-aligned">{{$post->jumlah}}</td>
                        <td>{{$post->lokasi}}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          @endif  

          @if($detail_toko->fitur_konsinyasi==1)
            @if(!empty($produk_konsinyasi))
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title"><b>Produk Konsinyasi</b></h3>
                </div>
                <div class="box-body">
                  <table id="tabeldata" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th class="no-sort center-aligned"></th>
                        <th>ID Produk</th>
                        <th>Nama Produk</th>
                        <th class="right-aligned">Harga Jual</th>
                        <th class="right-aligned">Jumlah</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($produk_konsinyasi as $post)
                        <tr>
                          <td class="center-aligned"><img src="../images/{{$post->nama_foto}}.{{$post->ekstensi}}" style="width: 100px; height: 100px;"></td>
                          <td>{{$post->idproduk_konsinyasi}}</td>
                          <td>{{$post->nama_produk}}</td>
                          <td class="right-aligned">{{number_format($post->harga_jual,2,".",",")}}</td>
                          <td class="right-aligned">{{$post->jumlah}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            @endif  
          @endif
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-xs-12">
          <h3><b>PEMBAYARAN</b></h3>
          @if($nota_jual->metode_pembayaran=='COD')
            <h4>Pembayaran dilakukan secara COD.</h4>
          @elseif($nota_jual->bukti_transfer==null)
            <h4>Pelanggan belum mengirimkan bukti transfer.</h4>
          @else
            <div class="col-md-4">
              <img src="../images/{{$nota_jual->bukti_transfer}}" style="width: 300px; height: 300px;">
            </div>
            <div class="col-md-8">
              <h4>Pelanggan telah mengirim bukti transfer.</h4>
              
              @if($nota_jual->status_transaksi=='Belum Divalidasi')
                <h4>Apakah pembayaran valid?</h4>
              
                <button class="btn btn-success big-button"><a href="{{route('validitas-bukti', [$id, 'ya'])}}" class="font-white">Ya</a></button>
                <button class="btn btn-danger big-button"><a href="{{route('validitas-bukti', [$id, 'tidak'])}}" class="font-white">Tidak</a></button>
              @elseif($nota_jual->status_pembayaran=='Lunas')
                <h4>Pembayaran telah dikonfirmasi.</h4>
                @if($nota_jual->status_transaksi=='Belum Diproses')
                  <h4>Segera terima dan proses pesanan.</h4>
                @elseif($nota_jual->status_transaksi=='Diproses')
                  <h4>Segera proses pesanan.</h4>
                @endif
              @elseif($nota_jual->status_transaksi=='Bukti Pembayaran Tidak Valid')
                <h4>Pembayaran tidak diterima.</h4>
              @endif
              @endif
              @if($nota_jual->status_transaksi=='Siap Diambil')
                <h4>Pelanggan telah diingatkan untuk mengambil pesanan.</h4>
              @elseif($nota_jual->status_transaksi=='Dikirim')
                <h4>Pesanan sedang dikirim.</h4>
              @endif
            </div>
        </div>
      </div>


    </section>

@if (session('status'))
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection