@extends('layouts.index-admin', ['title' => 'Atur Pengiriman'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        ATUR PENGIRIMAN
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <form role="form" method="POST" action="{{route('atur-pengiriman.store')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <!-- /.box-header -->
              <div class="box-body">
                <table id="tabeldata" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th class="no-sort center-aligned">Cek</th>
                    <th>No. Nota</th>
                    <th>Tanggal Transaksi</th>
                    <th class="no-sort">Pelanggan</th>
                    <th class="no-sort">Alamat</th>
                    <th class="right-aligned">Grand Total (Rp)</th>
                    <th class="no-sort">Metode Pembayaran</th>
                    <th class="no-sort">Status Pembayaran</th>
                    <th class="no-sort">Keterangan</th>
                  </tr>
                  </thead>
                  <tbody>
                  @if(count($list_nota_jual)>0)
                    <?php $index=0; ?>
                      @foreach($list_nota_jual as $post)
                          <tr>
                            <td class="center-aligned"><input type="checkbox" name="nota{{$index}}" class="cek_nota" value="{{$post->no_nota_jual}}"></td>
                            <td><a href="{{route('detail-transaksi', $post->no_nota_jual)}}">{{$post->no_nota_jual}}</a></td>
                            <td>{{date('d-m-Y H:i', strtotime($post->tanggal))}}</td>
                            <td>{{$post->nama_pelanggan}}</td>
                            <td>{{$post->alamat}}</td>
                            <td class="right-aligned">{{number_format($post->grand_total,0,".",",")}}</td>
                            <td>{{$post->metode_pembayaran}}</td>
                            <td>{{$post->status_pembayaran}}</td>
                            <td>{{$post->keterangan_pengiriman}}</td>
                          </tr>
                        <?php $index++; ?>
                      @endforeach
                    @endif 
                  </tbody>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <label><input type="checkbox" name="cek_all" id="cek_all" value="cek_all">&nbsp;&nbsp;Cek Semua</label>
          </div>
          @if(count($list_nota_jual)>0)
            <div class="col-xs-12">
              <h4><b>Pilih Kurir</b></h4>
              <div class="form-group">
                <select id="kurir" name="kurir" class="form-control" required>
                  <option disabled selected value>Pilih Kurir</option>
                  @foreach($list_kurir as $kurir)
                  <option value="{{$kurir->idpegawai}}">{{$kurir->nama_pegawai}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-xs-12">
              <input type="hidden" name="total_nota" value="{{count($list_nota_jual)}}">
              <button type="submit" class="btn btn-success">Kirimkan Pesanan</button>
            </div>
          @endif
          <!-- /.col -->
        </div>
      <!-- /.row -->
      </form>
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif

<script src="{{ asset('customer/js/jquery.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('customer/js/popper.min.js') }}"></script>
<script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
<script type="text/javascript">
  $('#cek_all').on('click', function(e) {
    $('.cek_nota').prop('checked', $(e.target).prop('checked'));
  });
</script>
@endsection