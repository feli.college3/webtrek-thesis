@extends('layouts.index-admin', ['title' => 'Pengiriman Hari Ini'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        PESANAN SELESAI DIKIRIM KURIR
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <!-- /.box-header -->
              <div class="box-body">
                <table id="tabeldata" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th class="no-sort center-aligned">No</th>
                    <th>No. Nota</th>
                    <th>Waktu Kirim</th>
                    <th class="no-sort">Pelanggan</th>
                    <th class="no-sort">Alamat</th>
                    <th class="right-aligned">Grand Total (Rp)</th>
                    <th class="no-sort">Kurir</th>
                    <th class="no-sort"></th>
                  </tr>
                  </thead>
                  <tbody>
                  @if(count($list_nota)>0)
                    <?php $index=0; ?>
                      @foreach($list_nota as $post)
                          <tr>
                            <td class="center-aligned">{{$index+1}}</td>
                            <td><a href="{{route('detail-transaksi', $post->no_nota_jual)}}">{{$post->no_nota_jual}}</a></td>
                            <td>{{date('d-m-Y H:i', strtotime($post->tanggal_kirim))}}</td>
                            <td>{{$post->nama_pelanggan}}</td>
                            <td>{{$post->alamat}}</td>
                            <td class="right-aligned">{{number_format($post->grand_total,0,".",",")}}</td>
                            <td>{{$post->nama_pegawai}}</td>
                            <form action="{{ route('pesanan-selesai', $post->no_nota_jual) }}">
                              <td class="center-aligned"><button class="btn btn-success" type="submit" name="submit">Selesai</button></td>
                            </form>        
                          </tr>
                        <?php $index++; ?>
                      @endforeach
                    @else
                      <tr><td colspan="7" class="center-aligned">Tidak ada pengiriman hari ini</td></tr>
                    @endif 
                  </tbody>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->

          </div>
          <!-- /.col -->
        </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection