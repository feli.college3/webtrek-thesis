<!DOCTYPE html>
<html>
<head>
	<title>Daftar Keluar Produk</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
		@page { margin: 50px 25px; }
	    footer { position: fixed; bottom: -20px; left: 0px; right: 0px; height: 20px; font-size: 15px }
	    p { page-break-after: always; }
	    p:last-child { page-break-after: never; }
	    .center-aligned{
	    	text-align: center;
	    }
	    .right-aligned{
	    	text-align: right;
	    }
	</style>
	<center>
		<h5>Daftar Keluar Produk untuk Pengiriman {{$detail_toko->nama_toko}}</h5>
		<h6>Pengiriman {{date("d-m-Y, H:i", strtotime($tanggal))}}</h6>
		<hr>
	</center>
 	
	<h6>Produk Toko</h6>
	<table class='table table-bordered'>
		<thead>
			<tr>
				<th class="center-aligned">No</th>
				<th>ID Produk</th>
                <th>Nama Produk</th>
                <th>Tanggal Kedaluwarsa</th>
                <th>Lokasi</th>
                <th class="right-aligned">Jumlah</th>
			</tr>
		</thead>
		<tbody>
			@if(count($produk_toko)>0)
				@php $i=1 @endphp
                @foreach($produk_toko as $post)
                    <tr>
                    	<td class="center-aligned">{{ $i++ }}</td>
                        <td>{{$post->idproduk}}</td>
                        <td>{{$post->nama_produk}}</td>
                        <td>{{date('d-m-Y', strtotime($post->tanggal_kadaluwarsa))}}</td>
                        <td>{{$post->lokasi}}</td>
                        <td class="right-aligned">{{number_format($post->jumlah_toko,0,".",",")}}</td>
                    </tr>
                @endforeach
            @else
                <tr><td class="center-aligned" colspan="6">Tidak ada produk toko</td></tr>
            @endif 
		</tbody>
	</table>

	@if($detail_toko->fitur_konsinyasi==1)
		<hr>
		<h6>Produk Konsinyasi</h6>
		<table class='table table-bordered'>
			<thead>
				<tr>
					<th class="center-aligned">No</th>
					<th>ID Produk</th>
	                <th>Nama Produk</th>
	                <th class="right-aligned">Jumlah</th>
				</tr>
			</thead>
			<tbody>
				@if(count($produk_konsinyasi)>0)
					@php $i=1 @endphp
	                @foreach($produk_konsinyasi as $post)
	                    <tr>
	                    	<td class="center-aligned">{{ $i++ }}</td>
	                        <td>{{$post->idproduk_konsinyasi}}</td>
	                        <td>{{$post->nama_produk}}</td>
	                        <td class="right-aligned">{{number_format($post->jumlah_konsi,0,".",",")}}</td>
	                    </tr>
	                @endforeach
	            @else
	                <tr><td class="center-aligned" colspan="6">Tidak ada produk konsinyasi</td></tr>
	            @endif 
			</tbody>
		</table>
	@endif
 	<footer>Dibuat oleh <b>WEBTREK</b></footer>
</body>
</html>