@extends('layouts.kurir', ['title' => 'Dashboard'])
@section('content')
    <!-- Content Header (Page header) -->

<!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.2.1/css/ol.css" type="text/css"> -->
<!-- <style>
  .map {
    height: 400px;
    width: 100%;
  }
</style> -->

<section class="content-header">
  <h1><b>
    RUTE PENGIRIMAN
  </b></h1>
  <button class="btn btn-primary btn-kembali"><a href="{{ url('rute-pengiriman') }}" class="font-white">Kembali ke Titik Kirim</a></button>
  <button class="btn btn-success btn-tambah" onclick="telah_dikirim()">Produk Telah Dikirim</button>
  {!! $map['js'] !!}
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <h3><b>{{$nota->no_nota_jual}} - {{$nota->nama_pelanggan}}</b></h3>
      <h4>No. Pesanan: <a href="{{route('nota-jual.show', $nota->no_nota_jual)}}">{{$nota->no_nota_jual}}</a> (klik untuk melihat produk)</h4>
      <h4>Nama Pelanggan: {{$nota->nama_pelanggan}}</h4>
      <h4>Metode Pembayaran: {{$nota->metode_pembayaran}}</h4>
      <h4>Grand Total: Rp{{number_format($nota->grand_total,0,".",",")}}</h4>
      <h4>Keterangan Pengiriman: {{$nota->keterangan}}</h4>
    </div>
    <div class="col-md-12">
      <br>
    </div>
    <div class="col-md-12">
      <!-- <div id="map" class="full-map"></div>
    <div id="location" class="marker"><span class="icon-arrow-up"></span></div> -->
      {!! $map['html'] !!}
      <div id="directions"></div>
    </div>
  </div>
</section>



<!-- <script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.2.1/build/ol.js"></script>    -->

<script src="{{ asset('customer/js/jquery.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script type="text/javascript">
  var nota = {!! json_encode($nota) !!};
  function telah_dikirim(){
    var no_nota=nota['no_nota_jual'];
    var nama_pelanggan=nota['nama_pelanggan'];
    Swal.fire({
      title: "Nama Penerima:",
      input: 'text',
      inputValue: nama_pelanggan,
      showCancelButton: true,
      confirmButtonColor: '#44b839',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Batalkan',
      confirmButtonText: 'Simpan',
      reverseButtons: true,
    }).then((result) => {
      var nama_penerima=result.value;
      if (nama_penerima) {
        console.log("Result: " + no_nota + nama_penerima);
        window.location = '../sukses-dikirim/'+no_nota+'/'+nama_penerima;
      }
      else if(nama_penerima==""){
        Swal.fire({
          title: "Harap masukkan nama penerima!",
          icon: 'error',
          showConfirmButton: false,
          timer:1000,
        });
      }
    });
  }
  // var map = new ol.Map({
  //   target: 'map',
  //   layers: [
  //     new ol.layer.Tile({
  //       source: new ol.source.OSM()
  //     })
  //   ],
  //   view: new ol.View({
  //     center: ol.proj.fromLonLat([112.725304, -7.2555075]),
  //     zoom: 18
  //   })
  // });
</script>
@endsection