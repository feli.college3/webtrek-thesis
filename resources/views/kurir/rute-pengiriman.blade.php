@extends('layouts.kurir', ['title' => 'Dashboard'])
@section('content')
    <!-- Content Header (Page header) -->

<!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.2.1/css/ol.css" type="text/css"> -->
<!-- <style>
  .map {
    height: 400px;
    width: 100%;
  }
</style> -->

<section class="content-header">
  <h1><b>
    RUTE PENGIRIMAN
  </b></h1>
  {!! $map['js'] !!}
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    @if(count($list_nota)>0)
      <div class="col-md-12">
        <h4><b>Pilih Pesanan yang Akan Dikirim</b></h4>
      </div>
      <div class="col-md-12">
        @foreach($list_nota as $nota)
        <div class="col-md-6">
          <button class="btn btn-success"><a href="{{route('kirim-sekarang', $nota->no_nota_jual)}}" class="font-white">Pilih</a></button>
          <label class="delivery-label">{{$nota->no_nota_jual}} - {{$nota->nama_pelanggan}}</label><br>
          <label class="delivery-note">Keterangan: {{$nota->keterangan}}</label>
        </div>
        @endforeach
    @else
      <div class="col-md-12">
        <h4><b>Tidak Ada Pesanan yang Perlu Dikirim</b></h4>
      </div>
    @endif
    </div>
    <div class="col-md-12">
      <br>
    </div>
    <div class="col-md-12">
      <!-- <div id="map" class="full-map"></div>
    <div id="location" class="marker"><span class="icon-arrow-up"></span></div> -->
      {!! $map['html'] !!}
      <div id="directions"></div>
    </div>
  </div>
</section>



<!-- <script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.2.1/build/ol.js"></script>    -->
<script type="text/javascript">
  // var map = new ol.Map({
  //   target: 'map',
  //   layers: [
  //     new ol.layer.Tile({
  //       source: new ol.source.OSM()
  //     })
  //   ],
  //   view: new ol.View({
  //     center: ol.proj.fromLonLat([112.725304, -7.2555075]),
  //     zoom: 18
  //   })
  // });
</script>
@endsection