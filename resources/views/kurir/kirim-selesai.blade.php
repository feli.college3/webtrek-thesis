@extends('layouts.kurir', ['title' => 'Pengiriman Berlangsung'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DAFTAR PENGIRIMAN SELESAI
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No. Nota</th>
                  <th class="no-sort">Pelanggan</th>
                  <th class="no-sort">Alamat</th>
                  <th class="no-sort">Nama Penerima</th>
                  <th class="no-sort">Metode Pembayaran</th>
                  <th class="no-sort right-aligned">Grand Total (Rp)</th>
                  <th class="no-sort">Keterangan</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($list_pengiriman))
                  @foreach($list_pengiriman as $post)
                    <tr>
                      <td><a href="{{route('nota-jual.show', $post->no_nota_jual)}}">{{$post->no_nota_jual}}</a></td>
                      <td>{{$post->nama_pelanggan}}</td>
                      <td>{{$post->alamat}}</td>
                      <td>{{$post->nama_penerima}}</td>
                      <td>{{$post->metode_pembayaran}}</td>
                      <td class="right-aligned">{{number_format($post->grand_total,0,".",",")}}</td>
                      <td>{{$post->keterangan}}</td>
                    </tr>
                   @endforeach
                  @endif  
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection