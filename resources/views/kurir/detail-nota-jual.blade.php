@extends('layouts.kurir', ['title' => 'Detail Nota'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DETAIL NOTA - {{$id}}
      </b></h1>
        <!-- <button class="btn btn-success btn-tambah"><a href="{{ route('produk.create') }}" class="font-white">Tambah Produk Baru</a></button> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @if(!empty($produk_toko))
            <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"><b>Produk Toko</b></h3>
            </div>
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th class="no-sort center-aligned"></th>
                    <th>ID Produk</th>
                    <th>Nama Produk</th>
                    @if($detail_toko->fitur_stok_fifo==1)
                      <th class="no-sort">Tanggal Kedaluwarsa</th>
                    @endif
                      <th class="no-sort right-aligned">Harga Jual</th>
                      <th class="no-sort right-aligned">Jumlah</th>
                  </tr>
                </thead>
                <tbody>
                  @for ($i = 0; $i < count($produk_toko); $i++)
                    <tr>
                      <td class="center-aligned"><img src="../images/{{$produk_toko[$i]->nama_foto}}.{{$produk_toko[$i]->ekstensi}}" style="width: 100px; height: 100px;"></td>
                      <td>{{$produk_toko[$i]->idproduk}}</td>
                      <td>{{$produk_toko[$i]->nama_produk}}</td>
                      @if($detail_toko->fitur_stok_fifo==1)
                        <td>{{date('d-m-Y', strtotime($produk_toko[$i]->tanggal_kadaluwarsa))}}</td>
                        <td class="right-aligned">{{number_format($produk_toko[$i]->harga_jual,0,".",",")}}</td>
                        <td class="right-aligned">{{$produk_toko[$i]->jumlah}}</td>
                      @else
                        <td class="right-aligned">{{number_format($harga_jual[$i]->harga_jual,0,".",",")}}</td>
                        <td class="right-aligned">{{$jumlah_produk_toko[$i]->jumlah_produk}}</td>
                      @endif
                    </tr>
                  @endfor
                </tbody>
              </table>
            </div>
          @endif  

          @if(count($produk_konsinyasi)>0)
            <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title"><b>Produk Konsinyasi</b></h3>
            </div>
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                  <tr>
                  <th class="no-sort center-aligned"></th>
                  <th>ID Produk</th>
                  <th>Nama Produk</th>
                  <th class="right-aligned">Harga Jual</th>
                  <th class="right-aligned">Jumlah</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($produk_konsinyasi as $post)
                    <tr>
                      <td class="center-aligned"><img src="../images/{{$post->nama_foto}}.{{$post->ekstensi}}" style="width: 100px; height: 100px;"></td>
                      <td>{{$post->idproduk_konsinyasi}}</td>
                      <td>{{$post->nama_produk}}</td>
                      <td class="right-aligned">{{number_format($post->harga_jual,0,".",",")}}</td>
                      <td class="right-aligned">{{$post->jumlah}}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          @endif  
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection