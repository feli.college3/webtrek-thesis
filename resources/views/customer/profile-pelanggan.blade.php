@extends('layouts.pelanggan', ['title' => 'Profile Pelanggan'])
@section('content')
<div class="hero-wrap" style="background-color: black;">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <p class="breadcrumbs"></p>
        <h1 class="mb-0 bread">Profile Pelanggan</h1>
      </div>
    </div>
  </div>
</div>


<section class="ftco-section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <form role="form" method="POST" action="{{ route('simpan-profile') }}" id="form-profile" enctype="multipart/form-data">
          {{ csrf_field() }}
            <div class="form-group">
              <label class="font-black">Nama Pelanggan(*)</label>
              <input type="text" name="nama" class="form-control" value="{{$pelanggan->nama_pelanggan}}" required autofocus/>
            </div><br>
            <div class="form-group">
              <label class="font-black">Alamat(*)</label><br>
              <label class="font-orange"><b>Keterangan: Masukkan nama kabupaten/kota, contoh: Jl. ABC No. 1 Surabaya</b></label>
              <input type="text" name="alamat" id="alamat" class="form-control" value="{{$pelanggan->alamat_pelanggan}}" required/><br>
            </div>
            <div class="form-group">
              <label class="font-black">Telepon(*)</label>
              <input type="number" name="telepon" class="form-control" value="{{$pelanggan->telepon_pelanggan}}" required/>
            </div><br>
            <p class="font-black"><b>(*) wajib diisi</b></p>
            <!-- <button type="submit" class="btn btn-primary py-3 px-4">CHECKOUT</button> -->
            <input type="hidden" name="idpelanggan" value="{{$pelanggan->idpelanggan}}">
            <input type="hidden" name="jarak" id="jarak">
            <div id="map" style="width: 400px; height: 300px;" class="hide"></div> 
            <p class="text-center"><button type="submit" class="btn btn-primary py-3 px-4" id="simpan">Simpan</button></p>
        </form>
      </div>
    </div>
  </div>
</section>
@if (session('alert'))
    <script>
        swal("{{ session('alert') }}");
    </script>
@endif

<script src="{{ asset('customer/js/jquery.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('customer/js/popper.min.js') }}"></script>
<script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyC2UlF6oXbQDWRWv4CPbphCK0U8sKlUzJE"></script>

<script type="text/javascript">
  var detail_toko = {!! json_encode($detail_toko) !!};
  window.onload = function() {
  };
  $('#form-profile').on('submit', (event) => {
        event.preventDefault();
        var tujuan = document.getElementById('alamat').value;
        var distance=0;
        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer();

        var myOptions = {
         zoom:7,
         mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        var map = new google.maps.Map(document.getElementById("map"), myOptions);
        directionsDisplay.setMap(map);
        var request = {
           origin: detail_toko['alamat_toko'], 
           destination: tujuan,
           travelMode: google.maps.DirectionsTravelMode.DRIVING
        };

        directionsService.route(request, function(response, status) {
          if (status == google.maps.DirectionsStatus.OK) {
            for(i = 0; i < response.routes[0].legs.length; i++){
               distance += parseFloat(response.routes[0].legs[i].distance.value);
            }
            directionsDisplay.setDirections(response);

            distance/=1000;
            distance= distance.toFixed(1);
            distance=parseFloat(distance, 10);
            console.log(distance);
            console.log(detail_toko['maksimal_jarak']);
            document.getElementById('jarak').value=distance;
          }
        });

        setTimeout(function(){
          if(detail_toko['fitur_kirim']==1){
            var jarak=parseFloat(document.getElementById('jarak').value);
            var maksimal_jarak=parseFloat(detail_toko['maksimal_jarak']);
            console.log('luar jarak:'+jarak);
            console.log('luar'+detail_toko['maksimal_jarak']);
            if(jarak<=maksimal_jarak){
              Swal.fire({
                title: 'Data tersimpan',
                icon: 'success',
                showConfirmButton: false,
              })
              setTimeout(function(){
                var form = $('#form-profile');
                form.trigger('submit');
              },500);
            }
            else{
              Swal.fire({
                title: 'Alamat Anda Berada di Luar Wilayah Pengiriman',
                text: "Jarak maksimal pengiriman adalah "+maksimal_jarak+" km. Jika Anda tetap melanjutkan, maka pesanan tidak dapat dikirimkan ke alamat ini.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#44b839',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Tetap Lanjutkan',
                confirmButtonText: 'Ganti Alamat',
                reverseButtons: true,
              }).then((result) => {
                if (result.value) {}
                else{
                  var form = $('#form-profile');
                  form.trigger('submit');
                }
              })
            }
          }
          else{
            var form = $('#form-profile');
            form.trigger('submit');
          }
        }, 800);
    });
  if(detail_toko['fitur_kirim']==1){
    // $('#form-profile').submit(function() {  
    /*  var aman=false;
      event.preventDefault();
      // setTimeout(getjarak(),20000);
      function getjarak(){
        var tujuan = document.getElementById('alamat').value;
        var distance=10;
        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer();

        var myOptions = {
         zoom:7,
         mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        var map = new google.maps.Map(document.getElementById("map"), myOptions);
        directionsDisplay.setMap(map);
        var request = {
           origin: detail_toko['alamat_toko'], 
           destination: tujuan,
           travelMode: google.maps.DirectionsTravelMode.DRIVING
        };

        directionsService.route(request, function(response, status) {
          if (status == google.maps.DirectionsStatus.OK) {
            for(i = 0; i < response.routes[0].legs.length; i++){
               distance += parseFloat(response.routes[0].legs[i].distance.value);
            }
            directionsDisplay.setDirections(response);

            distance/=1000;
            distance= distance.toFixed(1);
            distance=parseFloat(distance, 10);
            console.log(distance);
            console.log(detail_toko['maksimal_jarak']);
            if(distance<=detail_toko['maksimal_jarak']){
              swal({
                title: "Anda yakin mengubah data?",
                icon: "warning",
                buttons: {
                  cancel: {
                    text: "Tidak",
                    value: false,
                    visible: true,
                  },
                  confirm: {
                    text: "Ya",
                    value: true,
                    visible: true,
                  }
                }
              }).then((value) => {
              switch (value) {
                case false:
                  document.getElementById('lanjot').value=0;
                  console.log(document.getElementById('lanjot').value);
                  break;
                case true:
                  document.getElementById('lanjot').value=1;
                  console.log(document.getElementById('lanjot').value);
                  break;
              }
            });
            }
            else{
              swal({
                title: "Alamat Anda Berada di Luar Wilayah Pengiriman",
                text: "Jarak maksimal pengiriman adalah "+detail_toko['maksimal_jarak']+" km. Jika Anda tetap melanjutkan, maka pesanan tidak dapat dikirimkan ke alamat ini.",
                icon: "warning",
                buttons: ['Tetap Lanjutkan','Ganti Alamat'],
              });
            }
          }
        });
      }
*/
      // return true;
        // setTimeout(function() {var lanjot = document.getElementById('lanjot').value;
        //   if(lanjot==1){
        //   alert(1);
        //   return true;
        // }
        // else{
        //   alert(0);
        //   return false;
        // }}, 5000);
        


    // });
  }
</script>
@endsection
