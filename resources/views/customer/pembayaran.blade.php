@extends('layouts.pelanggan', ['title' => 'Pembayaran'])
@section('content')
<div class="hero-wrap" style="background-color: black;">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <p class="breadcrumbs"></p>
        <h1 class="mb-0 bread">Pembayaran</h1>
      </div>
    </div>
  </div>
</div>


<section class="ftco-section">
	<div class="container">
		<div class="row">
				<form role="form" method="POST" action="{{ url('pembayaran') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
          <h4>Pembayaran dengan Transfer Bank</h4>
          <h5>Nomor Pesanan: <b><a href="{{route('detail-pesanan', $nota_jual->no_nota_jual)}}">{{$nota_jual->no_nota_jual}}</a></b></h5>
          <h5><b>Total Pembayaran: Rp{{number_format($nota_jual->grand_total+$nota_jual->ongkos_kirim,0,".",",")}}</b></h5>
          <h5><b>Nomor Rekening: {{$detail_toko->nomor_rekening}}</b> ({{$detail_toko->nama_bank}} a/n {{$detail_toko->pemilik_rekening}})</h5>
          @if($nota_jual->bukti_transfer==null)
            <h5>Status: <b>Belum Dibayar</b></h5>
            @if($detail_toko->fitur_pembatalan_pesanan==1)
              <h6 class="font-red">Reminder: Pesanan akan dibatalkan dalam {{$detail_toko->maksimal_pembatalan}} jam jika tidak dilakukan pembayaran valid!</h6>
            @endif
            <br>
            <div class="card-body">
              <h5>Unggah bukti transfer</h5>
              <div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
                <input type="file" name="foto"/>
                @if ($errors->has('foto'))
                  <span class="help-block">
                    <strong>{{ $errors->first('foto') }}</strong>
                  </span>
                @endif
              </div>
              <input type="hidden" name="no_nota" value="{{$nota_jual->no_nota_jual}}"/>
            </div>
            <span><button type="submit" class="btn btn-primary py-3 px-4">Simpan</button>
            <a class="pay-later" href="{{route('pesanan', 'belum-bayar')}}">Bayar Nanti</a></span>
          @else
          <br>
            <h5>Bukti transfer telah diunggah.</h5>
            <h5>Status: 
            @if($nota_jual->status_transaksi=='Belum Divalidasi')
              <b>Belum Divalidasi</b></h5><br>
              <img src="../images/{{$nota_jual->bukti_transfer}}" style="width: 300px; height: 300px;">
            @elseif($nota_jual->status_transaksi=='Belum Diproses'||$nota_jual->status_transaksi=='Diproses')
              <b><span class="font-green">Bukti Valid</span></b></h5><br>
              <img src="../images/{{$nota_jual->bukti_transfer}}" style="width: 300px; height: 300px;">
            @elseif($nota_jual->status_transaksi=='Bukti Pembayaran Tidak Valid')
              <b><span class="font-red">Bukti Tidak Valid</span></b></h5><br>
              <img src="../images/{{$nota_jual->bukti_transfer}}" style="width: 300px; height: 300px;">
              <div class="card-body">
              <br>
              <h5>Unggah bukti transfer baru</h5>
              <div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
                <input type="file" name="foto"/>
                @if ($errors->has('foto'))
                  <span class="help-block">
                    <strong>{{ $errors->first('foto') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <p><button type="submit" class="btn btn-primary py-3 px-4">Simpan</button></p>
            @elseif($nota_jual->status_transaksi=='Dibatalkan')
              <b><span class="font-red">Dibatalkan</span></b></h5><br>
              <img src="../images/{{$nota_jual->bukti_transfer}}" style="width: 300px; height: 300px;">
            @endif
          @endif
          <input type="hidden" name="no_nota" value="{{$nota_jual->no_nota_jual}}"/>
        </form>
		</div>
	</div>
</section>

@endsection