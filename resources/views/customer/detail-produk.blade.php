@extends('layouts.pelanggan', ['title' => 'Detail Produk'])
@section('content')
<div class="hero-wrap" style="background-color: black;">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
        <p class="breadcrumbs"><span class="mr-2"><span>Detail</span></p>
        <h1 class="mb-0 bread">Produk</h1>
      </div>
    </div>
  </div>
</div>


<section class="ftco-section">
	<div class="container">
		<div class="row">

			<div class="col-lg-6 mb-5 ftco-animate">
				<a href="../images/{{$produk->nama_foto}}.{{$produk->ekstensi}}" class="image-popup"><img src="../images/{{$produk->nama_foto}}.{{$produk->ekstensi}}" class="img-fluid" alt="Foto Produk"></a>
			</div>
			<div class="col-lg-6 product-details pl-md-5 ftco-animate">
				<h3>{{$produk->nama_produk}}</h3>
        @if($panjang_id==6)
          <p class="price"><span>Rp{{number_format($produk->harga_jual_eceran)}}/{{$produk->satuan}}</span></p>
          @if($detail_toko->fitur_harga_grosir==1)
            <h5>Harga Grosir: Rp{{number_format($produk->harga_jual_grosir)}}/{{$produk->satuan}}</h5>
            <h6 class="font-black">(minimal pembelian: {{$detail_toko->jumlah_grosir}})</h6>
          @endif
        @else
          <p class="price"><span>Rp{{number_format($produk->harga_jual)}}/{{$produk->satuan}}</span></p>
        @endif
        
				<p>{{$produk->deskripsi}}</p>
				<div class="row mt-4">
					<div class="w-100"></div>

          <form role="form" id="form-detail" method="POST" action="{{ url('keranjang') }}" enctype="multipart/form-data">
          {{ csrf_field() }}
						<div class="input-group col-md-8 d-flex mb-3">
             	<span class="input-group-btn mr-2">
                	<button type="button" class="quantity-left-minus btn"  data-type="minus" data-field="">
                   <i class="ion-ios-remove"></i>
                	</button>
          		</span>
                @if($panjang_id==6)
                    @if($stok>0)
                      <input type="text" id="jumlah" name="jumlah" class="form-control" value="1" min="1" max="{{$stok}}" onkeyup="getstok()">
                    @else
                      <input type="text" id="jumlah" name="jumlah" class="form-control" value="0" min="0" max="0" onkeyup="getstok()">
                    @endif
                  @else
                    @if($produk->stok>0)
                      <input type="text" id="jumlah" name="jumlah" class="form-control" value="1" min="1" max="{{$produk->stok}}" onkeyup="getstok()">
                    @else
                      <input type="text" id="jumlah" name="jumlah" class="form-control" value="0" min="0" max="0" onkeyup="getstok()">
                    @endif
                  @endif
             	<span class="input-group-btn ml-2">
                	<button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
                     <i class="ion-ios-add"></i>
                 </button>
             	</span>
          	</div>
          	<div class="w-100"></div>
          	<div class="col-md-12">
                  @if($panjang_id==6)
                    @if($stok>0)
                      <p style="color: #000;">Tersedia&nbsp;
                        {{number_format($stok)}}
                        &nbsp;{{$produk->satuan}}</p>
                    @else
                      <p style="color: red;">Produk habis</p>
                    @endif
                  @else
                    @if($produk->stok>0)
                    <p style="color: #000;">Tersedia&nbsp;
                      {{number_format($produk->stok)}}
                      &nbsp;{{$produk->satuan}}</p>
                    @else
                      <p style="color: red;">Produk habis</p>
                    @endif
                  @endif
          	</div>
        	</div>
        	<p>
            @if(Auth::user())
              @if($produk->stok>0||$stok>0)
                <button type="submit" id="button-chart">Tambahkan ke Keranjang</button>
              @else
                <button type="submit" id="button-chart" disabled>Tambahkan ke Keranjang</button>
              @endif
            @else
              @if($produk->stok>0||$stok>0)
                <a href="{{url('login')}}" class="btn btn-black py-3 px-5">Tambahkan ke Keranjang</a>
              @else
                <a href="{{url('login')}}" class="btn btn-black py-3 px-5" disabled>Tambahkan ke Keranjang</a>
              @endif
              
            @endif
          </p>
          <input type="hidden" name="idproduk" value="{{$produk->idproduk}}"/>
        </form>
		  </div>
		</div>
	</div>
</section>

  <!-- loader -->
  <!-- <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div> -->


<script src="{{ asset('customer/js/jquery.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('customer/js/popper.min.js') }}"></script>
<script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
  
<script>
    $('#form-detail').on('submit', (event) => {
    event.preventDefault();
    var produk={!! json_encode($produk) !!};
    var stok={!! json_encode($stok) !!};
    var stok_produk=produk['stok'];
    if(stok_produk==undefined){
      stok_produk=0;
    }
    if(stok_produk>0||stok>0){
      Swal.fire({
        title: 'Berhasil Ditambahkan ke Keranjang',
        icon: 'success',
        showConfirmButton: false,
      })
      setTimeout(function(){
        var form = $('#form-detail');
        form.trigger('submit');
      },500);
    }
  })

	$(document).ready(function(){
	 var quantitiy=0;
	   $('.quantity-right-plus').click(function(e){
	        
	        // Stop acting like a button
	        e.preventDefault();
	        // Get the field name
	        var quantity = parseInt($('#jumlah').val());
	        var max_quantity = $('#jumlah').attr('max');
            
            // If is not undefined
                if(quantity<max_quantity){
                $('#jumlah').val(quantity + 1);
            }
	        
	    });

	     $('.quantity-left-minus').click(function(e){
	        // Stop acting like a button
	        e.preventDefault();
	        // Get the field name
	        var quantity = parseInt($('#jumlah').val());
	        
	        // If is not undefined
	      
	            // Increment
	            if(quantity>1){
	            $('#jumlah').val(quantity - 1);
	            }
	    });
	    
	});

  function getstok(){
    var panjang_id={!! json_encode($panjang_id) !!};
    var stok=0
    if(panjang_id==6){
      stok={!! json_encode($stok) !!};
    }
    else{
      stok={!! json_encode($produk->stok) !!};
    }
    var jumlah_element = document.getElementById("jumlah");
    if(jumlah_element.value<1){
      jumlah_element.value=1;
    }
    else if(jumlah_element.value>stok){
      jumlah_element.value=stok;
    }
  }
</script>

@endsection