@extends('layouts.pelanggan', ['title' => 'Checkout'])
@section('content')
  
  <div class="hero-wrap" style="background-color: black;">
	  <div class="container">
	    <div class="row no-gutters slider-text align-items-center justify-content-center">
	      <div class="col-md-9 ftco-animate text-center">
	      	<p class="breadcrumbs">
	        <h1 class="mb-0 bread">CHECKOUT</h1>
	      </div>
	    </div>
	  </div>
  </div>

    <section class="ftco-section">
	  <form class="billing-form" method="POST" action="{{ url('checkout') }}" enctype="multipart/form-data">
  	  {{ csrf_field() }}
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xl-7 ftco-animate">
	          	<div class="row align-items-end">
	          		<div class="col-md-12">
	                <div class="form-group">
	                	<p class="font-red">(*) wajib diisi</p>
	                	<label for="firstname">Nama Pelanggan:</label>
	                	<label>{{$pelanggan->nama_pelanggan}}</label>
	                </div>
	              </div>
	            <div class="col-md-12">
	            	<div class="form-group">
	            		<label for="country">Alamat(*)</label>
	                  	<input type="text" name="alamat" class="form-control" value="{{$pelanggan->alamat_pelanggan}}">
	                </div>
	            </div>
	            <div class="col-md-12">
	            	<div class="form-group">
	            		<label for="postcodezip">Kode Pos(*)</label>
	                  	<input type="number" name="kodepos" class="form-control" value="60147">
	                </div>
	            </div>
		            <div class="col-md-12">
	                <div class="form-group">
	                	<label for="phone">No. Telepon(*)</label>
	                  <input type="text" name="telepon" class="form-control" value="{{$pelanggan->telepon_pelanggan}}">
	                </div>
	              </div>
	            </div>
	            <p><a href="#"class="btn btn-primary py-3 px-4">Cek Ongkir</a></p>
		  </div>
			<div class="col-xl-5">
	          <div class="row mt-5 pt-3">
	          	<div class="col-md-12 d-flex mb-5">
	          		<div class="cart-detail cart-total p-3 p-md-4">
	          			<h3>TOTAL KERANJANG</h3>
						<p class="d-flex">
							<span class="font-black">Total</span>
							<span class="font-black">Rp{{number_format($total_keranjang,0,".",",")}}</span>
						</p>
						<p class="d-flex">
							<span class="font-black">Ongkos Kirim</span>
							<span class="font-black">Rp{{number_format($ongkir,0,".",",")}}</span>
							<input type="hidden" name="ongkir" value="{{$ongkir}}">
						</p>
						<p class="d-flex">
							<span class="font-black">Diskon</span>
							<span class="font-black">Rp{{number_format(0,0,".",",")}}</span>
						</p>
						<p class="d-flex">
							<span class="font-black">PPN</span>
							<span class="font-black">Rp{{number_format(0,0,".",",")}}</span>
						</p>
						<hr>
						<p class="d-flex total-price">
							<span class="font-black">Grand Total</span>
							<span class="font-black">Rp{{number_format($grand_total,0,".",",")}}</span>
						</p>
					</div>
	          	</div>
	          </div>
	        </div>
          </div> <!-- .col-md-8 -->
          <div class="row justify-content-end">
			<div class="col-lg-6 mt-5 cart-wrap ftco-animate">
				<div class="cart-total mb-3">
					<h3>METODE PEMBAYARAN(*)</h3>
					<div class="form-group">
						@if($detail_toko->pembayaran_cod==1)
							<div class="col-md-12">
								<label><input type="radio" name="metode" class="mr-2" value="COD" checked/>Cash on Delivery (COD)</label>
							</div>
						@endif
						@if($detail_toko->pembayaran_transfer==1)
							<div class="col-md-12">
								<label><input type="radio" name="metode" class="mr-2" value="Transfer" checked/>Transfer Bank</label>
							</div>
						@endif
					</div>
				</div>
			</div>
			<div class="col-lg-6 mt-5 cart-wrap ftco-animate">
				<div class="cart-total mb-3">
					<h3>PENGIRIMAN(*)</h3>
					<div class="form-group">
						@if($detail_toko->fitur_kirim==1)
							<div class="col-md-12">
								<label><input type="radio" name="pengiriman" class="mr-2" value="1" checked/>Kirim ke Alamat Tertera</label>
							</div>
						@endif
						<div class="col-md-12">
						   <label><input type="radio" name="pengiriman" class="mr-2" value="0" checked/>Ambil Sendiri</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="checkbox">
			   <label><input type="checkbox" value="" class="mr-2" checked>Saya sudah membaca dan menerima syarat dan ketentuan yang berlaku</label>
			</div>
		</div>
		<p><button type="submit" class="btn btn-primary py-3 px-4">CHECKOUT</button></p>
        </div>
      </div>
  	  </form><!-- END -->
    </section> <!-- .section -->
    
  


<script src="{{ asset('customer/js/jquery.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('customer/js/popper.min.js') }}"></script>
<script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>

  <script>
		$(document).ready(function(){
		});
	</script>
    
@endsection