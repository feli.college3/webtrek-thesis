@extends('layouts.pelanggan', ['title' => 'Dashboard'])
@section('content')
<section id="home-section" class="hero">
	  <div class="home-slider owl-carousel">
      <div class="slider-item" style="background-image: url(customer/images/home1.jpg);">
      	<div class="overlay"></div>
        <div class="container">
          <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

            <div class="col-md-12 ftco-animate text-center">
              <h1 class="mb-2">Kami Siap Mengantarkan Sampai di Rumah</h1>
              <h2 class="subheading mb-4">Pilih Produk Anda, Kami Antarkan</h2>
              <p><a href="{{ url('belanja') }}" class="btn btn-primary">Belanja Sekarang</a></p>
            </div>

          </div>
        </div>
      </div>

     <!-- <div class="slider-item" style="background-image: url(customer/images/bg_2.jpg);">
      	<div class="overlay"></div>
	        <div class="container">
	          <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

	            <div class="col-sm-12 ftco-animate text-center">
	              <h1 class="mb-2">100% Fresh &amp; Organic Foods</h1>
	              <h2 class="subheading mb-4">We deliver organic vegetables &amp; fruits</h2>
	              <p><a href="{{ url('belanja') }}" class="btn btn-primary">Belanja Sekarang</a></p>
	            </div>

	          </div>
	        </div>
      	</div>
    </div> -->
</section>
<br>

@if(Auth::user())
    <section class="ftco-section testimony-section">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-3">
              <div class="col-md-7 heading-section ftco-animate text-center">
                <span class="subheading"></span>
                <h2 class="mb-4">Beli Lagi Produk Favorit Anda</h2>
                <p>Produk favorit dapat Anda beli sekarang juga! Tunggu apa lagi?</p>
              </div>
            </div>
            <div class="row ftco-animate">
              <div class="col-md-12">
                <div class="carousel-testimony owl-carousel">

                	@if(count($array_toko_dibeli)>0)
                		@for ($i = 0; $i < count($array_toko_dibeli); $i++)
    					<div class="item">
    						<div class="testimony-wrap p-4 pb-5">
    							<a href="{{route('belanja.show', $array_toko_dibeli[$i][0]->idproduk)}}"><div class="user-img mb-5 img-index" style="background-image: url(images/{{$array_toko_dibeli[$i][0]->nama_foto}}.{{$array_toko_dibeli[$i][0]->ekstensi}})">
    								</div></a>
    							<div class="text text-center">
    								<a href="{{route('keranjang-satuan', $array_toko_dibeli[$i][0]->idproduk)}}" class="buy-now d-flex justify-content-center align-items-center mx-1 link-add">
                                        <span><i class="ion-ios-cart" style="zoom:2.0;"></i></span>
                                    </a>
    								<p class="name"><a href="{{route('belanja.show', $array_toko_dibeli[$i][0]->idproduk)}}">{{$array_toko_dibeli[$i][0]->nama_produk}}</a></p>
    								<p class="mb-5 pl-4 line">{{$array_toko_dibeli[$i][0]->deskripsi}}</p>
    							</div>
    						</div>
    					</div>
    					@endfor
                  	@endif
                  	@if(count($array_konsi_dibeli)>0)
                		@for ($i = 0; $i < count($array_konsi_dibeli); $i++)
    					<div class="item">
    						<div class="testimony-wrap p-4 pb-5">
    							<a href="{{route('belanja.show', $array_konsi_dibeli[$i][0]->idproduk)}}"><div class="user-img mb-5 img-index" style="background-image: url(images/{{$array_konsi_dibeli[$i][0]->nama_foto}}.{{$array_konsi_dibeli[$i][0]->ekstensi}})">
    								</div></a>
    								<div class="text text-center">
    									<a href="{{route('keranjang-satuan', $array_konsi_dibeli[$i][0]->idproduk)}}" class="buy-now d-flex justify-content-center align-items-center mx-1 link-add">
    	                                    <span><i class="ion-ios-cart" style="zoom:2.0;"></i></span>
    	                                </a>
    									<p class="name"><a href="{{route('belanja.show', $array_konsi_dibeli[$i][0]->idproduk)}}">{{$array_konsi_dibeli[$i][0]->nama_produk}}</a></p>
    									<p class="mb-5 pl-4 line">{{$array_konsi_dibeli[$i][0]->deskripsi}}</p>
    								</div>
    						</div>
    					</div>
    					@endfor
                  	@endif
                </div>
              </div>
            </div>
        </div>
    </section>
@endif

@if(count($array_toko_terlaris)>0||count($array_konsi_terlaris)>0)
<section class="ftco-section">
	<div class="container">
		<div class="row justify-content-center mb-3 pb-3">
            <div class="col-md-12 heading-section text-center ftco-animate">
            	<span class="subheading">Produk Terlaris</span>
                <h2 class="mb-4">Produk Terlaris dari Toko</h2>
                <p>Produk paling laris terjual dan paling digemari pelanggan. Segera beli sebelum Anda kehabisan!</p>
            </div>
        </div>   		
    </div>
	<div class="container">
		<div class="row">
            @if(count($array_toko_terlaris)>0)
                @foreach($array_toko_terlaris as $toko)
        			<div class="col-md-6 col-lg-3 ftco-animate">
        				<div class="product">
        					<a href="#" class="img-prod"><img class="img-fluid" src="images/{{$toko->nama_foto}}.{{$toko->ekstensi}}" alt="Foto Produk">
        						<!-- <span class="status">30%</span> -->
        						<div class="overlay"></div>
        					</a>
        					<div class="text py-3 pb-4 px-3 text-center">
        						<h3><a href="#">{{$toko->nama_produk}}</a></h3>
        						<div class="d-flex">
        							<div class="pricing">
                                        <p class="price"><span>Rp{{number_format($toko->harga_jual_eceran,0,".",",")}}/{{$toko->satuan}}</span></p>
                                    </div>
            					</div>
            					<div class="bottom-area d-flex px-3">
            						<div class="m-auto d-flex">
            							<a href="{{route('belanja.show', $toko->idproduk)}}" class="add-to-cart d-flex justify-content-center align-items-center text-center">
            								<span><i class="ion-ios-menu"></i></span>
            							</a>
            							<a href="{{route('keranjang-satuan', $toko->idproduk)}}" class="buy-now d-flex justify-content-center align-items-center mx-1 link-add">
            								<span><i class="ion-ios-cart"></i></span>
            							</a>
        							</div>
        						</div>
        					</div>
        				</div>
        			</div>
                @endforeach
            @endif
            @if(count($array_konsi_terlaris)>0)
                @foreach($array_konsi_terlaris as $konsi)
                    <div class="col-md-6 col-lg-3 ftco-animate">
                        <div class="product">
                            <a href="#" class="img-prod"><img class="img-fluid" src="images/{{$konsi->nama_foto}}.{{$konsi->ekstensi}}" alt="Foto Produk">
                                <!-- <span class="status">30%</span> -->
                                <div class="overlay"></div>
                            </a>
                            <div class="text py-3 pb-4 px-3 text-center">
                                <h3><a href="#">{{$konsi->nama_produk}}</a></h3>
                                <div class="d-flex">
                                    <div class="pricing">
                                        <p class="price"><span>Rp{{number_format($konsi->harga_jual,0,".",",")}}/{{$konsi->satuan}}</span></p>
                                    </div>
                                </div>
                                <div class="bottom-area d-flex px-3">
                                    <div class="m-auto d-flex">
                                        <a href="{{route('belanja.show', $konsi->idproduk)}}" class="add-to-cart d-flex justify-content-center align-items-center text-center">
                                        <span><i class="ion-ios-menu"></i></span></a>
                                        <a href="{{route('keranjang-satuan', $konsi->idproduk)}}" class="buy-now d-flex justify-content-center align-items-center mx-1 link-add">
                                        <span><i class="ion-ios-cart"></i></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
		</div>
	</div>
</section>
@endif
<section class="">
	<div class="container">
		<div class="row">
			<!-- @if(!empty($list_kategori))
    			@for ($i = 0; $i < count($list_kategori); $i++)
    				@if(count($list_kategori)%2!=0)
						<div class="col-md-4" style="border-radius:20px;background-color: {{$warna[$i]}};">
							<div class="mr-1 mb-1">
								<h2 class="font-white"><a href="#">{{$list_kategori[$i]->nama_kategori}}</a></h2>
							</div>
						</div>
						<br>
					@else
						<div class="col-md-6" style="border-radius:20px;background-color: {{$warna[$i]}};">
							<div class="mr-2 mb-1">
								<div class="text py-1">
									<h2 class="font-white"><a href="#" class="font-white">{{$list_kategori[$i]->nama_kategori}}</a></h2>
								</div>
							</div>
						</div>
						<br>
					@endif
				@endfor
			@endif -->
		</div>
	</div>
</section>
@if (session('alert'))
    <script>
        swal("{{ session('alert') }}");
    </script>
@endif
<script src="{{ asset('customer/js/jquery.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('customer/js/popper.min.js') }}"></script>
<script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
  
<script type="text/javascript">
    $('.link-add').click(function(e){
        e.preventDefault();
        var link = $(this).attr('href');
        Swal.fire({
            title: 'Berhasil Ditambahkan ke Keranjang',
            icon: 'success',
            showConfirmButton: false,
            timer: 1000,
        }).then(function() {
            window.location.href = link;
        });
    });
</script>
@endsection