@extends('layouts.pelanggan', ['title' => 'Belanja'])
@section('content')

<div class="hero-wrap" style="background-color: black;">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
      	<p class="breadcrumbs">
        <h1 class="mb-0 bread">Daftar Produk</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10 mb-5 text-center">
				<ul class="product-category">
                    @if($kategori_terpilih=='Semua')
					   <li><a href="{{url('belanja')}}" class="active">Semua</a></li>
                       @foreach($kategori as $kat)
                            <li><a href="{{route('belanja-perkategori', strtolower($kat->nama_kategori))}}">{{$kat->nama_kategori}}</a></li>
                       @endforeach
                    @else
                       <li><a href="{{url('belanja')}}">Semua</a></li>
    					@foreach($kategori as $kat)
                            @if($kategori_terpilih->nama_kategori==$kat->nama_kategori)
    						<li><a href="{{route('belanja-perkategori', strtolower($kat->nama_kategori))}}" class="active">{{$kat->nama_kategori}}</a></li>
                            @else
                            <li><a href="{{route('belanja-perkategori', strtolower($kat->nama_kategori))}}">{{$kat->nama_kategori}}</a></li>
    						<!-- <li class="{{ request()->is('belanja/kategori/*') ? 'active' : '' }}">{{$kat->nama_kategori}}</li> -->
                            @endif
					   @endforeach
                    @endif
				</ul>
			</div>
		</div>
		<div class="row">
            @if(!empty($produk_toko))
                @for ($i = 0; $i < count($produk_toko); $i++)
                    <div class="col-md-6 col-lg-3 ftco-animate">
                        <div class="product">
                            @if($kategori_terpilih=='Semua')
                               <a href="{{route('belanja.show', $produk_toko[$i]->idproduk)}}" class="img-prod"><img class="img-fluid" src="images/{{$produk_toko[$i]->nama_foto}}.{{$produk_toko[$i]->ekstensi}}" alt="Foto Produk">
                                <div class="overlay"></div>
                                </a>
                            @else
                               <a href="{{route('belanja.show', $produk_toko[$i]->idproduk)}}" class="img-prod"><img class="img-fluid" src="../../images/{{$produk_toko[$i]->nama_foto}}.{{$produk_toko[$i]->ekstensi}}" alt="Foto Produk">
                                <div class="overlay"></div>
                            </a>
                            @endif
                            
                            <div class="text py-3 pb-4 px-3 text-center">
                                <h3><a href="#">{{$produk_toko[$i]->nama_produk}}</a></h3>
                                <div class="d-flex">
                                    <div class="pricing">
                                        <p class="price"><span>Rp{{number_format($produk_toko[$i]->harga_jual_eceran)}}/{{$produk_toko[$i]->satuan}}</span></p>
                                    </div>
                                </div>
                                <div class="bottom-area d-flex px-3">
                                    <div class="m-auto d-flex">
                                        <a href="{{route('belanja.show', $produk_toko[$i]->idproduk)}}" class="add-to-cart d-flex justify-content-center align-items-center text-center">
                                            <span><i class="ion-ios-menu"></i></span>
                                        </a>
                                        <a href="{{route('keranjang-satuan', $produk_toko[$i]->idproduk)}}" class="buy-now d-flex justify-content-center align-items-center mx-1 link-add">
                                            <span><i class="ion-ios-cart"></i></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endfor
            @endif
            @if(!empty($produk_konsinyasi))
                @for ($i = 0; $i < count($produk_konsinyasi); $i++)
                    <div class="col-md-6 col-lg-3 ftco-animate">
                        <div class="product">
                            <a href="{{route('belanja.show', $produk_konsinyasi[$i]->idproduk)}}" class="img-prod"><img class="img-fluid" src="images/{{$produk_konsinyasi[$i]->nama_foto}}.{{$produk_konsinyasi[$i]->ekstensi}}" alt="Foto Produk">
                                <div class="overlay"></div>
                            </a>
                            <div class="text py-3 pb-4 px-3 text-center">
                                <h3><a href="#">{{$produk_konsinyasi[$i]->nama_produk}}</a></h3>
                                <div class="d-flex">
                                    <div class="pricing">
                                        <p class="price"><span>Rp{{number_format($produk_konsinyasi[$i]->harga_jual)}}/{{$produk_konsinyasi[$i]->satuan}}</span></p>
                                    </div>
                                </div>
                                <div class="bottom-area d-flex px-3">
                                    <div class="m-auto d-flex">
                                        <a href="{{route('belanja.show', $produk_konsinyasi[$i]->idproduk)}}" class="add-to-cart d-flex justify-content-center align-items-center text-center">
                                            <span><i class="ion-ios-menu"></i></span>
                                        </a>
                                        <a href="{{route('keranjang-satuan', $produk_konsinyasi[$i]->idproduk)}}" class="buy-now d-flex justify-content-center align-items-center mx-1 link-add">
                                            <span><i class="ion-ios-cart"></i></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endfor
            @endif
			
        </div>
		<div class="row mt-5">
      <div class="col text-center">
        <div class="block-27">
          <ul>
            <li><a>&lt;</a></li>
            <li class="active"><span>1</span></li>
            <!-- <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li> -->
            <li><a>&gt;</a></li>
          </ul>
        </div>
      </div>
    </div>
	</div>
</section>
<script src="{{ asset('customer/js/jquery.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('customer/js/popper.min.js') }}"></script>
<script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
  
<script type="text/javascript">
    $('.link-add').click(function(e){
        e.preventDefault();
        var link = $(this).attr('href');
        Swal.fire({
            title: 'Berhasil Ditambahkan ke Keranjang',
            icon: 'success',
            showConfirmButton: false,
            timer: 1000,
        }).then(function() {
            window.location.href = link;
        });
    });
</script>
@endsection