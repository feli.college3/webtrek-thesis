@extends('layouts.pelanggan', ['title' => 'Pesanan'])
@section('content')

<div class="hero-wrap" style="background-color: black;">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
      	<p class="breadcrumbs"><span class="mr-2"><span>Pesanan</span></p>
      	@if($status=='Diproses'||$status=='Belum Diproses')
        	<h1 class="mb-0 bread">Diproses</h1>
        @elseif($status=='Dikirim')
        	<h1 class="mb-0 bread">Dikirim</h1>
    	@elseif($status=='Siap Diambil')
        	<h1 class="mb-0 bread">Siap Diambil</h1>
        @elseif($status=='Selesai')
        	<h1 class="mb-0 bread">Selesai</h1>
        @elseif($status=='Dibatalkan')
        	<h1 class="mb-0 bread">Dibatalkan</h1>
        @elseif($status=='Belum Dibayar')
        	<h1 class="mb-0 bread">Belum Dibayar</h1>
        @endif
      </div>
    </div>
  </div>
</div>

<section class="ftco-section ftco-cart">
	<div class="container">
		<div class="row">
			<div class="col-md-12 ftco-animate">
				<div class="cart-list">
		    	@if(count($list_nota_jual)>0)
	  				<table class="table">
					    <thead class="thead-primary">
					      <tr class="text-center">
					        <th>Produk</th>
					        <th>Waktu</th>
					        <th class="harga-bro">Grand Total (Rp)</th>
					        <th>Metode Pembayaran</th>
					        @if($detail_toko->fitur_kirim==1)
					        	<th>Dikirim</th>
					        @endif
					        <th>&nbsp;</th>
					        <th>&nbsp;</th>
					      </tr>
					    </thead>
					    <tbody>
					    	@for ($i = 0; $i < count($list_nota_jual); $i++)
							      <tr class="text-center">
							        
							        <td class="image-prod">
							        	@if($list_nota_jual[$i]->foto_toko!=null)
							        		<div class="img" style="background-image:url(../images/{{$list_nota_jual[$i]->foto_toko}}.{{$list_nota_jual[$i]->ekstensi_toko}});"></div>
							        	@else
							        		<div class="img" style="background-image:url(../images/{{$list_nota_jual[$i]->foto_konsinyasi}}.{{$list_nota_jual[$i]->ekstensi_konsinyasi}});"></div>
							        	@endif
							        </td>

							        <?php
										// function tgl_indo($tanggal){
										// 	$bulan = array (
										// 		1 =>   'Januari',
										// 		'Februari',
										// 		'Maret',
										// 		'April',
										// 		'Mei',
										// 		'Juni',
										// 		'Juli',
										// 		'Agustus',
										// 		'September',
										// 		'Oktober',
										// 		'November',
										// 		'Desember'
										// 	);
										// 	$pecahkan = explode('-', $tanggal);
										// 	$waktu = explode(' ', $pecahkan[2]);
										// 	$detail_waktu = explode(':', $waktu[1]);
										// 	return $waktu[0].' '.$bulan[ (int)$pecahkan[1] ].' '.$pecahkan[0].' pukul '.$detail_waktu[0].':'.$detail_waktu[1];
										// }
									?>
							        
							        <td class="price">{{date("d-m-Y, H:i", strtotime($list_nota_jual[$i]->tanggal))}}</td>
							        <td class="price">{{number_format($list_nota_jual[$i]->grand_total,0,".",",")}}</td>
							        <td class="price">{{$list_nota_jual[$i]->metode_pembayaran}}</td>
							        @if($detail_toko->fitur_kirim==1)
							        	<td class="price">
								        @if($list_nota_jual[$i]->kirim=="0")Tidak
								    	@else Ya
								    	@endif
								        </td>
							        @endif


						        	<td class="detail-pesanan"><a href="{{route('detail-pesanan', $list_nota_jual[$i]->no_nota_jual)}}">Detail</a></td>

							        @if($list_nota_jual[$i]->metode_pembayaran=='Transfer')
							        	<td class="detail-pesanan">
							        		@if($list_nota_jual[$i]->keterangan=='Bukti Pembayaran Tidak Valid')
							        		<a href="{{route('pembayaran.edit', $list_nota_jual[$i]->no_nota_jual)}}">
							        		@else
							        		<a href="{{route('pembayaran.show', $list_nota_jual[$i]->no_nota_jual)}}">
							        		@endif
							        		Pembayaran</a></td>
							        @endif
							      </tr>
							    @endfor
					    </tbody>
					  </table>
					@else
						@if($status=='Diproses'||$status=='Belum Diproses')
				        	<p class="text-center">Tidak ada pesanan yang sedang diproses</p>
				        @elseif($status=='Dikirim')
				        	<p class="text-center">Tidak ada pesanan yang sedang dikirim</p>
			        	@elseif($status=='Siap Diambil')
				        	<p class="text-center">Belum ada pesanan yang siap diambil</p>
				        @elseif($status=='Selesai')
				        	<p class="text-center">Belum ada pesanan selesai</p>
				        @elseif($status=='Dibatalkan')
				        	<p class="text-center">Tidak ada pesanan yang dibatalkan</p>
				        @elseif($status=='Belum Dibayar')
				        	<p class="text-center">Tidak ada pesanan yang belum dibayar</p>
				        @endif
					@endif
			  </div>
			</div>
		</div>
	</div>
</section>

		
  

  <!-- loader -->
 <!--  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div> -->


  <script src="{{ asset('customer/js/jquery.min.js') }}"></script>
    <script src="{{ asset('customer/js/jquery-migrate-3.0.1.min.js') }}"></script>
    <script src="{{ asset('customer/js/popper.min.js') }}"></script>
    <script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>

  <script>
		
	</script>

@endsection