@extends('layouts.pelanggan', ['title' => 'Keranjang'])
@section('content')

<div class="hero-wrap" style="background-color: black;">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
      	<p class="breadcrumbs">
        <h1 class="mb-0 bread">Keranjang Saya</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section ftco-cart">
	<div class="container">
		<div class="row">
			<div class="col-md-12 ftco-animate">
				<div class="cart-list">
		    	@if(count($produk_toko)>0 || count($produk_konsinyasi)>0)
	  				<table class="table">
					    <thead class="thead-primary">
					      <tr class="text-center">
					        <th>&nbsp;</th>
					        <th>Produk</th>
					        <th class="harga-bro">Harga (Rp)</th>
					        <th>Jumlah</th>
					        <th>Subtotal (Rp)</th>
					        <th>&nbsp;</th>
					      </tr>
					    </thead>
					    <tbody>
					    	@for ($i = 0; $i < count($produk_toko); $i++)
							      <tr class="text-center">
							        <td class="image-prod"><a href="{{route('belanja.show', $produk_toko[$i]->idproduk)}}"><div class="img" style="background-image:url(images/{{$produk_toko[$i]->nama_foto}}.{{$produk_toko[$i]->ekstensi}});" href="{{route('belanja.show', $produk_toko[$i]->idproduk)}}"></div></a></td>
							        
							        <td class="product-name">
							        	<a href="{{route('belanja.show', $produk_toko[$i]->idproduk)}}">{{$produk_toko[$i]->nama_produk}}</a>
							        	<p>{{$produk_toko[$i]->deskripsi}}</p>
							        </td>
							        
							        <td class="price">
							        	@if($detail_toko->fitur_harga_grosir==1)
							        		@if($produk_toko[$i]->jumlah>=$detail_toko->jumlah_grosir)
							        			{{number_format($produk_toko[$i]->harga_jual_grosir,0,".",",")}}
							        		@else
							        			{{number_format($produk_toko[$i]->harga_jual_eceran,0,".",",")}}
							        		@endif
							        	@else
							        		{{number_format($produk_toko[$i]->harga_jual_eceran,0,".",",")}}
							        	@endif
							        </td>
							        
							        <td class="quantity">
							        	<div class="input-group mb-3">
							        		<span class="input-group-btn mr-2">
							        			<form action="{{ route('ubah-keranjang',['kurang', $produk_toko[$i]->idproduk]) }}">
												    <button type="submit" class="btn-plus-minus remove-from-cart">-</button>
												</form>
						            		</span>
							             	<input type="text" id="quantity-toko[{{$i}}]" name="quantity-toko[{{$i}}]" class="form-control input-number" value="{{$produk_toko[$i]->jumlah}}" min="1" max="100" readonly>
							             	<span class="input-group-btn ml-2">
							             		<form action="{{ route('ubah-keranjang',['tambah', $produk_toko[$i]->idproduk]) }}">
												    <button type="submit" class="btn-plus-minus remove-from-cart">+</button>
												</form>
							             	</span>
						          		</div>
						          	</td>
							        
							        <td class="total">{{number_format($subtotal_toko[$i]->subtotal,0,".",",")}}</td>

							        <form method="post" class="form-hapus" id="form-hapus{{$produk_toko[$i]->idproduk}}" action="{{ route('keranjang.destroy', $produk_toko[$i]->idproduk) }}">
		                              {{ method_field('delete')}}
		                              {{ csrf_field() }}
							        	<td class="product-remove"><button class="remove-from-cart hapus" id="hapus_keranjang.{{$produk_toko[$i]->idproduk}}"><span class="ion-ios-close"></span></button></td>
							        </form>
							        
							      </tr>
							    @endfor
	                    		
							    @for ($i = 0; $i < count($produk_konsinyasi); $i++)
							      <tr class="text-center">
							        <td class="image-prod"><a href="{{route('belanja.show', $produk_konsinyasi[$i]->idproduk)}}"><div class="img" style="background-image:url(images/{{$produk_konsinyasi[$i]->nama_foto}}.{{$produk_konsinyasi[$i]->ekstensi}});"></div></a></td>
							        
							        <td class="product-name">
							        	<a href="{{route('belanja.show', $produk_konsinyasi[$i]->idproduk)}}">{{$produk_konsinyasi[$i]->nama_produk}}</a>
							        	<p>{{$produk_konsinyasi[$i]->deskripsi}}</p>
							        </td>
							        
							        <td class="price">{{number_format($produk_konsinyasi[$i]->harga_jual,0,".",",")}}</td>
							        
							        <td class="quantity">
							        	<div class="input-group mb-3">
							        		<span class="input-group-btn mr-2">
							                	<form action="{{ route('ubah-keranjang',['kurang', $produk_konsinyasi[$i]->idproduk]) }}">
												    <button type="submit" class="btn-plus-minus remove-from-cart">-</button>
												</form>
						            		</span>
							             	<input type="text" name="jumlah" class="form-control input-number" value="{{$produk_konsinyasi[$i]->jumlah}}" min="1" max="{{$produk_konsinyasi[$i]->stok}}">
							             	<span class="input-group-btn ml-2">
							                	<form action="{{ route('ubah-keranjang',['tambah', $produk_konsinyasi[$i]->idproduk]) }}">
												    <button type="submit" class="btn-plus-minus remove-from-cart">+</button>
												</form>
							             	</span>
						          		</div>
						          	</td>
							        
							        <td class="total">{{number_format($subtotal_konsinyasi[$i]->subtotal,0,".",",")}}</td>

							        <form method="post" class="form-hapus" id="form-hapus{{$produk_konsinyasi[$i]->idproduk}}" action="{{ route('keranjang.destroy', $produk_konsinyasi[$i]->idproduk_konsinyasi) }}">
		                              {{ method_field('delete')}}
		                              {{ csrf_field() }}
							        	<td class="product-remove"><button class="remove-from-cart hapus" id="hapus_keranjang.{{$produk_konsinyasi[$i]->idproduk}}"><span class="ion-ios-close"></span></button></td>
							        </form>
							        
							      </tr>
							    @endfor
					    </tbody>
					  </table>
					@else
						<p class="text-center">Belum ada produk dalam keranjang</p>
              			<p class="text-center"><a href="{{ url('belanja') }}" class="btn btn-primary">Belanja Sekarang</a></p>
					@endif
			  </div>
			</div>
		</div>
		<div class="row justify-content-end">
			<div class="col-lg-12 mt-5 cart-wrap ftco-animate">
				<div class="cart-total mb-3">
					<h3>TOTAL KERANJANG</h3>
					<p class="d-flex">
						<span class="font-black">Total</span>
						<span class="font-black">Rp{{number_format($total_keranjang,0,".",",")}}</span>
					</p>
					<p class="d-flex">
						<span class="font-black">Diskon</span>
						<span class="font-black">Rp{{number_format(0,0,".",",")}}</span>
					</p>
					<p class="d-flex">
						<span class="font-black">PPN</span>
						<span class="font-black">Rp{{number_format(0,0,".",",")}}</span>
					</p>
					<hr>
					<p class="d-flex total-price">
						<span class="font-black">Grand Total</span>
						<span class="font-black">Rp{{number_format($total_keranjang,0,".",",")}}</span>
					</p>
					@if($detail_toko->fitur_kirim==1)
						<p class="d-flex">
							<span class="font-red">*tidak termasuk ongkos kirim</span>
						</p>
					@endif
					@if($jumlah_produk>0)
	            		<p><a href="{{url('checkout')}}"class="btn btn-primary py-3 px-4">Buat Pesanan</a></p>
    				@endif
				</div>
			</div>
		</div>
	</div>
</section>

		
  

  <!-- loader -->
 <!--  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div> -->


<script src="{{ asset('customer/js/jquery.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('customer/js/popper.min.js') }}"></script>
<script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>

  <script>
  	// $('.form-hapus').on('submit', (event) => {
  		// event.preventDefault();
  		$(".hapus").click(function() 
  		{
  			event.preventDefault();
		    var id_button = $(this).attr('id');
		    var parts=id_button.toString().split(".");
            var id_produk=parts[1];
	        Swal.fire({
		        title: 'Yakin menghapus produk dari keranjang?',
		        icon: 'warning',
		        showCancelButton: true,
		        confirmButtonColor: '#44b839',
		        cancelButtonColor: '#d33',
		        cancelButtonText: 'Tidak',
		        confirmButtonText: 'Ya',
		        reverseButtons: true,
	      	}).then((result) => {
		        if (result.value) {
		        	var form = $('#form-hapus'+id_produk);
		          	form.trigger('submit');
		        	// var form= $(this).find("input[type=submit]:focus");
		        }
	      	})
		});
        
    // });
		$(document).ready(function(){
			// var total_produk = {!! json_encode($total_produk) !!};
		 //   $('#quantity-right-plus'+$i).click(function(e){
		        
		 //        for ($i = 0; $i < total_produk.size; $i++){
		 //        // Stop acting like a button
			//         e.preventDefault();
			//         // Get the field name
			//         var quantity = parseInt($('#quantity-toko['+$i+']').val());
			//         var max_quantity = $('#quantity-toko['+$i+']').attr('max');
			//         alert(quantity);
			//         // If is not undefined
			//             if(quantity<max_quantity){
			//             $('#quantity-toko['+$i+']').val(quantity + 1);
			//         }
		          
		 //            // Increment
		 //        }
		 //    });

		 //   	for ($i = 0; $i < total_produk.size; $i++){
			//      $('#quantity-left-minus'+$i).click(function(e){
			//         // Stop acting like a button
			//         e.preventDefault();
			//         // Get the field name
			//         var quantity = parseInt($('#quantity-toko['+$i+']').val());
			        
			//         // If is not undefined
			      
			//             // Increment
			//             if(quantity>1){
			//             $('#quantity-toko['+$i+']').val(quantity - 1);
			//             }
			//     });
			// }
		    
		});
	</script>

@endsection