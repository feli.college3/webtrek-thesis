@extends('layouts.pelanggan', ['title' => 'Detail Pesanan'])
@section('content')

<div class="hero-wrap" style="background-color: black;">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
      	<p class="breadcrumbs"><span>Detail</span></p>
        <h1 class="mb-0 bread">Pesanan</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section ftco-cart">
	<div class="container">
		@if($nota_jual->status_transaksi=='Dibatalkan')
			<div class="row">
				<div class="col-md-12">
					<p class="font-black"><b>Keterangan: {{$nota_jual->keterangan_pesanan}}</b></p>
				</div>
			</div>
		@endif

		<div class="row">
			<div class="col-md-12 ftco-animate">
				<div class="cart-list">
	  				<table class="table">
					    <thead class="thead-primary">
					      <tr class="text-center">
					        <th>&nbsp;</th>
					        <th>Produk</th>
					        <th class="harga-bro">Harga (Rp)</th>
					        <th>Jumlah</th>
					        <th>Subtotal (Rp)</th>
					        <th>&nbsp;</th>
					      </tr>
					    </thead>
					    <tbody>
					    	@if($produk_toko!=null)
						    	@for ($i = 0; $i < count($produk_toko); $i++)
							      <tr class="text-center">
							        <td class="image-prod"><div class="img" style="background-image:url(../images/{{$produk_toko[$i]->nama_foto}}.{{$produk_toko[$i]->ekstensi}});"></div></td>
							        
							        <td class="product-name">
							        	<a href="{{route('belanja.show', $produk_toko[$i]->idproduk)}}">{{$produk_toko[$i]->nama_produk}}</a>
							        	<p>{{$produk_toko[$i]->deskripsi}}</p>
							        </td>
							        
							        <td class="price">{{number_format($harga_jual[$i]->harga_jual,0,".",",")}}</td>
							        
							        <td class="quantity">
						             	<input type="text" id="quantity-toko[{{$i}}]" name="quantity-toko[{{$i}}]" class="form-control input-number" value="{{$jumlah_produk_toko[$i]->jumlah_produk}}" min="1" max="100" readonly>
						          	</td>
							        
							        <td class="total">{{number_format($subtotal_toko[$i]->subtotal,0,".",",")}}</td>
							      </tr>
							    @endfor
	                    	@endif
	                    	@if($produk_konsinyasi!=null)
							    @for ($i = 0; $i < count($produk_konsinyasi); $i++)
							      <tr class="text-center">
							        <td class="image-prod"><div class="img" style="background-image:url(../images/{{$produk_konsinyasi[$i]->nama_foto}}.{{$produk_konsinyasi[$i]->ekstensi}});"></div></td>
							        
							        <td class="product-name">
							        	<a href="{{route('belanja.show', $produk_konsinyasi[$i]->idproduk_konsinyasi)}}">{{$produk_konsinyasi[$i]->nama_produk}}</a>
							        	<p>{{$produk_konsinyasi[$i]->deskripsi}}</p>
							        </td>
							        
							        <td class="price">{{number_format($produk_konsinyasi[$i]->harga_jual,0,".",",")}}</td>
							        
							        <td class="quantity">
						             	<input type="text" id="quantity" name="jumlah" class="form-control input-number" value="{{$produk_konsinyasi[$i]->jumlah}}" min="1" max="{{$produk_konsinyasi[$i]->stok}}">
						          	</td>
							        
							        <td class="total">{{number_format($subtotal_konsinyasi[$i]->subtotal,0,".",",")}}</td>
							      </tr>
							    @endfor
							@endif
					    </tbody>
					  </table>
			  </div>
			</div>
		</div>
		<div class="row justify-content-end">
			<div class="col-lg-12 mt-5 cart-wrap ftco-animate">
				<div class="cart-total mb-3">
					<h3>TOTAL PESANAN</h3>
					<p class="d-flex">
						<span class="font-black">Total</span>
						<span>Rp{{number_format($total_keranjang,0,".",",")}}</span>
					</p>
					@if($detail_toko->fitur_kirim==1)
						<p class="d-flex">
							<span>Ongkos Kirim</span>
							<span>Rp{{number_format($nota_jual->ongkos_kirim,0,".",",")}}</span>
						</p>
					@endif
					<p class="d-flex">
						<span>Diskon</span>
						<span>Rp{{number_format($nota_jual->diskon,0,".",",")}}</span>
					</p>
					<p class="d-flex">
						<span>PPN</span>
						<span>Rp{{number_format($nota_jual->ppn,0,".",",")}}</span>
					</p>
					<hr>
					<p class="d-flex total-price">
						<span>Grand Total</span>
						<span>Rp{{number_format($nota_jual->grand_total+$nota_jual->ongkos_kirim,0,".",",")}}</span>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>

		
  

  <!-- loader -->
 <!--  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div> -->


<script src="{{ asset('customer/js/jquery.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('customer/js/popper.min.js') }}"></script>
<script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>

@endsection