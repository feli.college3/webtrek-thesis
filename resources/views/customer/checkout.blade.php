@extends('layouts.pelanggan', ['title' => 'Checkout'])
@section('content')
  
  <div class="hero-wrap" style="background-color: black;">
	  <div class="container">
	    <div class="row no-gutters slider-text align-items-center justify-content-center">
	      <div class="col-md-9 ftco-animate text-center">
	      	<p class="breadcrumbs">
	        <h1 class="mb-0 bread">CHECKOUT</h1>
	      </div>
	    </div>
	  </div>
  </div>

    <section class="ftco-section">
	  <form class="billing-form" method="POST" id="form-checkout" action="{{ url('checkout') }}" enctype="multipart/form-data">
  	  {{ csrf_field() }}
      <div class="container">
        <div class="row justify-content-center">
        	<div class="col-md-7">
	          <div class="">
	          	<div class="col-md-12 d-flex mb-5">
	          		<div class="cart-detail cart-total p-3 p-md-4">
	          			<h3>TOTAL KERANJANG</h3>
						<p class="d-flex">
							<span class="font-black">Total</span>
							<span class="font-black">Rp{{number_format($total_keranjang,0,".",",")}}</span>
							<input type="hidden" name="total_keranjang" id="total_keranjang">
						</p>
						<div class="hide" id="ongkos-kirim">
							<p class="d-flex">
								<span class="font-black">Ongkos Kirim</span>
								<span class="font-black" id="label-ongkir">Rp0</span>
								<input type="hidden" name="ongkir" id="ongkir">
							</p>
						</div>
						<p class="d-flex">
							<span class="font-black">Diskon</span>
							<span class="font-black">Rp{{number_format(0,0,".",",")}}</span>
						</p>
						<p class="d-flex">
							<span class="font-black">PPN</span>
							<span class="font-black">Rp{{number_format(0,0,".",",")}}</span>
						</p>
						<hr>
						<p class="d-flex total-price">
							<span class="font-black">Grand Total</span>
							<span class="font-black" id="label-grandtotal">Rp{{number_format($total_keranjang,0,".",",")}}</span>
							<input type="hidden" name="grand_total" id="grand_total">
						</p>
					</div>
	          	</div>
	          </div>
	        </div>
	        <div class="col-md-5">
		        <div class="">
					<div class="cart-total mb-3">
						<h3>METODE PEMBAYARAN(*)</h3>
						<div class="form-group box-checkout">
              @if($detail_toko->pembayaran_cod==0&&$detail_toko->pembayaran_transfer==0)
                <div class="col-md-12">
                  <label><input type="radio" name="metode" class="mr-2" value="COD" checked/>Cash on Delivery (COD)</label>
                </div>
              @else
  							@if($detail_toko->pembayaran_cod==1)
  								<div class="col-md-12">
  									<label><input type="radio" name="metode" class="mr-2" value="COD" checked/>Cash on Delivery (COD)</label>
  								</div>
  							@endif
  							@if($detail_toko->pembayaran_transfer==1)
  								<div class="col-md-12">
  									<label><input type="radio" name="metode" class="mr-2" value="Transfer" checked/>Transfer Bank</label>
  								</div>
  							@endif
              @endif
						</div>
					</div>
				</div>
				<div class="">
					<div class="cart-total mb-3">
						<h3>PENGIRIMAN(*)</h3>
						<div class="form-group" id="box-checkout">
							@if($detail_toko->fitur_kirim==1)
								<div class="col-md-12">
									<label><input type="radio" name="pengiriman" class="mr-2" value="1" checked onclick="kirim_pesanan()" />Kirim ke Alamat Tertera</label>
								</div>
							@endif
							<div class="col-md-12">
							   <label><input type="radio" name="pengiriman" class="mr-2" value="0" onclick="kirim_pesanan()" checked/>Ambil Sendiri</label>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
		<div class="row justify-content-center hide" id="data-pengiriman">
          <div class="col-xl-7 ftco-animate">
	          	<div class="row align-items-end">
	          		<div class="col-md-12">
	                <div class="form-group">
	                	<p class="font-red">(*) wajib diisi</p>
	                	<label for="firstname">Nama Pelanggan:</label>
	                	<label>{{$pelanggan->nama_pelanggan}}</label>
	                </div>
	              </div>
	            <div class="col-md-12">
	            	<div class="form-group">
	            		<label for="country">Alamat(*)</label><br>
	            		<h6 class="font-orange text-small"><b>Keterangan: Masukkan nama kabupaten/kota, contoh: Jl. ABC No. 1 Surabaya</b></h6>
	                  	<input type="text" name="alamat" id="alamat" class="form-control" value="{{$pelanggan->alamat_pelanggan}}">
	                </div>
	            </div>
	            <!-- <div class="col-md-12">
	            	<div class="form-group">
	            		<label for="postcodezip">Kode Pos(*)</label>
	                  	<input type="number" name="kodepos" class="form-control" value="60147">
	                </div>
	            </div> -->
	            <div class="col-md-12">
                <div class="form-group">
                	<label for="phone">No. Telepon(*)</label>
                  <input type="number" name="telepon" class="form-control" value="{{$pelanggan->telepon_pelanggan}}">
                </div>
                <div class="form-group">
                	<label for="phone">Keterangan Pengiriman (Misalkan: Sebelum pk. 4 sore)</label>
                	<textarea name="keterangan" class="form-control" maxlength="100">-</textarea>
                </div>
              </div>
            </div><br>
            <p onclick="panggil_ongkir('cek')"><a href="#"class="btn btn-primary py-3 px-4">Cek Ongkir</a></p>
		  </div>
			
          </div> <!-- .col-md-8 -->
          <br>
		<div class="row justify-content-center">
			<div class="checkbox">
			   <label><input type="checkbox" value="" class="mr-2" checked>Saya sudah membaca dan menerima syarat dan ketentuan yang berlaku</label>
			</div>
		</div>
        <input type="hidden" name="jarak" id="jarak">
		<div class="row justify-content-center">
			<p><button type="submit" class="btn btn-primary py-3 px-4">CHECKOUT</button></p>
        </div>
        </div>
      </div>
      <div id="map" style="width: 400px; height: 300px;" class="hide"></div> 
  	  </form><!-- END -->
    </section> <!-- .section -->
    
  


<script src="{{ asset('customer/js/jquery.min.js') }}"></script>
<script src="{{ asset('customer/js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ asset('customer/js/popper.min.js') }}"></script>
<script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyC2UlF6oXbQDWRWv4CPbphCK0U8sKlUzJE"></script>

<script>
	$(document).ready(function(){
	});
	window.onload = function() {
        kirim_pesanan();
	    var keranjang = {!! json_encode($total_keranjang) !!};
	    document.getElementById('total_keranjang').value = keranjang;
    };

    var detail_toko = {!! json_encode($detail_toko) !!};

    $('#form-checkout').on('submit', (event) => {
        event.preventDefault();
        panggil_ongkir('submit');
    });
    function panggil_ongkir(aksi){
        var radios = document.getElementsByName('pengiriman');
        var kirim=0;
        for (var i = 0, length = radios.length; i < length; i++) {
            if (radios[i].checked) {
            	kirim=radios[i].value;
                console.log(radios[i].value);
                break;
            }
        }
        if(kirim==1){
            getongkir(document.getElementById('alamat').value,aksi);
        }
        else{
        	if(aksi=='submit'){
        		var form = $('#form-checkout');
                form.trigger('submit');
        	}
        }
    }

    function getongkir(tujuan,aksi){
        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer();

        var myOptions = {
         zoom:7,
         mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        var map = new google.maps.Map(document.getElementById("map"), myOptions);
        directionsDisplay.setMap(map);
        var request = {
           origin: detail_toko['alamat_toko'], 
           destination: tujuan,
           travelMode: google.maps.DirectionsTravelMode.DRIVING
        };

        directionsService.route(request, function(response, status) {
          if (status == google.maps.DirectionsStatus.OK) {
            var distance= 0;
            for(i = 0; i < response.routes[0].legs.length; i++){
               distance += parseFloat(response.routes[0].legs[i].distance.value);
            }
            distance/=1000;
            distance=distance.toFixed(1);
            interval=detail_toko['interval_jarak'];
            ongkir_interval=detail_toko['ongkir_interval'];
            ongkir_perkm=ongkir_interval/interval;
            console.log('ongkir_perkm'+ongkir_perkm);
            ongkir=ongkir_perkm*distance;
            console.log('ongkir_awal'+ongkir);
            if(ongkir>0){
                ongkir=pembulatan_ribuan(ongkir);
            }
            document.getElementById('jarak').value = distance;
            document.getElementById('ongkir').value = ongkir;
            console.log('jarak'+distance);
            console.log('ongkir'+ongkir);
            document.getElementById('label-ongkir').innerHTML ="Rp"+separator(ongkir)+" ("+distance+" km)";

            var keranjang = {!! json_encode($total_keranjang) !!};
            var grand_total = keranjang+ongkir;
            document.getElementById('label-grandtotal').innerHTML ="Rp"+separator(grand_total);
            document.getElementById('grand_total').value = grand_total;

            directionsDisplay.setDirections(response);
          }
        });

        setTimeout(function(){
            var jarak=parseFloat(document.getElementById('jarak').value);
            var maksimal_jarak=parseFloat(detail_toko['maksimal_jarak']);
            if(jarak<=maksimal_jarak){
              if(aksi=='submit'){
              	setTimeout(function(){
	                var form = $('#form-checkout');
	                form.trigger('submit');
              	},1000);
              }
            }
            else{
              Swal.fire({
                title: 'Alamat Anda Berada di Luar Wilayah Pengiriman',
                text: "Jarak maksimal pengiriman adalah "+maksimal_jarak+" km. Silahkan ubah alamat dalam batas jarak pengiriman.",
                icon: 'warning',
                confirmButtonColor: '#44b839',
                confirmButtonText: 'OK',
              })
            }
        }, 1000);
    }

    function pembulatan_ribuan(jumlah){
        var jum=Math.ceil(jumlah);
        jum=jum.toString();
        var jum_sub=jum.substr(-2);
        jum_sub=parseInt(jum_sub,10);
        var length=jum.length;
        var jumlah_diambil=length-3;
        if (jum_sub==0){
            jum=Math.round(jum,-2);
        } else {
        	var dibulatkan=jum.substr(-3,1);
        	dibulatkan++;
        	var awalan=jum.substr(0,jumlah_diambil);
        	var hasil=awalan+dibulatkan+"00";
        	jum=parseInt(hasil,10);
        }
        return jum;
    }

    function separator(number) {
        var parts=number.toString().split(".");
        return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + (parts[1] ? "." + parts[1] : "");
    }

	function kirim_pesanan(){
        var radios = document.getElementsByName('pengiriman');
        var kirim=0;
		for (var i = 0, length = radios.length; i < length; i++) {
		    if (radios[i].checked) {
		    	kirim=radios[i].value;
		        console.log(radios[i].value);
		        break;
		    }
		}
		var element = document.getElementById("data-pengiriman");
		var e_ongkir= document.getElementById("ongkos-kirim");
		if(kirim==1){
  			element.classList.remove("hide");
  			e_ongkir.classList.remove("hide");
  			panggil_ongkir();
			// $('#data-pengiriman').toggleClass('hide');
		}
		else{
  			element.classList.add("hide");
  			e_ongkir.classList.add("hide");
  			var keranjang = {!! json_encode($total_keranjang) !!};
  			document.getElementById('ongkir').value=0;
            document.getElementById('grand_total').value = keranjang;
            document.getElementById('label-grandtotal').innerHTML ="Rp"+separator(keranjang);
			// $('#data-pengiriman').toggleClass('hide');
		}
  }	
</script>
    
@endsection