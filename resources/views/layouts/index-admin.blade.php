 <!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WEBTREK | {{$title}}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

  <!-- <link rel="stylesheet" href="../../../public/bower_components/bootstrap/dist/css/bootstrap.min.css"> -->
  <!-- Font Awesome -->

  <link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <!-- <link rel="stylesheet" href="../../../public/bower_components/font-awesome/css/font-awesome.min.css"> -->
  <!-- Ionicons -->
  <link href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">
  <!-- <link rel="stylesheet" href="../../../public/bower_components/Ionicons/css/ionicons.min.css"> -->
  <!-- DataTables -->
  <link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
  <!-- <link rel="stylesheet" href="../../../public/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"> -->
  <!-- Theme style -->
  <link href="{{ asset('dist/css/AdminLTE.min.css') }}" rel="stylesheet">
  <!-- <link rel="stylesheet" href="../../../public/dist/css/AdminLTE.min.css"> -->
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link href="{{ asset('dist/css/skins/_all-skins.min.css') }}" rel="stylesheet">

  <!-- <link href="{{ asset('sweetalert/src/sweetalert.css') }}" rel="stylesheet"> -->
  <link href="{{ asset('dist/css/webtrek-admin.css') }}" rel="stylesheet">


  <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">

  <!-- sweetalert -->
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script> -->


  <!-- <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"> -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-green sidebar-mini sidebar-collapse">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="{{url('/dashboard')}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>W</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>WEBTREK</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          @if($detail_toko!=null)
            <li class="view-shop">
              <a href="{{url('home')}}"><i class="fa fa-eye"></i>&nbsp;Lihat Toko</a>
            </li>
          @endif
          <li class="dropdown page-date">
              <span id="date">{{ Carbon\Carbon::now()->setTimezone('Asia/Jakarta')->format('d-m-Y') }}</span>
              <span id="time">{{ Carbon\Carbon::now()->setTimezone('Asia/Jakarta')->format('H:i:s') }}</span>
          </li>

          <!-- Notifications: style can be found in dropdown.less -->
          <!-- <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li> -->
                <!-- inner menu: contains the actual data -->
                <!-- <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li> -->
          <!-- Tasks: style can be found in dropdown.less -->
          

          <!-- User Account: style can be found in dropdown.less -->
          <!-- <li class="dropdown user user-menu">
            <div class="dropdown">
              <button class="dropbtn"></button>
              <div class="dropdown-content">
              <a href="{{url('logout')}}">Sign out</a>
              </div>
            </div>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">Felicia Brilliant Benaly</span>
            </a>
            <ul class="dropdown-menu">
               Menu Footer
              <li>
                <div>
                  <a href="{{url('logout')}}" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li> -->
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->

          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Halo, {{$username}}</a>
            <ul class="dropdown-menu sign-out">
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li class="sign-out"><!-- Task item -->
                    <a href="{{url('logout')}}" class="right-aligned">Sign out</a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>

        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
@if($detail_toko!=null)
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('pelanggan')}}"><i class="fa fa-circle-o text-red"></i>Pelanggan</a></li>
            <li><a href="{{url('supplier')}}"><i class="fa fa-circle-o text-yellow"></i>Supplier</a></li>
            <li><a href="{{url('kategori')}}"><i class="fa fa-circle-o text-aqua"></i>Kategori</a></li>
            
            @if($detail_toko->fitur_konsinyasi==1)
              <li class="treeview">
                <a href="#"><i class="fa fa-circle-o text-green"></i>Produk
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{url('produk')}}"><i class="fa fa-circle-o text-black"></i> Produk Toko</a></li>
                  <li><a href="{{url('produk-konsinyasi')}}"><i class="fa fa-circle-o text-gray"></i> Produk Konsinyasi</a></li>
                </ul>
              </li>
            @else
              <li><a href="{{url('produk')}}"><i class="fa fa-circle-o text-black"></i> Produk</a></li>
            @endif
            <!-- <li><a href="{{url('jabatan')}}"><i class="fa fa-circle-o text-white"></i>Jabatan</a></li> -->
            @if($role=='manajer')
              <li><a href="{{url('pegawai')}}"><i class="fa fa-circle-o text-blue"></i>Pegawai</a></li>
            @endif
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-sticky-note-o"></i> <span>Transaksi Online</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('transaksi', 'belum-diproses')}}"><i class="fa fa-circle-o text-red"></i>Belum Diproses</a></li>
            <li><a href="{{route('transaksi', 'diproses')}}"><i class="fa fa-circle-o text-yellow"></i>Sedang Diproses</a></li>
            <!-- <li><a href="{{route('transaksi', 'siap-dikirim')}}"><i class="fa fa-circle-o text-aqua"></i>Siap Dikirim</a></li>
            <li><a href="{{route('transaksi', 'dikirim')}}"><i class="fa fa-circle-o text-aqua"></i>Sedang Dikirim</a></li> -->
            <li><a href="{{route('transaksi', 'belum-diambil')}}"><i class="fa fa-circle-o text-green"></i>Belum Diambil</a></li>
            <li><a href="{{route('transaksi', 'dibatalkan')}}"><i class="fa fa-circle-o text-aqua"></i>Dibatalkan</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i> <span>Transaksi Offline</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('nota-jual.create')}}"><i class="fa fa-circle-o text-red"></i>Penjualan</a></li>
            @if($role=='manajer')
              <li><a href="{{route('nota-beli.create')}}"><i class="fa fa-circle-o text-orange"></i>Order Pembelian</a></li>
              <li><a href="{{route('nota-beli.post')}}"><i class="fa fa-circle-o text-green"></i>Penerimaan Produk</a></li>
            @endif
          </ul>
        </li>

        @if($role=='manajer')
          @if($detail_toko->fitur_retur==1)
            <li class="treeview">
              <a href="#">
                <i class="fa fa-share"></i> <span>Retur</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{url('retur-penjualan')}}"><i class="fa fa-circle-o text-red"></i>Penjualan</a></li>
                <li><a href="{{url('retur-pembelian')}}"><i class="fa fa-circle-o text-aqua"></i>Pembelian</a></li>
              </ul>
            </li>
          @endif
        @endif

        @if($detail_toko->fitur_kirim==1)
          <li class="treeview">
            <a href="#">
              <i class="fa fa-send"></i> <span>Pengiriman</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('atur-pengiriman')}}"><i class="fa fa-circle-o text-red"></i>Atur Pengiriman</a></li>
              <!-- <li><a href="{{route('transaksi', 'siap-dikirim')}}"><i class="fa fa-circle-o text-aqua"></i>Siap Dikirim</a></li> -->
              <li><a href="{{route('transaksi', 'dikirim')}}"><i class="fa fa-circle-o text-yellow"></i>Sedang Dikirim</a></li>
              <li><a href="{{route('selesai-dikirim')}}"><i class="fa fa-circle-o text-green"></i>Selesai Dikirim Kurir</a></li>
              <li><a href="{{route('pengiriman-hari-ini')}}"><i class="fa fa-circle-o text-blue"></i>Pengiriman Hari Ini</a></li>
            </ul>
          </li>
        @endif
        @if($role=='manajer')
          @if($detail_toko->fitur_konsinyasi==1)
            <li class="treeview">
              <a href="#">
                <i class="fa fa-cubes"></i> <span>Konsinyasi</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{url('beli-konsinyasi')}}"><i class="fa fa-circle-o text-red"></i>Order Penitipan</a></li>
                <li><a href="{{url('titip-konsinyasi-post')}}"><i class="fa fa-circle-o text-yellow"></i>Penerimaan Produk</a></li>
                <li><a href="{{url('kembali-konsinyasi')}}"><i class="fa fa-circle-o text-aqua"></i>Pengembalian</a></li>
              </ul>
            </li>
          @endif
        @endif
        @if($role=='manajer')
          <li class="treeview">
            <a href="#">
              <i class="fa fa-book"></i> <span>Laporan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('laporan-penjualan')}}"><i class="fa fa-circle-o text-red"></i>Penjualan</a></li>
              <li><a href="{{url('laporan-pembelian')}}"><i class="fa fa-circle-o text-yellow"></i>Pembelian</a></li>
              <li><a href="{{url('laporan-produk')}}"><i class="fa fa-circle-o text-green"></i>Produk</a></li>
              @if($detail_toko->fitur_konsinyasi==1)
                <li><a href="{{url('laporan-konsinyasi')}}"><i class="fa fa-circle-o text-aqua"></i>Konsinyasi</a></li>
              @endif
              <li><a href="{{url('laporan-labarugi')}}"><i class="fa fa-circle-o text-white"></i>Laba-Rugi Bulanan</a></li>
              <li><a href="{{url('laporan-labarugi-tahunan')}}"><i class="fa fa-circle-o text-gray"></i>Laba-Rugi Tahunan</a></li>
            </ul>
          </li>

          <li><a href="{{route('pengaturan-awal.edit',$detail_toko->nama_toko)}}"><i class="fa fa-cogs"></i> <span>Konfigurasi Sistem</span></a></li>
        @endif
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
@endif

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    @yield('content')
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.18
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- Ajax 3.4.1 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- jQuery 3 -->
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js') }}"></script>

<!-- <script src="{{ asset('sweetalert/src/sweetalert.js') }}"></script> -->
<!-- page script -->


<!-----------------------SCRIPT--------------------------------------->
<script>
  $(function () {
    $('#tabeldata').DataTable({
        "order": [],
        "columnDefs": [ {
        "targets"  : 'no-sort',
        "orderable": false,
        // "scrollX": true,
    }]
    })
    // $('#example2').DataTable({
    //   'paging'      : true,
    //   'lengthChange': false,
    //   'searching'   : false,
    //   'ordering'    : true,
    //   'info'        : true,
    //   'autoWidth'   : false
    // })
    // $('.select2').select2()
  })
  
  function showTime() {
      var indonesiaTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Jakarta"});
      var date = new Date(indonesiaTime);
      document.getElementById('time').innerHTML = date.toLocaleTimeString('en-US', { hour12: false });
  }
  setInterval(showTime, 1000);

  // $(function(){
  //   // Will only work if string in href matches with location
  // $('ul.nav a[href="'+ url +'"]').parent().addClass('active');

  // // Will also work for relative and absolute hrefs
  // $('ul.nav a').filter(function() {
  //     return this.href == url;
  // }).parent().addClass('active');
  //   })
</script>

    @yield('morescript')
<!-----------------------SCRIPT--------------------------------------->
</body>
</html>
