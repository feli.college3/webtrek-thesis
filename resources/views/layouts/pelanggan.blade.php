<!DOCTYPE html>
<html lang="en">
  <head>
    @if($detail_toko!=null)
      <title>{{$detail_toko->nama_toko}} | {{$title}}</title>
    @else
      <title>WEBTREK | {{$title}}</title>
    @endif
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('customer/css/open-iconic-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('customer/css/animate.css') }}">
    
    <link rel="stylesheet" href="{{ asset('customer/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('customer/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('customer/css/magnific-popup.css') }}">

    <link rel="stylesheet" href="{{ asset('customer/css/aos.css') }}">

    <link rel="stylesheet" href="{{ asset('customer/css/ionicons.min.css') }}">

    <link rel="stylesheet" href="{{ asset('customer/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('customer/css/jquery.timepicker.css') }}">

    
    <link rel="stylesheet" href="{{ asset('customer/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('customer/css/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('customer/css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ asset('customer/css/style.css') }}">

    <!-- sweetalert -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" /> -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <link href="{{ asset('dist/css/webtrek-customer.css') }}" rel="stylesheet">
  </head>
  <body class="goto-here">
	<!-- <div class="py-1 bg-primary">
    	<div class="container">
    		<div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
	    		<div class="col-lg-12 d-block">
		    		<div class="row d-flex">
		    			<div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
						    <span class="text">+ 1235 2355 98</span>
					    </div>
					    <div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
						    <span class="text">youremail@email.com</span>
					    </div>
					    <div class="col-md-5 pr-4 d-flex topper align-items-center text-lg-right">
						    <span class="text">3-5 Business days delivery &amp; Free Returns</span>
					    </div>
				    </div>
			    </div>
		    </div>
	  	</div>
    </div> -->
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
        @if($detail_toko!=null)
          <a class="navbar-brand" href="{{ url('home') }}">{{$detail_toko->nama_toko}}</a>
        @else
          <a class="navbar-brand" href="{{ url('home') }}">WEBTREK</a>
        @endif
	      
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
            @if(Auth::user())
              @if($role=='kasir'||$role=='manajer')
                <li class="nav-item"><a href="{{ url('dashboard') }}" class="nav-link"><b>ADMIN</b></a></li>
              @endif
            @endif
	          <li class="nav-item"><a href="{{ url('home') }}" class="nav-link">Home</a></li>
	          <li class="nav-item"><a href="{{ url('belanja') }}" class="nav-link">Belanja</a></li>

            @if(Auth::user())
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pesanan</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="{{route('pesanan', 'belum-bayar')}}">Belum Bayar
                    @if($belum_bayar>0)
                      <span class="floating-number">{{$belum_bayar}}</span>
                    @endif
                  </a>
                  <a class="dropdown-item" href="{{route('pesanan', 'diproses')}}">Diproses 
                    @if($diproses>0)
                    <span class="floating-number">{{$diproses}}</span>
                    @endif
                  </a>
                  @if($detail_toko->fitur_kirim==1)
                    <a class="dropdown-item" href="{{route('pesanan', 'dikirim')}}">Dikirim 
                      @if($dikirim>0)
                      <span class="floating-number">{{$dikirim}}</span>
                      @endif
                    </a>
                  @endif
                  <a class="dropdown-item" href="{{route('pesanan', 'siap-diambil')}}">Siap Ambil 
                    @if($siap_diambil>0)
                    <span class="floating-number">{{$siap_diambil}}</span>
                    @endif
                  </a>
                  <a class="dropdown-item" href="{{route('pesanan', 'selesai')}}">Selesai 
                    @if($selesai>0)
                    <span class="floating-number">{{$selesai}}</span>
                    @endif
                  </a>
                  <a class="dropdown-item" href="{{route('pesanan', 'dibatalkan')}}">Dibatalkan 
                    @if($dibatalkan>0)
                    <span class="floating-number">{{$dibatalkan}}</span>
                    @endif
                  </a>
                </div>
              </li>
            @endif

	          <!-- <li class="nav-item"><a href="#" class="nav-link">Tentang Kami</a></li> -->
	          <li class="nav-item cta cta-colored"><a href="{{ url('keranjang') }}" class="nav-link"><span class="icon-shopping_cart"></span>
              @if(Auth::user())
              [{{$total_produk}}]
            @else
              [0]
            @endif
            </a></li>
            
            @if(Auth::user())
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Halo, {{$username}}</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="{{ route('profile',$username) }}">Profile</a>
                  <a class="dropdown-item" href="{{ url('logout') }}" id="logout">Log Out</a>
                </div>
              </li>
            @else
              <li class="nav-item"><a href="{{ url('login') }}" class="nav-link">Login</a></li>
            @endif

	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->

    @yield('content')

    <footer class="ftco-footer ftco-section">
      <div class="container">
      	<div class="row">
      		<div class="mouse">
				<a href="#" class="mouse-icon">
					<div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
				</a>
			</div>
      	</div>
        <div class="row mb-5">
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
              @if($detail_toko!=null)
                <h2 class="ftco-heading-2">{{$detail_toko->nama_toko}}</h2>
                <p>{{$detail_toko->deskripsi_toko}}</p>
              @else
                <h2 class="ftco-heading-2">WEBTREK</h2>
                <p>Deskripsi Toko</p>
              @endif
              
              <!-- <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
              </ul> -->
            </div>
          </div>
          <div class="col-md">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Ingin Menghubungi Kami?</h2>
            	<div class="block-23 mb-3">
	              <ul>
                  @if($detail_toko!=null)
                    <li><span class="icon icon-map-marker"></span><span class="text">{{$detail_toko->alamat_toko}}</span></li>
                    <li><a href="#"><span class="icon icon-phone"></span><span class="text">{{$detail_toko->telepon_toko}}</span></a></li>
                  @else
                    <li><span class="icon icon-map-marker"></span><span class="text">Alamat Toko</span></li>
                  <li><a href="#"><span class="icon icon-phone"></span><span class="text">Telepon Toko</span></a></li>
                  @endif
	                
	                <!-- <li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@yourdomain.com</span></a></li> -->
	              </ul>
	            </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						  Copyright &copy;<script>document.write(new Date().getFullYear());</script> WEBTREK | Template using <a href="https://colorlib.com" target="_blank">Colorlib</a>
						  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="{{ asset('customer/js/jquery.min.js') }}"></script>
  <script src="{{ asset('customer/js/jquery-migrate-3.0.1.min.js') }}"></script>
  <script src="{{ asset('customer/js/popper.min.js') }}"></script>
  <script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('customer/js/jquery.easing.1.3.js') }}"></script>
  <script src="{{ asset('customer/js/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('customer/js/jquery.stellar.min.js') }}"></script>
  <script src="{{ asset('customer/js/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('customer/js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('customer/js/aos.js') }}"></script>
  <script src="{{ asset('customer/js/jquery.animateNumber.min.js') }}"></script>
  <script src="{{ asset('customer/js/bootstrap-datepicker.js') }}"></script>
  <script src="{{ asset('customer/js/scrollax.min.js') }}"></script>
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script> -->
  <!-- <script src="{{ asset('customer/js/google-map.js') }}"></script> -->
  <script src="{{ asset('customer/js/main.js') }}"></script>

  <script type="text/javascript">
    $('#logout').on('click', (event) => {
        event.preventDefault();
        Swal.fire({
        title: 'Anda yakin untuk log out?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Tidak',
        confirmButtonText: 'Ya, log out',
        reverseButtons: true,
      }).then((result) => {
        if (result.value) {
          window.location.href = "{{ route('logout')}}";
        }
      })
    });
  </script>