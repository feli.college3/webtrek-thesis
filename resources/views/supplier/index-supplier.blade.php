@extends('layouts.index-admin', ['title' => 'Supplier'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        DATA SUPPLIER
      </b></h1>
      @if($role=='manajer')
        <button class="btn btn-success btn-tambah"><a href="{{ route('supplier.create') }}" class="font-white">Tambah Supplier Baru</a></button>
      @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title"><b>Supplier</b></h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama Supplier</th>
                  <th>Alamat Supplier</th>
                  <th>Telepon Supplier</th>
                  <th>Nomor Rekening</th>
                  @if($role=='manajer')
                    @if($detail_toko->pembayaran_hutang==1)
                      <th>Total Piutang</th>
                    @endif
                  @endif
                  <th>Status</th>
                  @if($role=='manajer')
                    <th class="no-sort"></th>
                    <th class="no-sort"></th>
                  @endif
                </tr>
                </thead>
                <tbody>
                @if(!empty($list_supplier))
                  @foreach($list_supplier as $post)
                    @if($post->konsinyator == "0")
                      <tr>
                        <td>{{$post->idsupplier}}</td>
                        <td>{{$post->nama_supplier}}</td>
                        <td>{{$post->alamat_supplier}}</td>
                        <td>{{$post->telepon_supplier}}</td>
                        <td>{{$post->nomor_rekening}}</td>
                        @if($role=='manajer')
                          @if($detail_toko->pembayaran_hutang==1)
                            <td class="right-aligned">{{number_format($post->total_piutang,0,".",",")}}</td>
                          @endif
                        @endif
                        <td>{{$post['status']}}</td>
                        @if($role=='manajer')
                          <td class="center-aligned">
                            <button class="btn btn-warning"><a href="{{ route('supplier.edit', $post->idsupplier) }}" class="font-white">Edit</a></button>
                          </td>
                          <td class="center-aligned">
                            <form method="post" action="{{ route('supplier.destroy', $post->idsupplier) }}">
                              {{ method_field('delete')}}
                              {{ csrf_field() }}
                              <button class="btn btn-danger" type="submit" name="delete" onclick="return confirm('Anda yakin untuk menghapus supplier?')">Hapus</button>
                            </form>         
                          </td>
                        @endif
                      </tr>
                    @endif
                   @endforeach
                  @endif
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      @if($detail_toko->fitur_konsinyasi==1)
        @if(!empty($list_supplier))
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title"><b>Konsinyator</b></h3>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                  <table id="tabeldata" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>Nama Konsinyator</th>
                      <th>Alamat Konsinyator</th>
                      <th>Telepon Konsinyator</th>
                      <th>Nomor Rekening</th>
                      @if($role=='manajer')
                        @if($detail_toko->pembayaran_hutang==1)
                          <th>Total Piutang</th>
                        @endif
                      @endif
                      <th>Status</th>
                      @if($role=='manajer')
                        <th class="no-sort"></th>
                        <th class="no-sort"></th>
                      @endif
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($list_supplier as $post)
                        @if($post->konsinyator == "1")
                            <tr>
                              <td>{{$post->nama_supplier}}</td>
                              <td>{{$post->alamat_supplier}}</td>
                              <td>{{$post->telepon_supplier}}</td>
                              <td>{{$post->nomor_rekening}}</td>
                              @if($role=='manajer')
                                @if($detail_toko->pembayaran_hutang==1)
                                  <td class="right-aligned">{{number_format($post->total_piutang,0,".",",")}}</td>
                                @endif
                              @endif
                              <td>{{$post['status']}}</td>
                              @if($role=='manajer')
                                <td class="center-aligned">
                                  <button class="btn btn-warning"><a href="{{ route('supplier.edit', $post->idsupplier) }}" class="font-white">Edit</a></button>
                                </td>
                                <td class="center-aligned">
                                  <form method="post" action="{{ route('supplier.destroy', $post->idsupplier) }}">
                                    {{ method_field('delete')}}
                                    {{ csrf_field() }}
                                    <button class="btn btn-danger" type="submit" name="delete" onclick="return confirm('Are you sure?')">Hapus</button>
                                  </form>         
                                </td>
                              @endif
                            </tr>
                          @endif
                        @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->

            </div>
            <!-- /.col -->
          </div>
        @endif
      @endif
    </section>

@endsection
@section('morescript')
@if (session('status'))
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection