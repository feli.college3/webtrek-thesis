@extends('layouts.index-admin', ['title' => 'Supplier'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Ubah Data Supplier
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{ route('supplier.update', $id) }}" enctype="multipart/form-data">
                {{ method_field("PUT") }}
                {{ csrf_field() }}
                <!-- text input -->
                <div class="form-group">
                  <label>Nama Supplier(*)</label>
                  <input type="text" name="nama" class="form-control" value="{{$sup->nama_supplier}}" required autofocus/>
                  @if ($errors->has('Nama Supplier(*)'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nama') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Alamat Supplier</label>
                  <input type="text" name="alamat" class="form-control" value="{{$sup->alamat_supplier}}"/>
                  @if ($errors->has('Nama Supplier(*)'))
                    <span class="help-block">
                        <strong>{{ $errors->first('alamat') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Telepon Supplier</label>
                  <input type="number" name="telepon" class="form-control" value="{{$sup->telepon_supplier}}"/>
                  @if ($errors->has('Telepon Supplier(*)'))
                    <span class="help-block">
                        <strong>{{ $errors->first('telepon') }}</strong>
                    </span>
                  @endif
                </div>
                @if($detail_toko->fitur_konsinyasi==1)
                  <div class="form-group">
                    <label>Konsinyator(*)</label>
                      <select id="status" name="konsinyator" class="form-control" required>
                        @if($sup->konsinyator=="0")
                            <option value="0" selected>Tidak</option>
                            <option value="1">Ya</option>
                        @else
                            <option value="0">Tidak</option>
                            <option value="1" selected>Ya</option>
                        @endif
                      </select>
                      @if ($errors->has('status'))
                        <span class="help-block">
                          <strong>{{ $errors->first('status') }}</strong>
                        </span>
                      @endif
                  </div>
                @endif
                <div class="form-group">
                  <label>Nomor Rekening</label>
                  <input type="number" name="rekening" class="form-control" value="{{$sup->nomor_rekening}}"/>
                  @if ($errors->has('Batas Hutang(*)'))
                    <span class="help-block">
                        <strong>{{ $errors->first('rekening') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Status(*)</label>
                    <select id="status" name="status" class="form-control" required>
                      @if($sup->status=="Aktif")
                          <option value="Aktif" selected>Aktif</option>
                          <option value="Non-aktif">Non-aktif</option>
                      @else
                          <option value="Aktif">Aktif</option>
                          <option value="Non-aktif" selected>Non-aktif</option>
                      @endif
                    </select>
                    @if ($errors->has('status'))
                      <span class="help-block">
                        <strong>{{ $errors->first('status') }}</strong>
                      </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
  <script>
    alert('{{ session('status') }}')
  </script>
@endif
@endsection