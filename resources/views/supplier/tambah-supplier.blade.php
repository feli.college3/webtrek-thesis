@extends('layouts.index-admin', ['title' => 'Supplier'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        Tambah Supplier Baru
      </b></h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-warning">
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" method="POST" action="{{ url('supplier') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <!-- text input -->
                <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                  <label>Nama Supplier(*)</label>
                  <input type="text" name="nama" class="form-control" placeholder="Nama Supplier" required autofocus/>
                  @if ($errors->has('nama'))
                    <span class="help-block">
                      <strong>{{ $errors->first('nama') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
                  <label>Alamat(*)</label><br>
                  <label class="orange-text"><b>Keterangan: Masukkan nama kabupaten/kota, contoh: Jl. ABC No. 1 Surabaya</b></label>
                  <input type="text" name="alamat" class="form-control" placeholder="Jl..." value="-" required/>
                  @if ($errors->has('alamat'))
                    <span class="help-block">
                      <strong>{{ $errors->first('alamat') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group{{ $errors->has('telepon') ? ' has-error' : '' }}">
                  <label>Telepon(*)</label>
                  <input type="number" name="telepon" class="form-control" placeholder="0311234567" value="0" required/>
                  @if ($errors->has('telepon'))
                    <span class="help-block">
                      <strong>{{ $errors->first('telepon') }}</strong>
                    </span>
                  @endif
                </div>
                @if($detail_toko->fitur_konsinyasi==1)
                  <div class="form-group{{ $errors->has('konsinyator') ? ' has-error' : '' }}">
                    <label>Konsinyator(*)</label>
                    <select id="konsinyator" name="konsinyator" class="form-control" required>
                      <option value="0">Tidak</option>
                      <option value="1">Ya</option>
                    </select>
                    @if ($errors->has('konsinyator'))
                      <span class="help-block">
                        <strong>{{ $errors->first('konsinyator') }}</strong>
                      </span>
                    @endif
                  </div>
                @endif
                <div class="form-group{{ $errors->has('rekening') ? ' has-error' : '' }}">
                  <label>Nomor Rekening</label>
                  <input type="number" name="rekening" class="form-control" placeholder="0880000000"/>
                  @if ($errors->has('rekening'))
                    <span class="help-block">
                      <strong>{{ $errors->first('rekening') }}</strong>
                    </span>
                  @endif
                </div>
                <p><b>(*) wajib diisi</b></p>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection