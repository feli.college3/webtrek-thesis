<!DOCTYPE html>
<html>
<head>
	<title>Laporan Konsinyasi {{$detail_toko->nama_toko}}</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
		@page { margin: 50px 25px; }
	    footer { position: fixed; bottom: -20px; left: 0px; right: 0px; height: 20px; font-size: 15px }
	    p { page-break-after: always; }
	    p:last-child { page-break-after: never; }
	    .center-aligned{
	    	text-align: center;
	    }
	    .right-aligned{
	    	text-align: right;
	    }
	</style>
	<center>
		<h5>Laporan Konsinyasi {{$detail_toko->nama_toko}}</h5>
		<h6>Periode {{$list_bulan[$bulan-1]}} {{$tahun}}</h6>
		<hr>
		@if($keuntungan>=0)
	        <h6><b>Total Keuntungan: Rp{{number_format($keuntungan,0,".",",")}}</b></h6>
	    @else
	        <h6><b>Total Kerugian: Rp{{number_format($keuntungan*-1,0,".",",")}}</b></h6>
	    @endif
	</center>
 	

	<table class='table table-bordered'>
		<thead>
			<tr>
				<th class="no-sort center-aligned">ID Produk</th>
                <th>Nama Produk</th>
                <th class="right-aligned">Harga Beli</th>
                <th class="right-aligned">Jumlah Beli</th>
                <th class="right-aligned">Harga Jual</th>
                <th class="right-aligned">Jumlah Jual</th>
                <th class="right-aligned">Keuntungan/Kerugian</th>
			</tr>
		</thead>
		<tbody>
			@if(count($list_nota)>0)
                @foreach($list_nota as $post)
                    <tr>
                        <td class="center-aligned">{{$post['idproduk']}}</td>
                        <td>{{$post['nama_produk']}}</td>
                        <td class="right-aligned">{{number_format($post['harga_beli'],0,".",",")}}</td>
                        <td class="right-aligned">{{number_format($post['jumlah_beli'],0,".",",")}}</td>
                        <td class="right-aligned">{{number_format($post['harga_jual'],0,".",",")}}</td>
                        <td class="right-aligned">{{number_format($post['jumlah_terjual'],0,".",",")}}</td>
                        <td class="right-aligned">{{number_format($post['keuntungan'],0,".",",")}}</td>
                    </tr>
                @endforeach
            @else
                <tr><td class="center-aligned" colspan="8">Tidak ada konsinyasi pada periode ini</td></tr>
            @endif 
		</tbody>
	</table>
 	<footer>Dibuat oleh <b>WEBTREK</b></footer>
</body>
</html>