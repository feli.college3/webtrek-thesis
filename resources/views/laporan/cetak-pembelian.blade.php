<!DOCTYPE html>
<html>
<head>
	<title>Laporan Pembelian {{$detail_toko->nama_toko}}</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
		@page { margin: 50px 25px; }
	    footer { position: fixed; bottom: -20px; left: 0px; right: 0px; height: 20px; font-size: 15px }
	    p { page-break-after: always; }
	    p:last-child { page-break-after: never; }
	    .center-aligned{
	    	text-align: center;
	    }
	    .right-aligned{
	    	text-align: right;
	    }
	</style>
	<center>
		<h5>Laporan Pembelian {{$detail_toko->nama_toko}}</h5>
		<h6>Periode {{$list_bulan[$bulan-1]}} {{$tahun}}</h6>
		@if($detail_toko->pembayaran_hutang==1)
	        <h6>Jenis: {{$jenis}}</h6>
	    @endif
		<hr>
		<h6>Total Pembelian: Rp{{number_format($total_beli,0,".",",")}}</h6>
		  @if($detail_toko->pembayaran_hutang==1)
			  @if($jenis!='Lunas')
	          <h6>Tanpa Piutang: Rp{{number_format($tanpa_hutang,0,".",",")}}</h6>
	        @endif
		  @endif
	</center>
 	

	<table class='table table-bordered'>
		<thead>
			<tr>
				<th class="center-aligned">No</th>
				<th>No. Nota</th>
                <th class="no-sort">Supplier</th>
                <th>Tanggal Dipesan</th>
                <th class="right-aligned">Grand Total</th>
                <th class="no-sort">Metode Pembayaran</th>
                @if($detail_toko->pembayaran_hutang==1)
                    @if($jenis=='Semua')
                    	<th class="no-sort">Status Pembayaran</th>
                    @endif
                    @if($jenis!='Lunas')
	                    <th>Tanggal Jatuh Tempo</th>
	                    <th class="right-aligned">Sisa Hutang</th>
                    @endif
                @endif
                <th class="no-sort">Status</th>
			</tr>
		</thead>
		<tbody>
			@if(count($list_nota_beli)>0)
				@php $i=1 @endphp
                @foreach($list_nota_beli as $post)
                    <tr>
                    	<td class="center-aligned">{{ $i++ }}</td>
                        <td>{{$post->no_nota_beli}}</td>
                        <td>{{$post->nama_supplier}}</td>
                        <td>{{date('d-m-Y', strtotime($post->tanggal_dipesan))}}</td>
                        <td class="right-aligned">{{number_format($post->grand_total,0,".",",")}}</td>
                        <td>{{$post->metode_pembayaran}}</td>
                        @if($detail_toko->pembayaran_hutang==1)
                          @if($jenis=='Semua')
                            <td>{{$post->status_pembayaran}}</td>
                          @endif
                          @if($jenis!='Lunas')
                            @if($post->tanggal_jatuh_tempo!=null)
                              <td>{{date('d-m-Y', strtotime($post->tanggal_jatuh_tempo))}}</td>
                            @else
                              <td>-</td>
                            @endif
                            <td class="right-aligned">{{number_format($post->sisa_hutang,0,".",",")}}</td>
                          @endif
                          
                        @endif
                        <td>{{$post->status_transaksi}}</td>
                    </tr>
                @endforeach
            @else
              @if($detail_toko->pembayaran_hutang==1)
                <tr><td class="center-aligned" colspan="10">Tidak ada transaksi pada periode ini</td></tr>
              @else
                <tr><td class="center-aligned" colspan="7">Tidak ada transaksi pada periode ini</td></tr>
              @endif
            @endif 
		</tbody>
	</table>
 	<footer>Dibuat oleh <b>WEBTREK</b></footer>
</body>
</html>