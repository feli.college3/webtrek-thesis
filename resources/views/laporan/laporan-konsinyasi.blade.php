@extends('layouts.index-admin', ['title' => 'Laporan Konsinyasi'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        LAPORAN KONSINYASI
      </b></h1>
        <button class="btn btn-success btn-tambah"><a href="{{ route('cetak-konsinyasi',[$tahun,$bulan]) }}" class="font-white">Download/Cetak</a></button>
      <h4><b>Periode: {{$list_bulan[$bulan-1]}} {{$tahun}}</b></h4>
      <hr>
      @if($keuntungan>=0)
        <h4><b>Total Keuntungan: Rp{{number_format($keuntungan,0,".",",")}}</b></h4>
      @else
        <h4><b>Total Kerugian: Rp{{number_format($keuntungan*-1,0,".",",")}}</b></h4>
      @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- <div class="box-header">
              
            </div> -->
            <!-- /.box-header -->
            
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="no-sort center-aligned">ID Produk</th>
                  <th>Nama Produk</th>
                  <th class="right-aligned">Harga Beli</th>
                  <th class="right-aligned">Jumlah Beli</th>
                  <th class="right-aligned">Harga Jual</th>
                  <th class="right-aligned">Jumlah Jual</th>
                  <th class="right-aligned">Keuntungan/Kerugian</th>
                </tr>
                </thead>
                <tbody>
                @if(count($list_nota)>0)
                    @foreach($list_nota as $post)
                        <tr>
                            <td class="center-aligned">{{$post['idproduk']}}</td>
                            <td>{{$post['nama_produk']}}</td>
                            <td class="right-aligned">{{number_format($post['harga_beli'],0,".",",")}}</td>
                            <td class="right-aligned">{{number_format($post['jumlah_beli'],0,".",",")}}</td>
                            <td class="right-aligned">{{number_format($post['harga_jual'],0,".",",")}}</td>
                            <td class="right-aligned">{{number_format($post['jumlah_terjual'],0,".",",")}}</td>
                            <td class="right-aligned">{{number_format($post['keuntungan'],0,".",",")}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr><td class="center-aligned" colspan="8">Tidak ada konsinyasi pada periode ini</td></tr>
                @endif 
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection