@extends('layouts.index-admin', ['title' => 'Laporan Laba Rugi'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        GRAFIK LABA RUGI TAHUNAN
      </b></h1>
      <h1><b>(dalam IDR)</b></h1>
      <h4><b>Tahun {{$tahun}}</b></h4>
      <hr>
      <h5 class="text-orange"><b>Petunjuk: Klik label untuk menampilkan/menghilangkan kategori</b></h5>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- <div class="box-header">
              
            </div> -->
            <!-- /.box-header -->
            
            <div class="box-body">
              <div class="container">
                {!! $chart->container() !!}
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
  {!! $chart->script() !!}
@endsection