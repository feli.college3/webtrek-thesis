@extends('layouts.index-admin', ['title' => 'Laporan Penjualan'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        LAPORAN PENJUALAN
      </b></h1>
        <button class="btn btn-success btn-tambah"><a href="{{ route('cetak-penjualan',[$tahun,$bulan,$jenis]) }}" class="font-white">Download/Cetak</a></button>
      <h4><b>Periode: {{$list_bulan[$bulan-1]}} {{$tahun}}</b></h4>
      @if($detail_toko->pembayaran_piutang==1)
        <h4><b>Jenis: {{$jenis}}</b></h4>
      @endif
      <hr>
      <h4><b>Hasil Penjualan: Rp{{number_format($total_jual,0,".",",")}}</b></h4>
      @if($detail_toko->pembayaran_piutang==1)
        @if($jenis!='Lunas')
          <h4><b>Tanpa Piutang: Rp{{number_format($tanpa_piutang,0,".",",")}}</b></h4>
        @endif
      @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- <div class="box-header">
              
            </div> -->
            <!-- /.box-header -->
            
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No. Nota</th>
                  <th class="no-sort">Pelanggan</th>
                  <th>Tanggal Transaksi</th>
                  <th class="no-sort">Metode Pembayaran</th>
                  <th class="right-aligned">Grand Total (Rp)</th>
                  @if($detail_toko->pembayaran_piutang==1)
                    @if($jenis=='Semua')
                      <th class="no-sort">Status Pembayaran</th>
                    @endif
                    @if($jenis!='Lunas')
                      <th>Tanggal Jatuh Tempo</th>
                      <th class="right-aligned">Sisa Piutang (Rp)</th>
                    @endif
                  @endif
                </tr>
                </thead>
                <tbody>
                @if(count($list_nota_jual)>0)
                    @foreach($list_nota_jual as $post)
                        <tr>
                            <td><a href="{{route('nota-jual.show', $post->no_nota_jual)}}">{{$post->no_nota_jual}}</a></td>
                            <td>{{$post->nama_pelanggan}}</td>
                            <td>{{date('d-m-Y', strtotime($post->tanggal))}}</td>
                            <td>{{$post->metode_pembayaran}}</td>
                            <td class="right-aligned">{{number_format($post->grand_total,0,".",",")}}</td>
                            @if($detail_toko->pembayaran_piutang==1)
                              @if($jenis=='Semua')
                                <td><a href="{{route('histori-pembayaran-penjualan.show', $post->no_nota_jual)}}">{{$post->status_pembayaran}}</a></td>
                              @endif
                              @if($jenis!='Lunas')
                                @if($post->tanggal_jatuh_tempo!=null)
                                  <td>{{date('d-m-Y', strtotime($post->tanggal_jatuh_tempo))}}</td>
                                @else
                                  <td>-</td>
                                @endif
                                <td class="right-aligned">{{number_format($post->sisa_piutang,0,".",",")}}</td>
                              @endif
                            @endif
                        </tr>
                    @endforeach
                @else
                  @if($detail_toko->pembayaran_piutang==1)
                    <tr><td class="center-aligned" colspan="8">Tidak ada transaksi pada periode ini</td></tr>
                  @else
                    <tr><td class="center-aligned" colspan="5">Tidak ada transaksi pada periode ini</td></tr>
                  @endif
                @endif 
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection