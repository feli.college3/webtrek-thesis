@extends('layouts.index-admin', ['title' => 'Laporan Pembelian'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        LAPORAN PEMBELIAN
      </b></h1>
        <button class="btn btn-success btn-tambah"><a href="{{ route('cetak-pembelian',[$tahun,$bulan,$jenis]) }}" class="font-white">Download/Cetak</a></button>
      <h4><b>Periode: {{$list_bulan[$bulan-1]}} {{$tahun}}</b></h4>
      @if($detail_toko->pembayaran_hutang==1)
        <h4><b>Jenis: {{$jenis}}</b></h4>
      @endif
      <hr>
      <h4><b>Total Pembelian: Rp{{number_format($total_beli,0,".",",")}}</b></h4>
      @if($detail_toko->pembayaran_hutang==1)
        @if($jenis!='Lunas')
          <h4><b>Tanpa Hutang: Rp{{number_format($tanpa_hutang,0,".",",")}}</b></h4>
        @endif
      @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- <div class="box-header">
              
            </div> -->
            <!-- /.box-header -->
            
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No. Nota</th>
                  <th class="no-sort">Supplier</th>
                  <th>Tanggal Dipesan</th>
                  <th class="right-aligned">Grand Total</th>
                  <th class="no-sort">Metode Pembayaran</th>
                  @if($detail_toko->pembayaran_hutang==1)
                    @if($jenis=='Semua')
                      <th class="no-sort">Status Pembayaran</th>
                    @endif
                    @if($jenis!='Lunas')
                      <th>Tanggal Jatuh Tempo</th>
                      <th class="right-aligned">Sisa Hutang</th>
                    @endif
                  @endif
                  <th class="no-sort">Status</th>
                </tr>
                </thead>
                <tbody>
                @if(count($list_nota_beli)>0)
                    @foreach($list_nota_beli as $post)
                        <tr>
                            <td><a href="{{route('nota-beli.show', $post->no_nota_beli)}}">{{$post->no_nota_beli}}</a></td>
                            <td>{{$post->nama_supplier}}</td>
                            <td>{{date('d-m-Y', strtotime($post->tanggal_dipesan))}}</td>
                            <td class="right-aligned">{{number_format($post->grand_total,0,".",",")}}</td>
                            <td>{{$post->metode_pembayaran}}</td>
                            @if($detail_toko->pembayaran_hutang==1)
                              @if($jenis=='Semua')
                                <td><a href="{{route('histori-pembayaran-pembelian.show', $post->no_nota_beli)}}">{{$post->status_pembayaran}}</a></td>
                              @endif
                              @if($jenis!='Lunas')
                                @if($post->tanggal_jatuh_tempo!=null)
                                  <td>{{date('d-m-Y', strtotime($post->tanggal_jatuh_tempo))}}</td>
                                @else
                                  <td>-</td>
                                @endif
                                <td class="right-aligned">{{number_format($post->sisa_hutang,0,".",",")}}</td>
                              @endif
                              
                            @endif
                            <td>{{$post->status_transaksi}}</td>
                        </tr>
                    @endforeach
                @else
                  @if($detail_toko->pembayaran_hutang==1)
                    <tr><td class="center-aligned" colspan="9">Tidak ada transaksi pada periode ini</td></tr>
                  @else
                    <tr><td class="center-aligned" colspan="6">Tidak ada transaksi pada periode ini</td></tr>
                  @endif
                @endif 
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection