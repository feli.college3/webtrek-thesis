@extends('layouts.index-admin', ['title' => 'Laporan Pembelian'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        LAPORAN KELUAR MASUK PRODUK
      </b></h1>
        <button class="btn btn-success btn-tambah"><a href="{{ route('cetak-produk',[$tahun,$bulan]) }}" class="font-white">Download/Cetak</a></button>
      <h4><b>Periode: {{$list_bulan[$bulan-1]}} {{$tahun}}</b></h4>
      <hr>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- <div class="box-header">
              
            </div> -->
            <!-- /.box-header -->
            
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th class="center-aligned">No</th>
                  <th class="no-sort">ID Produk</th>
                  <th>Nama Produk</th>
                  <th class="right-aligned">Total Dibeli</th>
                  <th class="right-aligned">Total Dijual</th>
                  <th class="right-aligned">Total Masuk (Penyesuaian)</th>
                  <th class="right-aligned">Total Keluar (Penyesuaian)</th>
                </tr>
                </thead>
                <tbody>
                  @for ($i = 0; $i < count($list_jual); $i++)
                    @php $index=$i+1; @endphp
                    <tr>
                        <td class="center-aligned">{{$index}}</td>
                        <td>{{$list_jual[$i]['idproduk']}}</td>
                        <td>{{$list_jual[$i]['nama_produk']}}</td>
                        <td class="right-aligned">{{$list_beli[$i]['jumlah']}}</td>
                        <td class="right-aligned">{{$list_jual[$i]['jumlah']}}</td>
                        <td class="right-aligned">{{$penyesuaian[$i]['jumlah_masuk']}}</td>
                        <td class="right-aligned">{{$penyesuaian[$i]['jumlah_keluar']}}</td>
                    </tr>
                  @endfor
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection