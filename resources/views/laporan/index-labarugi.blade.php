@extends('layouts.index-admin', ['title' => 'Laporan Laba Rugi'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        LAPORAN LABA RUGI
      </b></h1>
        <!-- <button class="btn btn-success btn-tambah"><a href="{{ route('nota-beli.create') }}" class="font-white">Buat Nota Jual</a></button> -->
      <h4><b>Pilih Periode</b></h4>
      <form role="form" method="POST" action="{{url('laporan-labarugi')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="col-xs-3">
          <select id="bulan" name="bulan" class="form-control" required>
            <option disabled selected value>Pilih Bulan</option>
            @for ($i = 0; $i < count($bulan); $i++)
            <option value="{{$i+1}}">{{$bulan[$i]}}</option>
            @endfor
          </select>
        </div>
        <div class="col-xs-3">
          <select id="tahun" name="tahun" class="form-control" required>
            <option disabled selected value>Pilih Tahun</option>
            @foreach($tahun as $thn)
            <option value="{{$thn}}">{{$thn}}</option>
            @endforeach
          </select>
        </div>
        <div class="col-xs-4">
          <button class="btn btn-success col-xs-4" type="submit">Lihat</button>
        </div>
        <br><br>
      </form>
    </section>


@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection