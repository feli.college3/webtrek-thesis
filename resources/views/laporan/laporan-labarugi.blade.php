@extends('layouts.index-admin', ['title' => 'Laporan Laba Rugi'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        LAPORAN LABA RUGI
      </b></h1>
        <button class="btn btn-success btn-tambah"><a href="{{ route('cetak-labarugi',[$tahun,$bulan]) }}" class="font-white">Download/Cetak</a></button>
      <h4><b>Periode: {{$list_bulan[$bulan-1]}} {{$tahun}}</b></h4>
      <hr>
      <h4><b>Total Pembelian: Rp{{number_format($total_pembelian,0,".",",")}}</b></h4>
      <h4><b>Total Penjualan: Rp{{number_format($total_penjualan,0,".",",")}}</b></h4>
      @if($keuntungan>=0)
        <h4 class="green"><b>Total Keuntungan: Rp{{number_format($keuntungan,0,".",",")}}</b></h4>
      @else
        <h4 class="red"><b>Total Kerugian: Rp{{number_format($keuntungan*-1,0,".",",")}}</b></h4>
      @endif
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- <div class="box-header">
              
            </div> -->
            <!-- /.box-header -->
            
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th rowspan="2" class="vertical-middle no-sort center-aligned">ID Produk</th>
                    <th rowspan="2" class="vertical-middle">Nama Produk</th>
                    <!-- <th rowspan="2" class="vertical-middle right-aligned">Jumlah Beli (Rp)</th> -->
                    <th rowspan="2" class="vertical-middle right-aligned">Total Pembelian</th>
                    <!-- <th rowspan="2" class="vertical-middle right-aligned">Jumlah Jual</th> -->
                    <th rowspan="2" class="vertical-middle right-aligned">Total Penjualan (Rp)</th>
                    <th colspan="2" class="vertical-middle center-aligned">Penyesuaian</th>
                    <th rowspan="2" class="vertical-middle right-aligned">Keuntungan/Kerugian (Rp)</th>
                  </tr>
                  <tr>
                    <!-- <th class="vertical-middle right-aligned">Penambahan</th> -->
                    <th class="vertical-middle right-aligned">Penambahan Stok (Rp)</th>
                    <!-- <th class="vertical-middle right-aligned">Pengeluaran</th> -->
                    <th class="vertical-middle right-aligned">Pengeluaran Stok (Rp)</th>
                  </tr>
                </thead>
                <tbody>
                  @if(count($all_list)>0)
                    @foreach($all_list as $post)
                        <tr>
                            <td class="center-aligned">{{$post['idproduk']}}</td>
                            <td>{{$post['nama_produk']}}</td>
                            <!-- <td class="right-aligned">{{number_format($post['jumlah_beli'],0,".",",")}}</td> -->
                            <td class="right-aligned">{{number_format($post['subtotal_beli'],0,".",",")}}</td>
                            <!-- <td class="right-aligned">{{number_format($post['jumlah_jual'],0,".",",")}}</td> -->
                            <td class="right-aligned">{{number_format($post['subtotal_jual'],0,".",",")}}</td>
                            <!-- <td class="right-aligned">{{number_format($post['jumlah_masuk'],0,".",",")}}</td> -->
                            <td class="right-aligned">{{number_format($post['subtotal_masuk'],0,".",",")}}</td>
                            <!-- <td class="right-aligned">{{number_format($post['jumlah_keluar'],0,".",",")}}</td> -->
                            <td class="right-aligned">{{number_format($post['subtotal_keluar'],0,".",",")}}</td>
                            <td class="right-aligned">{{number_format($post['keuntungan'],0,".",",")}}</td>
                        </tr>
                    @endforeach
                  @else
                    <tr><td class="center-aligned" colspan="11">Tidak ada transaksi pada periode ini</td></tr>
                  @endif 
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection