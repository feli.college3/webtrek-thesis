@extends('layouts.index-admin', ['title' => 'Laporan Penjualan'])
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><b>
        LAPORAN PENJUALAN
      </b></h1>
        <!-- <button class="btn btn-success btn-tambah"><a href="{{ route('nota-jual.create') }}" class="font-white">Buat Nota Jual</a></button> -->
      <form role="form" method="POST" action="{{url('laporan-penjualan')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        @if($detail_toko->pembayaran_piutang==1)
          <div class="col-xs-6">
            <h4><b>Pilih Periode</b></h4>
          </div>
          <div class="col-xs-6">
            <h4><b>Pilih Jenis</b></h4>
          </div>
        @else
          <div class="col-xs-12">
            <h4><b>Pilih Periode</b></h4>
          </div>
        @endif

        <div class="col-xs-3">
          <select id="bulan" name="bulan" class="form-control" required>
            <option disabled selected value>Pilih Bulan</option>
            @for ($i = 0; $i < count($bulan); $i++)
            <option value="{{$i+1}}">{{$bulan[$i]}}</option>
            @endfor
          </select>
        </div>
        <div class="col-xs-3">
          <select id="tahun" name="tahun" class="form-control" required>
            <option disabled selected value>Pilih Tahun</option>
            @foreach($tahun as $thn)
            <option value="{{$thn}}">{{$thn}}</option>
            @endforeach
          </select>
        </div>
        @if($detail_toko->pembayaran_piutang==1)
          <div class="col-xs-3">
            <select id="jenis" name="jenis" class="form-control" required>
              <option disabled selected value>Pilih Jenis</option>
              <option value="Semua">Semua</option>
              <option value="Belum Lunas">Belum Lunas</option>
              <option value="Jatuh Tempo">Jatuh Tempo</option>
              <option value="Lunas">Sudah Lunas</option>
            </select>
          </div>
        @endif
        <div class="col-xs-3">
          <button class="btn btn-success col-xs-4" type="submit">Lihat</button>
        </div>
        <br><br>
      </form>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <br>
          <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Nota Lunas</h3>
            </div> -->
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabeldata" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No. Nota</th>
                  <th class="no-sort">Pelanggan</th>
                  <th>Waktu Transaksi</th>
                  <th class="right-aligned">Grand Total</th>
                  <th class="no-sort">Metode Pembayaran</th>
                  @if($detail_toko->pembayaran_piutang==1)
                    <th class="no-sort">Status Pembayaran</th>
                    <th>Tanggal Jatuh Tempo</th>
                    <th class="right-aligned">Sisa Piutang</th>
                  @endif
                  <th class="no-sort">Pegawai</th>
                  <th class="no-sort">Status</th>
                  @if($detail_toko->fitur_kirim==1)
                    <th class="no-sort">Kirim</th>
                  @endif
                  <!-- <th class="no-sort"></th>
                  <th class="no-sort"></th> -->
                </tr>
                </thead>
                <tbody>
                @if(!empty($list_nota_jual))
                    @foreach($list_nota_jual as $post)
                        <tr>
                            <td><a href="{{route('nota-jual.show', $post->no_nota_jual)}}">{{$post->no_nota_jual}}</a></td>
                            <td>{{$post->nama_pelanggan}}</td>
                            <td>{{date('d-m-Y, H:i', strtotime($post->tanggal))}}</td>
                            <td class="right-aligned">{{number_format($post->grand_total,0,".",",")}}</td>
                            <td>{{$post->metode_pembayaran}}</td>
                            @if($detail_toko->pembayaran_piutang==1)
                              <td><a href="{{route('histori-pembayaran-penjualan.show', $post->no_nota_jual)}}">{{$post->status_pembayaran}}</a></td>
                              @if($post->tanggal_jatuh_tempo!=null)
                                  <td>{{date('d-m-Y', strtotime($post->tanggal_jatuh_tempo))}}</td>
                              @else
                                <td>-</td>
                              @endif
                              <td class="right-aligned">{{number_format($post->sisa_piutang,0,".",",")}}</td>
                            @endif
                            <td>{{$post->nama_pegawai}}</td>
                            <td>{{$post->status_transaksi}}</td>

                            @if($detail_toko->fitur_kirim==1)
                              @if($post->kirim=="1")
                                <td><a href="{{route('nota-jual.detail-pengiriman', $post->no_nota_jual)}}">Ya</a></td>
                              @else
                                <td>Tidak</td>
                              @endif
                            @endif

                          <!-- <td class="center-aligned">
                            <button class="btn btn-warning"><a href="{{ route('nota-jual.edit', $post->no_nota_jual) }}" class="font-white">Edit</a></button>
                          </td>
                          <td class="center-aligned">
                            <form method="post" action="{{ route('nota-jual.destroy', $post->no_nota_jual) }}">
                              {{ method_field('delete')}}
                              {{ csrf_field() }}
                              <button class="btn btn-danger" type="submit" name="delete" onclick="return confirm('Are you sure?')">Hapus</button>
                            </form>         
                          </td> -->
                        </tr>
                    @endforeach
                  @endif 
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@if (session('status'))
    <<!-- div class="alert alert-success">
        {{ session('status') }}
    </div> -->
    <script>
      alert('{{ session('status') }}')
    </script>
@endif
@endsection