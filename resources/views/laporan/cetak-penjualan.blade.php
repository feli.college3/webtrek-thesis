<!DOCTYPE html>
<html>
<head>
	<title>Laporan Penjualan {{$detail_toko->nama_toko}}</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
		@page { margin: 50px 25px; }
	    footer { position: fixed; bottom: -20px; left: 0px; right: 0px; height: 20px; font-size: 15px }
	    p { page-break-after: always; }
	    p:last-child { page-break-after: never; }
	    .center-aligned{
	    	text-align: center;
	    }
	    .right-aligned{
	    	text-align: right;
	    }
	</style>
	<center>
		<h5>Laporan Penjualan {{$detail_toko->nama_toko}}</h5>
		<h6>Periode {{$list_bulan[$bulan-1]}} {{$tahun}}</h6>
		@if($detail_toko->pembayaran_piutang==1)
	        <h6>Jenis: {{$jenis}}</h6>
	    @endif
		<hr>
		<h6>Hasil Penjualan: Rp{{number_format($total_jual,0,".",",")}}</h6>
		  @if($detail_toko->pembayaran_piutang==1)
		    <h6>Tanpa Piutang: Rp{{number_format($tanpa_piutang,0,".",",")}}</h6>
		  @endif
	</center>
 	

	<table class='table table-bordered'>
		<thead>
			<tr>
				<th class="center-aligned">No</th>
				<th>No. Nota</th>
                <th class="no-sort">Pelanggan</th>
                <th>Tanggal Transaksi</th>
                <th class="no-sort">Metode Pembayaran</th>
                <th class="right-aligned">Grand Total (Rp)</th>
                @if($detail_toko->pembayaran_piutang==1)
                    @if($jenis=='Semua')
                    <th class="no-sort">Status Pembayaran</th>
                    @endif
                    @if($jenis!='Lunas')
                    <th>Tanggal Jatuh Tempo</th>
                    <th class="right-aligned">Sisa Piutang (Rp)</th>
                    @endif
                @endif
			</tr>
		</thead>
		<tbody>
			@if(count($list_nota_jual)>0)
				@php $i=1 @endphp
                    @foreach($list_nota_jual as $post)
                        <tr>
                        	<td class="center-aligned">{{ $i++ }}</td>
                            <td>{{$post->no_nota_jual}}</td>
                            <td>{{$post->nama_pelanggan}}</td>
                            <td>{{date('d-m-Y', strtotime($post->tanggal))}}</td>
                            <td>{{$post->metode_pembayaran}}</td>
                            <td class="right-aligned">{{number_format($post->grand_total,0,".",",")}}</td>
                            @if($detail_toko->pembayaran_piutang==1)
                              @if($jenis=='Semua')
                                <td>{{$post->status_pembayaran}}</td>
                              @endif
                              @if($jenis!='Lunas')
                              	@if($post->tanggal_jatuh_tempo!=null)
                                  <td>{{date('d-m-Y', strtotime($post->tanggal_jatuh_tempo))}}</td>
                                @else
                                  <td>-</td>
                                @endif
                                <td class="right-aligned">{{number_format($post->sisa_piutang,0,".",",")}}</td>
                              @endif
                            @endif
                        </tr>
                    @endforeach
                @else
                  @if($detail_toko->pembayaran_piutang==1)
                    <tr><td class="center-aligned" colspan="9">Tidak ada transaksi pada periode ini</td></tr>
                  @else
                    <tr><td class="center-aligned" colspan="5">Tidak ada transaksi pada periode ini</td></tr>
                  @endif
                @endif 
		</tbody>
	</table>
 	<footer>Dibuat oleh <b>WEBTREK</b></footer>
</body>
</html>