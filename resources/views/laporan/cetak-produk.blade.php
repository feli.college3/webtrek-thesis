<!DOCTYPE html>
<html>
<head>
	<title>Laporan Produk {{$detail_toko->nama_toko}}</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
		@page { margin: 50px 25px; }
	    footer { position: fixed; bottom: -20px; left: 0px; right: 0px; height: 20px; font-size: 15px }
	    p { page-break-after: always; }
	    p:last-child { page-break-after: never; }
	    .center-aligned{
	    	text-align: center;
	    }
	    .right-aligned{
	    	text-align: right;
	    }
	</style>
	<center>
		<h5>Laporan Keluar Masuk Produk {{$detail_toko->nama_toko}}</h5>
		<h6>Periode {{$list_bulan[$bulan-1]}} {{$tahun}}</h6>
	</center>
 	

	<table class='table table-bordered'>
		<thead>
			<tr>
				<th class="center-aligned">No</th>
                  <th>ID Produk</th>
                  <th class="right-aligned">Nama Produk</th>
                  <th class="right-aligned">Total Dibeli</th>
                  <th class="right-aligned">Total Dijual</th>
                  <th class="right-aligned">Total Masuk (Penyesuaian)</th>
                  <th class="right-aligned">Total Keluar (Penyesuaian)</th>
			</tr>
		</thead>
		<tbody>
			@for ($i = 0; $i < count($list_jual); $i++)
                    @php $index=$i+1; @endphp
                    <tr>
                        <td class="center-aligned">{{$index}}</td>
                        <td>{{$list_jual[$i]['idproduk']}}</td>
                        <td>{{$list_jual[$i]['nama_produk']}}</td>
                        <td class="right-aligned">{{$list_beli[$i]['jumlah']}}</td>
                        <td class="right-aligned">{{$list_jual[$i]['jumlah']}}</td>
                        <td class="right-aligned">{{$penyesuaian[$i]['jumlah_masuk']}}</td>
                        <td class="right-aligned">{{$penyesuaian[$i]['jumlah_keluar']}}</td>
                    </tr>
                  @endfor
		</tbody>
	</table>
 	<footer>Dibuat oleh <b>WEBTREK</b></footer>
</body>
</html>