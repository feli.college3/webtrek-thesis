<!DOCTYPE html>
<html>
<head>
	<title>Laporan Laba Rugi {{$detail_toko->nama_toko}}</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
		@page { margin: 50px 25px; }
	    footer { position: fixed; bottom: -20px; left: 0px; right: 0px; height: 20px; font-size: 15px }
	    p { page-break-after: always; }
	    p:last-child { page-break-after: never; }
	    .center-aligned{
	    	text-align: center;
	    }
	    .right-aligned{
	    	text-align: right;
	    }
	    .green{
	    	color: green;
	    }
	    .red{
	    	color: red;
	    }
	    .vertical-middle{
		  vertical-align: middle !important;
		}
	</style>
	<center>
		<h5>Laporan Laba Rugi {{$detail_toko->nama_toko}}</h5>
		<h6>Periode {{$list_bulan[$bulan-1]}} {{$tahun}}</h6>
		<hr>
	    <h6>Total Pembelian: Rp{{number_format($total_pembelian,0,".",",")}}</h6>
	    <h6>Total Penjualan: Rp{{number_format($total_penjualan,0,".",",")}}</h6>
	    @if($keuntungan>=0)
	        <h6 class="green">Total Keuntungan: Rp{{number_format($keuntungan,0,".",",")}}</h6>
	    @else
	        <h6 class="red">Total Kerugian: Rp{{number_format($keuntungan*-1,0,".",",")}}</h6>
	    @endif
	</center>
 	

	<table class='table table-bordered'>
		<thead>
			<tr>
	            <th rowspan="2" class="vertical-middle center-aligned">No</th>
	            <th rowspan="2" class="vertical-middle center-aligned">ID Produk</th>
	            <th rowspan="2" class="vertical-middle">Nama Produk</th>
	            <!-- <th rowspan="2" class="vertical-middle right-aligned">Jumlah Beli (Rp)</th> -->
	            <th rowspan="2" class="vertical-middle right-aligned">Total Pembelian</th>
	            <!-- <th rowspan="2" class="vertical-middle right-aligned">Jumlah Jual</th> -->
	            <th rowspan="2" class="vertical-middle right-aligned">Total Penjualan (Rp)</th>
	            <th colspan="2" class="vertical-middle center-aligned">Penyesuaian</th>
	            <th rowspan="2" class="vertical-middle right-aligned">Keuntungan/Kerugian (Rp)</th>
          	</tr>
          	<tr>
	            <!-- <th class="vertical-middle right-aligned">Penambahan</th> -->
	            <th class="vertical-middle right-aligned">Penambahan Stok (Rp)</th>
	            <!-- <th class="vertical-middle right-aligned">Pengeluaran</th> -->
	            <th class="vertical-middle right-aligned">Pengeluaran Stok (Rp)</th>
          	</tr>
		</thead>
		<tbody>
			@if(count($all_list)>0)
				@php $i=1 @endphp
	            @foreach($all_list as $post)
	                <tr>
	                	<td class="center-aligned">{{ $i++ }}</td>
	                    <td class="center-aligned"><b>{{$post['idproduk']}}</b></td>
	                    <td>{{$post['nama_produk']}}</td>
	                    <!-- <td class="right-aligned">{{number_format($post['jumlah_beli'],0,".",",")}}</td> -->
	                    <td class="right-aligned">{{number_format($post['subtotal_beli'],0,".",",")}}</td>
	                    <!-- <td class="right-aligned">{{number_format($post['jumlah_jual'],0,".",",")}}</td> -->
	                    <td class="right-aligned">{{number_format($post['subtotal_jual'],0,".",",")}}</td>
	                    <!-- <td class="right-aligned">{{number_format($post['jumlah_masuk'],0,".",",")}}</td> -->
	                    <td class="right-aligned">{{number_format($post['subtotal_masuk'],0,".",",")}}</td>
	                    <!-- <td class="right-aligned">{{number_format($post['jumlah_keluar'],0,".",",")}}</td> -->
	                    <td class="right-aligned">{{number_format($post['subtotal_keluar'],0,".",",")}}</td>
	                    <td class="right-aligned"><b>{{number_format($post['keuntungan'],0,".",",")}}</b></td>
	                </tr>
	            @endforeach
	        @else
	            <tr><td class="center-aligned" colspan="11">Tidak ada transaksi pada periode ini</td></tr>
          	@endif 
		</tbody>
	</table>
 	<footer>Dibuat oleh <b>WEBTREK</b></footer>
</body>
</html>